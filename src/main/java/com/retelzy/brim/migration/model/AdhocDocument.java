package com.retelzy.brim.migration.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class AdhocDocument {

	private String adhocDocName;
	private String categoryId;
	private String clobValue;
	private String blobValue;
	private Timestamp createTs;
	private String createUser;
	private Timestamp modifyTs;
	private String modifyUser;
		
}
