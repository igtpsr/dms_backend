package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.SmuRoleRootMenu;
import com.retelzy.brim.entity.source.SmuRoleRootMenuPK;

public interface SourceSmuRoleRootMenuDao extends CrudRepository<SmuRoleRootMenu, SmuRoleRootMenuPK> {

	public List<Object> findByIdRoleId(Integer roleId);

	@Query("Select srrm from SmuRoleRootMenu srrm Where srrm.id.menuItemId >=:menuItemId "
			+ "And srrm.id.roleId >=:minRoleId And srrm.id.roleId Not In (:roleId) And srrm.id.roleId In (:listOfRoleId) ")
	public List<Object> getSmuRoleRootMenuDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId);

}
