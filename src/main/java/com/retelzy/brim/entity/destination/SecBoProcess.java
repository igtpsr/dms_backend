package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "SEC_BO_PROCESS")
@Data
@Accessors
@NoArgsConstructor
@AllArgsConstructor
public class SecBoProcess implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecBoProcessPK id;

	@Column(name = "PROCESS_NAME")
	private String processName;

	@Column(name = "ACTION_CLASS")
	private String actionClass;

	@Column(name = "PARAMS")
	private String params;

	@Column(name = "I18N_RES_ID")
	private Integer i18nResId;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
