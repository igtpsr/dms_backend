package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecQueryGroup;
import com.retelzy.brim.entity.destination.SecQueryGroupPK;

@Repository
public interface DestinationSecQueryGroupDao extends CrudRepository<SecQueryGroup, SecQueryGroupPK> {

	@Query("Select secg from SecQueryGroup secg Where secg.id.roleId =:roleId")
	List<Object> getTheDataFromSecQueryGroupTable(Integer roleId);

}
