package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "SMU_RES_SHOW_ATTR")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class SmuResShowAttr implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SmuResShowAttrPK id;

	@Column(name = "PROP_VALUE")
	private String propValue;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
