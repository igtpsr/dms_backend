package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * The persistent class for the LAYOUT_SECTION_D database table.
 * 
 */
@Entity
@Table(name="LAYOUT_SECTION_D")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutSectionD implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutSectionDPK id;

	@Column(name="ASSOC_LEVEL_ID")
	private Integer assocLevelId;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER", length=18)
	private String modifyUser;

	@Column(name="[VALUE]", nullable=false, length=35)
	private String value;

	//bi-directional many-to-one association to LayoutSection
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumns({
	 * 
	 * @JoinColumn(name="DOC_VIEW_ID", referencedColumnName="DOC_VIEW_ID",
	 * nullable=false, insertable=false, updatable=false),
	 * 
	 * @JoinColumn(name="LAYOUT_ID", referencedColumnName="LAYOUT_ID",
	 * nullable=false, insertable=false, updatable=false),
	 * 
	 * @JoinColumn(name="SECTION_ID", referencedColumnName="SECTION_ID",
	 * nullable=false, insertable=false, updatable=false) }) private LayoutSection
	 * layoutSection;
	 */

}