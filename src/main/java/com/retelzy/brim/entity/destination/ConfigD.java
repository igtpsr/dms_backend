package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "CONFIG_D")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class ConfigD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ConfigDPK id;

	@Column(name = "PROP_VALUE")
	private String propValue;

	@Column(name = "DATATYPE")
	private String datatype;

	@Column(name = "FACTORY_NAME")
	private String factoryName;

	@Column(name = "MAXLENGTH")
	private Integer maxlength;

	@Column(name = "ENCRYPTED")
	private String encrypted;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "DOC_ID_LIST")
	private String docIdList;

	@Column(name = "CLOB_VALUE")
	private Clob clobValue;

	@Column(name = "BLOB_VALUE")
	private Blob blobValue;

	@Column(name = "FUNC_URL1")
	private String funcUrl1;

	@Column(name = "FUNC_URL2")
	private String funcUrl2;

	@Column(name = "USE_CLOB")
	private Integer useClob;

	@Column(name = "CATEGORY_ID")
	private String categoryId;

	@Column(name = "LEVEL_ID")
	private Integer levelId;

	@Column(name = "POSITION")
	private Integer position;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
