package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBoFieldAcl;
import com.retelzy.brim.entity.destination.SecBoFieldAclPK;

@Repository
public interface DestinationSecBoFieldAclDao extends CrudRepository<SecBoFieldAcl, SecBoFieldAclPK> {

	@Query("Select bo from SecBoFieldAcl bo where bo.id.roleId =:roleId")
	List<Object> getTheDataFromSecBoFieldAclTable(Integer roleId);

	@Query("Select bo.id.queryId from SecBoFieldAcl bo Where bo.id.roleId =:roleId")
	List<Integer> getTheQueryIdByRoleId(Integer roleId);

}
