package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecRoleResCtl;
import com.retelzy.brim.entity.destination.SecRoleResCtlPK;

@Repository
public interface DestinationSecRoleResCtlDao extends CrudRepository<SecRoleResCtl, SecRoleResCtlPK> {

	@Query("Select sec from SecRoleResCtl sec where sec.id.roleId =:roleId")
	List<Object> getTheDataFromSecRoleResCtlTable(Integer roleId);

}
