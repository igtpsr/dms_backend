package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutPage;
import com.retelzy.brim.entity.source.LayoutPagePK;

@Repository
public interface SourceLayoutPageDao extends CrudRepository<LayoutPage, LayoutPagePK> {

	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);


}
