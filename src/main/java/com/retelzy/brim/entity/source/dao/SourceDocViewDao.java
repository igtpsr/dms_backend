package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.DocView;
import com.retelzy.brim.entity.source.DocViewPK;

@Repository
public interface SourceDocViewDao extends CrudRepository<DocView, DocViewPK> {

	public List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);

}
