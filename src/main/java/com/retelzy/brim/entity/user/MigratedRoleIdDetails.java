package com.retelzy.brim.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "MIGRATED_ROLE_ID_DETAILS")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class MigratedRoleIdDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MIGRATED_ROLE_ID_DETAILS_ID")
	private Integer migratedRoleIdDetailsId;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "ROLE_ID_MIGRATION_STATUS")
	private String roleIdMigrationStatus;

	@ManyToOne
	@JoinColumn(name = "MIGRATION_DETAILS_ID")
	private MigrationDetails migrationDetails;
}
