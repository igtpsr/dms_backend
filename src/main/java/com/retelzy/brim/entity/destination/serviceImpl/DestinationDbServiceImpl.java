package com.retelzy.brim.entity.destination.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.db2.Db2DataTypeFactory;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.util.MigrationDbUtil;
import com.retelzy.brim.entity.destination.AdhocDocuments;
import com.retelzy.brim.entity.destination.CollabField;
import com.retelzy.brim.entity.destination.ConfigD;
import com.retelzy.brim.entity.destination.DashSectionH;
import com.retelzy.brim.entity.destination.DocView;
import com.retelzy.brim.entity.destination.DocViewElementD;
import com.retelzy.brim.entity.destination.ExtValidation;
import com.retelzy.brim.entity.destination.ExtValidationMetaData;
import com.retelzy.brim.entity.destination.LayoutDetail;
import com.retelzy.brim.entity.destination.LayoutElementD;
import com.retelzy.brim.entity.destination.LayoutI18nRes;
import com.retelzy.brim.entity.destination.LayoutLevelGroup;
import com.retelzy.brim.entity.destination.LayoutPage;
import com.retelzy.brim.entity.destination.LayoutProfile;
import com.retelzy.brim.entity.destination.LayoutProfileD;
import com.retelzy.brim.entity.destination.LayoutSection;
import com.retelzy.brim.entity.destination.LayoutSectionD;
import com.retelzy.brim.entity.destination.RoleResAttr;
import com.retelzy.brim.entity.destination.SecBoFieldAcl;
import com.retelzy.brim.entity.destination.SecBoFilter;
import com.retelzy.brim.entity.destination.SecBoLevelAcl;
import com.retelzy.brim.entity.destination.SecBoProcess;
import com.retelzy.brim.entity.destination.SecBointFilter;
import com.retelzy.brim.entity.destination.SecBoviewRes;
import com.retelzy.brim.entity.destination.SecQuery;
import com.retelzy.brim.entity.destination.SecQueryGroup;
import com.retelzy.brim.entity.destination.SecRoleProfile;
import com.retelzy.brim.entity.destination.SecRoleResCtl;
import com.retelzy.brim.entity.destination.SequenceNumbers;
import com.retelzy.brim.entity.destination.SmuCommon;
import com.retelzy.brim.entity.destination.SmuResShowAttr;
import com.retelzy.brim.entity.destination.SmuRoleMenuItem;
import com.retelzy.brim.entity.destination.SmuRoleRootMenu;
import com.retelzy.brim.entity.destination.dao.DestinationAdhocDocumentsDao;
import com.retelzy.brim.entity.destination.dao.DestinationCollabFieldsDao;
import com.retelzy.brim.entity.destination.dao.DestinationConfigDDao;
import com.retelzy.brim.entity.destination.dao.DestinationDashSectionHDao;
import com.retelzy.brim.entity.destination.dao.DestinationDocViewElementDDao;
import com.retelzy.brim.entity.destination.dao.DestinationExtValidationDao;
import com.retelzy.brim.entity.destination.dao.DestinationLayoutProfileDao;
import com.retelzy.brim.entity.destination.dao.DestinationQueryHDao;
import com.retelzy.brim.entity.destination.dao.DestinationRoleResAttarDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecBoFieldAclDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecBoFilterDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecBoLevelAclDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecBoProcessDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecBointFilterDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecQueryDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecQueryGroupDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecRoleProfileDao;
import com.retelzy.brim.entity.destination.dao.DestinationSecRoleResCtlDao;
import com.retelzy.brim.entity.destination.dao.DestinationSequenceNumberDao;
import com.retelzy.brim.entity.destination.dao.DestinationSmuRoleMenuItemDao;
import com.retelzy.brim.entity.destination.dao.DestinationSmuRoleRootMenuDao;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.dao.SourceSecBoFieldAclDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoFilterDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoLevelAclDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoProcessDao;
import com.retelzy.brim.entity.source.dao.SourceSecBointFilterDao;
import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.migration.model.AdhocDocumentReport;
import com.retelzy.brim.migration.model.AppConfigReport;
import com.retelzy.brim.migration.model.LayoutReport;

@Service
public class DestinationDbServiceImpl implements DestinationDbService {

	private final Logger logger = LoggerFactory.getLogger(DestinationDbServiceImpl.class);

	@Autowired
	private DestinationQueryHDao queryHDao;

	@Autowired
	private DestinationSequenceNumberDao sequenceNumberDao;

	@Autowired
	private DestinationSecRoleProfileDao secRoleProfileDao;

	@Autowired
	private DestinationSecBoProcessDao secBoProcessDao;

	@Autowired
	private DestinationDashSectionHDao dashSectionHDao;

	@Autowired
	private MigrationDbUtil migrationDbUtil;

	@Autowired
	private DestinationLayoutProfileDao layoutProfileDao;

	@Autowired
	private DestinationExtValidationDao extValidationDao;

	@Autowired
	private DestinationConfigDDao configDDao;

	@Autowired
	private DestinationAdhocDocumentsDao adhocDocumentsDao;

	@Autowired
	private DestinationSecRoleResCtlDao secRoleResCtlDao;

	@Autowired
	private DestinationSmuRoleRootMenuDao smuRoleRootMenuDao;

	@Autowired
	private DestinationSmuRoleMenuItemDao smuRoleMenuItemDao;

	@Autowired
	private DestinationCollabFieldsDao collabFieldsDao;

	@Autowired
	private DestinationRoleResAttarDao roleResAttarDao;

	@Autowired
	private DestinationSecQueryDao secQueryDao;

	@Autowired
	private DestinationSecQueryGroupDao secQueryGroupDao;

	@Autowired
	private DestinationSecBoLevelAclDao secBoLevelAclDao;

	@Autowired
	private DestinationSecBoFieldAclDao secBoFieldAclDao;

	@Autowired
	private DestinationSecBoFilterDao secBoFilterDao;

	@Autowired
	private DestinationSecBointFilterDao secBointFilterDao;

	@Autowired
	private DestinationDocViewElementDDao docViewElementDao;

	@Autowired
	private SourceSecBoFieldAclDao sourceSecBoFieldAclDao;

	@Autowired
	private SourceSecBoLevelAclDao sourceSecBoLevelAclDao;

	@Autowired
	private SourceSecBoFilterDao sourceSecBoFilterDao;

	@Autowired
	private SourceSecBointFilterDao sourceSecBointFilterDao;

	@Autowired
	private SourceSecBoProcessDao sourceSecBoProcessDao;

	@Autowired
	private Environment env;

	@PersistenceUnit(unitName = "Destination")
	private EntityManagerFactory entityManagerFactory;

	@Override
	public IDatabaseConnection getConnection() throws Exception {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);
		Connection jdbcConnection = session.doReturningWork(new ReturningWork<Connection>() {
			@Override
			public Connection execute(Connection conn) throws SQLException {
				return conn;
			}
		});
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();

		String productName = jdbcConnection.getMetaData().getDatabaseProductName();
		logger.info("Destinatin DB Product name :" + productName);
		if (productName.contains("Microsoft SQL Server")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		} else if (productName.contains("Oracle RDBMS")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
		} else if (productName.contains("MySQL")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		} else if (productName.contains("IBM DB2")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Db2DataTypeFactory());
		}

		return connection;
	}

	@Override
	public void TestingMethod() {

		/*
		 * Map<String, List<Object>> map =
		 * source.getTheDataFromSourceTable("DashSectionH", 100113, null, null);
		 * 
		 * List<Object> listOfObjects = map.get("DashSectionH");
		 * List<com.retelzy.brim.entity.source.DashSectionH> listOfSourceDashSectionH =
		 * listOfObjects.parallelStream() .map(element ->
		 * (com.retelzy.brim.entity.source.DashSectionH)
		 * element).collect(Collectors.toList());
		 * 
		 * List<DashSectionH> listOfDestinationDashSectionH = new
		 * ArrayList<DashSectionH>();
		 * 
		 * for (Integer index = 0; index < listOfSourceDashSectionH.size(); index++) {
		 * 
		 * DashSectionH dashSectionH = new DashSectionH();
		 * dashSectionH.setAcl(listOfSourceDashSectionH.get(index).getAcl());
		 * dashSectionH.setBackground(listOfSourceDashSectionH.get(index).getBackground(
		 * )); dashSectionH.setColPos(listOfSourceDashSectionH.get(index).getColPos());
		 * dashSectionH.setGroupId(listOfSourceDashSectionH.get(index).getGroupId());
		 * dashSectionH.setHeight(listOfSourceDashSectionH.get(index).getHeight());
		 * dashSectionH.setI18nResId(listOfSourceDashSectionH.get(index).getI18nResId())
		 * ;
		 * dashSectionH.setModifyTs(listOfSourceDashSectionH.get(index).getModifyTs());
		 * dashSectionH.setModifyUser(listOfSourceDashSectionH.get(index).getModifyUser(
		 * )); dashSectionH.setRowPos(listOfSourceDashSectionH.get(index).getRowPos());
		 * dashSectionH.setSectionClass(listOfSourceDashSectionH.get(index).
		 * getSectionClass());
		 * dashSectionH.setSectionType(listOfSourceDashSectionH.get(index).
		 * getSectionType());
		 * dashSectionH.setWidth(listOfSourceDashSectionH.get(index).getWidth());
		 * 
		 * DashSectionHPK dashSectionHPK = new DashSectionHPK();
		 * dashSectionHPK.setRoleId(listOfSourceDashSectionH.get(index).getId().
		 * getRoleId());
		 * dashSectionHPK.setSectionId(listOfSourceDashSectionH.get(index).getId().
		 * getSectionId());
		 * 
		 * dashSectionH.setId(dashSectionHPK);
		 * 
		 * listOfDestinationDashSectionH.add(dashSectionH); }
		 * 
		 * EntityManager entityManager = entityManagerFactory.createEntityManager();
		 * Session session = entityManager.unwrap(Session.class);
		 * 
		 * try { session.beginTransaction();
		 * 
		 * Integer deleteStatus =
		 * session.createQuery("Delete From DashSectionH dh where dh.id.roleId =:roleId"
		 * ) .setParameter("roleId", 100113).executeUpdate();
		 * logger.info("Delete Status :" + deleteStatus);
		 * 
		 * session.setHibernateFlushMode(FlushMode.MANUAL);
		 * session.setJdbcBatchSize(listOfDestinationDashSectionH.size());
		 * listOfDestinationDashSectionH.forEach(dash -> { session.save(dash); });
		 * session.flush(); session.clear();
		 * 
		 * Integer status = 0;
		 * 
		 * if (status == 1) { throw new RuntimeException("Testing.....!"); }
		 * 
		 * session.getTransaction().commit(); } catch (Exception e) {
		 * logger.info("RoolBack Called"); session.getTransaction().rollback();
		 * session.close(); entityManager.close(); }
		 */

	}

	@Override
	public Integer getTheMaxQueryIdFromQuerH() {
		return queryHDao.getTheMaximumQueryId();
	}

	@Override
	public Integer getTheDestinationIdCounNumber(Integer queryId, String queryName) {
		return queryHDao.getTheDestinationId(queryId, queryName);
	}

	@Override
	public Integer getTheDestinationIdCounNumberByQueryId(Integer queryId) {
		return queryHDao.getTheDestinationIdCounNumberByQueryId(queryId);
	}

	@Override
	public List<Integer> getTheQueryIdByQueryName(String queryName) {
		return queryHDao.getTheQueryIdByQueryName(queryName);
	}

	@Override
	public Integer updateTheSequnceNumberBySequenceName(Integer nextQueryId, String sequenceName) {
		return sequenceNumberDao.updateTheSequnceNumberBySequenceName(nextQueryId, sequenceName);
	}

	@Override
	public List<Integer> getTheRoleIdsFromDestinationDB(Integer roleId) {
		return secRoleProfileDao.getAllTheRoleIdByRoleId(roleId);
	}

	@Override
	public List<Integer> getAllTheProcessId(Integer processId) {
		return secBoProcessDao.getByIdProcessIdGreaterThanEqual(processId);
	}

	@Override
	public boolean deleteSecurityData(String delTables[], Integer roleId) {

		boolean returnDeleteStatus = false;

		logger.info("Deleting the security data for security role " + roleId
				+ " from destination database.Please wait.........");

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);

		try {

			com.retelzy.brim.entity.source.SecRoleProfile secRole = new com.retelzy.brim.entity.source.SecRoleProfile();
			secRole.setRoleId(roleId);

			/*
			 * These four list details retrieval from Source db, because we are migrating
			 * the details from source db, sometimes some of the ids are available in source
			 * db but not available destination db, while deleting these ids are not
			 * deleting from destination,
			 */
			List<Integer> listOfQueryIdFromFieldAcl = sourceSecBoFieldAclDao.getAllTheQueryIdsByRoleId(roleId);
			List<Integer> listOfQueryIdFromLevelAcl = sourceSecBoLevelAclDao.getAllTheQueryIdsByRoleId(secRole);
			List<Integer> listOfQueryIdFromBoFilter = sourceSecBoFilterDao.getAllQueryIdByRoleId(roleId);
			List<Integer> listOfQueryIdFromBointFilter = sourceSecBointFilterDao.getAllTheQueryIdsByRoleId(roleId);

			logger.info("listOfQueryIdFromFieldAcl :" + listOfQueryIdFromFieldAcl.size());

			/*
			 * Delete child records before parent, parent table:QUERY_D, then Child
			 * table:SEC_QUERY
			 */
			session.beginTransaction();
			Integer deleteStatus = session.createQuery(
					"Delete From SecQuery sq Where sq.id.queryId IN (Select qd.queryId from QueryD qd) AND sq.id.roleId =:roleId")
					.setParameter("roleId", roleId).executeUpdate();
			logger.info("deleteTheDetailsFromSecQueryForQueryD() :" + deleteStatus);

			/*
			 * Delete child records before parent, parent table:QUERY_H, then Child
			 * table:SEC_QUERY, Written By Manas on the date of 12th May 2020
			 */
			deleteStatus = session.createQuery(
					"Delete From SecQuery sq Where sq.id.queryId IN (Select qd.queryId from QueryH qd) AND sq.id.roleId =:roleId")
					.setParameter("roleId", roleId).executeUpdate();
			logger.info("deleteTheDetailsFromSecQueryForQueryH() :" + deleteStatus);

			/* SEC_QUERY */

			/* SEC_BO_FILTER */
			deleteStatus = session
					.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromBoFilter)")
					.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();

			/* SEC_BOINT_FILTER */
			deleteStatus = session
					.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromBointFilter)")
					.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();

			/* SEC_BO_LEVEL_ACl */
			deleteStatus = session
					.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromLevelAcl)")
					.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();

			/* SEC_BO_FIELD_ACl */
			deleteStatus = session
					.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromFieldAcl)")
					.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();

			// delete query_d

			deleteStatus = session.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromBoFilter)")
					.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();
			logger.info("deleteTheDetailsBySecBoFilterTableForQueryD() :" + deleteStatus);

			deleteStatus = session
					.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromBointFilter)")
					.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();
			logger.info("deleteTheDetailsBySecBointFilterTableForQueryD() :" + deleteStatus);

			deleteStatus = session.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromFieldAcl)")
					.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();
			logger.info("deleteTheDetailsBySecBoFieldAclTableForQueryD() :" + deleteStatus);

			deleteStatus = session.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromLevelAcl)")
					.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();
			logger.info("deleteTheDetailsBySecBointLevelAclTableForQueryD() :" + deleteStatus);

			logger.info("Delete Completed For QueryD Table");

			/*
			 * Delete child records before parent, parent table:QUERY_H, then Child
			 * table:QUERY_GROUP_D
			 */
			deleteStatus = session
					.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromBoFilter)")
					.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();

			/*
			 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
			 * deleting the query_id details from QUERY_GROUP_D table by the use of
			 * SEC_BOINT_FILTER table's query_id
			 */
			deleteStatus = session
					.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromBointFilter)")
					.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();

			/*
			 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
			 * deleting the query_id details from QUERY_GROUP_D table by the use of
			 * SEC_BO_FIELD_ACL table's query_id
			 */
			deleteStatus = session
					.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromFieldAcl)")
					.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();

			/*
			 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
			 * deleting the query_id details from QUERY_GROUP_D table by the use of
			 * SEC_BO_LEVEL_ACL table's query_id
			 */
			deleteStatus = session
					.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromLevelAcl)")
					.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();

			// delete query_h

			deleteStatus = session.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromBoFilter)")
					.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();
			logger.info("deleteTheDetailsBySecBoFilterTableForQueryH() :" + deleteStatus);

			deleteStatus = session
					.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromBointFilter)")
					.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();
			logger.info("deleteTheDetailsBySecBointFilterTableForQueryH() :" + deleteStatus);

			deleteStatus = session.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromFieldAcl)")
					.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();
			logger.info("deleteTheDetailsBySecBoFieldAclTableForQueryH() :" + deleteStatus);

			deleteStatus = session.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromLevelAcl)")
					.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();
			logger.info("deleteTheDetailsBySecBoLevelAclTableForQueryH() :" + deleteStatus);

			logger.info("Delete Completed For QueryH Table");

			// delete ALL custom process
			Integer processId = 10000;
			Integer typeId = 1;
			List<Integer> listOfProcessId = sourceSecBoProcessDao.getByIdProcessIdGreaterThanEqual(processId);
			logger.info("List Of Process Id Got from Destination :" + listOfProcessId.size());

			deleteStatus = session.createQuery(
					"Delete from SecBoviewRes sb Where sb.id.resId In (:listOfProcessId) And sb.id.typeId =:typeId")
					.setParameter("listOfProcessId", listOfProcessId).setParameter("typeId", typeId).executeUpdate();
			logger.info("deleteTheDataFromSecBoviewResTable() :" + deleteStatus);

			deleteStatus = session.createQuery(
					"Delete from SmuResShowAttr sb Where sb.id.smuResId In (:listOfProcessId) And sb.id.smuTypeId =:typeId")
					.setParameter("listOfProcessId", listOfProcessId).setParameter("typeId", typeId).executeUpdate();
			logger.info("deleteTheDataFromSmuResShowAttrTable() :" + deleteStatus);

			/*
			 * same custom process may be attached to different roles - so this statement
			 * deletes the reference entry from dest database so that new entries can be
			 * inserted
			 */
			Integer minRoleId = 100000;
			deleteStatus = session
					.createQuery("Delete From SecRoleResCtl sr Where sr.id.roleId >=:minRoleId "
							+ "AND sr.id.roleId NOT In (:roleId) And sr.id.smuResId In (:listOfProcessId)")
					.setParameter("minRoleId", minRoleId).setParameter("roleId", roleId)
					.setParameter("listOfProcessId", listOfProcessId).executeUpdate();
			logger.info("deleteTheDataFromSecRoleResCtlTable() :" + deleteStatus);

			deleteStatus = session.createQuery("Delete From SecBoProcess sb Where sb.id.processId >=:processId")
					.setParameter("processId", processId).executeUpdate();
			logger.info("Delete Completed For Custome Process Table");

			// delete ALL custom menu
			Integer menuItemId = 5000000;
			deleteStatus = session.createQuery("Delete From SmuCommon sc Where sc.menuItemId >=:menuItemId")
					.setParameter("menuItemId", menuItemId).executeUpdate();
			logger.info("deleteTheDataFromSmuCommon() :" + deleteStatus);

			deleteStatus = session.createQuery(
					"Delete from SmuRoleRootMenu srm Where srm.id.menuItemId >=:menuItemId And srm.id.roleId >=:minRoleId")
					.setParameter("menuItemId", menuItemId).setParameter("minRoleId", minRoleId).executeUpdate();
			logger.info("deleteTheDataFromSmuRoleRootMenu() :" + deleteStatus);

			deleteStatus = session.createQuery(
					"Delete From SmuRoleMenuItem srm Where srm.id.menuItemId >=:menuItemId AND srm.id.roleId >=:minRoleId")
					.setParameter("menuItemId", menuItemId).setParameter("minRoleId", minRoleId).executeUpdate();
			logger.info("deleteTheDataFromSmuRoleMenuItem() :" + deleteStatus);

			/*
			 * update the sequence number to max(query_id) value - as validation query may
			 * have deleted during deleting security
			 */
			Integer updateStatus = session.createQuery(
					"Update SequenceNumbers seq set seq.currentnum =(SELECT MAX(qh.queryId) FROM QueryH qh) Where seq.sequenceName =:sequenceName")
					.setParameter("sequenceName", "QUERY_ID").executeUpdate();
			logger.info("Sequence Number Update Status :" + updateStatus);

			/**
			 * Delete other security tables data for a security
			 */

			for (Integer index = 0; index < delTables.length; index++) {
				String className = migrationDbUtil.getTheEntityClassName(delTables[index]);

				switch (className) {
				case "SecRoleProfile":
					deleteStatus = session.createQuery("Delete from SecRoleProfile sf Where sf.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "DashSectionH":
					deleteStatus = session.createQuery("Delete from DashSectionH dh Where dh.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecRoleResCtl":
					deleteStatus = session.createQuery("Delete from SecRoleResCtl srr Where srr.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SmuRoleRootMenu":
					deleteStatus = session.createQuery("Delete From SmuRoleRootMenu srm Where srm.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SmuRoleMenuItem":
					deleteStatus = session.createQuery("Delete From SmuRoleMenuItem srm Where srm.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "CollabFields":
					deleteStatus = session.createQuery("Delete from CollabField cf Where cf.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "RoleResAttr":
					deleteStatus = session.createQuery("Delete from RoleResAttr rra Where rra.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecQuery":
					deleteStatus = session.createQuery("Delete From SecQuery sq Where sq.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecQueryGroup":
					deleteStatus = session.createQuery("Delete from SecQueryGroup sqg where sqg.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecBoLevelAcl":
					deleteStatus = session.createQuery("Delete from SecBoLevelAcl sbl Where sbl.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecBoFieldAcl":
					deleteStatus = session.createQuery("Delete from SecBoFieldAcl sba Where sba.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecBoFilter":
					deleteStatus = session.createQuery("Delete from SecBoFilter sb Where sb.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				case "SecBointFilter":
					deleteStatus = session.createQuery("Delete from SecBointFilter sb Where sb.id.roleId =:roleId")
							.setParameter("roleId", roleId).executeUpdate();
					logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
					break;

				default:
					logger.error(
							"This Class is Not Added in Swich Case Statemnt Of deleteSecurityDataFromTable() method of DestinationDbServiceImpl Class"
									+ className);
				}
			}

			returnDeleteStatus = true;
			session.getTransaction().commit();
			session.close();
			entityManager.close();
			return returnDeleteStatus;

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			entityManager.close();
			logger.error(
					"Exception Occured in deleteSecuirtyData() method of MigrationDbUtil class :" + e.getMessage());
			returnDeleteStatus = false;
			return returnDeleteStatus;
		}

	}

	@Override
	public void saveAllSecRoleProfileDetails(List<SecRoleProfile> listOfSecRoleProfile) {
		secRoleProfileDao.saveAll(listOfSecRoleProfile);
	}

	@Override
	public void saveAllDestinationDashSectionHDetails(List<DashSectionH> listOfDestinationDashSectionH) {
		dashSectionHDao.saveAll(listOfDestinationDashSectionH);
	}

	@Override
	public CompletableFuture<Boolean> saveAllTheDestinationTableDetails(Map<String, List<Object>> insertQueryMap) {

		CompletableFuture<Boolean> futureDashSectionH = new CompletableFuture<Boolean>();
		EntityManager entityManager1 = entityManagerFactory.createEntityManager();
		Session session1 = entityManager1.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecRoleResCtl = new CompletableFuture<Boolean>();
		EntityManager entityManager2 = entityManagerFactory.createEntityManager();
		Session session2 = entityManager2.unwrap(Session.class);

		CompletableFuture<Boolean> futureSmuRoleRootMenu = new CompletableFuture<Boolean>();
		EntityManager entityManager3 = entityManagerFactory.createEntityManager();
		Session session3 = entityManager3.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSmuRoleRootMenu = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureSmuRoleMenuItem = new CompletableFuture<Boolean>();
		EntityManager entityManager4 = entityManagerFactory.createEntityManager();
		Session session4 = entityManager4.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSmuRoleMenuItem = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureCollabFields = new CompletableFuture<Boolean>();
		EntityManager entityManager5 = entityManagerFactory.createEntityManager();
		Session session5 = entityManager5.unwrap(Session.class);

		CompletableFuture<Boolean> futureRoleResAttr = new CompletableFuture<Boolean>();
		EntityManager entityManager6 = entityManagerFactory.createEntityManager();
		Session session6 = entityManager6.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecQuery = new CompletableFuture<Boolean>();
		EntityManager entityManager7 = entityManagerFactory.createEntityManager();
		Session session7 = entityManager7.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecQueryGroup = new CompletableFuture<Boolean>();
		EntityManager entityManager8 = entityManagerFactory.createEntityManager();
		Session session8 = entityManager8.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecBoLevelAcl = new CompletableFuture<Boolean>();
		EntityManager entityManager9 = entityManagerFactory.createEntityManager();
		Session session9 = entityManager9.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecuritySecBoLevelAcl = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureSecBoFieldAcl = new CompletableFuture<Boolean>();
		EntityManager entityManager10 = entityManagerFactory.createEntityManager();
		Session session10 = entityManager10.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecuritySecBoFieldAcl = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureSecBoFilter = new CompletableFuture<Boolean>();
		EntityManager entityManager11 = entityManagerFactory.createEntityManager();
		Session session11 = entityManager11.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecuritySecBoFilter = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureSecBointFilter = new CompletableFuture<Boolean>();
		EntityManager entityManager12 = entityManagerFactory.createEntityManager();
		Session session12 = entityManager12.unwrap(Session.class);

		CompletableFuture<Boolean> futureSecuritySecBointFilter = new CompletableFuture<Boolean>();

		CompletableFuture<Boolean> futureCustomSecBoProcess = new CompletableFuture<Boolean>();
		EntityManager entityManager13 = entityManagerFactory.createEntityManager();
		Session session13 = entityManager13.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSecBoviewRes = new CompletableFuture<Boolean>();
		EntityManager entityManager14 = entityManagerFactory.createEntityManager();
		Session session14 = entityManager14.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSmuResShowAttr = new CompletableFuture<Boolean>();
		EntityManager entityManager15 = entityManagerFactory.createEntityManager();
		Session session15 = entityManager15.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSmuCommon = new CompletableFuture<Boolean>();
		EntityManager entityManager16 = entityManagerFactory.createEntityManager();
		Session session16 = entityManager16.unwrap(Session.class);

		CompletableFuture<Boolean> futureCustomSecRoleResCtl = new CompletableFuture<Boolean>();

		Set<CompletableFuture<Boolean>> listOfFuture = new LinkedHashSet<CompletableFuture<Boolean>>();

		try {
			List<SequenceNumbers> listOfDestinationSequenceNumbers = new ArrayList<SequenceNumbers>();
			if (insertQueryMap.containsKey("SequenceNumbers")) {

				listOfDestinationSequenceNumbers = insertQueryMap.get("SequenceNumbers").parallelStream()
						.map(element -> (SequenceNumbers) element).collect(Collectors.toList());
			}

			logger.info("SequenceNumber Size :" + listOfDestinationSequenceNumbers.size());

			logger.info("Inside Insert Method");

			for (Entry<String, List<Object>> entry : insertQueryMap.entrySet()) {

				String key = entry.getKey();

				logger.info("Key :" + key);

				switch (key) {

				case "SecRoleProfile":

					logger.info(key + " Table Insertion Started");

					List<SecRoleProfile> listOfSecRoleProfiles = insertQueryMap.get(key).parallelStream()
							.map(element -> (SecRoleProfile) element).collect(Collectors.toList());

					secRoleProfileDao.saveAll(listOfSecRoleProfiles);

					logger.info(key + " Table Insertion Completed");

					break;

				case "DashSectionH":

					logger.info(key + " Table Insertion Started");

					try {
						session1.beginTransaction();
						session1.setHibernateFlushMode(FlushMode.MANUAL);
						futureDashSectionH = CompletableFuture.supplyAsync(() -> {

							List<DashSectionH> listOfDashSectionH = insertQueryMap.get(key).parallelStream()
									.map(element -> (DashSectionH) element).collect(Collectors.toList());

							session1.setJdbcBatchSize(listOfDashSectionH.size());

							listOfDashSectionH.forEach(dashSectionH -> {
								session1.save(dashSectionH);
							});
							return true;

						}).thenApplyAsync(result -> {
							session1.flush();
							session1.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In DashSectionH Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureDashSectionH);

					break;

				case "SecRoleResCtl":

					logger.info(key + " Table Insertion Started");

					try {
						session2.beginTransaction();
						session2.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecRoleResCtl = CompletableFuture.supplyAsync(() -> {

							List<SecRoleResCtl> listOfSecRoleResCtl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecRoleResCtl) element)).collect(Collectors.toList());

							session2.setJdbcBatchSize(listOfSecRoleResCtl.size());
							listOfSecRoleResCtl.forEach(secRoleResCtl -> {
								session2.save(secRoleResCtl);
							});

							return true;

						}).thenApplyAsync(result -> {
							session2.flush();
							session2.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In SecRoleResCtl Table :" + e.getMessage());
						throw e;
					}
					listOfFuture.add(futureSecRoleResCtl);

					break;

				case "SmuRoleRootMenu":

					logger.info(key + " Table Insertion Started");

					try {
						session3.beginTransaction();
						session3.setHibernateFlushMode(FlushMode.MANUAL);
						futureSmuRoleRootMenu = CompletableFuture.supplyAsync(() -> {

							List<SmuRoleRootMenu> listOfSmuRoleRootMenu = insertQueryMap.get(key).parallelStream()
									.map((element -> (SmuRoleRootMenu) element)).collect(Collectors.toList());

							session3.setJdbcBatchSize(listOfSmuRoleRootMenu.size());

							listOfSmuRoleRootMenu.forEach(smuRoleRootMenu -> {
								session3.save(smuRoleRootMenu);
							});
							return true;
						}).thenApplyAsync(result -> {
							session3.flush();
							session3.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSmuRoleRootMenu);

					break;

				case "SmuRoleMenuItem":

					logger.info(key + " Table Insertion Started");

					try {
						session4.beginTransaction();
						session4.setHibernateFlushMode(FlushMode.MANUAL);
						futureSmuRoleMenuItem = CompletableFuture.supplyAsync(() -> {

							List<SmuRoleMenuItem> listOfSmuRoleMenuItem = insertQueryMap.get(key).parallelStream()
									.map((element -> (SmuRoleMenuItem) element)).collect(Collectors.toList());

							session4.setJdbcBatchSize(listOfSmuRoleMenuItem.size());
							listOfSmuRoleMenuItem.forEach(smuRoleMenuItem -> {
								session4.save(smuRoleMenuItem);
							});

							return true;

						}).thenApplyAsync(result -> {
							session4.flush();
							session4.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSmuRoleMenuItem);

					break;

				case "CollabFields":

					logger.info(key + " Table Insertion Started");

					try {
						session5.beginTransaction();
						session5.setHibernateFlushMode(FlushMode.MANUAL);
						futureCollabFields = CompletableFuture.supplyAsync(() -> {

							List<CollabField> listOfCollabFields = insertQueryMap.get(key).parallelStream()
									.map((element -> (CollabField) element)).collect(Collectors.toList());

							session5.setJdbcBatchSize(listOfCollabFields.size());
							listOfCollabFields.forEach(collabFields -> {
								session5.save(collabFields);
							});

							return true;
						}).thenApplyAsync(result -> {
							session5.flush();
							session5.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCollabFields);

					break;

				case "RoleResAttr":

					logger.info(key + " Table Insertion Started");

					try {
						session6.beginTransaction();
						session6.setHibernateFlushMode(FlushMode.MANUAL);
						futureRoleResAttr = CompletableFuture.supplyAsync(() -> {

							List<RoleResAttr> listOfRoleResAttr = insertQueryMap.get(key).parallelStream()
									.map((element -> (RoleResAttr) element)).collect(Collectors.toList());

							session6.setJdbcBatchSize(listOfRoleResAttr.size());
							listOfRoleResAttr.forEach(roleResAttr -> {
								session6.save(roleResAttr);
							});

							return true;

						}).thenApplyAsync(result -> {
							session6.flush();
							session6.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureRoleResAttr);

					break;

				case "SecQuery":

					logger.info(key + " Table Insertion Started");

					try {
						session7.beginTransaction();
						session7.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecQuery = CompletableFuture.supplyAsync(() -> {

							List<SecQuery> listOfSecQuery = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecQuery) element)).collect(Collectors.toList());

							session7.setJdbcBatchSize(listOfSecQuery.size());

							listOfSecQuery.forEach(secQuery -> {
								session7.save(secQuery);
							});

							return true;

						}).thenApplyAsync(result -> {
							session7.flush();
							session7.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecQuery);

					break;

				case "SecQueryGroup":

					logger.info(key + " Table Insertion Started");

					try {
						session8.beginTransaction();
						session8.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecQueryGroup = CompletableFuture.supplyAsync(() -> {

							List<SecQueryGroup> listOfSecQueryGroups = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecQueryGroup) element)).collect(Collectors.toList());

							session8.setJdbcBatchSize(listOfSecQueryGroups.size());
							listOfSecQueryGroups.forEach(secQueryGroup -> {
								session8.save(secQueryGroup);
							});

							return true;
						}).thenApplyAsync(result -> {
							session8.flush();
							session8.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecQueryGroup);

					break;

				case "SecBoLevelAcl":

					logger.info(key + " Table Insertion Started");

					try {
						session9.beginTransaction();
						session9.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecBoLevelAcl = CompletableFuture.supplyAsync(() -> {

							List<SecBoLevelAcl> listOfSecBoLevelAcl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoLevelAcl) element)).collect(Collectors.toList());

							session9.setJdbcBatchSize(listOfSecBoLevelAcl.size());
							listOfSecBoLevelAcl.forEach(secBoLevelAcl -> {
								session9.save(secBoLevelAcl);
							});

							return true;
						}).thenApplyAsync(result -> {
							session9.flush();
							session9.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecBoLevelAcl);

					break;

				case "SecBoFieldAcl":

					logger.info(key + " Table Insertion Started");

					try {
						session10.beginTransaction();
						session10.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecBoFieldAcl = CompletableFuture.supplyAsync(() -> {

							List<SecBoFieldAcl> listOfSecBoFieldAcl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoFieldAcl) element)).collect(Collectors.toList());

							logger.info("List Of SecBoFieldAcl :" + listOfSecBoFieldAcl.size());

							session10.setJdbcBatchSize(listOfSecBoFieldAcl.size());
							listOfSecBoFieldAcl.forEach(secBoFieldAcl -> {
								session10.save(secBoFieldAcl);
							});

							return true;
						}).thenApplyAsync(result -> {
							session10.flush();
							session10.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecBoFieldAcl);

					break;

				case "SecBoFilter":

					logger.info(key + " Table Insertion Started");

					try {

						sequenceNumberDao.saveAll(listOfDestinationSequenceNumbers);

						session11.beginTransaction();
						session11.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecBoFilter = CompletableFuture.supplyAsync(() -> {

							List<SecBoFilter> listOfSecBoFilter = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoFilter) element)).collect(Collectors.toList());

							session11.setJdbcBatchSize(listOfSecBoFilter.size());
							listOfSecBoFilter.forEach(secBoFilter -> {
								session11.save(secBoFilter);
							});
							return true;
						}).thenApplyAsync(result -> {
							session11.flush();
							session11.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecBoFilter);

					break;

				case "SecBointFilter":

					logger.info(key + " Table Insertion Started");

					try {
						session12.beginTransaction();
						session12.setHibernateFlushMode(FlushMode.MANUAL);
						futureSecBointFilter = CompletableFuture.supplyAsync(() -> {

							List<SecBointFilter> listOfSecBointFilter = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBointFilter) element)).collect(Collectors.toList());

							session12.setJdbcBatchSize(listOfSecBointFilter.size());
							listOfSecBointFilter.forEach(secBointFilter -> {
								session12.save(secBointFilter);
							});
							return true;
						}).thenApplyAsync(result -> {
							session12.flush();
							session12.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

						if (!listOfDestinationSequenceNumbers.isEmpty()) {

							sequenceNumberDao.updateTheSequnceNumberBySequenceName(
									listOfDestinationSequenceNumbers.get(0).getCurrentnum(), "QUERY_ID");

							logger.info("Sequence Table Updated");
						}

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecBointFilter);

					break;

				case "SecuritySecBoFilter":

					logger.info(key + " Table Insertion Started");

					try {
						if (session11.isConnected()) {
							session11.getTransaction();
							session11.getFlushMode();
						} else {
							session11.beginTransaction();
							session11.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureSecuritySecBoFilter = CompletableFuture.supplyAsync(() -> {

							List<SecBoFilter> listOfSecuritySecBoFilter = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoFilter) element)).collect(Collectors.toList());

							session11.setJdbcBatchSize(listOfSecuritySecBoFilter.size());

							listOfSecuritySecBoFilter.forEach(secBoFilter -> {
								session11.save(secBoFilter);
							});
							return true;
						}).thenApplyAsync(result -> {
							session11.flush();
							session11.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecuritySecBoFilter);

					break;

				case "SecuritySecBointFilter":

					logger.info(key + " Table Insertion Started");

					try {
						if (session12.isConnected()) {
							session12.getTransaction();
							session12.getFlushMode();
						} else {
							session12.beginTransaction();
							session12.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureSecuritySecBointFilter = CompletableFuture.supplyAsync(() -> {

							List<SecBointFilter> listOfSecuritySecBointFilter = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBointFilter) element)).collect(Collectors.toList());

							session12.setJdbcBatchSize(listOfSecuritySecBointFilter.size());
							listOfSecuritySecBointFilter.forEach(secBointFilter -> {
								session12.save(secBointFilter);
							});
							return true;
						}).thenApplyAsync(result -> {
							session12.flush();
							session12.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecuritySecBointFilter);

					break;

				case "SecuritySecBoFieldAcl":

					logger.info(key + " Table Insertion Started");

					try {
						if (session10.isConnected()) {
							session10.getTransaction();
							session10.getFlushMode();
						} else {
							session10.beginTransaction();
							session10.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureSecuritySecBoFieldAcl = CompletableFuture.supplyAsync(() -> {

							List<SecBoFieldAcl> listOfSecuritySecBoFieldAcl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoFieldAcl) element)).collect(Collectors.toList());

							session10.setJdbcBatchSize(listOfSecuritySecBoFieldAcl.size());
							listOfSecuritySecBoFieldAcl.forEach(secBoFieldAcl -> {
								session10.save(secBoFieldAcl);
							});
							return true;
						}).thenApplyAsync(result -> {
							session10.flush();
							session10.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecuritySecBoFieldAcl);

					break;

				case "SecuritySecBoLevelAcl":

					logger.info(key + " Table Insertion Started");

					try {
						if (session9.isConnected()) {
							session9.getTransaction();
							session9.getFlushMode();
						} else {
							session9.beginTransaction();
							session9.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureSecuritySecBoLevelAcl = CompletableFuture.supplyAsync(() -> {

							List<SecBoLevelAcl> listOfSecuritySecBoLevelAcl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoLevelAcl) element)).collect(Collectors.toList());

							session9.setJdbcBatchSize(listOfSecuritySecBoLevelAcl.size());
							listOfSecuritySecBoLevelAcl.forEach(secBoLevelAcl -> {
								session9.save(secBoLevelAcl);
							});
							return true;
						}).thenApplyAsync(result -> {
							session9.flush();
							session9.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureSecuritySecBoLevelAcl);

					break;

				case "CustomProcessSecBoProcess":

					logger.info(key + " Table Insertion Started");

					try {

						session13.beginTransaction();
						session13.setHibernateFlushMode(FlushMode.MANUAL);
						futureCustomSecBoProcess = CompletableFuture.supplyAsync(() -> {

							List<SecBoProcess> listOfCustProcSecBoProcess = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoProcess) element)).collect(Collectors.toList());
							session13.setJdbcBatchSize(listOfCustProcSecBoProcess.size());
							listOfCustProcSecBoProcess.forEach(secBoProcess -> {
								session13.save(secBoProcess);
							});
							return true;

						}).thenApplyAsync(result -> {
							session13.flush();
							session13.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSecBoProcess);

					break;

				case "CustomProcessSecBoviewRes":

					logger.info(key + " Table Insertion Started");

					try {
						session14.beginTransaction();
						session14.setHibernateFlushMode(FlushMode.MANUAL);
						futureCustomSecBoviewRes = CompletableFuture.supplyAsync(() -> {

							List<SecBoviewRes> listOfCustSecBoviewRes = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecBoviewRes) element)).collect(Collectors.toList());

							session14.setJdbcBatchSize(listOfCustSecBoviewRes.size());
							listOfCustSecBoviewRes.forEach(secBoviewRes -> {
								session14.save(secBoviewRes);
							});
							return true;

						}).thenApplyAsync(result -> {
							session14.flush();
							session14.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSecBoviewRes);

					break;

				case "CustomProcessSmuResShowAttr":

					logger.info(key + " Table Insertion Started");

					try {
						session15.beginTransaction();
						session15.setHibernateFlushMode(FlushMode.MANUAL);
						futureCustomSmuResShowAttr = CompletableFuture.supplyAsync(() -> {

							List<SmuResShowAttr> listOfCustProcSmuResAttr = insertQueryMap.get(key).parallelStream()
									.map((element -> (SmuResShowAttr) element)).collect(Collectors.toList());

							session15.setJdbcBatchSize(listOfCustProcSmuResAttr.size());
							listOfCustProcSmuResAttr.forEach(smuResShowAttr -> {
								session15.save(smuResShowAttr);
							});
							return true;
						}).thenApplyAsync(result -> {
							session15.flush();
							session15.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSmuResShowAttr);

					break;

				case "CustomProcessSecRoleResCtl":

					logger.info(key + " Table Insertion Started");

					try {
						if (session2.isConnected()) {
							session2.getTransaction();
							session2.getFlushMode();
						} else {
							session2.beginTransaction();
							session2.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureCustomSecRoleResCtl = CompletableFuture.supplyAsync(() -> {

							List<SecRoleResCtl> listOfCustProcSecRoleResCtl = insertQueryMap.get(key).parallelStream()
									.map((element -> (SecRoleResCtl) element)).collect(Collectors.toList());

							session2.setJdbcBatchSize(listOfCustProcSecRoleResCtl.size());
							listOfCustProcSecRoleResCtl.forEach(secRoleResCtl -> {
								session2.save(secRoleResCtl);
							});
							return true;
						}).thenApply(result -> {
							session2.flush();
							session2.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSecRoleResCtl);

					break;

				case "CustomMenuSmuCommon":

					logger.info(key + " Table Insertion Started");

					try {

						session16.beginTransaction();
						session16.setHibernateFlushMode(FlushMode.MANUAL);
						futureCustomSmuCommon = CompletableFuture.supplyAsync(() -> {

							List<SmuCommon> listOfCustProcSmuCommon = insertQueryMap.get(key).parallelStream()
									.map((element -> (SmuCommon) element)).collect(Collectors.toList());

							session16.setJdbcBatchSize(listOfCustProcSmuCommon.size());
							listOfCustProcSmuCommon.forEach(smuCommon -> {
								session16.save(smuCommon);
							});
							return true;
						}).thenApply(result -> {
							session16.flush();
							session16.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSmuCommon);

					break;

				case "CustomMenuSmuRoleRootMenu":

					logger.info(key + " Table Insertion Started");

					try {
						if (session3.isConnected()) {
							session3.getTransaction();
							session3.getFlushMode();
						} else {
							session3.beginTransaction();
							session3.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureCustomSmuRoleRootMenu = CompletableFuture.supplyAsync(() -> {

							List<SmuRoleRootMenu> listOfCustMenuSmuRoleRootMenu = insertQueryMap.get(key)
									.parallelStream().map((element -> (SmuRoleRootMenu) element))
									.collect(Collectors.toList());

							session3.setJdbcBatchSize(listOfCustMenuSmuRoleRootMenu.size());
							listOfCustMenuSmuRoleRootMenu.forEach(smuRoleRootMenu -> {
								session3.save(smuRoleRootMenu);
							});
							return true;
						}).thenApply(result -> {
							session3.flush();
							session3.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSmuRoleRootMenu);

					break;

				case "CustomMenuSmuRoleMenuItem":

					logger.info(key + " Table Insertion Started");

					try {
						if (session4.isConnected()) {
							session4.getTransaction();
							session4.getFlushMode();
						} else {
							session4.beginTransaction();
							session4.setHibernateFlushMode(FlushMode.MANUAL);
						}
						futureCustomSmuRoleMenuItem = CompletableFuture.supplyAsync(() -> {

							List<SmuRoleMenuItem> listOfCustMenuSmuRoleMenuItem = insertQueryMap.get(key)
									.parallelStream().map((element -> (SmuRoleMenuItem) element))
									.collect(Collectors.toList());

							session4.setJdbcBatchSize(listOfCustMenuSmuRoleMenuItem.size());
							listOfCustMenuSmuRoleMenuItem.forEach(smuRoleMenuItem -> {
								session4.save(smuRoleMenuItem);
							});
							return true;
						}).thenApply(result -> {
							session4.flush();
							session4.clear();
							logger.info(key + " Table Insertion Completed");
							return result;
						});
					} catch (Exception e) {
						logger.error("Exception Occured In " + key + " Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureCustomSmuRoleMenuItem);

					break;

				default:
					logger.info(
							"No Class is Matching in Switch case of saveAllTheDestinationTableDetails() method of class DestinationDbServiceImpl:"
									+ key);
				}
			}

			Boolean insertStatus = CompletableFuture
					.allOf(listOfFuture.toArray(new CompletableFuture[listOfFuture.size()])).thenApply(result -> {
						logger.info("Insertion Completed Successfully ");
						return true;
					}).join();

			CompletableFuture<Boolean> returnStatus = new CompletableFuture<Boolean>();
			returnStatus.complete(insertStatus);
			logger.info("List Of Completable Future :" + listOfFuture.size());

			listOfFuture = new LinkedHashSet<CompletableFuture<Boolean>>();

			futureDashSectionH = CompletableFuture.supplyAsync(() -> {
				if (session1.getTransaction().isActive()) {
					session1.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureDashSectionH);

			futureSecRoleResCtl = CompletableFuture.supplyAsync(() -> {

				if (session2.getTransaction().isActive()) {
					session2.getTransaction().commit();
				}

				return true;
			});
			listOfFuture.add(futureSecRoleResCtl);

			futureSmuRoleRootMenu = CompletableFuture.supplyAsync(() -> {
				if (session3.getTransaction().isActive()) {
					session3.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSmuRoleRootMenu);

			futureSmuRoleMenuItem = CompletableFuture.supplyAsync(() -> {

				if (session4.getTransaction().isActive()) {
					session4.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSmuRoleMenuItem);

			futureCollabFields = CompletableFuture.supplyAsync(() -> {
				if (session5.getTransaction().isActive()) {
					session5.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureCollabFields);

			futureRoleResAttr = CompletableFuture.supplyAsync(() -> {

				if (session6.getTransaction().isActive()) {
					session6.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureRoleResAttr);

			futureSecQuery = CompletableFuture.supplyAsync(() -> {
				if (session7.getTransaction().isActive()) {
					session7.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecQuery);

			futureSecQueryGroup = CompletableFuture.supplyAsync(() -> {
				if (session8.getTransaction().isActive()) {
					session8.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecQueryGroup);

			futureSecBoLevelAcl = CompletableFuture.supplyAsync(() -> {
				if (session9.getTransaction().isActive()) {
					session9.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecBoLevelAcl);

			futureSecBoFieldAcl = CompletableFuture.supplyAsync(() -> {
				if (session10.getTransaction().isActive()) {
					session10.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecBoFieldAcl);

			futureSecBoFilter = CompletableFuture.supplyAsync(() -> {
				if (session11.getTransaction().isActive()) {
					session11.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecBoFilter);

			futureSecBointFilter = CompletableFuture.supplyAsync(() -> {
				if (session12.getTransaction().isActive()) {
					session12.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureSecBointFilter);

			futureCustomSecBoProcess = CompletableFuture.supplyAsync(() -> {
				if (session13.getTransaction().isActive()) {
					session13.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureCustomSecBoProcess);

			futureCustomSecBoviewRes = CompletableFuture.supplyAsync(() -> {
				if (session14.getTransaction().isActive()) {
					session14.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureCustomSecBoviewRes);

			futureCustomSmuResShowAttr = CompletableFuture.supplyAsync(() -> {

				if (session15.getTransaction().isActive()) {
					session15.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureCustomSmuResShowAttr);

			futureCustomSmuCommon = CompletableFuture.supplyAsync(() -> {
				if (session16.getTransaction().isActive()) {
					session16.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureCustomSmuCommon);

			CompletableFuture.allOf(listOfFuture.toArray(new CompletableFuture[listOfFuture.size()]))
					.thenApply(result -> {
						logger.info("Transaction Commit Done...");
						return true;
					}).join();

			closeTheSession(session1, entityManager1, session2, entityManager2, session3, entityManager3, session4,
					entityManager4, session5, entityManager5, session6, entityManager6, session7, entityManager7,
					session8, entityManager8, session9, entityManager9, session10, entityManager10, session11,
					entityManager11, session12, entityManager12, session13, entityManager13, session14, entityManager14,
					session15, entityManager15, session16, entityManager16);

			return returnStatus;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception Caught, Transaction RollBack Called");
			logger.error("Exception Occured :" + e.getMessage());

			/* Close the Session and RollBack The Transaction */
			closeTheSession(session1, entityManager1, session2, entityManager2, session3, entityManager3, session4,
					entityManager4, session5, entityManager5, session6, entityManager6, session7, entityManager7,
					session8, entityManager8, session9, entityManager9, session10, entityManager10, session11,
					entityManager11, session12, entityManager12, session13, entityManager13, session14, entityManager14,
					session15, entityManager15, session16, entityManager16);

			/* Stop All The Future Running Thread */
			cancelAllFutureRunningThread(futureDashSectionH, futureSecRoleResCtl, futureSmuRoleRootMenu,
					futureCustomSmuRoleRootMenu, futureSmuRoleMenuItem, futureCustomSmuRoleMenuItem, futureCollabFields,
					futureRoleResAttr, futureSecQuery, futureSecQueryGroup, futureSecBoLevelAcl,
					futureSecuritySecBoLevelAcl, futureSecBoFieldAcl, futureSecuritySecBoFieldAcl, futureSecBoFilter,
					futureSecuritySecBoFilter, futureSecBointFilter, futureSecuritySecBointFilter,
					futureCustomSecBoProcess, futureCustomSecBoviewRes, futureCustomSmuResShowAttr,
					futureCustomSmuCommon);

			rollBackTransaction(session1, session2, session3, session4, session5, session6, session7, session8,
					session9, session10, session11, session12, session13, session14, session15, session16);

			CompletableFuture<Boolean> returnStatus = new CompletableFuture<Boolean>();
			returnStatus.complete(false);
			return returnStatus;
		}

	}

	private void closeTheSession(Session session1, EntityManager entityManager1, Session session2,
			EntityManager entityManager2, Session session3, EntityManager entityManager3, Session session4,
			EntityManager entityManager4, Session session5, EntityManager entityManager5, Session session6,
			EntityManager entityManager6, Session session7, EntityManager entityManager7, Session session8,
			EntityManager entityManager8, Session session9, EntityManager entityManager9, Session session10,
			EntityManager entityManager10, Session session11, EntityManager entityManager11, Session session12,
			EntityManager entityManager12, Session session13, EntityManager entityManager13, Session session14,
			EntityManager entityManager14, Session session15, EntityManager entityManager15, Session session16,
			EntityManager entityManager16) {

		session1.close();
		session2.close();
		session3.close();
		session4.close();
		session5.close();
		session6.close();
		session7.close();
		session8.close();
		session9.close();
		session10.close();
		session11.close();
		session12.close();
		session13.close();
		session14.close();
		session15.close();
		session16.close();

		entityManager1.close();
		entityManager2.close();
		entityManager3.close();
		entityManager4.close();
		entityManager5.close();
		entityManager6.close();
		entityManager7.close();
		entityManager8.close();
		entityManager9.close();
		entityManager10.close();
		entityManager11.close();
		entityManager12.close();
		entityManager13.close();
		entityManager14.close();
		entityManager15.close();
		entityManager16.close();

	}

	private void rollBackTransaction(Session session1, Session session2, Session session3, Session session4,
			Session session5, Session session6, Session session7, Session session8, Session session9, Session session10,
			Session session11, Session session12, Session session13, Session session14, Session session15,
			Session session16) {
		session1.getTransaction().rollback();
		session2.getTransaction().rollback();
		session3.getTransaction().rollback();
		session4.getTransaction().rollback();
		session5.getTransaction().rollback();
		session6.getTransaction().rollback();
		session7.getTransaction().rollback();
		session8.getTransaction().rollback();
		session9.getTransaction().rollback();
		session10.getTransaction().rollback();
		session11.getTransaction().rollback();
		session12.getTransaction().rollback();
		session13.getTransaction().rollback();
		session14.getTransaction().rollback();
		session15.getTransaction().rollback();
		session16.getTransaction().rollback();
	}

	private void cancelAllFutureRunningThread(CompletableFuture<Boolean> futureDashSectionH,
			CompletableFuture<Boolean> futureSecRoleResCtl, CompletableFuture<Boolean> futureSmuRoleRootMenu,
			CompletableFuture<Boolean> futureCustomSmuRoleRootMenu, CompletableFuture<Boolean> futureSmuRoleMenuItem,
			CompletableFuture<Boolean> futureCustomSmuRoleMenuItem, CompletableFuture<Boolean> futureCollabFields,
			CompletableFuture<Boolean> futureRoleResAttr, CompletableFuture<Boolean> futureSecQuery,
			CompletableFuture<Boolean> futureSecQueryGroup, CompletableFuture<Boolean> futureSecBoLevelAcl,
			CompletableFuture<Boolean> futureSecuritySecBoLevelAcl, CompletableFuture<Boolean> futureSecBoFieldAcl,
			CompletableFuture<Boolean> futureSecuritySecBoFieldAcl, CompletableFuture<Boolean> futureSecBoFilter,
			CompletableFuture<Boolean> futureSecuritySecBoFilter, CompletableFuture<Boolean> futureSecBointFilter,
			CompletableFuture<Boolean> futureSecuritySecBointFilter,
			CompletableFuture<Boolean> futureCustomSecBoProcess, CompletableFuture<Boolean> futureCustomSecBoviewRes,
			CompletableFuture<Boolean> futureCustomSmuResShowAttr, CompletableFuture<Boolean> futureCustomSmuCommon) {
		futureDashSectionH.cancel(true);
		futureSecRoleResCtl.cancel(true);
		futureSmuRoleRootMenu.cancel(true);
		futureCustomSmuRoleRootMenu.cancel(true);
		futureSmuRoleMenuItem.cancel(true);
		futureCustomSmuRoleMenuItem.cancel(true);
		futureCollabFields.cancel(true);
		futureRoleResAttr.cancel(true);
		futureSecQuery.cancel(true);
		futureSecQueryGroup.cancel(true);
		futureSecBoLevelAcl.cancel(true);
		futureSecuritySecBoLevelAcl.cancel(true);
		futureSecBoFieldAcl.cancel(true);
		futureSecuritySecBoFieldAcl.cancel(true);
		futureSecBoFilter.cancel(true);
		futureSecuritySecBoFilter.cancel(true);
		futureSecBointFilter.cancel(true);
		futureSecuritySecBointFilter.cancel(true);
		futureCustomSecBoProcess.cancel(true);
		futureCustomSecBoviewRes.cancel(true);
		futureCustomSmuResShowAttr.cancel(true);
		futureCustomSmuCommon.cancel(true);
	}

	@Override
	public List<Object> getLayoutProfileInfo(Integer layoutId) {
		return layoutProfileDao.getAllTheDataByLayoutId(layoutId);
	}

	@Override
	public Integer getLayoutID() {
		return layoutProfileDao.getLayoutID();
	}

	@Override
	public Integer getTheMaxValidationIdFromExtValidation() {
		return extValidationDao.getTheMaxValidationIdFromExtValidation();
	}

	@Override
	public Integer getTheValidationIdCountNumber(Integer validationId, String validationName) {
		return extValidationDao.getTheValidationIdCountNumber(validationId, validationName);
	}

	@Override
	public boolean deleteLayoutData(String[] delTables, Integer layoutId) {
		boolean returnDeleteStatus = false;

		logger.info("Deleting the layout data for layout id " + layoutId
				+ " from destination database.Please wait.........");

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);

		try {
			session.beginTransaction();
			Integer deleteStatus = null;

			deleteStatus = session.createQuery(
					"delete FROM QueryD qh WHERE qh.queryId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='LOOKUPFILTER')")
					.setParameter("layoutId", layoutId).executeUpdate();
			logger.info("QueryD Delete Status :" + deleteStatus);

			deleteStatus = session.createQuery(
					"delete FROM QueryH qh WHERE qh.queryId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='LOOKUPFILTER')")
					.setParameter("layoutId", layoutId).executeUpdate();
			logger.info("QueryH Delete Status :" + deleteStatus);

			Integer updateStatus = session.createQuery(
					"Update SequenceNumbers seq set seq.currentnum =(SELECT MAX(qh.queryId) FROM QueryH qh) Where seq.sequenceName =:sequenceName")
					.setParameter("sequenceName", "QUERY_ID").executeUpdate();
			logger.info("Sequence Number Update Status :" + updateStatus);

			for (Integer index = 0; index < delTables.length; index++) {
				String className = migrationDbUtil.getTheEntityClassName(delTables[index]);

				switch (className) {
				case "DocView":
					deleteStatus = session.createQuery("Delete from DocView dv Where dv.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "DocViewElementD":
					deleteStatus = session
							.createQuery("Delete from DocViewElementD dve Where dve.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutDetail":
					deleteStatus = session.createQuery("Delete from LayoutDetail ld Where ld.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutElementD":
					deleteStatus = session
							.createQuery("Delete From LayoutElementD led Where led.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutI18nRes":
					deleteStatus = session.createQuery(
							"Delete From LayoutI18nRes li Where li.id.layoutId =:layoutId and language_id = '0'")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutLevelGroup":
					deleteStatus = session
							.createQuery("Delete from LayoutLevelGroup llg Where llg.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutPage":
					deleteStatus = session.createQuery("Delete from LayoutPage lp Where lp.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutProfile":
					deleteStatus = session.createQuery("Delete From LayoutProfile lp Where lp.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutProfileD":
					deleteStatus = session
							.createQuery("Delete from LayoutProfileD lpd where lpd.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutSection":
					deleteStatus = session.createQuery("Delete from LayoutSection ls Where ls.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				case "LayoutSectionD":
					deleteStatus = session
							.createQuery("Delete from LayoutSectionD lsd Where lsd.id.layoutId =:layoutId")
							.setParameter("layoutId", layoutId).executeUpdate();
					logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
					break;

				default:
					logger.error(
							"This Class is Not Added in Swich Case Statemnt Of deleteLayoutData() method of DestinationDbServiceImpl Class"
									+ className);
				}
			}

			session.getTransaction().commit();
			returnDeleteStatus = true;
			session.close();
			entityManager.close();
			return returnDeleteStatus;

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			entityManager.close();
			logger.error("Exception Occured in deleteLayoutData() method of MigrationDbUtil class :" + e.getMessage());
			returnDeleteStatus = false;
			return returnDeleteStatus;
		}

	}
	
	
	@Override
	public CompletableFuture<Boolean> saveAllTheDestinationLayoutTableDetails(
			Map<String, List<Object>> insertQueryMap) {

		CompletableFuture<Boolean> futureDocView = new CompletableFuture<Boolean>();
		EntityManager entityManager1 = entityManagerFactory.createEntityManager();
		Session session1 = entityManager1.unwrap(Session.class);

		EntityManager entityManager2 = entityManagerFactory.createEntityManager();
		Session session2 = entityManager2.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutDetail = new CompletableFuture<Boolean>();
		EntityManager entityManager3 = entityManagerFactory.createEntityManager();
		Session session3 = entityManager3.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutElementD = new CompletableFuture<Boolean>();
		EntityManager entityManager4 = entityManagerFactory.createEntityManager();
		Session session4 = entityManager4.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutI18nRes = new CompletableFuture<Boolean>();
		EntityManager entityManager5 = entityManagerFactory.createEntityManager();
		Session session5 = entityManager5.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutLevelGroup = new CompletableFuture<Boolean>();
		EntityManager entityManager6 = entityManagerFactory.createEntityManager();
		Session session6 = entityManager6.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutPage = new CompletableFuture<Boolean>();
		EntityManager entityManager7 = entityManagerFactory.createEntityManager();
		Session session7 = entityManager7.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutProfile = new CompletableFuture<Boolean>();
		EntityManager entityManager8 = entityManagerFactory.createEntityManager();
		Session session8 = entityManager8.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutProfileD = new CompletableFuture<Boolean>();
		EntityManager entityManager9 = entityManagerFactory.createEntityManager();
		Session session9 = entityManager9.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutSection = new CompletableFuture<Boolean>();
		EntityManager entityManager10 = entityManagerFactory.createEntityManager();
		Session session10 = entityManager10.unwrap(Session.class);

		CompletableFuture<Boolean> futureLayoutSectionD = new CompletableFuture<Boolean>();
		EntityManager entityManager11 = entityManagerFactory.createEntityManager();
		Session session11 = entityManager11.unwrap(Session.class);

		CompletableFuture<Boolean> futureExtValidation = new CompletableFuture<Boolean>();
		EntityManager entityManager12 = entityManagerFactory.createEntityManager();
		Session session12 = entityManager12.unwrap(Session.class);

		CompletableFuture<Boolean> futureExtValidationMetaData = new CompletableFuture<Boolean>();
		EntityManager entityManager13 = entityManagerFactory.createEntityManager();
		Session session13 = entityManager13.unwrap(Session.class);

		Set<CompletableFuture<Boolean>> listOfFuture = new LinkedHashSet<CompletableFuture<Boolean>>();
		try {

			logger.info("Inside Insert Method");

			logger.info("For testing no of query: " + insertQueryMap.size()); //Added by LOKESH for testing
			for (Entry<String, List<Object>> entry : insertQueryMap.entrySet()) {

				String key = entry.getKey();

				logger.info("Key :" + key);

				switch (key) {

				case "DocView":

					logger.info(key + " Table Insertion Started");

					try {

						session1.beginTransaction();
						session1.setHibernateFlushMode(FlushMode.MANUAL);
						futureDocView = CompletableFuture.supplyAsync(() -> {

							List<DocView> listOfDocView = insertQueryMap.get(key).parallelStream()
									.map(element -> (DocView) element).collect(Collectors.toList());

							session1.setJdbcBatchSize(listOfDocView.size());

							listOfDocView.forEach(docView -> {
								session1.save(docView);
							});
							return true;

						}).thenApplyAsync(result -> {
							session1.flush();
							session1.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("doc view completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In DocView Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureDocView);

					break;

				case "DocViewElementD":

					logger.info(key + " Table Insertion Started");

					try {

						session2.beginTransaction();

						List<DocViewElementD> listOfDocViewElementD = insertQueryMap.get(key).parallelStream()
								.map(element -> (DocViewElementD) element).collect(Collectors.toList());

						session2.setJdbcBatchSize(listOfDocViewElementD.size());

						listOfDocViewElementD.forEach(docViewElementD -> {
							session2.save(docViewElementD);
						});

						session2.getTransaction().commit();
						System.out.print("DocViewElementD completed");

					} catch (Exception e) {
						logger.error("Exception Occured In DocViewElementD Table :" + e.getMessage());
						throw e;
					}

					break;

				case "LayoutDetail":

					logger.info(key + " Table Insertion Started");

					try {

						session3.beginTransaction();
						futureLayoutDetail = CompletableFuture.supplyAsync(() -> {

							List<LayoutDetail> listOfLayoutDetail = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutDetail) element).collect(Collectors.toList());

							session3.setJdbcBatchSize(listOfLayoutDetail.size());

							listOfLayoutDetail.forEach(layoutDetail -> {
								session3.save(layoutDetail);
							});
							return true;

						}).thenApplyAsync(result -> {
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutDetail completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutDetail Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutDetail);

					break;

				case "LayoutElementD":

					logger.info(key + " Table Insertion Started");

					try {

						session4.beginTransaction();
						session4.setHibernateFlushMode(FlushMode.MANUAL);
						futureLayoutElementD = CompletableFuture.supplyAsync(() -> {

							List<LayoutElementD> listOfLayoutElementD = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutElementD) element).collect(Collectors.toList());

							session4.setJdbcBatchSize(listOfLayoutElementD.size());

							listOfLayoutElementD.forEach(layoutElementD -> {
								session4.save(layoutElementD);
							});
							return true;

						}).thenApplyAsync(result -> {
							session4.flush();
							session4.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutElementD completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutElementD Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutElementD);

					break;

				case "LayoutI18nRes":

					logger.info(key + " Table Insertion Started");

					try {

						session5.beginTransaction();
						session5.setHibernateFlushMode(FlushMode.MANUAL);
						futureLayoutI18nRes = CompletableFuture.supplyAsync(() -> {

							List<LayoutI18nRes> listOfLayoutI18nRes = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutI18nRes) element).collect(Collectors.toList());

							session5.setJdbcBatchSize(listOfLayoutI18nRes.size());

							listOfLayoutI18nRes.forEach(layoutI18nRes -> {
								session5.save(layoutI18nRes);
							});
							return true;

						}).thenApplyAsync(result -> {
							session5.flush();
							session5.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutI18nRes completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutI18nRes Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutI18nRes);

					break;

				case "LayoutLevelGroup":

					logger.info(key + " Table Insertion Started");

					try {

						session6.beginTransaction();
						session6.setHibernateFlushMode(FlushMode.MANUAL);
						futureLayoutLevelGroup = CompletableFuture.supplyAsync(() -> {

							List<LayoutLevelGroup> listOfLayoutLevelGroup = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutLevelGroup) element).collect(Collectors.toList());

							session6.setJdbcBatchSize(listOfLayoutLevelGroup.size());

							listOfLayoutLevelGroup.forEach(layoutLevelGroup -> {
								session6.save(layoutLevelGroup);
							});
							return true;

						}).thenApplyAsync(result -> {
							session6.flush();
							session6.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutLevelGroup completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutLevelGroup Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutLevelGroup);

					break;

				case "LayoutPage":

					logger.info(key + " Table Insertion Started");

					try {

						session7.beginTransaction();
						futureLayoutPage = CompletableFuture.supplyAsync(() -> {

							List<LayoutPage> listOfLayoutPage = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutPage) element).collect(Collectors.toList());

							session7.setJdbcBatchSize(listOfLayoutPage.size());

							listOfLayoutPage.forEach(layoutPage -> {
								session7.save(layoutPage);
							});
							return true;

						}).thenApplyAsync(result -> {
							logger.info(key + " Table Insertion Completed");
							System.out.print("Layoutpage completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutPage Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutPage);

					break;

				case "LayoutProfile":

					logger.info(key + " Table Insertion Started");

					try {
						List<LayoutProfile> listOfLayoutProfile = insertQueryMap.get(key).parallelStream()
								.map(element -> (LayoutProfile) element).collect(Collectors.toList());

						layoutProfileDao.saveAll(listOfLayoutProfile);
						System.out.print("LayoutProfile completed");

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutProfile Table :" + e.getMessage());
						throw e;
					}

					break;

				case "LayoutProfileD":

					logger.info(key + " Table Insertion Started");

					try {

						session9.beginTransaction();
						session9.setHibernateFlushMode(FlushMode.MANUAL);
						futureLayoutProfileD = CompletableFuture.supplyAsync(() -> {

							List<LayoutProfileD> listOfLayoutProfileD = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutProfileD) element).collect(Collectors.toList());

							session9.setJdbcBatchSize(listOfLayoutProfileD.size());

							listOfLayoutProfileD.forEach(layoutProfileD -> {
								session9.save(layoutProfileD);
							});
							return true;

						}).thenApplyAsync(result -> {
							session9.flush();
							session9.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutProfileD completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutProfileD Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutProfileD);

					break;

				case "LayoutSection":

					logger.info(key + " Table Insertion Started");

					try {

						session10.beginTransaction();
						session10.setHibernateFlushMode(FlushMode.MANUAL);
						futureLayoutSection = CompletableFuture.supplyAsync(() -> {

							List<LayoutSection> listOfLayoutSection = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutSection) element).collect(Collectors.toList());

							session10.setJdbcBatchSize(listOfLayoutSection.size());

							listOfLayoutSection.forEach(layoutSection -> {
								session10.save(layoutSection);
							});
							
							return true;

						}).thenApplyAsync(result -> {
							session10.flush();
							session10.clear();
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutSection completed Flush and clear");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutSection Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutSection);

					break;

				case "LayoutSectionD":

					logger.info(key + " Table Insertion Started");

					try {

						session11.beginTransaction();
						futureLayoutSectionD = CompletableFuture.supplyAsync(() -> {

							List<LayoutSectionD> listOfLayoutSectionD = insertQueryMap.get(key).parallelStream()
									.map(element -> (LayoutSectionD) element).collect(Collectors.toList());

							session11.setJdbcBatchSize(listOfLayoutSectionD.size());

							listOfLayoutSectionD.forEach(layoutSectionD -> {
								session11.save(layoutSectionD);
							});
							return true;

						}).thenApplyAsync(result -> {
							logger.info(key + " Table Insertion Completed");
							System.out.print("LayoutSectionD completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In LayoutSectionD Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureLayoutSectionD);

					break;

				case "ExtValidation":

					logger.info(key + " Table Insertion Started");

					try {

						session12.beginTransaction();
						futureExtValidation = CompletableFuture.supplyAsync(() -> {

							List<ExtValidation> listOfExtValidation = insertQueryMap.get(key).parallelStream()
									.map(element -> (ExtValidation) element).collect(Collectors.toList());

							session12.setJdbcBatchSize(listOfExtValidation.size());

							listOfExtValidation.forEach(extValidation -> {
								session12.merge(extValidation);
							});
							return true;

						}).thenApplyAsync(result -> {
							logger.info(key + " Table Insertion Completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In ExtValidation Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureExtValidation);

					break;

				case "ExtValidationMetaData":

					logger.info(key + " Table Insertion Started");

					try {

						session13.beginTransaction();
						futureExtValidationMetaData = CompletableFuture.supplyAsync(() -> {

							List<ExtValidationMetaData> listOfExtValidationMetaData = insertQueryMap.get(key)
									.parallelStream().map(element -> (ExtValidationMetaData) element)
									.collect(Collectors.toList());

							session13.setJdbcBatchSize(listOfExtValidationMetaData.size());

							listOfExtValidationMetaData.forEach(extValidationMetaData -> {
								session13.merge(extValidationMetaData);
							});
							return true;

						}).thenApplyAsync(result -> {
							logger.info(key + " Table Insertion Completed");
							System.out.print("ExtravalidationMetaData completed");
							return result;
						});

					} catch (Exception e) {
						logger.error("Exception Occured In ExtValidationMetaData Table :" + e.getMessage());
						throw e;
					}

					listOfFuture.add(futureExtValidationMetaData);

					break;

				case "LayoutSequenceNumbers":

					logger.info(key + " Table Updation Started");

					try {
						List<SequenceNumbers> listOfDestinationSequenceNumbers = insertQueryMap.get(key)
								.parallelStream().map(element -> (SequenceNumbers) element)
								.collect(Collectors.toList());
						if (!listOfDestinationSequenceNumbers.isEmpty()) {
							sequenceNumberDao.updateTheSequnceNumberBySequenceName(
									listOfDestinationSequenceNumbers.get(0).getCurrentnum(), "LAYOUT_ID");
						}
						logger.info(key + " Table Updation Completed");
					} catch (Exception e) {
						logger.error("Exception Occured In UpdateSequenceNumbers Table :" + e.getMessage());
						throw e;
					}

					break;

				case "UpdateSequenceNumbers":

					logger.info(key + " Table Updation Started");

					try {
						List<SequenceNumbers> listOfDestinationSequenceNumbers = insertQueryMap.get(key)
								.parallelStream().map(element -> (SequenceNumbers) element)
								.collect(Collectors.toList());
						if (!listOfDestinationSequenceNumbers.isEmpty()) {
							sequenceNumberDao.updateTheSequnceNumberBySequenceName(
									listOfDestinationSequenceNumbers.get(0).getCurrentnum(), "QUERY_ID");
						}
						logger.info(key + " Table Updation Completed");
					} catch (Exception e) {
						logger.error("Exception Occured In UpdateSequenceNumbers Table :" + e.getMessage());
						throw e;
					}

					break;

				case "UpdateDocViewElementD":

					logger.info(key + " Table Updation Started");

					try {
						List<DocViewElementD> listOfDestinationDocViewElementD = insertQueryMap.get(key)
								.parallelStream().map(element -> (DocViewElementD) element)
								.collect(Collectors.toList());

						if (!listOfDestinationDocViewElementD.isEmpty()) {
							docViewElementDao.updateTheDocViewElementData(
									listOfDestinationDocViewElementD.get(0).getPropValue(),
									listOfDestinationDocViewElementD.get(0).getId().getPropName(),
									listOfDestinationDocViewElementD.get(0).getId().getLayoutId());
						}
						logger.info(key + " Table Updation Completed");
					} catch (Exception e) {
						logger.error("Exception Occured In UpdateDocViewElementD Table :" + e.getMessage());
						throw e;
					}

					break;

				default:
					logger.info(
							"No Class is Matching in Switch case of saveAllTheDestinationLayoutTableDetails() method of class DestinationDbServiceImpl:"
									+ key);
				}
			}

			Boolean insertStatus = CompletableFuture
					.allOf(listOfFuture.toArray(new CompletableFuture[listOfFuture.size()])).thenApply(result -> {
						logger.info("Insertion Completed Successfully ");
						return true;
					}).join();

			CompletableFuture<Boolean> returnStatus = new CompletableFuture<Boolean>();
			returnStatus.complete(insertStatus);
			logger.info("List Of Completable Future :" + listOfFuture.size());

			listOfFuture = new LinkedHashSet<CompletableFuture<Boolean>>();

			futureDocView = CompletableFuture.supplyAsync(() -> {
				if (session1.getTransaction().isActive()) {
					session1.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureDocView);

			futureLayoutDetail = CompletableFuture.supplyAsync(() -> {
				if (session3.getTransaction().isActive()) {
					session3.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutDetail);

			futureLayoutElementD = CompletableFuture.supplyAsync(() -> {

				if (session4.getTransaction().isActive()) {
					session4.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutElementD);

			futureLayoutI18nRes = CompletableFuture.supplyAsync(() -> {
				if (session5.getTransaction().isActive()) {
					session5.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutI18nRes);

			futureLayoutLevelGroup = CompletableFuture.supplyAsync(() -> {

				if (session6.getTransaction().isActive()) {
					session6.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutLevelGroup);

			futureLayoutPage = CompletableFuture.supplyAsync(() -> {
				if (session7.getTransaction().isActive()) {
					session7.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutPage);

			/*
			 * futureLayoutProfile = CompletableFuture.supplyAsync(() -> { if
			 * (session8.getTransaction().isActive()) { session8.getTransaction().commit();
			 * } return true; }); listOfFuture.add(futureLayoutProfile);
			 */

			futureLayoutProfileD = CompletableFuture.supplyAsync(() -> {
				if (session9.getTransaction().isActive()) {
					session9.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutProfileD);

			futureLayoutSection = CompletableFuture.supplyAsync(() -> {
				if (session10.getTransaction().isActive()) {
					session10.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutSection);

			futureLayoutSectionD = CompletableFuture.supplyAsync(() -> {
				if (session11.getTransaction().isActive()) {
					session11.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureLayoutSectionD);

			futureExtValidation = CompletableFuture.supplyAsync(() -> {
				if (session12.getTransaction().isActive()) {
					session12.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureExtValidation);

			futureExtValidationMetaData = CompletableFuture.supplyAsync(() -> {
				if (session13.getTransaction().isActive()) {
					session13.getTransaction().commit();
				}
				return true;
			});
			listOfFuture.add(futureExtValidationMetaData);

			CompletableFuture.allOf(listOfFuture.toArray(new CompletableFuture[listOfFuture.size()]))
					.thenApply(result -> {
						logger.info("Transaction Commit Done...");
						return true;
					}).join();

			closeTheLayoutSession(session1, entityManager1, session2, entityManager2, session3, entityManager3,
					session4, entityManager4, session5, entityManager5, session6, entityManager6, session7,
					entityManager7, session8, entityManager8, session9, entityManager9, session10, entityManager10,
					session11, entityManager11, session12, entityManager12, session13, entityManager13);

			returnStatus.complete(true);

			return returnStatus;

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception Caught, Transaction RollBack Called");
			logger.error("Exception Occured (LOKESH Testing) :" + e.getMessage());

			/* Close the Session and RollBack The Transaction */

			closeTheLayoutSession(session1, entityManager1, session2, entityManager2, session3, entityManager3,
					session4, entityManager4, session5, entityManager5, session6, entityManager6, session7,
					entityManager7, session8, entityManager8, session9, entityManager9, session10, entityManager10,
					session11, entityManager11, session12, entityManager12, session13, entityManager13);

			/* Stop All The Future Running Thread */

			cancelAllLayoutFutureRunningThread(futureDocView, futureLayoutDetail, futureLayoutElementD,
					futureLayoutI18nRes, futureLayoutLevelGroup, futureLayoutPage, futureLayoutProfile,
					futureLayoutProfileD, futureLayoutSection, futureLayoutSectionD);

			rollBackLayoutTransaction(session1, session2, session3, session4, session5, session6, session7, session8,
					session9, session10, session11, session12, session13);

			CompletableFuture<Boolean> returnStatus = new CompletableFuture<Boolean>();
			returnStatus.complete(false);
			return returnStatus;
		}
	}

	private void rollBackLayoutTransaction(Session session1, Session session2, Session session3, Session session4,
			Session session5, Session session6, Session session7, Session session8, Session session9, Session session10,
			Session session11, Session session12, Session session13) {
		session1.getTransaction().rollback();
		session2.getTransaction().rollback();
		session3.getTransaction().rollback();
		session4.getTransaction().rollback();
		session5.getTransaction().rollback();
		session6.getTransaction().rollback();
		session7.getTransaction().rollback();
		session8.getTransaction().rollback();
		session9.getTransaction().rollback();
		session10.getTransaction().rollback();
		session11.getTransaction().rollback();
	}

	private void cancelAllLayoutFutureRunningThread(CompletableFuture<Boolean> futureDocView,
			CompletableFuture<Boolean> futureLayoutDetail, CompletableFuture<Boolean> futureLayoutElementD,
			CompletableFuture<Boolean> futureLayoutI18nRes, CompletableFuture<Boolean> futureLayoutLevelGroup,
			CompletableFuture<Boolean> futureLayoutPage, CompletableFuture<Boolean> futureLayoutProfile,
			CompletableFuture<Boolean> futureLayoutProfileD, CompletableFuture<Boolean> futureLayoutSection,
			CompletableFuture<Boolean> futureLayoutSectionD) {

		futureDocView.cancel(true);
		futureLayoutDetail.cancel(true);
		futureLayoutElementD.cancel(true);
		futureLayoutI18nRes.cancel(true);
		futureLayoutLevelGroup.cancel(true);
		futureLayoutPage.cancel(true);
		futureLayoutProfile.cancel(true);
		futureLayoutProfileD.cancel(true);
		futureLayoutSection.cancel(true);
		futureLayoutSectionD.cancel(true);

	}

	private void closeTheLayoutSession(Session session1, EntityManager entityManager1, Session session2,
			EntityManager entityManager2, Session session3, EntityManager entityManager3, Session session4,
			EntityManager entityManager4, Session session5, EntityManager entityManager5, Session session6,
			EntityManager entityManager6, Session session7, EntityManager entityManager7, Session session8,
			EntityManager entityManager8, Session session9, EntityManager entityManager9, Session session10,
			EntityManager entityManager10, Session session11, EntityManager entityManager11, Session session12,
			EntityManager entityManager12, Session session13, EntityManager entityManager13) {
		session1.close();
		session2.close();
		session3.close();
		session4.close();
		session5.close();
		session6.close();
		session7.close();
		session8.close();
		session9.close();
		session10.close();
		session11.close();
		session12.close();
		session13.close();

		entityManager1.close();
		entityManager2.close();
		entityManager3.close();
		entityManager4.close();
		entityManager5.close();
		entityManager6.close();
		entityManager7.close();
		entityManager8.close();
		entityManager9.close();
		entityManager10.close();
		entityManager11.close();
		entityManager12.close();
		entityManager13.close();

	}

	@Override
	public void updateTheConigDTableDetails(List<ConfigD> listOfConfigDDetailsFromDestination) {
		configDDao.saveAll(listOfConfigDDetailsFromDestination);
	}

	@Override
	public void updateTheAdhocDocumentsTableDetails(List<AdhocDocuments> listOfAdhocDocumentsDetailsFromDestination) {
		adhocDocumentsDao.saveAll(listOfAdhocDocumentsDetailsFromDestination);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LayoutReport> getTheLayoutReport(List<Integer> listOfLayoutIds, Date migrationFromDate,
			Date migrationToDate) {
		List<LayoutReport> listOflayoutReportDetails = new ArrayList<LayoutReport>();
		List<Object[]> layoutReportDetails = new ArrayList<>();
		EntityManager em = entityManagerFactory.createEntityManager();
		try {

			// The string builder used to construct the string
			StringBuilder commaSepValues = new StringBuilder();

			// Looping through the list
			for (int i = 0; i < listOfLayoutIds.size(); i++) {
				// append the value into the builder
				commaSepValues.append("'" + listOfLayoutIds.get(i) + "'");

				// if the value is not the last element of the list
				// then append the comma(,) as well
				if (i != listOfLayoutIds.size() - 1) {
					commaSepValues.append(", ");
				}
			}
			String sqlQuery = "SELECT LAYOUT_PROFILE.LAYOUT_DESC  AS LAYOUT_DESCRIPTION, DOC_NAME.DESCRIPTION  AS DOC_NAME_DESCRIPTION,C1.Field_NAME, LIR.LABEL  AS DOC_VIEW_NAME,B.LABEL  AS SECTION_NAME,  I18N1.LABEL AS FIELD_LABEL, C1.SCALE,  EXT_VALIDATION.VALIDATION_NAME,LT.WIDGET_NAME WIDGET_NAME,VO.PROP_VALUE VALIDATION_OVERRIDE, SD.PROP_VALUE SHOW_DESCRIPTION, EA.PROP_VALUE ENABLE_AUTOSUGG,DL.PROP_VALUE DISPLAY_LENGTH, REQ.PROP_VALUE REQUIRED, SLURL.PROP_VALUE SHOW_LOOKUP_URL,  BG.PROP_VALUE BGCOLOR, FG.PROP_VALUE FGCOLOR,CV.PROP_VALUE CUSTOM_VALIDATOR FROM LAYOUT_PROFILE LAYOUT_PROFILE LEFT JOIN DOC_VIEW DV ON DV.LAYOUT_ID = LAYOUT_PROFILE.LAYOUT_ID LEFT JOIN DOC_NAME DOC_NAME ON DV.DOC_ID = DOC_NAME.DOC_ID LEFT JOIN LAYOUT_I18N_RES LIR ON  LIR.LAYOUT_ID = DV.LAYOUT_ID AND DV.I18N_RES_ID = LIR.I18N_RES_ID LEFT JOIN LAYOUT_SECTION A ON A.LAYOUT_ID = LIR.LAYOUT_ID AND A.DOC_VIEW_ID = DV.DOC_VIEW_ID LEFT JOIN LAYOUT_I18N_RES B ON A.LAYOUT_ID = B.LAYOUT_ID AND A.I18N_RES_ID = B.I18N_RES_ID AND B.LANGUAGE_ID = LIR.LANGUAGE_ID LEFT JOIN LAYOUT_DETAIL A1 ON A1.LAYOUT_ID = B.LAYOUT_ID AND A1.SECTION_ID = A.SECTION_ID AND A1.DOC_VIEW_ID = DV.DOC_VIEW_ID LEFT JOIN VIEW_DOC_ELEMENTS C1 ON A1.DOC_ID = C1.DOC_ID AND A1.DOC_LEVEL_ID = C1.LEVEL_ID AND A1.DOC_FIELD_ID = C1.FIELD_ID LEFT JOIN DEFAULT_ELEMENT_D DED ON DED.DOC_ID = A1.DOC_ID AND DED.FIELD_ID = A1.DOC_FIELD_ID AND DED.LEVEL_ID = A1.DOC_LEVEL_ID AND DED.PROP_NAME = 'I18N_RES_ID' AND C1.DOC_ID = DED.DOC_ID LEFT JOIN LAYOUT_I18N_RES I18N1 ON I18N1.I18N_RES_ID = CAST(DED.PROP_VALUE AS INT) AND I18N1.LAYOUT_ID = A1.LAYOUT_ID LEFT JOIN DOC_VIEW_ELEMENT_D VAL ON C1.LEVEL_ID = VAL.LEVEL_ID AND DED.FIELD_ID = VAL.FIELD_ID AND I18N1.LAYOUT_ID = VAL.LAYOUT_ID AND DV.DOC_VIEW_ID = VAL.DOC_VIEW_ID AND VAL.PROP_NAME = 'NEW_REF_ID' LEFT JOIN EXT_VALIDATION ON EXT_VALIDATION.VALIDATION_ID = CAST (RTRIM(VAL.PROP_VALUE) AS INT) LEFT JOIN DOC_VIEW_ELEMENT_D WT ON C1.DOC_ID = WT.DOC_ID AND C1.LEVEL_ID = WT.LEVEL_ID AND C1.FIELD_ID = WT.FIELD_ID AND WT.PROP_NAME = 'WIDGET_TYPE_ID' AND I18N1.LAYOUT_ID = WT.LAYOUT_ID AND DV.DOC_VIEW_ID = WT.DOC_VIEW_ID LEFT JOIN LAYOUT_WIDGET_TYPE LT ON LT.WIDGET_TYPE_ID = CAST (RTRIM(WT.PROP_VALUE) AS INT) LEFT JOIN DOC_VIEW_ELEMENT_D FP ON C1.DOC_ID = FP.DOC_ID AND C1.LEVEL_ID = FP.LEVEL_ID AND DED.FIELD_ID = FP.FIELD_ID AND I18N1.LAYOUT_ID = FP.LAYOUT_ID AND DV.DOC_VIEW_ID = FP.DOC_VIEW_ID AND FP.PROP_NAME = 'FORMAT_PATTERN' LEFT JOIN DOC_VIEW_ELEMENT_D VO ON C1.LEVEL_ID = VO.LEVEL_ID AND DED.FIELD_ID = VO.FIELD_ID AND I18N1.LAYOUT_ID = VO.LAYOUT_ID AND DV.DOC_VIEW_ID = VO.DOC_VIEW_ID AND VO.PROP_NAME = 'VAL_OVERRIDE' LEFT JOIN DOC_VIEW_ELEMENT_D SD ON C1.LEVEL_ID = SD.LEVEL_ID AND DED.FIELD_ID = SD.FIELD_ID AND I18N1.LAYOUT_ID = SD.LAYOUT_ID AND DV.DOC_VIEW_ID = SD.DOC_VIEW_ID AND SD.PROP_NAME = 'SHOW_DESCRIPTION' LEFT JOIN DOC_VIEW_ELEMENT_D EA ON C1.LEVEL_ID = EA.LEVEL_ID AND DED.FIELD_ID = EA.FIELD_ID AND I18N1.LAYOUT_ID = EA.LAYOUT_ID AND DV.DOC_VIEW_ID = EA.DOC_VIEW_ID AND EA.PROP_NAME = 'ENABLE_AUTOSUGG' LEFT JOIN DOC_VIEW_ELEMENT_D EV ON C1.LEVEL_ID = EV.LEVEL_ID AND DED.FIELD_ID = EV.FIELD_ID AND I18N1.LAYOUT_ID = EV.LAYOUT_ID AND DV.DOC_VIEW_ID = EV.DOC_VIEW_ID AND EV.PROP_NAME = 'ENABLE_VALSEARCH' LEFT JOIN DOC_VIEW_ELEMENT_D DL ON C1.LEVEL_ID = DL.LEVEL_ID AND DED.FIELD_ID = DL.FIELD_ID AND I18N1.LAYOUT_ID = DL.LAYOUT_ID AND DV.DOC_VIEW_ID = DL.DOC_VIEW_ID AND DL.PROP_NAME = 'DISPLAY_LENGTH' LEFT JOIN DOC_VIEW_ELEMENT_D REQ ON C1.LEVEL_ID = REQ.LEVEL_ID AND DED.FIELD_ID = REQ.FIELD_ID AND I18N1.LAYOUT_ID = REQ.LAYOUT_ID AND DV.DOC_VIEW_ID = REQ.DOC_VIEW_ID AND REQ.PROP_NAME = 'REQUIRED' LEFT JOIN DOC_VIEW_ELEMENT_D SLURL ON C1.LEVEL_ID = SLURL.LEVEL_ID AND DED.FIELD_ID = SLURL.FIELD_ID AND I18N1.LAYOUT_ID = SLURL.LAYOUT_ID AND DV.DOC_VIEW_ID = SLURL.DOC_VIEW_ID AND SLURL.PROP_NAME = 'SHOW_LOOKUP_URL' LEFT JOIN DOC_VIEW_ELEMENT_D SVH ON C1.LEVEL_ID = SVH.LEVEL_ID AND DED.FIELD_ID = SVH.FIELD_ID AND I18N1.LAYOUT_ID = SVH.LAYOUT_ID AND DV.DOC_VIEW_ID = SVH.DOC_VIEW_ID AND SVH.PROP_NAME = 'SHOW_VALUE_HOVER' LEFT JOIN DOC_VIEW_ELEMENT_D BG ON C1.LEVEL_ID = BG.LEVEL_ID AND DED.FIELD_ID = BG.FIELD_ID AND I18N1.LAYOUT_ID = BG.LAYOUT_ID AND DV.DOC_VIEW_ID = BG.DOC_VIEW_ID AND BG.PROP_NAME = 'BGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D FG ON C1.LEVEL_ID = FG.LEVEL_ID AND DED.FIELD_ID = FG.FIELD_ID AND I18N1.LAYOUT_ID = FG.LAYOUT_ID AND DV.DOC_VIEW_ID = FG.DOC_VIEW_ID AND FG.PROP_NAME = 'FGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D CH ON C1.LEVEL_ID = CH.LEVEL_ID AND DED.FIELD_ID = CH.FIELD_ID AND I18N1.LAYOUT_ID = CH.LAYOUT_ID AND DV.DOC_VIEW_ID = CH.DOC_VIEW_ID AND CH.PROP_NAME = 'CELL_HEIGHT' LEFT JOIN DOC_VIEW_ELEMENT_D CW ON C1.LEVEL_ID = CW.LEVEL_ID AND DED.FIELD_ID = CW.FIELD_ID AND I18N1.LAYOUT_ID = CW.LAYOUT_ID AND DV.DOC_VIEW_ID = CW.DOC_VIEW_ID AND CW.PROP_NAME = 'CELL_WIDTH' LEFT JOIN DOC_VIEW_ELEMENT_D AB ON C1.LEVEL_ID = AB.LEVEL_ID AND DED.FIELD_ID = AB.FIELD_ID AND I18N1.LAYOUT_ID = AB.LAYOUT_ID AND DV.DOC_VIEW_ID = AB.DOC_VIEW_ID AND AB.PROP_NAME = 'AS_BACKGROUND' LEFT JOIN DOC_VIEW_ELEMENT_D ED ON C1.LEVEL_ID = ED.LEVEL_ID AND DED.FIELD_ID = ED.FIELD_ID AND I18N1.LAYOUT_ID = ED.LAYOUT_ID AND DV.DOC_VIEW_ID = ED.DOC_VIEW_ID AND ED.PROP_NAME = 'ENABLE_DRAG' LEFT JOIN DOC_VIEW_ELEMENT_D DAL ON C1.LEVEL_ID = DAL.LEVEL_ID AND DED.FIELD_ID = DAL.FIELD_ID AND I18N1.LAYOUT_ID = DAL.LAYOUT_ID AND DV.DOC_VIEW_ID = DAL.DOC_VIEW_ID AND DAL.PROP_NAME = 'DISPLAY_AS_LABEL' LEFT JOIN DOC_VIEW_ELEMENT_D SF ON C1.LEVEL_ID = SF.LEVEL_ID AND DED.FIELD_ID = SF.FIELD_ID AND I18N1.LAYOUT_ID = SF.LAYOUT_ID AND DV.DOC_VIEW_ID = SF.DOC_VIEW_ID AND SF.PROP_NAME = 'SORT_FIELD' LEFT JOIN DOC_VIEW_ELEMENT_D DEFVAL ON C1.LEVEL_ID = DEFVAL.LEVEL_ID AND C1.FIELD_ID = DEFVAL.FIELD_ID AND LAYOUT_PROFILE.LAYOUT_ID = DEFVAL.LAYOUT_ID AND DV.DOC_VIEW_ID = DEFVAL.DOC_VIEW_ID AND DV.LAYOUT_ID = DEFVAL.LAYOUT_ID AND DEFVAL.PROP_NAME = 'DEFAULTVAL' LEFT JOIN DOC_VIEW_ELEMENT_D CC ON C1.LEVEL_ID = CC.LEVEL_ID AND C1.FIELD_ID = CC.FIELD_ID AND I18N1.LAYOUT_ID = CC.LAYOUT_ID AND DV.DOC_VIEW_ID = CC.DOC_VIEW_ID AND CC.PROP_NAME = 'CHANGECASE' LEFT JOIN DOC_VIEW_ELEMENT_D FB ON C1.LEVEL_ID = FB.LEVEL_ID AND C1.FIELD_ID = FB.FIELD_ID AND I18N1.LAYOUT_ID = FB.LAYOUT_ID AND DV.DOC_VIEW_ID = FB.DOC_VIEW_ID AND FB.PROP_NAME = 'FRACTION_BASE' LEFT JOIN DOC_VIEW_ELEMENT_D FV ON C1.LEVEL_ID = FV.LEVEL_ID AND C1.FIELD_ID = FV.FIELD_ID AND I18N1.LAYOUT_ID = FV.LAYOUT_ID AND DV.DOC_VIEW_ID = FV.DOC_VIEW_ID AND FV.PROP_NAME = 'FRACTION_VALUE' LEFT JOIN DOC_VIEW_ELEMENT_D RF ON C1.LEVEL_ID = RF.LEVEL_ID AND C1.FIELD_ID = RF.FIELD_ID AND I18N1.LAYOUT_ID = RF.LAYOUT_ID AND DV.DOC_VIEW_ID = RF.DOC_VIEW_ID AND RF.PROP_NAME = 'REDUCE_FRACTION' LEFT JOIN DOC_VIEW_ELEMENT_D CV ON C1.LEVEL_ID = CV.LEVEL_ID AND C1.FIELD_ID = CV.FIELD_ID AND I18N1.LAYOUT_ID = CV.LAYOUT_ID AND DV.DOC_VIEW_ID = CV.DOC_VIEW_ID AND CV.PROP_NAME = 'CUSTOM_VALIDATOR' WHERE LAYOUT_PROFILE.LAYOUT_ID IN ("
					+ commaSepValues + ") AND A1.MODIFY_TS  BETWEEN '" + migrationFromDate + "' AND '" + migrationToDate
					+ "'";
			layoutReportDetails = em.createNativeQuery(sqlQuery).getResultList();

			logger.info("List Of layoutReportDetails Available :" + layoutReportDetails.size());

			for (Object[] obj : layoutReportDetails) {
				LayoutReport lr = new LayoutReport();
				lr.setLayoutDesc((String) obj[0]);
				lr.setDocNameDesc((String) obj[1]);
				lr.setFieldName((String) obj[2]);
				lr.setDocViewName((String) obj[3]);
				lr.setSectionName((String) obj[4]);
				lr.setFieldLabel((String) obj[5]);
				lr.setScale((Integer) obj[6]);
				lr.setValidationName((String) obj[7]);
				lr.setWidgetName((String) obj[8]);
				lr.setValidationOverride((String) obj[9]);
				lr.setShowDesc((String) obj[10]);
				lr.setEnableAutoSugg((String) obj[11]);
				lr.setDisplayLength((String) obj[12]);
				lr.setRequired((String) obj[13]);
				lr.setShowLookupUrl((String) obj[14]);
				lr.setBgColor((String) obj[15]);
				lr.setFgColor((String) obj[16]);
				lr.setCustomValidator((String) obj[17]);

				listOflayoutReportDetails.add(lr);
			}

		} catch (Exception e) {
			logger.error("Error occured in destionation dao in getTheLayoutReport() " + e.getMessage());
			throw e;
		}
		return listOflayoutReportDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AppConfigReport> getTheAppConfigReport(String fromDate, String toDate) {
		List<AppConfigReport> listOfAppConfigReportDetails = new ArrayList<AppConfigReport>();
		List<Object[]> appConfigReportDetails = new ArrayList<>();
		EntityManager em = entityManagerFactory.createEntityManager();
		try {
			String sqlQuery = "select cc.category_name,cd.description,cd.prop_value from config_d cd inner join config_category cc on cd.category_id = cc.category_id"
					+ " where cd.modify_ts between '" + fromDate + "' and '" + toDate + "'";
			appConfigReportDetails = em.createNativeQuery(sqlQuery).getResultList();

			logger.info("List Of getTheAppConfigReport Available :" + appConfigReportDetails.size());

			for (Object[] obj : appConfigReportDetails) {
				AppConfigReport acr = new AppConfigReport();
				acr.setCategoryName((String) obj[0]);
				acr.setDescription((String) obj[1]);
				acr.setPropValue((String) obj[2]);
				listOfAppConfigReportDetails.add(acr);
			}

		} catch (Exception e) {
			logger.error("Error occured in destionation dao in getTheAppConfigReport() " + e.getMessage());
			throw e;
		}
		return listOfAppConfigReportDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdhocDocumentReport> getTheAdhocDocumentsReport(String fromDate, String toDate) {
		List<AdhocDocumentReport> listOfAdhocDocumentReportDetails = new ArrayList<AdhocDocumentReport>();
		List<Object[]> adhocDocumentReportDetails = new ArrayList<>();
		EntityManager em = entityManagerFactory.createEntityManager();
		try {
			String sqlQuery = "select category_id,adhoc_doc_name from ADHOC_DOCUMENTS where modify_ts between '"
					+ fromDate + "' and '" + toDate + "'";
			adhocDocumentReportDetails = em.createNativeQuery(sqlQuery).getResultList();

			logger.info("List Of getTheAdhocDocumentsReport Available :" + adhocDocumentReportDetails.size());

			for (Object[] obj : adhocDocumentReportDetails) {
				AdhocDocumentReport adr = new AdhocDocumentReport();
				adr.setCategoryId((String) obj[0]);
				adr.setAdhocDocName((String) obj[1]);
				listOfAdhocDocumentReportDetails.add(adr);
			}

		} catch (Exception e) {
			logger.error("Error occured in destionation dao in getTheAdhocDocumentsReport() " + e.getMessage());
			throw e;
		}
		return listOfAdhocDocumentReportDetails;
	}

	@Override
	public Optional<SecRoleProfile> isRoleIdAvailable(Integer roleId) {
		return secRoleProfileDao.findById(roleId);
	}

	@Override
	public Connection getTheDestinationDbConnection() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);
		Connection jdbcConnection = session.doReturningWork(new ReturningWork<Connection>() {
			@Override
			public Connection execute(Connection conn) throws SQLException {
				return conn;
			}
		});
		return jdbcConnection;
	}

	public List<Object> getTheDataFromDestinationDbByTableName(String className, Integer roleId) {

		switch (className) {
		case "DashSectionH":
			return dashSectionHDao.getTheDataFromDashSectionHtable(roleId);

		case "SecRoleResCtl":
			return secRoleResCtlDao.getTheDataFromSecRoleResCtlTable(roleId);

		case "SmuRoleRootMenu":
			return smuRoleRootMenuDao.getTheDataFromSmuRoleRootMenuTable(roleId);

		case "SmuRoleMenuItem":
			return smuRoleMenuItemDao.getTheDataFromSmuRoleMenuItemTable(roleId);

		case "CollabFields":
			return collabFieldsDao.getTheDataFromCollabFieldsTable(roleId);

		case "RoleResAttr":
			return roleResAttarDao.getTheDataFromRoleResAttarTable(roleId);

		case "SecQuery":
			return secQueryDao.getTheDataFromSecQueryTable(roleId);

		case "SecQueryGroup":
			return secQueryGroupDao.getTheDataFromSecQueryGroupTable(roleId);

		case "SecBoLevelAcl":
			return secBoLevelAclDao.getTheDataFromSecBoLevelAclTable(roleId);

		case "SecBoFieldAcl":
			return secBoFieldAclDao.getTheDataFromSecBoFieldAclTable(roleId);

		case "SecBoFilter":
			return secBoFilterDao.getTheDataFromSecBoFilterTable(roleId);

		case "SecBointFilter":
			return secBointFilterDao.getTheDataFromSecBointFilterTable(roleId);

		default:
			logger.error("No Class Name Matching In Switch Case :" + className);
			return null;
		}

	}

	@Override
	public List<Object[]> getTheLayoutColumnsByModifiedTime(Date migrationFromDate, Date migrationToDate) {
		EntityManager em = entityManagerFactory.createEntityManager();

		String sqlQuery = "SELECT DISTINCT   LAYOUT_PROFILE.LAYOUT_ID, DOC_NAME.DOC_ID "
				+ "FROM LAYOUT_PROFILE LAYOUT_PROFILE "
				+ "LEFT JOIN DOC_VIEW DV ON DV.LAYOUT_ID = LAYOUT_PROFILE.LAYOUT_ID "
				+ "LEFT JOIN DOC_NAME DOC_NAME ON DV.DOC_ID = DOC_NAME.DOC_ID "
				+ "LEFT JOIN LAYOUT_I18N_RES LIR ON  LIR.LAYOUT_ID = DV.LAYOUT_ID AND DV.I18N_RES_ID = LIR.I18N_RES_ID "
				+ "LEFT JOIN LAYOUT_" + "SECTION A ON A.LAYOUT_ID = LIR.LAYOUT_ID AND A.DOC_VIEW_ID = DV.DOC_VIEW_ID "
				+ "LEFT JOIN LAYOUT_I18N_RES B ON A.LAYOUT_ID = B.LAYOUT_ID AND A.I18N_RES_ID = "
				+ "B.I18N_RES_ID AND B.LANGUAGE_ID = LIR.LANGUAGE_ID LEFT JOIN LAYOUT_DETAIL A1 ON A1.LAYOUT_ID ="
				+ " B.LAYOUT_ID AND A1.SECTION_ID = A.SECTION_ID AND A1.DOC_VIEW_ID = "
				+ "DV.DOC_VIEW_ID LEFT JOIN VIEW_DOC_ELEMENTS C1 ON A1.DOC_ID = C1.DOC_ID AND A1.DOC_LEVEL_ID = "
				+ "C1.LEVEL_ID AND A1.DOC_FIELD_ID = C1.FIELD_ID LEFT JOIN DEFAULT_ELEMENT_D DED ON DED.DOC_ID = "
				+ "A1.DOC_ID AND DED.FIELD_ID = A1.DOC_FIELD_ID AND DED.LEVEL_ID = A1.DOC_LEVEL_ID AND DED.PROP_NAME = "
				+ "'I18N_RES_ID' AND C1.DOC_ID = DED.DOC_ID LEFT JOIN LAYOUT_I18N_RES I18N1 ON I18N1.I18N_RES_ID = "
				+ "CAST(DED.PROP_VALUE AS INT) AND I18N1.LAYOUT_ID = A1.LAYOUT_ID LEFT JOIN DOC_VIEW_ELEMENT_D VAL ON "
				+ "C1.LEVEL_ID = VAL.LEVEL_ID AND DED.FIELD_ID = VAL.FIELD_ID AND I18N1.LAYOUT_ID = VAL.LAYOUT_ID "
				+ "AND DV.DOC_VIEW_ID = VAL.DOC_VIEW_ID AND VAL.PROP_NAME = "
				+ "'NEW_REF_ID' LEFT JOIN EXT_VALIDATION ON EXT_VALIDATION.VALIDATION_ID = "
				+ "CAST (RTRIM(VAL.PROP_VALUE) AS INT) LEFT JOIN DOC_VIEW_ELEMENT_D WT ON C1.DOC_ID = "
				+ "WT.DOC_ID AND C1.LEVEL_ID = WT.LEVEL_ID AND C1.FIELD_ID = WT.FIELD_ID AND WT.PROP_NAME = "
				+ "'WIDGET_TYPE_ID' AND I18N1.LAYOUT_ID = WT.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "WT.DOC_VIEW_ID LEFT JOIN LAYOUT_WIDGET_TYPE LT ON LT.WIDGET_TYPE_ID = "
				+ "CAST (RTRIM(WT.PROP_VALUE) AS INT) LEFT JOIN DOC_VIEW_ELEMENT_D FP ON C1.DOC_ID = "
				+ "FP.DOC_ID AND C1.LEVEL_ID = FP.LEVEL_ID AND DED.FIELD_ID = FP.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "FP.LAYOUT_ID AND DV.DOC_VIEW_ID = FP.DOC_VIEW_ID AND FP.PROP_NAME = "
				+ "'FORMAT_PATTERN' LEFT JOIN DOC_VIEW_ELEMENT_D VO ON C1.LEVEL_ID = "
				+ "VO.LEVEL_ID AND DED.FIELD_ID = VO.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "VO.LAYOUT_ID AND DV.DOC_VIEW_ID = VO.DOC_VIEW_ID AND VO.PROP_NAME = "
				+ "'VAL_OVERRIDE' LEFT JOIN DOC_VIEW_ELEMENT_D SD ON C1.LEVEL_ID = "
				+ "SD.LEVEL_ID AND DED.FIELD_ID = SD.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "SD.LAYOUT_ID AND DV.DOC_VIEW_ID = SD.DOC_VIEW_ID AND SD.PROP_NAME = "
				+ "'SHOW_DESCRIPTION' LEFT JOIN DOC_VIEW_ELEMENT_D EA ON C1.LEVEL_ID = "
				+ "EA.LEVEL_ID AND DED.FIELD_ID = EA.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "EA.LAYOUT_ID AND DV.DOC_VIEW_ID = EA.DOC_VIEW_ID AND EA.PROP_NAME = "
				+ "'ENABLE_AUTOSUGG' LEFT JOIN DOC_VIEW_ELEMENT_D EV ON C1.LEVEL_ID = "
				+ "EV.LEVEL_ID AND DED.FIELD_ID = EV.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "EV.LAYOUT_ID AND DV.DOC_VIEW_ID = EV.DOC_VIEW_ID AND EV.PROP_NAME = "
				+ "'ENABLE_VALSEARCH' LEFT JOIN DOC_VIEW_ELEMENT_D DL ON C1.LEVEL_ID = "
				+ "DL.LEVEL_ID AND DED.FIELD_ID = DL.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "DL.LAYOUT_ID AND DV.DOC_VIEW_ID = DL.DOC_VIEW_ID AND DL.PROP_NAME = "
				+ "'DISPLAY_LENGTH' LEFT JOIN DOC_VIEW_ELEMENT_D REQ ON C1.LEVEL_ID = "
				+ "REQ.LEVEL_ID AND DED.FIELD_ID = REQ.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "REQ.LAYOUT_ID AND DV.DOC_VIEW_ID = REQ.DOC_VIEW_ID AND REQ.PROP_NAME = "
				+ "'REQUIRED' LEFT JOIN DOC_VIEW_ELEMENT_D SLURL ON C1.LEVEL_ID = "
				+ "SLURL.LEVEL_ID AND DED.FIELD_ID = SLURL.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "SLURL.LAYOUT_ID AND DV.DOC_VIEW_ID = SLURL.DOC_VIEW_ID AND SLURL.PROP_NAME = "
				+ "'SHOW_LOOKUP_URL' LEFT JOIN DOC_VIEW_ELEMENT_D SVH ON C1.LEVEL_ID = "
				+ "SVH.LEVEL_ID AND DED.FIELD_ID = SVH.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "SVH.LAYOUT_ID AND DV.DOC_VIEW_ID = SVH.DOC_VIEW_ID AND SVH.PROP_NAME = "
				+ "'SHOW_VALUE_HOVER' LEFT JOIN DOC_VIEW_ELEMENT_D BG ON C1.LEVEL_ID = "
				+ "BG.LEVEL_ID AND DED.FIELD_ID = BG.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "BG.LAYOUT_ID AND DV.DOC_VIEW_ID = BG.DOC_VIEW_ID AND BG.PROP_NAME = "
				+ "'BGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D FG ON C1.LEVEL_ID = "
				+ "FG.LEVEL_ID AND DED.FIELD_ID = FG.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "FG.LAYOUT_ID AND DV.DOC_VIEW_ID = FG.DOC_VIEW_ID AND FG.PROP_NAME = "
				+ "'FGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D CH ON C1.LEVEL_ID = "
				+ "CH.LEVEL_ID AND DED.FIELD_ID = CH.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "CH.LAYOUT_ID AND DV.DOC_VIEW_ID = CH.DOC_VIEW_ID AND CH.PROP_NAME = "
				+ "'CELL_HEIGHT' LEFT JOIN DOC_VIEW_ELEMENT_D CW ON C1.LEVEL_ID = "
				+ "CW.LEVEL_ID AND DED.FIELD_ID = CW.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "CW.LAYOUT_ID AND DV.DOC_VIEW_ID = CW.DOC_VIEW_ID AND CW.PROP_NAME = "
				+ "'CELL_WIDTH' LEFT JOIN DOC_VIEW_ELEMENT_D AB ON C1.LEVEL_ID = "
				+ "AB.LEVEL_ID AND DED.FIELD_ID = AB.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "AB.LAYOUT_ID AND DV.DOC_VIEW_ID = AB.DOC_VIEW_ID AND AB.PROP_NAME = "
				+ "'AS_BACKGROUND' LEFT JOIN DOC_VIEW_ELEMENT_D ED ON C1.LEVEL_ID = ED.LEVEL_ID AND DED.FIELD_ID = "
				+ "ED.FIELD_ID AND I18N1.LAYOUT_ID = ED.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "ED.DOC_VIEW_ID AND ED.PROP_NAME = "
				+ "'ENABLE_DRAG' LEFT JOIN DOC_VIEW_ELEMENT_D DAL ON C1.LEVEL_ID = "
				+ "DAL.LEVEL_ID AND DED.FIELD_ID = DAL.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "DAL.LAYOUT_ID AND DV.DOC_VIEW_ID = DAL.DOC_VIEW_ID AND DAL.PROP_NAME = "
				+ "'DISPLAY_AS_LABEL' LEFT JOIN DOC_VIEW_ELEMENT_D SF ON C1.LEVEL_ID = "
				+ "SF.LEVEL_ID AND DED.FIELD_ID = SF.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "SF.LAYOUT_ID AND DV.DOC_VIEW_ID = SF.DOC_VIEW_ID AND SF.PROP_NAME = "
				+ "'SORT_FIELD' LEFT JOIN DOC_VIEW_ELEMENT_D DEFVAL ON C1.LEVEL_ID = "
				+ "DEFVAL.LEVEL_ID AND C1.FIELD_ID = DEFVAL.FIELD_ID AND LAYOUT_PROFILE.LAYOUT_ID = "
				+ "DEFVAL.LAYOUT_ID AND DV.DOC_VIEW_ID = DEFVAL.DOC_VIEW_ID AND DV.LAYOUT_ID = "
				+ "DEFVAL.LAYOUT_ID AND DEFVAL.PROP_NAME = 'DEFAULTVAL' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D CC ON C1.LEVEL_ID = CC.LEVEL_ID AND C1.FIELD_ID = "
				+ "CC.FIELD_ID AND I18N1.LAYOUT_ID = CC.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "CC.DOC_VIEW_ID AND CC.PROP_NAME = 'CHANGECASE' LEFT JOIN DOC_VIEW_ELEMENT_D FB "
				+ "ON C1.LEVEL_ID = FB.LEVEL_ID AND C1.FIELD_ID = FB.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "FB.LAYOUT_ID AND DV.DOC_VIEW_ID = FB.DOC_VIEW_ID AND FB.PROP_NAME = 'FRACTION_BASE' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D FV ON C1.LEVEL_ID = FV.LEVEL_ID AND C1.FIELD_ID = "
				+ "FV.FIELD_ID AND I18N1.LAYOUT_ID = FV.LAYOUT_ID AND DV.DOC_VIEW_ID = FV.DOC_VIEW_ID AND "
				+ "FV.PROP_NAME = 'FRACTION_VALUE' LEFT JOIN DOC_VIEW_ELEMENT_D RF ON C1.LEVEL_ID = "
				+ "RF.LEVEL_ID AND C1.FIELD_ID = RF.FIELD_ID AND I18N1.LAYOUT_ID = RF.LAYOUT_ID AND "
				+ "DV.DOC_VIEW_ID = RF.DOC_VIEW_ID AND RF.PROP_NAME = 'REDUCE_FRACTION' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D CV ON C1.LEVEL_ID = CV.LEVEL_ID AND C1.FIELD_ID = "
				+ "CV.FIELD_ID AND I18N1.LAYOUT_ID = CV.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "CV.DOC_VIEW_ID AND CV.PROP_NAME = 'CUSTOM_VALIDATOR' WHERE A1.MODIFY_TS BETWEEN " + "'"
				+ migrationFromDate + "' AND '" + migrationToDate + "'";

		@SuppressWarnings("unchecked")
		List<Object[]> listOfObjectArray = em.createNativeQuery(sqlQuery).getResultList();

		logger.info("List Of Object Array Got From Destination Environment :" + listOfObjectArray.size());

		return listOfObjectArray;
	}

	@Override
	public Integer getTheCountOfLayoutColumn(Integer layoutId, Integer docId, Date migrationFromDate,
			Date migrationToDate) {

		EntityManager em = entityManagerFactory.createEntityManager();

		String sqlQuery = "SELECT count(*) FROM LAYOUT_PROFILE LAYOUT_PROFILE "
				+ "LEFT JOIN DOC_VIEW DV ON DV.LAYOUT_ID = " + "LAYOUT_PROFILE.LAYOUT_ID "
				+ "LEFT JOIN DOC_NAME DOC_NAME ON DV.DOC_ID = " + "DOC_NAME.DOC_ID LEFT JOIN LAYOUT_I18N_RES LIR ON  "
				+ "LIR.LAYOUT_ID = DV.LAYOUT_ID AND DV.I18N_RES_ID = LIR.I18N_RES_ID "
				+ "LEFT JOIN LAYOUT_SECTION A ON A.LAYOUT_ID = LIR.LAYOUT_ID AND A.DOC_VIEW_ID = DV.DOC_VIEW_ID "
				+ "LEFT JOIN LAYOUT_I18N_RES B ON A.LAYOUT_ID = B.LAYOUT_ID AND A.I18N_RES_ID = B.I18N_RES_ID AND B.LANGUAGE_ID = "
				+ "LIR.LANGUAGE_ID LEFT JOIN LAYOUT_DETAIL A1 ON A1.LAYOUT_ID = B.LAYOUT_ID AND A1.SECTION_ID = "
				+ "A.SECTION_ID AND A1.DOC_VIEW_ID = DV.DOC_VIEW_ID LEFT JOIN VIEW_DOC_ELEMENTS C1 ON A1.DOC_ID = "
				+ "C1.DOC_ID AND A1.DOC_LEVEL_ID = C1.LEVEL_ID AND A1.DOC_FIELD_ID = C1.FIELD_ID "
				+ "LEFT JOIN DEFAULT_ELEMENT_D DED ON DED.DOC_ID = A1.DOC_ID AND DED.FIELD_ID = "
				+ "A1.DOC_FIELD_ID AND DED.LEVEL_ID = A1.DOC_LEVEL_ID AND DED.PROP_NAME = 'I18N_RES_ID' "
				+ "AND C1.DOC_ID = DED.DOC_ID LEFT JOIN LAYOUT_I18N_RES I18N1 ON I18N1.I18N_RES_ID = "
				+ "CAST(DED.PROP_VALUE AS INT) AND I18N1.LAYOUT_ID = A1.LAYOUT_ID LEFT JOIN DOC_VIEW_ELEMENT_D "
				+ "VAL ON C1.LEVEL_ID = VAL.LEVEL_ID AND DED.FIELD_ID = VAL.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "VAL.LAYOUT_ID AND DV.DOC_VIEW_ID = VAL.DOC_VIEW_ID AND VAL.PROP_NAME = 'NEW_REF_ID' LEFT "
				+ "JOIN EXT_VALIDATION ON EXT_VALIDATION.VALIDATION_ID = CAST (RTRIM(VAL.PROP_VALUE) AS INT) "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D WT ON C1.DOC_ID = WT.DOC_ID AND C1.LEVEL_ID = WT.LEVEL_ID AND "
				+ "C1.FIELD_ID = WT.FIELD_ID AND WT.PROP_NAME = 'WIDGET_TYPE_ID' AND I18N1.LAYOUT_ID = "
				+ "WT.LAYOUT_ID AND DV.DOC_VIEW_ID = WT.DOC_VIEW_ID LEFT JOIN LAYOUT_WIDGET_TYPE LT ON "
				+ "LT.WIDGET_TYPE_ID = CAST (RTRIM(WT.PROP_VALUE) AS INT) LEFT JOIN DOC_VIEW_ELEMENT_D FP ON C1.DOC_ID = "
				+ "FP.DOC_ID AND C1.LEVEL_ID = FP.LEVEL_ID AND DED.FIELD_ID = FP.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "FP.LAYOUT_ID AND DV.DOC_VIEW_ID = FP.DOC_VIEW_ID AND FP.PROP_NAME = 'FORMAT_PATTERN' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D VO ON C1.LEVEL_ID = VO.LEVEL_ID AND DED.FIELD_ID = VO.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "VO.LAYOUT_ID AND DV.DOC_VIEW_ID = VO.DOC_VIEW_ID AND VO.PROP_NAME = 'VAL_OVERRIDE' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D SD ON C1.LEVEL_ID = SD.LEVEL_ID AND DED.FIELD_ID = "
				+ "SD.FIELD_ID AND I18N1.LAYOUT_ID = SD.LAYOUT_ID AND DV.DOC_VIEW_ID = SD.DOC_VIEW_ID AND SD.PROP_NAME = "
				+ "'SHOW_DESCRIPTION' LEFT JOIN DOC_VIEW_ELEMENT_D EA ON C1.LEVEL_ID = EA.LEVEL_ID AND DED.FIELD_ID = "
				+ "EA.FIELD_ID AND I18N1.LAYOUT_ID = EA.LAYOUT_ID AND DV.DOC_VIEW_ID = EA.DOC_VIEW_ID AND EA.PROP_NAME = "
				+ "'ENABLE_AUTOSUGG' LEFT JOIN DOC_VIEW_ELEMENT_D EV ON C1.LEVEL_ID = EV.LEVEL_ID AND DED.FIELD_ID = "
				+ "EV.FIELD_ID AND I18N1.LAYOUT_ID = EV.LAYOUT_ID AND DV.DOC_VIEW_ID = EV.DOC_VIEW_ID AND EV.PROP_NAME = "
				+ "'ENABLE_VALSEARCH' LEFT JOIN DOC_VIEW_ELEMENT_D DL ON C1.LEVEL_ID = DL.LEVEL_ID AND DED.FIELD_ID = "
				+ "DL.FIELD_ID AND I18N1.LAYOUT_ID = DL.LAYOUT_ID AND DV.DOC_VIEW_ID = DL.DOC_VIEW_ID AND DL.PROP_NAME = "
				+ "'DISPLAY_LENGTH' LEFT JOIN DOC_VIEW_ELEMENT_D REQ ON C1.LEVEL_ID = REQ.LEVEL_ID AND DED.FIELD_ID = "
				+ "REQ.FIELD_ID AND I18N1.LAYOUT_ID = REQ.LAYOUT_ID AND DV.DOC_VIEW_ID = REQ.DOC_VIEW_ID AND REQ.PROP_NAME = "
				+ "'REQUIRED' LEFT JOIN DOC_VIEW_ELEMENT_D SLURL ON C1.LEVEL_ID = SLURL.LEVEL_ID AND DED.FIELD_ID = "
				+ "SLURL.FIELD_ID AND I18N1.LAYOUT_ID = SLURL.LAYOUT_ID AND DV.DOC_VIEW_ID = SLURL.DOC_VIEW_ID AND SLURL.PROP_NAME = "
				+ "'SHOW_LOOKUP_URL' LEFT JOIN DOC_VIEW_ELEMENT_D SVH ON C1.LEVEL_ID = SVH.LEVEL_ID AND DED.FIELD_ID = SVH.FIELD_ID "
				+ "AND I18N1.LAYOUT_ID = SVH.LAYOUT_ID AND DV.DOC_VIEW_ID = SVH.DOC_VIEW_ID AND SVH.PROP_NAME = 'SHOW_VALUE_HOVER' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D BG ON C1.LEVEL_ID = BG.LEVEL_ID AND DED.FIELD_ID = BG.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "BG.LAYOUT_ID AND DV.DOC_VIEW_ID = BG.DOC_VIEW_ID AND BG.PROP_NAME = 'BGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D FG ON "
				+ "C1.LEVEL_ID = FG.LEVEL_ID AND DED.FIELD_ID = FG.FIELD_ID AND I18N1.LAYOUT_ID = FG.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "FG.DOC_VIEW_ID AND FG.PROP_NAME = 'FGCOLOR' LEFT JOIN DOC_VIEW_ELEMENT_D CH ON C1.LEVEL_ID = CH.LEVEL_ID AND DED.FIELD_ID = "
				+ "CH.FIELD_ID AND I18N1.LAYOUT_ID = CH.LAYOUT_ID AND DV.DOC_VIEW_ID = CH.DOC_VIEW_ID AND CH.PROP_NAME = "
				+ "'CELL_HEIGHT' LEFT JOIN DOC_VIEW_ELEMENT_D CW ON C1.LEVEL_ID = CW.LEVEL_ID AND DED.FIELD_ID = CW.FIELD_ID "
				+ "AND I18N1.LAYOUT_ID = CW.LAYOUT_ID AND DV.DOC_VIEW_ID = CW.DOC_VIEW_ID AND CW.PROP_NAME = 'CELL_WIDTH' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D AB ON C1.LEVEL_ID = AB.LEVEL_ID AND DED.FIELD_ID = AB.FIELD_ID AND "
				+ "I18N1.LAYOUT_ID = AB.LAYOUT_ID AND DV.DOC_VIEW_ID = AB.DOC_VIEW_ID AND AB.PROP_NAME = 'AS_BACKGROUND' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D ED ON C1.LEVEL_ID = ED.LEVEL_ID AND DED.FIELD_ID = ED.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "ED.LAYOUT_ID AND DV.DOC_VIEW_ID = ED.DOC_VIEW_ID AND ED.PROP_NAME = 'ENABLE_DRAG' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D DAL ON C1.LEVEL_ID = DAL.LEVEL_ID AND DED.FIELD_ID = DAL.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "DAL.LAYOUT_ID AND DV.DOC_VIEW_ID = DAL.DOC_VIEW_ID AND DAL.PROP_NAME = 'DISPLAY_AS_LABEL' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D SF ON C1.LEVEL_ID = SF.LEVEL_ID AND DED.FIELD_ID = SF.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "SF.LAYOUT_ID AND DV.DOC_VIEW_ID = SF.DOC_VIEW_ID AND SF.PROP_NAME = 'SORT_FIELD' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D DEFVAL ON C1.LEVEL_ID = DEFVAL.LEVEL_ID AND C1.FIELD_ID = "
				+ "DEFVAL.FIELD_ID AND LAYOUT_PROFILE.LAYOUT_ID = DEFVAL.LAYOUT_ID AND DV.DOC_VIEW_ID = "
				+ "DEFVAL.DOC_VIEW_ID AND DV.LAYOUT_ID = DEFVAL.LAYOUT_ID AND DEFVAL.PROP_NAME = 'DEFAULTVAL' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D CC ON C1.LEVEL_ID = CC.LEVEL_ID AND C1.FIELD_ID = CC.FIELD_ID AND "
				+ "I18N1.LAYOUT_ID = CC.LAYOUT_ID AND DV.DOC_VIEW_ID = CC.DOC_VIEW_ID AND CC.PROP_NAME = 'CHANGECASE' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D FB ON C1.LEVEL_ID = FB.LEVEL_ID AND C1.FIELD_ID = FB.FIELD_ID AND "
				+ "I18N1.LAYOUT_ID = FB.LAYOUT_ID AND DV.DOC_VIEW_ID = FB.DOC_VIEW_ID AND FB.PROP_NAME = 'FRACTION_BASE' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D FV ON C1.LEVEL_ID = FV.LEVEL_ID AND C1.FIELD_ID = FV.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "FV.LAYOUT_ID AND DV.DOC_VIEW_ID = FV.DOC_VIEW_ID AND FV.PROP_NAME = 'FRACTION_VALUE' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D RF ON C1.LEVEL_ID = RF.LEVEL_ID AND C1.FIELD_ID = RF.FIELD_ID AND "
				+ "I18N1.LAYOUT_ID = RF.LAYOUT_ID AND DV.DOC_VIEW_ID = RF.DOC_VIEW_ID AND RF.PROP_NAME = 'REDUCE_FRACTION' "
				+ "LEFT JOIN DOC_VIEW_ELEMENT_D CV ON C1.LEVEL_ID = CV.LEVEL_ID AND C1.FIELD_ID = CV.FIELD_ID AND I18N1.LAYOUT_ID = "
				+ "CV.LAYOUT_ID AND DV.DOC_VIEW_ID = CV.DOC_VIEW_ID AND CV.PROP_NAME = 'CUSTOM_VALIDATOR' WHERE "
				+ "LAYOUT_PROFILE.LAYOUT_ID IN ('" + layoutId + "') AND DOC_NAME.DOC_ID in(" + docId
				+ ") AND A1.MODIFY_TS BETWEEN " + "'" + migrationFromDate + "' AND '" + migrationToDate + "'";

		return (Integer) em.createNativeQuery(sqlQuery).getResultList().get(0);

	}

	@Override
	public void restoreTheModuleData(StringBuilder query) {

		EntityManager em = entityManagerFactory.createEntityManager();
		String sqlString = new String(query);
		em.getTransaction().begin();
		try {
			if (!sqlString.isEmpty()) {
				em.createNativeQuery(sqlString).executeUpdate();
				em.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error("Exception Occured While Restoring the Security module data :" + e.getCause());
			em.getTransaction().rollback();
			throw new MigrationException("Opps, Unable to restore");
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void restoreQueryHandQueryDTableData(MigrationDetails migrationDetails) throws Exception {

		String queryHfileName = env.getProperty("dir.path.backup") + migrationDetails.getQueryHfileName();
		String queryDfileName = env.getProperty("dir.path.backup") + migrationDetails.getQueryDfileName();

		File queryHInsertfile = new File(queryHfileName);
		File queryDInsertfile = new File(queryDfileName);

		IDatabaseConnection connection = null;

		logger.info("Query_H File Name We got From DB:" + migrationDetails.getQueryHfileName());
		logger.info("Query_D File Name We got From DB:" + migrationDetails.getQueryDfileName());

		logger.info("is Query_H file exist :" + queryHInsertfile.exists());

		logger.info("is Query_D file exist :" + queryDInsertfile.exists());

		connection = getConnection();

		try {
			logger.info("Inserting QueryHandQueryDtableData()..... ");

			if (!migrationDetails.getQueryHfileName().isEmpty() && queryHInsertfile.exists()) {
				DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));
			}

			if (!migrationDetails.getQueryDfileName().isEmpty() && queryDInsertfile.exists()) {
				DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
			}

			connection.getConnection().commit();
			logger.info("Completed Restoring QueryHandQueryDtableData()..... ");
		} catch (Exception e) {
			logger.error("Exception Occured While Restoring QueryD And QueryH Table :" + e.getMessage());
			logger.info("Updating-Restoring QueryHandQueryDtableData()..... ");
			if (!migrationDetails.getQueryHfileName().isEmpty() && queryHInsertfile.exists())
				DatabaseOperation.UPDATE.execute(connection, new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));

			if (!migrationDetails.getQueryDfileName().isEmpty() && queryDInsertfile.exists())
				DatabaseOperation.UPDATE.execute(connection, new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
			connection.getConnection().commit();
			logger.info("Updating-Restoring QueryHandQueryDtableData() is completed..... ");
		} finally {
			connection.close();
			System.gc();

		}

		logger.info("QueryH And QueryD Table Insertion Completed");

	}

	@Override
	public Boolean deleteSecurityDataForRestoringData(String[] delTables, List<MigratedRoleIdDetails> listOfRoleId) {

		boolean returnDeleteStatus = false;

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);

		try {
			session.beginTransaction();

			listOfRoleId.forEach(migrationDetails -> {

				Integer roleId = migrationDetails.getRoleId();

				com.retelzy.brim.entity.source.SecRoleProfile secRole = new com.retelzy.brim.entity.source.SecRoleProfile();
				secRole.setRoleId(roleId);
				List<Integer> listOfQueryIdFromFieldAcl = sourceSecBoFieldAclDao.getAllTheQueryIdsByRoleId(roleId);
				List<Integer> listOfQueryIdFromLevelAcl = sourceSecBoLevelAclDao.getAllTheQueryIdsByRoleId(secRole);
				List<Integer> listOfQueryIdFromBoFilter = sourceSecBoFilterDao.getAllQueryIdByRoleId(roleId);
				List<Integer> listOfQueryIdFromBointFilter = sourceSecBointFilterDao.getAllTheQueryIdsByRoleId(roleId);

				logger.info("listOfQueryIdFromFieldAcl :" + listOfQueryIdFromFieldAcl.size());

				/*
				 * Delete child records before parent, parent table:QUERY_D, then Child
				 * table:SEC_QUERY
				 */
				Integer deleteStatus = session.createQuery(
						"Delete From SecQuery sq Where sq.id.queryId IN (Select qd.queryId from QueryD qd) AND sq.id.roleId =:roleId")
						.setParameter("roleId", roleId).executeUpdate();
				logger.info("deleteTheDetailsFromSecQueryForQueryD() :" + deleteStatus);

				/*
				 * Delete child records before parent, parent table:QUERY_H, then Child
				 * table:SEC_QUERY, Written By Manas on the date of 12th May 2020
				 */
				deleteStatus = session.createQuery(
						"Delete From SecQuery sq Where sq.id.queryId IN (Select qd.queryId from QueryH qd) AND sq.id.roleId =:roleId")
						.setParameter("roleId", roleId).executeUpdate();
				logger.info("deleteTheDetailsFromSecQueryForQueryH() :" + deleteStatus);

				/* SEC_QUERY */

				/* SEC_BO_FILTER */
				deleteStatus = session
						.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromBoFilter)")
						.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();

				/* SEC_BOINT_FILTER */
				deleteStatus = session
						.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromBointFilter)")
						.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();

				/* SEC_BO_LEVEL_ACl */
				deleteStatus = session
						.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromLevelAcl)")
						.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();

				/* SEC_BO_FIELD_ACl */
				deleteStatus = session
						.createQuery("Delete from SecQuery sq where sq.id.queryId in(:listOfQueryIdFromFieldAcl)")
						.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();

				// delete query_d

				deleteStatus = session
						.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromBoFilter)")
						.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();
				logger.info("deleteTheDetailsBySecBoFilterTableForQueryD() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromBointFilter)")
						.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();
				logger.info("deleteTheDetailsBySecBointFilterTableForQueryD() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromFieldAcl)")
						.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();
				logger.info("deleteTheDetailsBySecBoFieldAclTableForQueryD() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryD qd Where qd.queryId in(:listOfQueryIdFromLevelAcl)")
						.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();
				logger.info("deleteTheDetailsBySecBointLevelAclTableForQueryD() :" + deleteStatus);

				logger.info("Delete Completed For QueryD Table");

				/*
				 * Delete child records before parent, parent table:QUERY_H, then Child
				 * table:QUERY_GROUP_D
				 */
				deleteStatus = session
						.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromBoFilter)")
						.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();

				/*
				 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
				 * deleting the query_id details from QUERY_GROUP_D table by the use of
				 * SEC_BOINT_FILTER table's query_id
				 */
				deleteStatus = session
						.createQuery(
								"Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromBointFilter)")
						.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();

				/*
				 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
				 * deleting the query_id details from QUERY_GROUP_D table by the use of
				 * SEC_BO_FIELD_ACL table's query_id
				 */
				deleteStatus = session
						.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromFieldAcl)")
						.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();

				/*
				 * This query is written By Manas & Jagadeesh On the date of 7th May Of 2020 for
				 * deleting the query_id details from QUERY_GROUP_D table by the use of
				 * SEC_BO_LEVEL_ACL table's query_id
				 */
				deleteStatus = session
						.createQuery("Delete From QueryGroupD qd Where qd.id.queryId IN (:listOfQueryIdFromLevelAcl)")
						.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();

				// delete query_h

				deleteStatus = session
						.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromBoFilter)")
						.setParameter("listOfQueryIdFromBoFilter", listOfQueryIdFromBoFilter).executeUpdate();
				logger.info("deleteTheDetailsBySecBoFilterTableForQueryH() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromBointFilter)")
						.setParameter("listOfQueryIdFromBointFilter", listOfQueryIdFromBointFilter).executeUpdate();
				logger.info("deleteTheDetailsBySecBointFilterTableForQueryH() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromFieldAcl)")
						.setParameter("listOfQueryIdFromFieldAcl", listOfQueryIdFromFieldAcl).executeUpdate();
				logger.info("deleteTheDetailsBySecBoFieldAclTableForQueryH() :" + deleteStatus);

				deleteStatus = session
						.createQuery("Delete From QueryH qd Where qd.queryId in(:listOfQueryIdFromLevelAcl)")
						.setParameter("listOfQueryIdFromLevelAcl", listOfQueryIdFromLevelAcl).executeUpdate();
				logger.info("deleteTheDetailsBySecBoLevelAclTableForQueryH() :" + deleteStatus);

				logger.info("Delete Completed For QueryH Table");

				// delete ALL custom process
				Integer processId = 10000;
				Integer typeId = 1;
				List<Integer> listOfProcessIdFromSource = sourceSecBoProcessDao
						.getByIdProcessIdGreaterThanEqual(processId);

				List<Integer> listOfProcessIdFromDestination = secBoProcessDao
						.getByIdProcessIdGreaterThanEqual(processId);

				List<Integer> listOfProcessId = new ArrayList<Integer>();

				listOfProcessId.addAll(listOfProcessIdFromSource);
				listOfProcessId.addAll(listOfProcessIdFromDestination);

				logger.info("List Of Process Id Got from Destination :" + listOfProcessId.size());

				deleteStatus = session.createQuery(
						"Delete from SecBoviewRes sb Where sb.id.resId In (:listOfProcessId) And sb.id.typeId =:typeId")
						.setParameter("listOfProcessId", listOfProcessId).setParameter("typeId", typeId)
						.executeUpdate();
				logger.info("deleteTheDataFromSecBoviewResTable() :" + deleteStatus);

				deleteStatus = session.createQuery(
						"Delete from SmuResShowAttr sb Where sb.id.smuResId In (:listOfProcessId) And sb.id.smuTypeId =:typeId")
						.setParameter("listOfProcessId", listOfProcessId).setParameter("typeId", typeId)
						.executeUpdate();
				logger.info("deleteTheDataFromSmuResShowAttrTable() :" + deleteStatus);

				/*
				 * same custom process may be attached to different roles - so this statement
				 * deletes the reference entry from dest database so that new entries can be
				 * inserted
				 */
				Integer minRoleId = 100000;
				deleteStatus = session
						.createQuery("Delete From SecRoleResCtl sr Where sr.id.roleId >=:minRoleId "
								+ "AND sr.id.roleId NOT In (:roleId) And sr.id.smuResId In (:listOfProcessId)")
						.setParameter("minRoleId", minRoleId).setParameter("roleId", roleId)
						.setParameter("listOfProcessId", listOfProcessId).executeUpdate();
				logger.info("deleteTheDataFromSecRoleResCtlTable() :" + deleteStatus);

				deleteStatus = session.createQuery("Delete From SecBoProcess sb Where sb.id.processId >=:processId")
						.setParameter("processId", processId).executeUpdate();
				logger.info("Delete Completed For Custome Process Table");

				// delete ALL custom menu
				Integer menuItemId = 5000000;
				deleteStatus = session.createQuery("Delete From SmuCommon sc Where sc.menuItemId >=:menuItemId")
						.setParameter("menuItemId", menuItemId).executeUpdate();
				logger.info("deleteTheDataFromSmuCommon() :" + deleteStatus);

				deleteStatus = session.createQuery(
						"Delete from SmuRoleRootMenu srm Where srm.id.menuItemId >=:menuItemId And srm.id.roleId >=:minRoleId")
						.setParameter("menuItemId", menuItemId).setParameter("minRoleId", minRoleId).executeUpdate();
				logger.info("deleteTheDataFromSmuRoleRootMenu() :" + deleteStatus);

				deleteStatus = session.createQuery(
						"Delete From SmuRoleMenuItem srm Where srm.id.menuItemId >=:menuItemId AND srm.id.roleId >=:minRoleId")
						.setParameter("menuItemId", menuItemId).setParameter("minRoleId", minRoleId).executeUpdate();
				logger.info("deleteTheDataFromSmuRoleMenuItem() :" + deleteStatus);

				/*
				 * update the sequence number to max(query_id) value - as validation query may
				 * have deleted during deleting security
				 */
				Integer updateStatus = session.createQuery(
						"Update SequenceNumbers seq set seq.currentnum =(SELECT MAX(qh.queryId) FROM QueryH qh) Where seq.sequenceName =:sequenceName")
						.setParameter("sequenceName", "QUERY_ID").executeUpdate();
				logger.info("Sequence Number Update Status :" + updateStatus);

				/**
				 * Delete other security tables data for a security
				 */

				for (Integer index = 0; index < delTables.length; index++) {
					String className = migrationDbUtil.getTheEntityClassName(delTables[index]);

					switch (className) {
					case "SecRoleProfile":
						deleteStatus = session.createQuery("Delete from SecRoleProfile sf Where sf.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "DashSectionH":
						deleteStatus = session.createQuery("Delete from DashSectionH dh Where dh.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecRoleResCtl":
						deleteStatus = session.createQuery("Delete from SecRoleResCtl srr Where srr.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SmuRoleRootMenu":
						deleteStatus = session
								.createQuery("Delete From SmuRoleRootMenu srm Where srm.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SmuRoleMenuItem":
						deleteStatus = session
								.createQuery("Delete From SmuRoleMenuItem srm Where srm.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "CollabFields":
						deleteStatus = session.createQuery("Delete from CollabField cf Where cf.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "RoleResAttr":
						deleteStatus = session.createQuery("Delete from RoleResAttr rra Where rra.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecQuery":
						deleteStatus = session.createQuery("Delete From SecQuery sq Where sq.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecQueryGroup":
						deleteStatus = session.createQuery("Delete from SecQueryGroup sqg where sqg.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecBoLevelAcl":
						deleteStatus = session.createQuery("Delete from SecBoLevelAcl sbl Where sbl.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecBoFieldAcl":
						deleteStatus = session.createQuery("Delete from SecBoFieldAcl sba Where sba.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecBoFilter":
						deleteStatus = session.createQuery("Delete from SecBoFilter sb Where sb.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					case "SecBointFilter":
						deleteStatus = session.createQuery("Delete from SecBointFilter sb Where sb.id.roleId =:roleId")
								.setParameter("roleId", roleId).executeUpdate();
						logger.info("Delete Sattus Of " + className + " :" + deleteStatus);
						break;

					default:
						logger.error(
								"This Class is Not Added in Swich Case Statemnt Of deleteSecurityDataFromTable() method of DestinationDbServiceImpl Class"
										+ className);
					}
				}

			});

			returnDeleteStatus = true;
			session.getTransaction().commit();
			session.close();
			entityManager.close();
			return returnDeleteStatus;

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			entityManager.close();
			logger.error(
					"Exception Occured in deleteSecuirtyData() method of MigrationDbUtil class :" + e.getMessage());
			returnDeleteStatus = false;
			return returnDeleteStatus;
		}

	}

	@Override
	public Boolean deleteLayoutDataForRestoringData(String[] delTables, List<MigratedLayoutIdDetails> listOfLayoutId) {

		boolean returnDeleteStatus = false;

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);

		session.beginTransaction();

		try {

			listOfLayoutId.forEach(migrationDetails -> {

				Integer layoutId = migrationDetails.getLayoutId();

				logger.info("Deleting the layout data for layout id " + layoutId
						+ " from destination database.Please wait.........");

				Integer deleteStatus = null;

				deleteStatus = session.createQuery(
						"delete FROM QueryD qh WHERE qh.queryId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='LOOKUPFILTER')")
						.setParameter("layoutId", layoutId).executeUpdate();
				logger.info("QueryD Delete Status :" + deleteStatus);

				deleteStatus = session.createQuery(
						"delete FROM QueryH qh WHERE qh.queryId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='LOOKUPFILTER')")
						.setParameter("layoutId", layoutId).executeUpdate();
				logger.info("QueryH Delete Status :" + deleteStatus);

				Integer updateStatus = session.createQuery(
						"Update SequenceNumbers seq set seq.currentnum =(SELECT MAX(qh.queryId) FROM QueryH qh) Where seq.sequenceName =:sequenceName")
						.setParameter("sequenceName", "QUERY_ID").executeUpdate();
				logger.info("Sequence Number Update Status :" + updateStatus);

				for (Integer index = 0; index < delTables.length; index++) {
					String className = migrationDbUtil.getTheEntityClassName(delTables[index]);

					switch (className) {
					case "DocView":
						deleteStatus = session.createQuery("Delete from DocView dv Where dv.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "DocViewElementD":
						deleteStatus = session
								.createQuery("Delete from DocViewElementD dve Where dve.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutDetail":
						deleteStatus = session
								.createQuery("Delete from LayoutDetail ld Where ld.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutElementD":
						deleteStatus = session
								.createQuery("Delete From LayoutElementD led Where led.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutI18nRes":
						deleteStatus = session.createQuery(
								"Delete From LayoutI18nRes li Where li.id.layoutId =:layoutId and language_id = '0'")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutLevelGroup":
						deleteStatus = session
								.createQuery("Delete from LayoutLevelGroup llg Where llg.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutPage":
						deleteStatus = session.createQuery("Delete from LayoutPage lp Where lp.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutProfile":
						deleteStatus = session.createQuery("Delete From LayoutProfile lp Where lp.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutProfileD":
						deleteStatus = session
								.createQuery("Delete from LayoutProfileD lpd where lpd.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutSection":
						deleteStatus = session
								.createQuery("Delete from LayoutSection ls Where ls.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					case "LayoutSectionD":
						deleteStatus = session
								.createQuery("Delete from LayoutSectionD lsd Where lsd.id.layoutId =:layoutId")
								.setParameter("layoutId", layoutId).executeUpdate();
						logger.info("Delete Satatus Of " + className + " :" + deleteStatus);
						break;

					default:
						logger.error(
								"This Class is Not Added in Swich Case Statemnt Of deleteLayoutData() method of DestinationDbServiceImpl Class"
										+ className);
					}
				}

			});

			session.getTransaction().commit();
			returnDeleteStatus = true;
			session.close();
			entityManager.close();
			return returnDeleteStatus;

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			entityManager.close();
			logger.error("Exception Occured in deleteLayoutData() method of MigrationDbUtil class :" + e.getMessage());
			returnDeleteStatus = false;
			return returnDeleteStatus;
		}

	}

	@Override
	public List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleIdOrLayoutId) {
		return secRoleProfileDao.getAllRoleIdExistInSecRoleProfileTable(listOfActiveRoleIdOrLayoutId);
	}

	@Override
	public List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveRoleIdOrLayoutId) {
		return layoutProfileDao.getAllLayoutIdExistInLayoutProfile(listOfActiveRoleIdOrLayoutId);
	}

	@Override
	public List<String> getTheListOfReportNamesWithExtension() {
		return adhocDocumentsDao.getTheListOfReportNamesWithExtension();
	}

	@Override
	public Boolean deleteTheReportFile(String destinationFileName) {

		Boolean deleteStatus = false;
		try {
			adhocDocumentsDao.deleteTheReportFile(destinationFileName);
			deleteStatus = true;
			return deleteStatus;
		} catch (Exception e) {
			logger.error("Exception Occured While Deleting Report/Custom File From Destination DB :" + e.getCause());
			return deleteStatus;
		}

	}

	@Override
	public List<String> getTheListOfAdhocDocumentsNames() {
		return adhocDocumentsDao.getTheListOfAdhocDocumentsNames();
	}

	@Override
	public List<String> getTheListOfRuleNames() {
		return configDDao.getTheListOfRuleNames();
	}

	@Override
	public boolean deleteTheRulesFile(String destinationFileName) {
		Boolean deleteStatus = false;
		try {
			configDDao.deleteTheRulesFile(destinationFileName);
			deleteStatus = true;
			return deleteStatus;
		} catch (Exception e) {
			logger.error("Exception Occured While Deleting Rules File From Destination DB :" + e.getCause());
			return deleteStatus;
		}
	}

	@Override
	public List<ConfigD> getTheAppConfigReport(Date migrationFromDate, Date migrationToDate) {

		List<ConfigD> listOfConfigD = configDDao.getTheAppConfigReport(migrationFromDate, migrationToDate);
		listOfConfigD.forEach(configD -> {
			configD.setBlobValue(null);
			configD.setClobValue(null);
		});
		return listOfConfigD;
	}

}
