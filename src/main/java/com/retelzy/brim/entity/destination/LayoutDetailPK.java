package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the DocViewPK database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutDetailPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "LAYOUT_ID")
	private Integer layoutId;

	@Column(name = "DOC_VIEW_ID")
	private Integer docViewId;

	@Column(name = "SECTION_ID")
	private Integer sectionId;

	@Column(name = "DOC_ID")
	private Integer docId;

	@Column(name = "DOC_LEVEL_ID")
	private Integer docLevelId;

	@Column(name = "DOC_FIELD_ID")
	private Integer docFieldId;

}
