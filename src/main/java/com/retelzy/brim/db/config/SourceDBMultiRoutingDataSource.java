package com.retelzy.brim.db.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.InvalidDbDetailsException;
import com.retelzy.brim.entity.user.ClientDbDetails;
import com.retelzy.brim.entity.user.service.UserService;
import com.retelzy.brim.util.GoogleGuavaCache;
import com.zaxxer.hikari.HikariDataSource;

public class SourceDBMultiRoutingDataSource extends AbstractRoutingDataSource {

	private final static Logger logger = LoggerFactory.getLogger(SourceDBMultiRoutingDataSource.class);

	@Autowired
	private GoogleGuavaCache googleCache;

	@Autowired
	private UserService userService;

	public static Integer sourceLookupCode = 0;

	private static Integer sourceCounter = 0;

	@Override
	protected Object determineCurrentLookupKey() {
		logger.info("Source DB Lookup is calling");

		if (sourceCounter == 0) {
			logger.info("Source DB Server");
			sourceCounter++;
			return SourceDBContextHolder.getCurrentDb();
		} else if (sourceCounter > 1) {

			String devEnvironment = googleCache.getToken("Source-DEV");
			String testEnvironment = googleCache.getToken("Source-TEST");
			String qaEnvironment = googleCache.getToken("Source-QA");
			String stagingEnvironment = googleCache.getToken("Source-STAGING");
			String prodEnvironment = googleCache.getToken("Source-PROD");

			/*
			 * logger.info("devEnvironment :" + devEnvironment);
			 * logger.info("testEnvironment :" + testEnvironment);
			 * logger.info("qaEnvironment :" + qaEnvironment);
			 * logger.info("stagingEnvironment :" + stagingEnvironment);
			 * logger.info("prodEnvironment :" + prodEnvironment);
			 */

			List<String> listOfEnvironmentType = new ArrayList<String>();
			listOfEnvironmentType.add(devEnvironment);
			listOfEnvironmentType.add(testEnvironment);
			listOfEnvironmentType.add(qaEnvironment);
			listOfEnvironmentType.add(stagingEnvironment);
			listOfEnvironmentType.add(prodEnvironment);

			logger.info("Source Env List :" + listOfEnvironmentType);

			logger.info("Is Migration Running :" + MigrationController.isMigrationRunning);

			if ((!devEnvironment.equals("") || !testEnvironment.equals("") || !qaEnvironment.equals("")
					|| !stagingEnvironment.equals("") || !prodEnvironment.equals("")) && sourceLookupCode == 0) {
				try {

					if (MigrationController.isMigrationRunning == true) {
						sourceLookupCode = 1;
					}

					logger.info("Inside DataSource Change method...");

					Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
							.getClientDbDetailsByEnvironmentType(listOfEnvironmentType);

					logger.info("Source environment type :" + isClientDbDetailsAvailable.get().getDbEnvironmentType());

					ClientDbDetails clientDbDetails = isClientDbDetailsAvailable.get();

					String serverPrefixURL = "";
					String jdbcURL = "";
					HikariDataSource hikariDataSource = new HikariDataSource();
					if (clientDbDetails.getDbType().equalsIgnoreCase("SQLSERVER")) {

						serverPrefixURL = "jdbc:sqlserver://";
						jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":"
								+ clientDbDetails.getPortNumber() + ";database=" + clientDbDetails.getDbName();
						hikariDataSource.setJdbcUrl(jdbcURL);

					} else if (clientDbDetails.getDbType().equalsIgnoreCase("ORACLE")) {

						serverPrefixURL = "jdbc:oracle:thin:@";
						jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":"
								+ clientDbDetails.getPortNumber() + ":xe";
						hikariDataSource.setJdbcUrl(jdbcURL);

					} else if (clientDbDetails.getDbType().equalsIgnoreCase("MYSQL")) {

						serverPrefixURL = "jdbc:mysql://";
						jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":"
								+ clientDbDetails.getPortNumber() + "/" + clientDbDetails.getDbName();
						hikariDataSource.setJdbcUrl(jdbcURL);

					} else if (clientDbDetails.getDbType().equalsIgnoreCase("DB2")) {

						serverPrefixURL = "jdbc:db2://";
						jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":"
								+ clientDbDetails.getPortNumber() + "/" + clientDbDetails.getDbName();
						hikariDataSource.setJdbcUrl(jdbcURL);

					}

					hikariDataSource.setDriverClassName(isClientDbDetailsAvailable.get().getDbDriverClassName());
					hikariDataSource.setUsername(isClientDbDetailsAvailable.get().getDbUsername());
					hikariDataSource.setPassword(isClientDbDetailsAvailable.get().getDbPassword());
					hikariDataSource.setIdleTimeout(300000);
					hikariDataSource.setConnectionTimeout(20000);
					hikariDataSource.setAutoCommit(false);
					hikariDataSource.setMaximumPoolSize(50);
					hikariDataSource.setMaxLifetime(1200000);
					hikariDataSource.setMinimumIdle(10);
					hikariDataSource.setPoolName("Source");
					initDatasource(hikariDataSource);
					return SourceDBContextHolder.getCurrentDb();

				} catch (Exception e) {
					e.printStackTrace();
					googleCache.clearToken("Source-DEV");
					googleCache.clearToken("Source-TEST");
					googleCache.clearToken("Source-QA");
					googleCache.clearToken("Source-STAGING");
					googleCache.clearToken("Source-PROD");
					logger.error("Exception Occured In SourceDBMultiRoutingDataSource class :" + e.getMessage());
					throw new InvalidDbDetailsException("Inavlid DB details");
				}
			} else {
				// logger.info("Default Source DB Configuration Calling....");
				return SourceDBContextHolder.getCurrentDb();
			}

		} else {
			sourceCounter++;
			return SourceDBContextHolder.getCurrentDb();
		}

	}

	public void initDatasource(DataSource dataSource) {
		Map<Object, Object> targetDataSources = new HashMap<>();
		targetDataSources.put(DBTypeEnum.SOURCE, dataSource);
		this.setDefaultTargetDataSource(dataSource);
		this.setTargetDataSources(targetDataSources);
		this.afterPropertiesSet();

	}

}
