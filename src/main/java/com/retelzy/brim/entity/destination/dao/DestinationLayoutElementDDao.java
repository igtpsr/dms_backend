package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutElementD;
import com.retelzy.brim.entity.destination.LayoutElementDPK;

@Repository
public interface DestinationLayoutElementDDao extends CrudRepository<LayoutElementD, LayoutElementDPK> {


}
