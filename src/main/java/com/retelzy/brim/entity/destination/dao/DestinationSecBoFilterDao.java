package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBoFilter;
import com.retelzy.brim.entity.destination.SecBoFilterPK;

@Repository
public interface DestinationSecBoFilterDao extends CrudRepository<SecBoFilter, SecBoFilterPK> {

	@Query("Select bo from SecBoFilter bo where bo.id.roleId =:roleId")
	List<Object> getTheDataFromSecBoFilterTable(Integer roleId);

	@Query("Select bo.id.queryId from SecBoFilter bo where bo.id.roleId =:roleId")
	List<Integer> getTheQueryIdByRoleId(Integer roleId);

}
