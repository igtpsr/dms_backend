package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the ROLE_RES_ATTR database table.
 * 
 */
@Entity
@Table(name="ROLE_RES_ATTR")
@DynamicUpdate
public class RoleResAttr implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RoleResAttrPK id;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER")
	private String modifyUser;

	@Column(name="PROP_VALUE")
	private String propValue;

	public RoleResAttr() {
	}

	public RoleResAttrPK getId() {
		return this.id;
	}

	public void setId(RoleResAttrPK id) {
		this.id = id;
	}

	public Timestamp getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getPropValue() {
		return this.propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}

}