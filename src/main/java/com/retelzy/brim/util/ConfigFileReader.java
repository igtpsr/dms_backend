package com.retelzy.brim.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

@Component
public class ConfigFileReader {

	private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	private final static Logger logger = LoggerFactory.getLogger(ConfigFileReader.class);

	@Autowired
	private Environment environment;

	public String getInsertLayoutTableSequence() {
		return environment.getProperty("insert.layouttable.sequence");
	}

	public String getDeleteLayoutTableSHelperequenc() {
		return environment.getProperty("delete.layouttable.sequence");
	}

	public String getInsertSecurityTableSequence() {
		return environment.getProperty("insert.securitytable.sequence");
	}

	public String getDeleteSecurityTableSequenc() {
		return environment.getProperty("delete.securitytable.sequence");
	}

	public String getInsertNewValidationTableSequence() {
		return environment.getProperty("insert.newvalidationtable.sequence");
	}

	public String getPropertyValue(String name) {
		return environment.getProperty(name);
	}

	/**
	 * Get the DOM Document object from XML string
	 *
	 * @param boXML
	 * @return
	 * @throws org.xml.sax.SAXException
	 * @throws javax.xml.parsers.ParserConfigurationException
	 *
	 * @throws java.io.IOException
	 */
	public static Document getXMLDocument(String boXML) throws SAXException, ParserConfigurationException, IOException {
		InputStream is = new ByteArrayInputStream(revertEncoding(boXML));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		dbf.setNamespaceAware(true);
		dbf.setValidating(true);
		try {
			dbf.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);

		} catch (IllegalArgumentException x) {
			// This can happen if the parser does not support JAXP 1.2
		}
		dbf.setIgnoringComments(true);
		dbf.setIgnoringElementContentWhitespace(true);
		Document doc = docBuilder.parse(is);
		doc.getDocumentElement().normalize();
		return doc;
	}

	/**
	 * Check if the encoding system use is different with the XML file used. If
	 * different, return the byte array decoded as the XML file designated encoding,
	 * otherwise, use default byte array.
	 *
	 * @param xmlString
	 *
	 * @return
	 */
	private static byte[] revertEncoding(String xmlString) {
		// Check if the encoding system use is different with the XML
		// content used.
		InputStreamReader isr = new InputStreamReader(System.in);
		String dftEncoding = isr.getEncoding();

		// Obtain the encoding used by the XML string
		int pos = xmlString.indexOf("encoding=");
		if (pos < 0) {
			return xmlString.getBytes();
		}

		int posStart = xmlString.indexOf("\"", pos);
		if (posStart < 0) {
			return xmlString.getBytes();
		}

		int posEnd = xmlString.indexOf("\"", posStart + 1);
		if (posEnd < 0) {
			return xmlString.getBytes();
		}

		String xmlEncoding = xmlString.substring(posStart + 1, posEnd);

		if (dftEncoding.equals(xmlEncoding)) {
			return xmlString.getBytes();
		}

		// revert encoding to the XML content specified.
		try {

			return xmlString.getBytes(xmlEncoding);
		} catch (UnsupportedEncodingException ueex) {
			return xmlString.getBytes();
		}
	}

	/**
	 * Convert document value to string.
	 *
	 * @param element
	 * @return document string value.
	 */
	public static String documentToString(Element element) {
		try {
			Source source = new DOMSource(element);
			StringWriter stringWriter = new StringWriter();
			Result result = new StreamResult(stringWriter);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, result);
			return stringWriter.getBuffer().toString();
		} catch (Exception e) {
			// Unable to convert document object to string format
			e.printStackTrace();
			logger.error("Exception Occured In documentToString() method :" + e.getMessage());

		}
		return "";
	}

	public static String mapKeyToString(Map<Integer, String> map, String delimiter) {
		StringBuffer sb = new StringBuffer();

		if (delimiter == null)
			delimiter = ",";

		Iterator<Integer> i = map.keySet().iterator();

		int nIndex = 0;
		int nMaxSize = map.size() - 1;
		while (i.hasNext()) {
			Object element = i.next();
			if (element != null) {
				if (nIndex++ < nMaxSize) {
					sb.append(element.toString() + delimiter);
				} else {
					sb.append(element.toString());
				}
			}
		}

		return sb.toString();
	}

	public static String numListToString(Collection<String> col, String delimiter) {
		StringBuffer sb = new StringBuffer();

		if (delimiter == null)
			delimiter = ",";

		Iterator<String> i = col.iterator();

		int nIndex = 0;
		int nMaxSize = col.size() - 1;
		while (i.hasNext()) {
			Object element = i.next();
			if (element != null) {
				if (nIndex++ < nMaxSize) {
					sb.append(element.toString() + delimiter);
				} else {
					sb.append(element.toString());
				}
			}
		}

		return sb.toString();
	}

	/**
	 * Convert the comma seprated tables string to String array
	 *
	 * @param tableList
	 * @return
	 */
	public static String[] toArray(String tableList) {
		int count = 0;
		StringTokenizer szTemp = new StringTokenizer(tableList, ",");
		String[] tables = new String[szTemp.countTokens()];
		while (szTemp.hasMoreTokens()) {
			tables[count] = szTemp.nextToken().trim();
			count++;
		}
		return tables;
	}

	/**
	 * Replaces all the occurences of a substring found within a given string by a
	 * replace string.
	 *
	 * @param szSource  the string to replace
	 * @param szFind    the substring to find and replace
	 * @param szReplace the replace string
	 * @return String - the original String where all the occurences of find string
	 *         are replaced by replace string.
	 */
	public static String replaceAll(String szSource, String szFind, String szReplace) {
		if (szSource == null || szSource.length() == 0) {
			return szSource;
		}
		StringBuffer sb = new StringBuffer(szSource);
		int bufidx = sb.length() - 1;
		int offset = szFind.length();
		while (bufidx > -1) {
			int findidx = offset - 1;
			while (findidx > -1) {
				if (bufidx == -1) {
					// Done
					return sb.toString();
				}
				if (sb.charAt(bufidx) == szFind.charAt(findidx)) {
					findidx--; // Look for next char
					bufidx--;
				} else {
					findidx = offset - 1; // Start looking again
					bufidx--;
					if (bufidx == -1) {
						// Done
						return sb.toString();
					}
					continue;
				}
			}

			// Found
			sb.replace(bufidx + 1, bufidx + 1 + offset, szReplace);
			// start looking again
		}
		// No more matches
		return sb.toString();
	}

}
