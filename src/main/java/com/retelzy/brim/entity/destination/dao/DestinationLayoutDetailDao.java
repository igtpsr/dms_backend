package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutDetail;
import com.retelzy.brim.entity.destination.LayoutDetailPK;

@Repository
public interface DestinationLayoutDetailDao extends CrudRepository<LayoutDetail, LayoutDetailPK> {


}
