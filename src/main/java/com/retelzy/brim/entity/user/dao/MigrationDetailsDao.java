package com.retelzy.brim.entity.user.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.user.MigrationDetails;

@Repository
public interface MigrationDetailsDao extends CrudRepository<MigrationDetails, Integer> {

	// @Query("select top 1 md from MigrationDetails md where
	// migrationModuleName='LAYOUT' order by createTs asc")
	@Query("Select mg from MigrationDetails mg where "
			+ "mg.migrationDetailsId=(Select MAX(mgr.migrationDetailsId) from MigrationDetails mgr)")
	Optional<MigrationDetails> getTheLastMigratedTimePeriod();

	@Query("Select mg from MigrationDetails mg where "
			+ "mg.migrationDetailsId=(Select MAX(mgr.migrationDetailsId) from MigrationDetails mgr where mgr.migrationModuleName=:moduleName)")
	Optional<MigrationDetails> getTheLatestMigrationDetailsByModuleName(String moduleName);

}
