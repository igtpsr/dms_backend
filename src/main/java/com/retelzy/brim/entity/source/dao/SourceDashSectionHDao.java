package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.DashSectionH;
import com.retelzy.brim.entity.source.DashSectionHPK;

public interface SourceDashSectionHDao extends CrudRepository<DashSectionH, DashSectionHPK> {

	public List<Object> findByIdRoleId(Integer roleId);
}
