package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecRoleResCtl;
import com.retelzy.brim.entity.source.SecRoleResCtlPK;

@Repository
public interface SourceSecRoleResCtlDao extends CrudRepository<SecRoleResCtl, SecRoleResCtlPK> {

	public List<Object> findByIdRoleId(Integer roleId);

	@Query("Select src From SecRoleResCtl src Where src.id.roleId >=:minRoleId AND src.id.roleId NOT IN (:roleId) "
			+ "AND src.id.roleId IN(:listOfRoleId) AND src.id.smuResId in(:listOfProcessId)")
	public List<Object> getAllTheSecRoleResCtlDetails(Integer minRoleId, Integer roleId, List<Integer> listOfProcessId,
			List<Integer> listOfRoleId);
}
