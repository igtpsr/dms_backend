package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the DocViewPK database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class DocViewPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "LAYOUT_ID", insertable = false, updatable = false, unique = true, nullable = false)
	private Integer layoutId;

	@Column(name = "DOC_VIEW_ID", unique = true, nullable = false)
	private Integer docViewId;

	@Column(name = "DOC_ID", insertable = false, updatable = false, unique = true, nullable = false)
	private Integer docId;

	
}
