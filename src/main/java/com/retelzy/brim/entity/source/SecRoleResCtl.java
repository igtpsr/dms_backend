package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the SEC_ROLE_RES_CTL database table.
 * 
 */
@Entity
@Table(name = "SEC_ROLE_RES_CTL")
@DynamicUpdate
public class SecRoleResCtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecRoleResCtlPK id;

	@Column(name = "ENABLED")
	private Integer enabled;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	// bi-directional many-to-one association to SecRoleProfile
	@MapsId("ROLE_ID")
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private SecRoleProfile secRoleProfile;

	public SecRoleResCtl() {
	}

	public SecRoleResCtlPK getId() {
		return id;
	}

	public void setId(SecRoleResCtlPK id) {
		this.id = id;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public SecRoleProfile getSecRoleProfile() {
		return secRoleProfile;
	}

	public void setSecRoleProfile(SecRoleProfile secRoleProfile) {
		this.secRoleProfile = secRoleProfile;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}