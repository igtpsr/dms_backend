package com.retelzy.brim.migration.model;

import java.util.List;

import com.retelzy.brim.entity.destination.ConfigD;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class DashboardReports {

	private List<LayoutReport> listOflayoutReportDetails;
	private List<ConfigD> listOfAppConfigDetails;

}
