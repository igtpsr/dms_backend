package com.retelzy.brim.migrationModules;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.util.MigrationBackupUtil;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.Helper;

@Component
public class MigrationBackupForSecurityModule {

	private final static Logger logger = LoggerFactory.getLogger(MigrationBackupForSecurityModule.class);

	@Autowired
	private ConfigFileReader configFileReader;

	@Autowired
	private MigrationBackupUtil migrationBackupUtil;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private Environment environment;

	public Boolean getBackupSecurityInsertSql(List<Integer> listOfRoleId, PrintWriter pw, File file)
			throws IOException {

		logger.info("Backup Started for Security");

		Connection connection = null;
		Statement statement1 = null;
		Statement statement2 = null;

		try {
			connection = destinationDbService.getTheDestinationDbConnection();
			String[] securityTableInsertSeq = ConfigFileReader
					.toArray(configFileReader.getInsertSecurityTableSequence());

			String dbType = connection.getMetaData().getDatabaseProductName();

			for (Integer numberOfRoleId = 0; numberOfRoleId < listOfRoleId.size(); numberOfRoleId++) {

				for (Integer index = 0; index < securityTableInsertSeq.length; index++) {

					String table = securityTableInsertSeq[index];
					String where = "role_Id= " + listOfRoleId.get(numberOfRoleId);
					migrationBackupUtil.writeQuriesInBackupFile(where, pw, connection, statement1, statement2, dbType,
							table, 0, null, null, file);

				}

			}

			backupSecurityFilterQuery(connection, listOfRoleId);
			backupAllCustomProcess(connection, pw, listOfRoleId, file, dbType, statement1, statement2);
			backupAllCustomMenu(connection, pw, listOfRoleId, file, dbType, statement1, statement2);

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			Helper.clearTheBackupFileNames();
			System.gc();
			logger.error("Exception Occured while Taking Backup Of Security Module :" + e.getMessage());
			return false;
		} finally {
			pw.close();
			try {
				if (statement1 != null)
					statement1.close();
				if (statement2 != null)
					statement2.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * This method backup the security filer queries from destination database
	 *
	 * @param destCon
	 * @param
	 * @throws Exception
	 */
	private void backupSecurityFilterQuery(Connection destCon, List<Integer> listOfRoleId) throws Exception {

		logger.info("Backup Security Filter Part Called");

		Map<Integer, String> sourceQueryIdMap = new TreeMap<Integer, String>();

		String date = Helper.getDateTime();
		String queryHfileName = "";
		String queryDfileName = "";

		if (listOfRoleId.size() > 1) {
			queryHfileName = "query_h_backup_sec_" + "MULTIPLE_" + date + ".xml";
			MigrationController.queryHfileName = queryHfileName;

			queryDfileName = "query_d_backup_sec_" + "MULTIPLE_" + date + ".xml";
			MigrationController.queryDfileName = queryDfileName;
		} else if (listOfRoleId.size() == 1) {
			queryHfileName = "query_h_backup_sec_" + listOfRoleId.get(0) + "_" + date + ".xml";
			MigrationController.queryHfileName = queryHfileName;

			queryDfileName = "query_d_backup_sec_" + listOfRoleId.get(0) + "_" + date + ".xml";
			MigrationController.queryDfileName = queryDfileName;
		}

		for (Integer roleIndex = 0; roleIndex < listOfRoleId.size(); roleIndex++) {

			sourceQueryIdMap = getSecQueryIdMap(destCon, sourceQueryIdMap, "SEC_BO_FILTER",
					listOfRoleId.get(roleIndex));
			sourceQueryIdMap = getSecQueryIdMap(destCon, sourceQueryIdMap, "SEC_BOINT_FILTER",
					listOfRoleId.get(roleIndex));
			sourceQueryIdMap = getSecQueryIdMap(destCon, sourceQueryIdMap, "SEC_BO_FIELD_ACL",
					listOfRoleId.get(roleIndex));
			sourceQueryIdMap = getSecQueryIdMap(destCon, sourceQueryIdMap, "SEC_BO_LEVEL_ACL",
					listOfRoleId.get(roleIndex));

			String szIdList = ConfigFileReader.mapKeyToString(sourceQueryIdMap, ",");
			logger.info("szIdList  size-->" + szIdList);
			if (!szIdList.equals("") && szIdList != null) {
				String migrateQueriesSQL = "select * from query_h where query_id in(" + szIdList + ")";

				logger.info(
						"Exporting the backup security data for security role from destination database.Please wait........");

				IDatabaseConnection connection = destinationDbService.getConnection();

				try {

					QueryDataSet partialDataSet = new QueryDataSet(connection);
					partialDataSet.addTable("query_h", migrateQueriesSQL);
					FlatXmlDataSet.write(partialDataSet,
							new FileOutputStream(environment.getProperty("dir.path.backup") + queryHfileName));

					QueryDataSet partialDataSet2 = new QueryDataSet(connection);
					partialDataSet2.addTable("query_d", "select * from query_d where query_id in(" + szIdList + ")");
					FlatXmlDataSet.write(partialDataSet2,
							new FileOutputStream(environment.getProperty("dir.path.backup") + queryDfileName));
					logger.info("Completed Backup for QueryH and QueryD of Security..");
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Exception Occured while Taking Backup Of QueryH And QueryD Table :" + e.getMessage());
					Helper.clearTheBackupFileNames();
					throw new MigrationException("Backup Failed");
				} finally {
					connection.close();
				}
			}
		}
	}

	/**
	 * This method backup ALL the custom process from the destination database
	 *
	 * @param destCon
	 * @param pw
	 * @param securityInsertSql
	 * @param roleId
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private void backupAllCustomProcess(Connection destCon, PrintWriter pw, List<Integer> listOfRoleId, File f,
			String dbType, Statement statement1, Statement statement2) throws SQLException, IOException {

		// get the Insert SQL statements export all custom process
		// All process above 10000 are custom process so we export all custom process
		migrationBackupUtil.writeQuriesInBackupFile("PROCESS_ID >=10000", pw, destCon, statement1, statement2, dbType,
				"SEC_BO_PROCESS", 0, null, null, f);

		// SEC_BOVIEW_RES table contains dov_view_id and res_id mapping - copy all
		// entries for custom process
		migrationBackupUtil.writeQuriesInBackupFile(
				"RES_ID IN (SELECT process_id FROM SEC_BO_PROCESS WHERE PROCESS_ID >=10000) AND TYPE_ID=1", pw, destCon,
				statement1, statement2, dbType, "SEC_BOVIEW_RES", 0, null, null, f);

		// SMU_RES_SHOW_ATTR contains attributes (URL , POSITION) for custom process
		migrationBackupUtil.writeQuriesInBackupFile(
				"SMU_RES_ID IN (SELECT process_id FROM SEC_BO_PROCESS WHERE PROCESS_ID >=10000) AND SMU_TYPE_ID=1", pw,
				destCon, statement1, statement2, dbType, "SMU_RES_SHOW_ATTR", 0, null, null, f);

		// By default new custom process is disabled for all roles and user has ability
		// to enable and diable custom process for role- These Inserts cover that
		// senario

		for (Integer roleIndex = 0; roleIndex < listOfRoleId.size(); roleIndex++) {

			// todo - recheck if this condition needed for backup data
			String szRoleList = getDestRoleIdList(destCon, listOfRoleId.get(roleIndex));

			migrationBackupUtil.writeQuriesInBackupFile(
					"role_id >= 100000 AND role_id NOT IN (" + listOfRoleId.get(roleIndex) + ") and role_id in ("
							+ szRoleList
							+ ") AND smu_res_id IN (SELECT process_id  FROM SEC_BO_PROCESS WHERE PROCESS_ID >=10000)",
					pw, destCon, statement1, statement2, dbType, "SEC_ROLE_RES_CTL", 0, null, null, f);
		}
	}

	/**
	 * This method backup ALL the custom menus from the destination database
	 *
	 * @param destCon
	 * @param pw
	 * @param securityInsertSql
	 * @param roleId
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private void backupAllCustomMenu(Connection destCon, PrintWriter pw, List<Integer> listOfRoleId, File f,
			String dbType, Statement statement1, Statement statement2) throws SQLException, IOException {
		// get the Insert SQL statements export all custom menu
		// All menu above 5000000 are custom menu so we export all custom menu

		migrationBackupUtil.writeQuriesInBackupFile("MENU_ITEM_ID >=5000000", pw, destCon, statement1, statement2,
				dbType, "SMU_COMMON", 0, null, null, f);
		// SMU_ROLE_ROOT_MENU table contains role attached to menu

		for (Integer roleIndex = 0; roleIndex < listOfRoleId.size(); roleIndex++) {

			// todo - recheck if this condition needed for backup data
			String szRoleList = getDestRoleIdList(destCon, listOfRoleId.get(roleIndex));

			migrationBackupUtil.writeQuriesInBackupFile(
					"MENU_ITEM_ID >=5000000 AND ROLE_ID >= 100000 AND ROLE_ID NOT IN (" + listOfRoleId.get(roleIndex)
							+ ") AND ROLE_ID IN (" + szRoleList + ")",
					pw, destCon, statement1, statement2, dbType, "SMU_ROLE_ROOT_MENU", 0, null, null, f);

			// SMU_ROLE_MENU_ITEM contains access right for menu

			migrationBackupUtil.writeQuriesInBackupFile(
					"MENU_ITEM_ID >=5000000 AND ROLE_ID >= 100000 AND ROLE_ID NOT IN (" + listOfRoleId.get(roleIndex)
							+ ")  AND ROLE_ID IN (" + szRoleList + ")",
					pw, destCon, statement1, statement2, dbType, "SMU_ROLE_MENU_ITEM", 0, null, null, f);
		}

	}

	private Map<Integer, String> getSecQueryIdMap(Connection sourceCon, Map<Integer, String> sourceQueryIdMap,
			String table, Integer roleId) throws SQLException {
		Statement stmt1 = null;
		String querySQL = "SELECT query_id , query_name FROM QUERY_H WHERE query_id IN (SELECT query_id FROM " + table
				+ " WHERE role_id =" + roleId + ")";
		try {
			stmt1 = sourceCon.createStatement();
			ResultSet rsSource = stmt1.executeQuery(querySQL);
			while (rsSource.next()) {
				Integer qKey = Integer.parseInt(rsSource.getString(1));
				String qVal = rsSource.getString(2);
				if (!sourceQueryIdMap.containsKey(qKey)) {
					sourceQueryIdMap.put(qKey, qVal);
				}
			}
			rsSource.close();
			stmt1.close();
			stmt1 = null;
		} finally {
			try {
				if (stmt1 != null)
					stmt1.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return sourceQueryIdMap;
	}

	private String getDestRoleIdList(Connection destCon, Integer roleId) throws SQLException {
		List<String> list = new ArrayList<String>();
		String querySQL = "SELECT role_id  FROM sec_role_profile WHERE role_id >= " + roleId;
		Statement stmt = destCon.createStatement();
		ResultSet rsSource = stmt.executeQuery(querySQL);
		while (rsSource.next()) {
			list.add(rsSource.getString(1));

		}
		rsSource.close();
		String sz = ConfigFileReader.numListToString(list, ",");
		if (sz.equals("")) {
			sz = "-1";
		}
		stmt.close();
		stmt = null;
		return sz;
	}

}
