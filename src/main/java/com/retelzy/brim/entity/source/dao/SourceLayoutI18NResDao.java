package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutI18nRes;
import com.retelzy.brim.entity.source.LayoutI18nResPK;

@Repository
public interface SourceLayoutI18NResDao extends CrudRepository<LayoutI18nRes, LayoutI18nResPK> {

	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqualAndIdLanguageId(Integer layoutId,
			Timestamp fromDate, Timestamp toDate, Integer i);


}
