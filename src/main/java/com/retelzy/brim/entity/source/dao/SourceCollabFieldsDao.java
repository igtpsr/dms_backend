package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.CollabFieldPK;
import com.retelzy.brim.entity.source.CollabFields;

public interface SourceCollabFieldsDao extends CrudRepository<CollabFields, CollabFieldPK> {

	public List<Object> findByIdRoleId(Integer roleId);
}
