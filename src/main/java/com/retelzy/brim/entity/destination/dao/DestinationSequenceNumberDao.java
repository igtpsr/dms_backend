package com.retelzy.brim.entity.destination.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SequenceNumbers;

@Repository
public interface DestinationSequenceNumberDao extends CrudRepository<SequenceNumbers, Integer> {

	@Modifying
	@Transactional
	@Query("Update SequenceNumbers seq set seq.currentnum =:nextQueryNumebr Where seq.sequenceName =:sequenceName")
	public Integer updateTheSequnceNumberBySequenceName(Integer nextQueryNumebr, String sequenceName);

}
