package com.retelzy.brim.entity.user.dao;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.retelzy.brim.migration.model.LayoutIdDetails;
import com.retelzy.brim.migration.model.MigrationId;

@Service
public class LayoutMigrationStoredProcDao {
	
private static final Logger log = LoggerFactory.getLogger(LayoutMigrationStoredProcDao.class);

	
	@PersistenceUnit(unitName = "User")
	private EntityManagerFactory emf;
	
	
	public Map<String, Object> MigrateLayoutStoredProc(MigrationId migrationId, String sourceEnvType,String destinationEnvType) throws Exception {
		try {
			return this.callStoredProcedureToMigrateLayout(migrationId,sourceEnvType,destinationEnvType);
		} catch (Exception e) {
			log.error(" Error while running migration Layout stored procedure --> " + e.getMessage());
			throw e;
		}
	}
	
	
	private Map<String, Object> callStoredProcedureToMigrateLayout(MigrationId migrationId, String sourceEnvType, String destinationEnvType) throws Exception {
		log.info("Stored procedure migration of --> \n " + migrationId.getModuleName());
		
		if (migrationId.getModuleName().equalsIgnoreCase("LAYOUT")) {
			
			//--------------------------------------------------------
			//Converting list of values to Comma separated String of values
			StringBuilder strbul=new StringBuilder();
	        for(LayoutIdDetails str : migrationId.getListOfLayoutId())
	        {
	            strbul.append(str.getLayoutId());
	            strbul.append(",");
	        }
	        strbul.setLength(strbul.length()-1);
	        String layoutIDs = strbul.toString();
	        System.out.print("Layout IDs  : " + layoutIDs);
			//--------------------------------------------------------
			
			EntityManager em = emf.createEntityManager();
			try {
				Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
				
				String storedProcName = "";
				if (sourceEnvType.equalsIgnoreCase("DEV") && destinationEnvType.equalsIgnoreCase("TEST")) {
					storedProcName = "RIM_MIGRATION_FROM_DEV_TO_TEST_LAYOUT";
				} else if (sourceEnvType.equalsIgnoreCase("TEST") && destinationEnvType.equalsIgnoreCase("DEV")) {
					storedProcName = "RIM_MIGRATION_FROM_TEST_TO_DEV_LAYOUT";
				}
				
				StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcName)
						.registerStoredProcedureParameter("layoutIdList", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("startdate", Date.class, ParameterMode.IN)
						.registerStoredProcedureParameter("enddate", Date.class, ParameterMode.IN)
						.registerStoredProcedureParameter("totalCount", Integer.class, ParameterMode.OUT)
						.registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
						.setParameter("layoutIdList", layoutIDs).setParameter("startdate", migrationId.getFromDate())
						.setParameter("enddate", migrationId.getToDate());

				query.execute();
				String result = (String) query.getOutputParameterValue("result");
				Integer totalCount = (Integer) query.getOutputParameterValue("totalCount");
				
				log.info("  Result  : " + result);
				log.info("  Total count  : " + totalCount);
				
				migrationStatusMap.put("Status", result);
				migrationStatusMap.put("Count", totalCount);
				return migrationStatusMap;
			} catch (Exception e) {
				log.error(String.format(" Layout data has not been updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
		return null;
	}
}
