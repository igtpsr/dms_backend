package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBoLevelAcl;
import com.retelzy.brim.entity.source.SecRoleProfile;

@Repository
public interface SourceSecBoLevelAclDao extends CrudRepository<SecBoLevelAcl, Integer> {

	public List<Object> findBySecRoleProfile(SecRoleProfile secRoleProfile);

	@Query("Select sb.id.queryId from SecBoLevelAcl sb where sb.secRoleProfile =:secRoleProfile")
	public List<Integer> getAllTheQueryIdsByRoleId(SecRoleProfile secRoleProfile);
}
