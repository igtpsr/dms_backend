package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The persistent class for the LAYOUT_PROFILE database table.
 * 
 */
@Entity
@Table(name = "LAYOUT_PROFILE")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "LAYOUT_ID")
	private Integer layoutId;

	@Column(name = "LAYOUT_DESC", nullable = false, length = 35)
	private String layoutDesc;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER", length = 18)
	private String modifyUser;

}