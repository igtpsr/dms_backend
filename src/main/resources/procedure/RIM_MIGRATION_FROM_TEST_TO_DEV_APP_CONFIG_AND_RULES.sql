USE [DMS]
GO

IF OBJECT_ID ( 'RIM_MIGRATION_FROM_TEST_TO_DEV_APP_CONFIG_AND_RULES', 'P' ) IS NOT NULL   
    DROP PROCEDURE RIM_MIGRATION_FROM_TEST_TO_DEV_APP_CONFIG_AND_RULES;  
GO

CREATE PROCEDURE [dbo].[RIM_MIGRATION_FROM_TEST_TO_DEV_APP_CONFIG_AND_RULES]
	  @moduletype			VARCHAR(50) = ''
	, @configid				VARCHAR(50) = ''
	, @propname				VARCHAR(100) = ''
	, @propvalues			VARCHAR(100) = ''
	, @startdate			datetime
	, @enddate				datetime
	, @totalCount 			INT OUTPUT
	, @result				VARCHAR(500) OUTPUT

AS 
BEGIN

	  SET NOCOUNT ON;
	  DECLARE @rowcount					INT;
	  DECLARE @CONFIG_ID               VARCHAR(10);
      DECLARE @CONFIG_TYPE             VARCHAR(18);
      DECLARE @PROP_NAME               VARCHAR(80);
      DECLARE @PROP_VALUE              VARCHAR(1000);
      DECLARE @DATATYPE                VARCHAR(10);
      DECLARE @FACTORY_NAME            VARCHAR(50);
      DECLARE @MAXLENGTH               INT;
      DECLARE @ENCRYPTED               VARCHAR(5);
      DECLARE @DESCRIPTION             VARCHAR(500);
      DECLARE @COMMENTS                VARCHAR(2000);
      DECLARE @DOC_ID_LIST             VARCHAR(500);
      DECLARE @CLOB_VALUE               NVARCHAR(max);
      DECLARE @BLOB_VALUE               VARBINARY(max);
      DECLARE @FUNC_URL1               VARCHAR(255);
      DECLARE @FUNC_URL2               VARCHAR(255);
      DECLARE @USE_CLOB                INT;
      DECLARE @CATEGORY_ID             VARCHAR(80);
      DECLARE @LEVEL_ID                INT;
      DECLARE @POSITION                INT;
      DECLARE @MODIFY_TS               datetime;
      DECLARE @MODIFY_USER             VARCHAR(18);
	  
-- Spliting list of @propvalues's
	-- dbo.SplitString function getting call
	SELECT * INTO #propvalueslist 
	FROM dbo.SplitString(@propvalues, ',')


	IF @moduletype = 'APP CONFIG'
	BEGIN
		PRINT 'CR_CONFIG_D_APP_CONFIG_C1';
		IF @propname = 'clientspec'
			BEGIN
				PRINT 'CR_CONFIG_D_APP_CONFIG_C1 CASE 1';
				--Cursor for CR_CONFIG_D_APP_CONFIG_C1 Table -Begin For APP_CONFIG CASE 1
				IF OBJECT_ID ( 'CR_CONFIG_D_APP_CONFIG_C1', 'C' ) IS NOT NULL   
					DEALLOCATE CR_CONFIG_D_APP_CONFIG_C1;
				
				DECLARE CR_CONFIG_D_APP_CONFIG_C1 CURSOR FOR 
					SELECT * FROM [DMS_TEMP].[dbo].[CONFIG_D]
							WHERE CONFIG_ID 	= @configid 
								AND PROP_NAME 	= @propname
				EXCEPT
					SELECT * FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
							WHERE CONFIG_ID 	= @configid 
								AND PROP_NAME 	= @propname
								
			END
		ELSE
			BEGIN
			PRINT 'CR_CONFIG_D_APP_CONFIG_C1 CASE 2';
				--Cursor for CR_CONFIG_D_APP_CONFIG_C2 Table -Begin For APP_CONFIG CASE 2
				IF OBJECT_ID ( 'CR_CONFIG_D_APP_CONFIG_C2', 'C' ) IS NOT NULL   
					DEALLOCATE CR_CONFIG_D_APP_CONFIG_C2;
				
				DECLARE CR_CONFIG_D_APP_CONFIG_C2 CURSOR FOR 
					SELECT * FROM [DMS_TEMP].[dbo].[CONFIG_D]
							WHERE 	MODIFY_TS 
									BETWEEN 	@startdate 
									AND 		@enddate+1
				EXCEPT
					SELECT * FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
							WHERE   MODIFY_TS 
									BETWEEN 	@startdate 
									AND 		@enddate+1
			END
	END	

IF @moduletype = 'RULES'
 BEGIN
        PRINT 'CR_CONFIG_D_RULES';
			
	--Cursor for CR_CONFIG_D_RULES Table -Begin For RULES
	IF OBJECT_ID ( 'CR_CONFIG_D_RULES', 'C' ) IS NOT NULL   
		DEALLOCATE CR_CONFIG_D_RULES;
	
	DECLARE CR_CONFIG_D_RULES CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[CONFIG_D]
				WHERE PROP_VALUE 	IN (SELECT VALUE FROM #propvalueslist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
				WHERE PROP_VALUE 	IN (SELECT VALUE FROM #propvalueslist)
						
END	
						
	BEGIN TRANSACTION
	BEGIN TRY
	--DECLARE @message as varchar(500) = 'SUCCESS'
	SET @rowcount = 0;
	
IF @moduletype = 'APP CONFIG'
BEGIN
	PRINT 'OPEN CR_CONFIG_D_APP_CONFIG_C1';
	IF @propname = 'clientspec'
	BEGIN
	PRINT 'OPEN CR_CONFIG_D_APP_CONFIG_C1 CASE 1';
	-- Start the execution of Cursor CR_CONFIG_D_APP_CONFIG_C1.
	OPEN CR_CONFIG_D_APP_CONFIG_C1
		FETCH NEXT FROM  CR_CONFIG_D_APP_CONFIG_C1 INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
			, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
			IF EXISTS (	
						SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
							WHERE CONFIG_ID        =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND CONFIG_TYPE        =  @CONFIG_TYPE
							)
				BEGIN
						UPDATE SRP
							SET   PROP_VALUE       =  @PROP_VALUE
								, DATATYPE         =  @DATATYPE
								, FACTORY_NAME     =  @FACTORY_NAME
								, MAXLENGTH        =  @MAXLENGTH
								, ENCRYPTED        =  @ENCRYPTED
								, DESCRIPTION      =  @DESCRIPTION
								, COMMENTS         =  @COMMENTS
								, DOC_ID_LIST      =  @DOC_ID_LIST
								, CLOB_VALUE       =  @CLOB_VALUE
								, BLOB_VALUE       =  @BLOB_VALUE
								, FUNC_URL1        =  @FUNC_URL1
								, FUNC_URL2        =  @FUNC_URL2
								, USE_CLOB         =  @USE_CLOB
								, CATEGORY_ID      =  @CATEGORY_ID
								, LEVEL_ID         =  @LEVEL_ID
								, POSITION         =  @POSITION
								, MODIFY_TS        =  @MODIFY_TS
								, MODIFY_USER      =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D] SRP
							WHERE CONFIG_ID        =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND CONFIG_TYPE        =  @CONFIG_TYPE

					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[CONFIG_D]	 ([CONFIG_ID]
								,[CONFIG_TYPE]
								,[PROP_NAME]
								,[PROP_VALUE]
								,[DATATYPE]
								,[FACTORY_NAME]
								,[MAXLENGTH]
								,[ENCRYPTED]
								,[DESCRIPTION]
								,[COMMENTS]
								,[DOC_ID_LIST]
								,[CLOB_VALUE]
								,[BLOB_VALUE]
								,[FUNC_URL1]
								,[FUNC_URL2]
								,[USE_CLOB]
								,[CATEGORY_ID]
								,[LEVEL_ID]
								,[POSITION]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (	  @CONFIG_ID
								, @CONFIG_TYPE
								, @PROP_NAME
								, @PROP_VALUE
								, @DATATYPE
								, @FACTORY_NAME
								, @MAXLENGTH
								, @ENCRYPTED
								, @DESCRIPTION
								, @COMMENTS
								, @DOC_ID_LIST
								, @CLOB_VALUE
								, @BLOB_VALUE
								, @FUNC_URL1
								, @FUNC_URL2
								, @USE_CLOB
								, @CATEGORY_ID
								, @LEVEL_ID
								, @POSITION
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_CONFIG_D_APP_CONFIG_C1 INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
						, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			END;
		CLOSE  CR_CONFIG_D_APP_CONFIG_C1
		DEALLOCATE CR_CONFIG_D_APP_CONFIG_C1;
	END
	-- End the execution of Cursor CR_CONFIG_D_APP_CONFIG_C1.
ELSE	
	BEGIN
	PRINT 'OPEN CR_CONFIG_D_APP_CONFIG_C1 CASE 2';
		-- Start the execution of Cursor CR_CONFIG_D_APP_CONFIG_C2.
	OPEN CR_CONFIG_D_APP_CONFIG_C2
		FETCH NEXT FROM  CR_CONFIG_D_APP_CONFIG_C2 INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
			, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
			IF EXISTS (	
						SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
							WHERE CONFIG_ID        =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND CONFIG_TYPE        =  @CONFIG_TYPE
--							AND   MODIFY_TS 
--								BETWEEN 	@startdate 
--								AND 		@enddate+1
							)
				BEGIN
						UPDATE SRP
							SET  PROP_VALUE       =  @PROP_VALUE
								, DATATYPE         =  @DATATYPE
								, FACTORY_NAME     =  @FACTORY_NAME
								, MAXLENGTH        =  @MAXLENGTH
								, ENCRYPTED        =  @ENCRYPTED
								, DESCRIPTION      =  @DESCRIPTION
								, COMMENTS         =  @COMMENTS
								, DOC_ID_LIST      =  @DOC_ID_LIST
								, CLOB_VALUE       =  @CLOB_VALUE
								, BLOB_VALUE       =  @BLOB_VALUE
								, FUNC_URL1        =  @FUNC_URL1
								, FUNC_URL2        =  @FUNC_URL2
								, USE_CLOB         =  @USE_CLOB
								, CATEGORY_ID      =  @CATEGORY_ID
								, LEVEL_ID         =  @LEVEL_ID
								, POSITION         =  @POSITION
								, MODIFY_TS        =  @MODIFY_TS
								, MODIFY_USER      =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D] SRP
							WHERE CONFIG_ID       =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND CONFIG_TYPE        =  @CONFIG_TYPE
--							AND   MODIFY_TS 
--								BETWEEN 	@startdate 
--								AND 		@enddate+1
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[CONFIG_D]	 ([CONFIG_ID]
								,[CONFIG_TYPE]
								,[PROP_NAME]
								,[PROP_VALUE]
								,[DATATYPE]
								,[FACTORY_NAME]
								,[MAXLENGTH]
								,[ENCRYPTED]
								,[DESCRIPTION]
								,[COMMENTS]
								,[DOC_ID_LIST]
								,[CLOB_VALUE]
								,[BLOB_VALUE]
								,[FUNC_URL1]
								,[FUNC_URL2]
								,[USE_CLOB]
								,[CATEGORY_ID]
								,[LEVEL_ID]
								,[POSITION]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (	  @CONFIG_ID
								, @CONFIG_TYPE
								, @PROP_NAME
								, @PROP_VALUE
								, @DATATYPE
								, @FACTORY_NAME
								, @MAXLENGTH
								, @ENCRYPTED
								, @DESCRIPTION
								, @COMMENTS
								, @DOC_ID_LIST
								, @CLOB_VALUE
								, @BLOB_VALUE
								, @FUNC_URL1
								, @FUNC_URL2
								, @USE_CLOB
								, @CATEGORY_ID
								, @LEVEL_ID
								, @POSITION
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_CONFIG_D_APP_CONFIG_C2 INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
						, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			END;
		CLOSE  CR_CONFIG_D_APP_CONFIG_C2
		DEALLOCATE CR_CONFIG_D_APP_CONFIG_C2;
	END
	-- End the execution of Cursor CR_CONFIG_D_APP_CONFIG_C2.
	
END

IF @moduletype = 'RULES'
BEGIN
PRINT 'OPEN CR_CONFIG_D_RULES';
	OPEN CR_CONFIG_D_RULES
		FETCH NEXT FROM  CR_CONFIG_D_RULES INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
			, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
			IF EXISTS (	
						SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D]
							WHERE CONFIG_ID        =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND PROP_VALUE         =  @PROP_VALUE
							AND CONFIG_TYPE        =  @CONFIG_TYPE
							)
				BEGIN
						UPDATE SRP
							SET 
								  DATATYPE         =  @DATATYPE
								, FACTORY_NAME     =  @FACTORY_NAME
								, MAXLENGTH        =  @MAXLENGTH
								, ENCRYPTED        =  @ENCRYPTED
								, DESCRIPTION      =  @DESCRIPTION
								, COMMENTS         =  @COMMENTS
								, DOC_ID_LIST      =  @DOC_ID_LIST
								, CLOB_VALUE       =  @CLOB_VALUE
								, BLOB_VALUE       =  @BLOB_VALUE
								, FUNC_URL1        =  @FUNC_URL1
								, FUNC_URL2        =  @FUNC_URL2
								, USE_CLOB         =  @USE_CLOB
								, CATEGORY_ID      =  @CATEGORY_ID
								, LEVEL_ID         =  @LEVEL_ID
								, POSITION         =  @POSITION
								, MODIFY_TS        =  @MODIFY_TS
								, MODIFY_USER      =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[CONFIG_D] SRP
							WHERE CONFIG_ID        =  @CONFIG_ID
							AND PROP_NAME          =  @PROP_NAME
							AND PROP_VALUE         =  @PROP_VALUE
							AND CONFIG_TYPE        =  @CONFIG_TYPE
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[CONFIG_D]	 ([CONFIG_ID]
								,[CONFIG_TYPE]
								,[PROP_NAME]
								,[PROP_VALUE]
								,[DATATYPE]
								,[FACTORY_NAME]
								,[MAXLENGTH]
								,[ENCRYPTED]
								,[DESCRIPTION]
								,[COMMENTS]
								,[DOC_ID_LIST]
								,[CLOB_VALUE]
								,[BLOB_VALUE]
								,[FUNC_URL1]
								,[FUNC_URL2]
								,[USE_CLOB]
								,[CATEGORY_ID]
								,[LEVEL_ID]
								,[POSITION]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (	  @CONFIG_ID
								, @CONFIG_TYPE
								, @PROP_NAME
								, @PROP_VALUE
								, @DATATYPE
								, @FACTORY_NAME
								, @MAXLENGTH
								, @ENCRYPTED
								, @DESCRIPTION
								, @COMMENTS
								, @DOC_ID_LIST
								, @CLOB_VALUE
								, @BLOB_VALUE
								, @FUNC_URL1
								, @FUNC_URL2
								, @USE_CLOB
								, @CATEGORY_ID
								, @LEVEL_ID
								, @POSITION
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_CONFIG_D_RULES INTO @CONFIG_ID, @CONFIG_TYPE, @PROP_NAME, @PROP_VALUE, @DATATYPE, @FACTORY_NAME, @MAXLENGTH, @ENCRYPTED, @DESCRIPTION
						, @COMMENTS, @DOC_ID_LIST, @CLOB_VALUE, @BLOB_VALUE, @FUNC_URL1, @FUNC_URL2, @USE_CLOB, @CATEGORY_ID, @LEVEL_ID, @POSITION, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_CONFIG_D_RULES
		DEALLOCATE CR_CONFIG_D_RULES;
	-- End the execution of Cursor CR_CONFIG_D_RULES.
END

	SET @result = 'MIGRATION SUCCESS'
	SET @totalCount = @rowcount;
	COMMIT TRANSACTION

	END TRY

	BEGIN CATCH

	ROLLBACK TRANSACTION
		 if (ERROR_NUMBER() = 1222 )
		  BEGIN
		     PRINT ERROR_MESSAGE()
			 SET @result  = 'FAILED DUE TO DEADLOCK.  ERROR = ' + ERROR_MESSAGE()
		  END else 
		  BEGIN 
		   SET @result  = 'Failed Due to Error = ' + ERROR_MESSAGE()
		  END
	END CATCH
END