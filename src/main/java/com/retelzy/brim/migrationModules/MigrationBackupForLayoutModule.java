package com.retelzy.brim.migrationModules;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.util.MigrationBackupUtil;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.Helper;

@Component
public class MigrationBackupForLayoutModule {

	private final static Logger logger = LoggerFactory.getLogger(MigrationBackupForLayoutModule.class);

	@Autowired
	private ConfigFileReader configFileReader;

	@Autowired
	private MigrationBackupUtil migrationBackupUtil;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private Environment environment;

	public Boolean getBackupLayoutInsertSql(List<Integer> listOfLayoutId, PrintWriter pw, File file)
			throws IOException {

		logger.info("Backup Started for Layout");

		Connection connection = null;
		Statement statement1 = null;
		Statement statement2 = null;

		try {

			String[] layoutTableInsertSeq = ConfigFileReader.toArray(configFileReader.getInsertLayoutTableSequence());

			connection = destinationDbService.getTheDestinationDbConnection();

			String dbType = connection.getMetaData().getDatabaseProductName();

			for (Integer numberOfLayoutId = 0; numberOfLayoutId < listOfLayoutId.size(); numberOfLayoutId++) {

				for (Integer index = 0; index < layoutTableInsertSeq.length; index++) {

					String table = layoutTableInsertSeq[index];
					String where = " layout_id= " + listOfLayoutId.get(numberOfLayoutId);
					if (table.trim().equalsIgnoreCase("LAYOUT_I18N_RES")) {
						where = where + " and language_id =0";
					}
					migrationBackupUtil.writeQuriesInBackupFile(where, pw, connection, statement1, statement2, dbType,
							table, 0, null, null, file);

				}

			}
			backupValidationQuery(connection, listOfLayoutId);
			return true;

		} catch (Exception e) {
			System.gc();
			Helper.clearTheBackupFileNames();
			e.printStackTrace();
			logger.error("Exception Occured while Taking Backup Of Layout Module :" + e.getMessage());
			return false;
		} finally {
			pw.close();
			try {
				if (statement1 != null)
					statement1.close();
				if (statement2 != null)
					statement2.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private void backupValidationQuery(Connection destCon, List<Integer> listOfLayoutId) throws Exception {
		List<String> idList = new ArrayList<String>();
		// get the validation filters assigned to layout id

		String date = Helper.getDateTime();
		String queryHfileName = "";
		String queryDfileName = "";

		if (listOfLayoutId.size() > 1) {
			queryHfileName = "query_h_backup_lay_" + "MULTIPLE_" + date + ".xml";
			MigrationController.queryHfileName = queryHfileName;

			queryDfileName = "query_d_backup_lay_" + "MULTIPLE_" + date + ".xml";
			MigrationController.queryDfileName = queryDfileName;
		} else if (listOfLayoutId.size() == 1) {
			queryHfileName = "query_h_backup_lay_" + listOfLayoutId.get(0) + "_" + date + ".xml";
			MigrationController.queryHfileName = queryHfileName;

			queryDfileName = "query_d_backup_lay_" + listOfLayoutId.get(0) + "_" + date + ".xml";
			MigrationController.queryDfileName = queryDfileName;
		}

		for (Integer roleIdIndex = 0; roleIdIndex < listOfLayoutId.size(); roleIdIndex++) {

			Integer layoutId = listOfLayoutId.get(roleIdIndex);

			String destValidationQueryId = "SELECT cast(prop_value as integer) FROM doc_view_element_d where layout_id = "
					+ layoutId + " and prop_name ='LOOKUPFILTER'";
			Statement stmt = destCon.createStatement();
			ResultSet rsdest = stmt.executeQuery(destValidationQueryId);
			while (rsdest.next()) {
				idList.add(rsdest.getString(1));
			}
			rsdest.close();
			stmt.close();

			// String szIdList = MigrationUtil.listToString(idList, ",");
			String szIdList = numListToString(idList, ",");
			logger.info("szIdList  size-->" + szIdList);
			if (!szIdList.equals("") && szIdList != null) {
				String migrateQueriesSQL = "select * from query_h where query_id in(" + szIdList + ")";

				logger.info("Exporting the backup layout data for layout id " + layoutId
						+ " from destination database.Please wait........");

				IDatabaseConnection connection = destinationDbService.getConnection();
				try {

					QueryDataSet partialDataSet = new QueryDataSet(connection);
					partialDataSet.addTable("query_h", migrateQueriesSQL);
					System.out.println(migrateQueriesSQL);
					FlatXmlDataSet.write(partialDataSet,
							new FileOutputStream(environment.getProperty("dir.path.backup") + queryHfileName));

					QueryDataSet partialDataSet2 = new QueryDataSet(connection);
					partialDataSet2.addTable("query_d", "select * from query_d where query_id in(" + szIdList + ")");
					FlatXmlDataSet.write(partialDataSet2,
							new FileOutputStream(environment.getProperty("dir.path.backup") + queryDfileName));
					logger.info("Completed Backup for QueryH and QueryD of Layout..");
				} catch (Exception e) {
					Helper.clearTheBackupFileNames();
					logger.error("Exception Occured while Taking Backup Of QueryH And QueryD Table :" + e.getMessage());
					throw new MigrationException("Backup Failed");
				} finally {
					connection.close();
				}
			}
		}
	}

	public String numListToString(Collection<String> col, String delimiter) {
		StringBuffer sb = new StringBuffer();

		if (delimiter == null)
			delimiter = ",";

		Iterator<String> i = col.iterator();

		int nIndex = 0;
		int nMaxSize = col.size() - 1;
		while (i.hasNext()) {
			Object element = i.next();
			if (element != null) {
				if (nIndex++ < nMaxSize) {
					sb.append(element.toString() + delimiter);
				} else {
					sb.append(element.toString());
				}
			}
		}

		return sb.toString();
	}

}
