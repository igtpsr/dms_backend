USE [DMS]
GO

IF OBJECT_ID ( 'RIM_MIGRATION_FROM_DEV_TO_TEST_CUSTOM_RULES_AND_REPORTS', 'P' ) IS NOT NULL   
    DROP PROCEDURE RIM_MIGRATION_FROM_DEV_TO_TEST_CUSTOM_RULES_AND_REPORTS;  
GO

CREATE PROCEDURE [dbo].[RIM_MIGRATION_FROM_DEV_TO_TEST_CUSTOM_RULES_AND_REPORTS]
	  @adhocDocNameList 	VARCHAR(500) = ''
	, @totalCount 			INT OUTPUT
	, @result				VARCHAR(500) OUTPUT

AS 
BEGIN

	  SET NOCOUNT ON;
	  DECLARE @rowcount				    INT;		
	  DECLARE @ADHOC_DOC_NAME		    VARCHAR(128);		
	  DECLARE @CREATE_TS			    datetime;
	  DECLARE @CREATE_USER			    VARCHAR(18);
      DECLARE @CLOB_VALUE               NVARCHAR(max);
      DECLARE @BLOB_VALUE               VARBINARY(max);
      DECLARE @CATEGORY_ID              VARCHAR(10);
      DECLARE @MODIFY_TS                datetime;
      DECLARE @MODIFY_USER              VARCHAR(18);

	-- Spliting list of #adhoclist's and #categorylist's
	-- dbo.SplitString function getting call
	SELECT * INTO #adhoclist 
	FROM dbo.SplitString(@adhocDocNameList, ',')


	--Cursor for ADHOC_DOCUMENTS Table -Begin
	IF OBJECT_ID ( 'CR_ADHOC_DOCUMENTS', 'C' ) IS NOT NULL   
		DEALLOCATE CR_ADHOC_DOCUMENTS;
	
	DECLARE CR_ADHOC_DOCUMENTS CURSOR FOR 
		SELECT * FROM [TSSDEV02SQL01].[dbo].[ADHOC_DOCUMENTS] 
				WHERE ADHOC_DOC_NAME  
					IN (SELECT VALUE FROM #adhoclist)
	EXCEPT
		SELECT * FROM [DMS_TEMP].[dbo].[ADHOC_DOCUMENTS]
				WHERE ADHOC_DOC_NAME  
					IN (SELECT VALUE FROM #adhoclist)
					
	--Cursor for ADHOC_DOCUMENTS Table -End

	BEGIN TRANSACTION
	BEGIN TRY
	--DECLARE @message as varchar(500) = 'SUCCESS'
	SET @rowcount = 0;
	-- Start the execution of Cursor CR_ADHOC_DOCUMENTS.
	OPEN CR_ADHOC_DOCUMENTS
			FETCH NEXT FROM  CR_ADHOC_DOCUMENTS INTO @ADHOC_DOC_NAME, @CATEGORY_ID, @CLOB_VALUE, @BLOB_VALUE, @CREATE_TS, @CREATE_USER, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
					IF EXISTS (	
						SELECT 1 
							FROM [DMS_TEMP].[dbo].[ADHOC_DOCUMENTS]
							WHERE ADHOC_DOC_NAME      =  @ADHOC_DOC_NAME
							AND CATEGORY_ID   	      =  @CATEGORY_ID
							)
				BEGIN
						UPDATE SRP
							SET  ADHOC_DOC_NAME       =  @ADHOC_DOC_NAME
								, CLOB_VALUE 	      =  @CLOB_VALUE
								, BLOB_VALUE    	  =  @BLOB_VALUE
								, CREATE_TS    		  =  @CREATE_TS
								, CREATE_USER   	  =  @CREATE_USER
								, MODIFY_TS    		  =  @MODIFY_TS
								, MODIFY_USER         =  @MODIFY_USER
							FROM [DMS_TEMP].[dbo].[ADHOC_DOCUMENTS] SRP
							WHERE ADHOC_DOC_NAME      =  @ADHOC_DOC_NAME
							AND CATEGORY_ID   	      =  @CATEGORY_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [DMS_TEMP].[dbo].[ADHOC_DOCUMENTS]	 ([ADHOC_DOC_NAME]
								,[CATEGORY_ID]
								,[CLOB_VALUE]
								,[BLOB_VALUE]
								,[CREATE_TS]
								,[CREATE_USER]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (	  @ADHOC_DOC_NAME
								, @CATEGORY_ID
								, @CLOB_VALUE
								, @BLOB_VALUE
								, @CREATE_TS
								, @CREATE_USER
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_ADHOC_DOCUMENTS INTO @ADHOC_DOC_NAME, @CATEGORY_ID, @CLOB_VALUE, @BLOB_VALUE, @CREATE_TS, @CREATE_USER, @MODIFY_TS, @MODIFY_USER
				END;
	CLOSE  CR_ADHOC_DOCUMENTS
		DEALLOCATE CR_ADHOC_DOCUMENTS;
	-- End the execution of Cursor CR_ADHOC_DOCUMENTS.

	SET @result = 'MIGRATION SUCCESS'
	SET @totalCount = @rowcount;
	COMMIT TRANSACTION

	END TRY

	BEGIN CATCH

	ROLLBACK TRANSACTION
		 if (ERROR_NUMBER() = 1222 )
		  BEGIN
		     PRINT ERROR_MESSAGE()
			 SET @result  = 'FAILED DUE TO DEADLOCK.  ERROR = ' + ERROR_MESSAGE()
		  END else 
		  BEGIN 
		   SET @result  = 'Failed Due to Error = ' + ERROR_MESSAGE()
		  END
	END CATCH
END