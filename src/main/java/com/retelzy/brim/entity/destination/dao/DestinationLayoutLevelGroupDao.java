package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutLevelGroup;
import com.retelzy.brim.entity.destination.LayoutLevelGroupPK;

@Repository
public interface DestinationLayoutLevelGroupDao extends CrudRepository<LayoutLevelGroup, LayoutLevelGroupPK> {


}
