package com.retelzy.brim.entity.destination.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.ConfigD;
import com.retelzy.brim.entity.destination.ConfigDPK;

@Repository
public interface DestinationConfigDDao extends CrudRepository<ConfigD, ConfigDPK> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT PROP_VALUE FROM CONFIG_D WHERE CONFIG_ID = 1 AND PROP_VALUE LIKE '%.JAR'")
	List<String> getTheListOfRuleNames();

	@Transactional
	@Modifying
	@Query("Delete from ConfigD cd Where cd.propValue =:fileName")
	Integer deleteTheRulesFile(String fileName);

	@Query("select c from ConfigD c where c.id.configId = 1 AND c.propValue not like '%.jar' AND c.propValue not like '%.irl' AND c.modifyTs BETWEEN :migrationFromDate AND :migrationToDate ")
	List<ConfigD> getTheAppConfigReport(Date migrationFromDate, Date migrationToDate);
}
