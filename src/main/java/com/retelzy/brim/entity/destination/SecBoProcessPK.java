package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class SecBoProcessPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "DOC_ID")
	private Integer docId;

	@Column(name = "PROCESS_ID")
	private Integer processId;

}
