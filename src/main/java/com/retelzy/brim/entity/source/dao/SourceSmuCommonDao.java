package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SmuCommon;

@Repository
public interface SourceSmuCommonDao extends CrudRepository<SmuCommon, Integer> {

	@Query("Select sc From SmuCommon sc Where sc.menuItemId >=:menuItemId")
	List<Object> getSmuCommonAllDetails(Integer menuItemId);

}
