package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the SMU_ROLE_ROOT_MENU database table.
 * 
 */
@Embeddable
public class SmuRoleRootMenuPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "DOC_VIEW_ID")
	private Integer docViewId;

	@Column(name = "MENU_ITEM_ID")
	private Integer menuItemId;

	@Column(name = "MENU_ITEM_TYPE")
	private Integer menuItemType;

	@Column(name = "DOC_VIEW_REL_ID")
	private Integer docViewRelId;

	public SmuRoleRootMenuPK() {
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getDocViewId() {
		return docViewId;
	}

	public void setDocViewId(Integer docViewId) {
		this.docViewId = docViewId;
	}

	public Integer getMenuItemId() {
		return menuItemId;
	}

	public void setMenuItemId(Integer menuItemId) {
		this.menuItemId = menuItemId;
	}

	public Integer getMenuItemType() {
		return menuItemType;
	}

	public void setMenuItemType(Integer menuItemType) {
		this.menuItemType = menuItemType;
	}

	public Integer getDocViewRelId() {
		return docViewRelId;
	}

	public void setDocViewRelId(Integer docViewRelId) {
		this.docViewRelId = docViewRelId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docViewId == null) ? 0 : docViewId.hashCode());
		result = prime * result + ((docViewRelId == null) ? 0 : docViewRelId.hashCode());
		result = prime * result + ((menuItemId == null) ? 0 : menuItemId.hashCode());
		result = prime * result + ((menuItemType == null) ? 0 : menuItemType.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmuRoleRootMenuPK other = (SmuRoleRootMenuPK) obj;
		if (docViewId == null) {
			if (other.docViewId != null)
				return false;
		} else if (!docViewId.equals(other.docViewId))
			return false;
		if (docViewRelId == null) {
			if (other.docViewRelId != null)
				return false;
		} else if (!docViewRelId.equals(other.docViewRelId))
			return false;
		if (menuItemId == null) {
			if (other.menuItemId != null)
				return false;
		} else if (!menuItemId.equals(other.menuItemId))
			return false;
		if (menuItemType == null) {
			if (other.menuItemType != null)
				return false;
		} else if (!menuItemType.equals(other.menuItemType))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}

}