package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The persistent class for the DASH_SECTION_H database table.
 * 
 */
@Entity
@Table(name = "DASH_SECTION_H")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class DashSectionH implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DashSectionHPK id;

	@Column(name = "ACL")
	private Integer acl;

	@Column(name = "BACKGROUND")
	private String background;

	@Column(name = "COL_POS")
	private Integer colPos;

	@Column(name = "GROUP_ID")
	private Integer groupId;

	@Column(name = "HEIGHT")
	private Integer height;

	@Column(name = "I18N_RES_ID")
	private Integer i18nResId;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@Column(name = "ROW_POS")
	private Integer rowPos;

	@Column(name = "SECTION_CLASS")
	private String sectionClass;

	@Column(name = "SECTION_TYPE")
	private Integer sectionType;

	@Column(name = "WIDTH")
	private Integer width;

}