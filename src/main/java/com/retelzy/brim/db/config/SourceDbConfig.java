package com.retelzy.brim.db.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "sourceEntityManagerFactory", basePackages = {
		"com.retelzy.brim.entity.source.dao" }, transactionManagerRef = "sourceTransactionManagerRef")
public class SourceDbConfig {

	@Autowired
	private Environment environment;

	@Bean(name = "sourceDataSource")
	public DataSource sourceDataSource() {

		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName(environment.getProperty("spring.source.datasource.driverClassName"));
		dataSource.setJdbcUrl(environment.getProperty("spring.source.datasource.jdbcUrl"));
		dataSource.setUsername(environment.getProperty("spring.source.datasource.username"));
		dataSource.setPassword(environment.getProperty("spring.source.datasource.password"));
		dataSource.setIdleTimeout(300000);
		dataSource.setConnectionTimeout(20000);
		dataSource.setAutoCommit(false);
		dataSource.setMaximumPoolSize(50);
		dataSource.setMaxLifetime(1200000);
		dataSource.setMinimumIdle(10);
		dataSource.setPoolName("Source");

		return dataSource;
	}

	@Bean(name = "sourceMultiRoutingDataSource")
	public DataSource sourceMultiRoutingDataSource() {
		Map<Object, Object> targetDataSources = new HashMap<>();
		targetDataSources.put(DBTypeEnum.SOURCE, sourceDataSource());

		SourceDBMultiRoutingDataSource multiRoutingDataSource = new SourceDBMultiRoutingDataSource();
		multiRoutingDataSource.setDefaultTargetDataSource(sourceDataSource());
		multiRoutingDataSource.setTargetDataSources(targetDataSources);
		return multiRoutingDataSource;

	}

	@Bean(name = "sourceEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder builder,
			@Qualifier("sourceMultiRoutingDataSource") DataSource dataSource) {
		Map<String, Object> properties = new LinkedHashMap<String, Object>();
		properties.put("hibernate.hbm2ddl.auto", "none");
		properties.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		properties.put("hibernate.show_sql", "true");
		properties.put("connection.release_mode", "auto");
		return builder.dataSource(dataSource).properties(properties).packages("com.retelzy.brim.entity.source")
				.persistenceUnit("Source").build();
	}

	@Bean(name = "sourceTransactionManagerRef")
	public PlatformTransactionManager platformTransactionManager(
			@Qualifier("sourceEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory);
		jpaTransactionManager.setNestedTransactionAllowed(true);
		jpaTransactionManager.setRollbackOnCommitFailure(true);
		return jpaTransactionManager;
	}

}
