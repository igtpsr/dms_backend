// Generated with g9.

package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="LAYOUT_PROFILE_D")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutProfileD implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutProfileDPK id;

	@Column(name = "PROP_VALUE", nullable = false, length = 35)
	private String propValue;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER", length = 18)
	private String modifyUser;


}
