package com.retelzy.brim.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
@Entity
@Table(name = "CLIENT_DB_DETAILS")
@DynamicUpdate
public class ClientDbDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLIENT_DB_ID")
	private Integer clientDbCredentialId;

	@Column(name = "DB_ENVIRONMENT_TYPE")
	private String dbEnvironmentType;

	@Column(name = "DB_TYPE")
	private String dbType;

	@Column(name = "DB_SERVER_NAME")
	private String dbServerName;

	@Column(name = "DB_USERNAME")
	private String dbUsername;

	@Column(name = "DB_PASSWORD")
	private String dbPassword;

	@Column(name = "DB_DRIVER_CLASS_NAME")
	private String dbDriverClassName;

	@Column(name = "DB_NAME")
	private String dbName;

	@Column(name = "PORT_NUMBER")
	private Integer portNumber;
	
	@Column(name = "VERIFICATION_STATUS")
	private Integer isThisDbEnvVerified;
	
	@Transient
	private Integer priority;

}
