package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.SmuRoleMenuItem;
import com.retelzy.brim.entity.source.SmuRoleMenuItemPK;

public interface SourceSmuRoleMenuItemDao extends CrudRepository<SmuRoleMenuItem, SmuRoleMenuItemPK> {

	public List<Object> findByIdRoleId(Integer roleId);

	@Query("Select srrm from SmuRoleMenuItem srrm Where srrm.id.menuItemId >=:menuItemId "
			+ "And srrm.id.roleId >=:minRoleId And srrm.id.roleId Not In (:roleId) And srrm.id.roleId In (:listOfRoleId) ")
	public List<Object> getSmuRoleRootMenuItemDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId);

}
