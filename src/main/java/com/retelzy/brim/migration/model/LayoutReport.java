package com.retelzy.brim.migration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutReport {

	private String layoutDesc;
	private String docNameDesc;
	private String docViewName;
	private String sectionName;
	private String fieldLabel;
	private Integer scale;
	private String validationName;
	private String widgetName;
	private String validationOverride;
	private String showDesc;
	private String enableAutoSugg;
	private String displayLength;
	private String required;
	private String showLookupUrl;
	private String bgColor;
	private String fgColor;
	private String customValidator;
	private String fieldName;

}
