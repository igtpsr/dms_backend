package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * The persistent class for the LAYOUT_ELEMENT_D database table.
 * 
 */
@Entity
@Table(name="LAYOUT_ELEMENT_D")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutElementD implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutElementDPK id;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER", length=18)
	private String modifyUser;

	@Column(name="PROP_VALUE", nullable=false, length=80)
	private String propValue;


}