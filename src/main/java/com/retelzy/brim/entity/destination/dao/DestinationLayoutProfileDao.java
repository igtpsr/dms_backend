package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutProfile;

@Repository
public interface DestinationLayoutProfileDao extends CrudRepository<LayoutProfile, Integer> {

	@Query(value = "Select lp.layoutId from LayoutProfile lp where lp.layoutId =:layoutId")
	public List<Integer> getAllTheLayoutIdByLayoutId(Integer layoutId);

	@Query(value = "Select lp from LayoutProfile lp where lp.layoutId =:layoutId")
	public List<Object> getAllTheDataByLayoutId(Integer layoutId);

	/*
	 * @Query(value =
	 * "Select lp.layoutId,lp.layoutDesc from LayoutProfile lp where lp.layoutId =:layoutId"
	 * ) public List<Object> getLayoutProfileInfo(Integer layoutId);
	 */

	@Query(value = "Select max(lp.layoutId)+1 from LayoutProfile lp")
	public Integer getLayoutID();

	@Query(value = "Select lp from LayoutProfile lp Where lp.layoutId In (:listOfActiveRoleIdOrLayoutId)")
	public List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveRoleIdOrLayoutId);

}
