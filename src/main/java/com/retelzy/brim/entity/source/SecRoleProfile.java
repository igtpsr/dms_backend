package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the SEC_ROLE_PROFILE database table.
 * 
 */
@Entity
@Table(name = "SEC_ROLE_PROFILE")
@DynamicUpdate
public class SecRoleProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "I18N_RES_ID")
	private Integer i18nResId;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@Column(name = "ROLE_NAME")
	private String roleName;

	// bi-directional many-to-one association to SecBoFieldAcl
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecBoFieldAcl> secBoFieldAcls;

	// bi-directional many-to-one association to SecBoFilter
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecBoFilter> secBoFilters;

	// bi-directional many-to-one association to SecBoLevelAcl
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecBoLevelAcl> secBoLevelAcls;

	// bi-directional many-to-one association to SecRoleResCtl
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecRoleResCtl> secRoleResCtls;

	// bi-directional many-to-one association to SecRoleResCtl
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecQuery> secQuery;

	// bi-directional many-to-one association to SecRoleResCtl
	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecQueryGroup> secQueryGroup;

	@OneToMany(mappedBy = "secRoleProfile")
	private List<SecBointFilter> secBointFilter;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getI18nResId() {
		return i18nResId;
	}

	public void setI18nResId(Integer i18nResId) {
		this.i18nResId = i18nResId;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<SecBoFieldAcl> getSecBoFieldAcls() {
		return secBoFieldAcls;
	}

	public void setSecBoFieldAcls(List<SecBoFieldAcl> secBoFieldAcls) {
		this.secBoFieldAcls = secBoFieldAcls;
	}

	public List<SecBoFilter> getSecBoFilters() {
		return secBoFilters;
	}

	public void setSecBoFilters(List<SecBoFilter> secBoFilters) {
		this.secBoFilters = secBoFilters;
	}

	public List<SecBoLevelAcl> getSecBoLevelAcls() {
		return secBoLevelAcls;
	}

	public void setSecBoLevelAcls(List<SecBoLevelAcl> secBoLevelAcls) {
		this.secBoLevelAcls = secBoLevelAcls;
	}

	public List<SecRoleResCtl> getSecRoleResCtls() {
		return secRoleResCtls;
	}

	public void setSecRoleResCtls(List<SecRoleResCtl> secRoleResCtls) {
		this.secRoleResCtls = secRoleResCtls;
	}

	public List<SecQuery> getSecQuery() {
		return secQuery;
	}

	public void setSecQuery(List<SecQuery> secQuery) {
		this.secQuery = secQuery;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<SecQueryGroup> getSecQueryGroup() {
		return secQueryGroup;
	}

	public void setSecQueryGroup(List<SecQueryGroup> secQueryGroup) {
		this.secQueryGroup = secQueryGroup;
	}

	public List<SecBointFilter> getSecBointFilter() {
		return secBointFilter;
	}

	public void setSecBointFilter(List<SecBointFilter> secBointFilter) {
		this.secBointFilter = secBointFilter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((i18nResId == null) ? 0 : i18nResId.hashCode());
		result = prime * result + ((modifyTs == null) ? 0 : modifyTs.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((roleName == null) ? 0 : roleName.hashCode());
		result = prime * result + ((secBoFieldAcls == null) ? 0 : secBoFieldAcls.hashCode());
		result = prime * result + ((secBoFilters == null) ? 0 : secBoFilters.hashCode());
		result = prime * result + ((secBoLevelAcls == null) ? 0 : secBoLevelAcls.hashCode());
		result = prime * result + ((secBointFilter == null) ? 0 : secBointFilter.hashCode());
		result = prime * result + ((secQuery == null) ? 0 : secQuery.hashCode());
		result = prime * result + ((secQueryGroup == null) ? 0 : secQueryGroup.hashCode());
		result = prime * result + ((secRoleResCtls == null) ? 0 : secRoleResCtls.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecRoleProfile other = (SecRoleProfile) obj;
		if (i18nResId == null) {
			if (other.i18nResId != null)
				return false;
		} else if (!i18nResId.equals(other.i18nResId))
			return false;
		if (modifyTs == null) {
			if (other.modifyTs != null)
				return false;
		} else if (!modifyTs.equals(other.modifyTs))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (roleName == null) {
			if (other.roleName != null)
				return false;
		} else if (!roleName.equals(other.roleName))
			return false;
		if (secBoFieldAcls == null) {
			if (other.secBoFieldAcls != null)
				return false;
		} else if (!secBoFieldAcls.equals(other.secBoFieldAcls))
			return false;
		if (secBoFilters == null) {
			if (other.secBoFilters != null)
				return false;
		} else if (!secBoFilters.equals(other.secBoFilters))
			return false;
		if (secBoLevelAcls == null) {
			if (other.secBoLevelAcls != null)
				return false;
		} else if (!secBoLevelAcls.equals(other.secBoLevelAcls))
			return false;
		if (secBointFilter == null) {
			if (other.secBointFilter != null)
				return false;
		} else if (!secBointFilter.equals(other.secBointFilter))
			return false;
		if (secQuery == null) {
			if (other.secQuery != null)
				return false;
		} else if (!secQuery.equals(other.secQuery))
			return false;
		if (secQueryGroup == null) {
			if (other.secQueryGroup != null)
				return false;
		} else if (!secQueryGroup.equals(other.secQueryGroup))
			return false;
		if (secRoleResCtls == null) {
			if (other.secRoleResCtls != null)
				return false;
		} else if (!secRoleResCtls.equals(other.secRoleResCtls))
			return false;
		return true;
	}

}