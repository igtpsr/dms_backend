package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.RoleResAttr;
import com.retelzy.brim.entity.destination.RoleResAttrPK;

public interface DestinationRoleResAttarDao extends CrudRepository<RoleResAttr, RoleResAttrPK> {

	@Query("Select ro from RoleResAttr ro Where ro.id.roleId =:roleId")
	List<Object> getTheDataFromRoleResAttarTable(Integer roleId);

}
