package com.retelzy.brim.entity.source.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.AdhocDocuments;
import com.retelzy.brim.entity.source.AdhocDocumentsPK;

@Repository
public interface SourceAdhocDocumentsDao extends CrudRepository<AdhocDocuments, AdhocDocumentsPK> {

	@Query("Select ad from AdhocDocuments ad where ad.modifyTs BETWEEN :fromDate AND :toDate")
	List<AdhocDocuments> getTheAdhocDocumentsTableDetails(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "SELECT DISTINCT ADHOC_DOC_NAME FROM ADHOC_DOCUMENTS WHERE ADHOC_DOC_NAME LIKE '%.JAR%'")
	List<String> getTheListOfAdhocDocumentsNames();

	@Query(value = "select * from ADHOC_DOCUMENTS where adhoc_doc_name like %:fileName%", nativeQuery = true)
	List<AdhocDocuments> getAdhocDocumnetsData(String fileName);
	
	@Query(nativeQuery = true, value = "SELECT ADHOC_DOC_NAME FROM ADHOC_DOCUMENTS WHERE ADHOC_DOC_NAME NOT LIKE '%.IRL%' AND ADHOC_DOC_NAME NOT LIKE '%.JAR%'")
	List<String> getTheListOfReportNames();
	
	@Query(nativeQuery = true, value = "SELECT ADHOC_DOC_NAME FROM ADHOC_DOCUMENTS WHERE ADHOC_DOC_NAME NOT LIKE '%.IRL%'")
	List<String> getTheListOfReportNamesWithExtension();

}
