package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBoProcess;
import com.retelzy.brim.entity.source.SecBoProcessPK;

@Repository
public interface SourceSecBoProcessDao extends CrudRepository<SecBoProcess, SecBoProcessPK> {

	@Query("Select sb From SecBoProcess sb Where sb.id.processId >=:processId")
	public List<Object> findByIdProcessId(Integer processId);

	@Query("Select sb.id.processId From SecBoProcess sb Where sb.id.processId >=:processId")
	public List<Integer> getByIdProcessIdGreaterThanEqual(Integer processId);
}
