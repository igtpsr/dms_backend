package com.retelzy.brim.entity.source.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutProfile;

@Repository
public interface SourceLayoutProfileDao extends CrudRepository<LayoutProfile, Integer> {

	@Query(value = "Select lp.layoutId from LayoutProfile lp where lp.layoutId not in (0) order by lp.layoutId")
	public List<Integer> getAllTheLayoutIdByLayoutId();

	@Query(value = "Select lp from LayoutProfile lp where lp.layoutId =:layoutId")
	public List<Object> getAllTheDataByLayoutId(Integer layoutId);

	@Query(value = "Select lp from LayoutProfile lp Where lp.layoutId In (:listOfActiveLayoutId)")
	public List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveLayoutId);

	@Query(value = "Select lp.layoutId from LayoutProfile lp where lp.layoutId =:layoutId")
	public Optional<Integer> getTheLayoutIdByLayoutId(Integer layoutId);

}
