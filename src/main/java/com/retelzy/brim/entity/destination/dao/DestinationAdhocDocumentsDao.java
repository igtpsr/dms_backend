package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.AdhocDocuments;
import com.retelzy.brim.entity.destination.AdhocDocumentsPK;

@Repository
public interface DestinationAdhocDocumentsDao extends CrudRepository<AdhocDocuments, AdhocDocumentsPK> {

	@Query(nativeQuery = true, value = "SELECT ADHOC_DOC_NAME FROM ADHOC_DOCUMENTS WHERE ADHOC_DOC_NAME NOT LIKE '%.IRL%'")
	List<String> getTheListOfReportNamesWithExtension();

	@Transactional
	@Modifying
	@Query("Delete From AdhocDocuments ad Where ad.id.adhocDocName =:destinationFileName")
	public Integer deleteTheReportFile(String destinationFileName);
	
	@Query(nativeQuery = true, value = "SELECT DISTINCT ADHOC_DOC_NAME FROM ADHOC_DOCUMENTS WHERE ADHOC_DOC_NAME LIKE '%.JAR%'")
	List<String> getTheListOfAdhocDocumentsNames();

}
