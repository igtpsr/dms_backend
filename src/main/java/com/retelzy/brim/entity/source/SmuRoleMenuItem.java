package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the SMU_ROLE_MENU_ITEM database table.
 * 
 */
@Entity
@Table(name = "SMU_ROLE_MENU_ITEM")
@DynamicUpdate
public class SmuRoleMenuItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SmuRoleMenuItemPK id;

	@Column(name = "ENABLED")
	private Integer enabled;

	@Column(name = "MENU_POS")
	private Integer menuPos;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	public SmuRoleMenuItem() {
	}

	public SmuRoleMenuItemPK getId() {
		return id;
	}

	public void setId(SmuRoleMenuItemPK id) {
		this.id = id;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getMenuPos() {
		return menuPos;
	}

	public void setMenuPos(Integer menuPos) {
		this.menuPos = menuPos;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}