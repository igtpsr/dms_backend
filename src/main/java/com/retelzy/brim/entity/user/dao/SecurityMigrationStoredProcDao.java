package com.retelzy.brim.entity.user.dao;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.migration.model.MigrationId;
import com.retelzy.brim.migration.model.SecurityRoleIdDetais;

@Service
public class SecurityMigrationStoredProcDao {
private static final Logger log = LoggerFactory.getLogger(SecurityMigrationStoredProcDao.class);

	
	@PersistenceUnit(unitName = "User")
	private EntityManagerFactory emf;
	
	
	public Map<String, Object> MigrateSecurityStoredProc(MigrationId migrationId, MigrationDetails migrationDetails) throws Exception {
		try {
			String sourceEnvType = migrationDetails.getSourceEnvironmentType();
			String destinationEnvType = migrationDetails.getDestinationEnvironmentType();
			return this.callStoredProcedureToMigrateSecurity(migrationId, sourceEnvType, destinationEnvType);
		} catch (Exception e) {
			log.error(" Error while running migration Security stored procedure --> " + e.getMessage());
			throw e;
		}
	}
	
	
	private Map<String, Object> callStoredProcedureToMigrateSecurity(MigrationId migrationId, String sourceEnvType, String destinationEnvType) throws Exception {
		log.info("Stored procedure migration of --> \n " + migrationId.getModuleName());
		
		if (migrationId.getModuleName().equalsIgnoreCase("SECURITY")) {
			
			//--------------------------------------------------------
			//Converting list of values to Comma separated String of values
			StringBuilder strbul=new StringBuilder();
	        for(SecurityRoleIdDetais str : migrationId.getListOfRoleId())
	        {
	            strbul.append(str.getRoleId());
	            strbul.append(",");
	        }
	        strbul.setLength(strbul.length()-1);
	        String roleIDs = strbul.toString();
			//--------------------------------------------------------
			
			EntityManager em = emf.createEntityManager();
			try {
				
				Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
				
				String storedProcName = "";
				if (sourceEnvType.equalsIgnoreCase("DEV") && destinationEnvType.equalsIgnoreCase("TEST")) {
					storedProcName = "RIM_MIGRATION_FROM_DEV_TO_TEST_SECURITY";
				} else if (sourceEnvType.equalsIgnoreCase("TEST") && destinationEnvType.equalsIgnoreCase("DEV")) {
					storedProcName = "RIM_MIGRATION_FROM_TEST_TO_DEV_SECURITY";
				}
				
				StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcName)
											 .registerStoredProcedureParameter("roleIdList", String.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("totalCount", Integer.class, ParameterMode.OUT)
											 .registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
											 .setParameter("roleIdList", roleIDs);
				
				query.execute();
				String result = (String) query.getOutputParameterValue("result");
				Integer totalCount = (Integer) query.getOutputParameterValue("totalCount");
				
				log.info("  Result  : " + result);
				log.info("  Total count  : " + totalCount);
				
				migrationStatusMap.put("Status", result);
				migrationStatusMap.put("Count", totalCount);
				return migrationStatusMap;
			} catch (Exception e) {
				log.error(String.format("Data has not been updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
		return null;
	}
}
