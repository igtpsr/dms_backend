package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The persistent class for the EXT_VALIDATION_METADATA database table.
 * 
 */
@Entity
@Table(name="EXT_VALIDATION_METADATA")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class ExtValidationMetaData  implements Serializable {
	
	private static final long serialVersionUID = 1L;


	@EmbeddedId
	private ExtValidationMetaDataPK id;
	
	@Column(name = "PROP_VALUE", unique=true, nullable = false, length = 400)
	private String propValue;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER", length = 18)
	private String modifyUser;

}
