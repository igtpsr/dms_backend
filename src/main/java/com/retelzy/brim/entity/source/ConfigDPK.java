package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class ConfigDPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -289552350408778737L;

	@Column(name = "CONFIG_ID")
	private String configId;

	@Column(name = "CONFIG_TYPE")
	private String configType;

	@Column(name = "PROP_NAME")
	private String propName;

}
