package com.retelzy.brim.db.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "destinationEntityManagerFactory", basePackages = {
		"com.retelzy.brim.entity.destination.dao" }, transactionManagerRef = "destinationTransactionManagerRef")
public class DestinationDbConfig {

	@Autowired
	private Environment environment;

	@Bean(name = "destinationDatasource")
	public DataSource destinationDataSource() {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName(environment.getProperty("spring.destination.datasource.driverClassName"));
		dataSource.setJdbcUrl(environment.getProperty("spring.destination.datasource.jdbcUrl"));
		dataSource.setUsername(environment.getProperty("spring.destination.datasource.username"));
		dataSource.setPassword(environment.getProperty("spring.destination.datasource.password"));
		dataSource.setIdleTimeout(300000);
		dataSource.setConnectionTimeout(20000);
		dataSource.setAutoCommit(false);
		dataSource.setMaximumPoolSize(50);
		dataSource.setMaxLifetime(1200000);
		dataSource.setMinimumIdle(10);
		dataSource.setPoolName("Destination");

		return dataSource;

		/* return DataSourceBuilder.create().build(); */
	}

	@Bean(name = "destinationMultiRoutingDataSource")
	public DataSource destinationMultiRoutingDataSource() {
		Map<Object, Object> targetDataSources = new HashMap<>();
		targetDataSources.put(DBTypeEnum.DESTINATION, destinationDataSource());

		DestinationDBMultiRoutingDataSource multiRoutingDataSource = new DestinationDBMultiRoutingDataSource();
		multiRoutingDataSource.setDefaultTargetDataSource(destinationDataSource());
		multiRoutingDataSource.setTargetDataSources(targetDataSources);
		return multiRoutingDataSource;

	}

	@Bean(name = "destinationEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder builder,
			@Qualifier("destinationMultiRoutingDataSource") DataSource dataSource) {
		Map<String, Object> properties = new LinkedHashMap<String, Object>();
		properties.put("hibernate.hbm2ddl.auto", "none");
		properties.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		properties.put("hibernate.show_sql", "true");
		properties.put("connection.release_mode", "auto");
		return builder.dataSource(dataSource).properties(properties).packages("com.retelzy.brim.entity.destination")
				.persistenceUnit("Destination").build();
	}

	@Bean(name = "destinationTransactionManagerRef")
	public PlatformTransactionManager platformTransactionManager(
			@Qualifier("destinationEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory);
		jpaTransactionManager.setNestedTransactionAllowed(true);
		jpaTransactionManager.setRollbackOnCommitFailure(true);
		return jpaTransactionManager;
	}

}
