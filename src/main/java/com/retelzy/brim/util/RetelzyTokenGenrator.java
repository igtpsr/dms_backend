package com.retelzy.brim.util;

import java.nio.charset.Charset;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

@Component
public class RetelzyTokenGenrator {

	public static String generateToken(String username) {
		try {
			String secretKey = username + new Date().toString();

			byte[] array = new byte[256];
			new Random().nextBytes(array);

			String saltKey = new String(array, Charset.forName("UTF-8"));

			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), saltKey.getBytes(), 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKeySpec = new SecretKeySpec(tmp.getEncoded(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivspec);
			String tokenString = cipher.doFinal(saltKey.getBytes("UTF-8")).toString() + System.currentTimeMillis()
					+ System.nanoTime() + System.currentTimeMillis();
			System.out.println("Token String :" + tokenString);
			return Base64.getEncoder().encodeToString(tokenString.getBytes());
		} catch (Exception e) {
			System.out.println("Error while encrypting: " + e.toString());
			byte[] array = new byte[256];
			new Random().nextBytes(array);

			String saltKey = new String(array, Charset.forName("UTF-8"));
			return saltKey;
		}

	}

	/*
	 * private String secret = "retelzy";
	 * 
	 * public String extractUsername(String token) { return extractClaim(token,
	 * Claims::getSubject); }
	 * 
	 * public Date extractExpiration(String token) { return extractClaim(token,
	 * Claims::getExpiration); }
	 * 
	 * public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
	 * final Claims claims = extractAllClaims(token); return
	 * claimsResolver.apply(claims); }
	 * 
	 * private Claims extractAllClaims(String token) { return
	 * Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody(); }
	 * 
	 * private Boolean isTokenExpired(String token) { return
	 * extractExpiration(token).before(new Date()); }
	 * 
	 * public String generateToken(String username) { Map<String, Object> claims =
	 * new HashMap<>(); return createToken(claims, username); }
	 * 
	 * private String createToken(Map<String, Object> claims, String subject) {
	 * 
	 * return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new
	 * Date(System.currentTimeMillis())) .setExpiration(new
	 * Date(System.currentTimeMillis() + 100000)).signWith(SignatureAlgorithm.HS256,
	 * secret) .compact(); }
	 * 
	 * public Boolean validateToken(String token, UserDetails userDetails) { final
	 * String username = extractUsername(token); return
	 * (username.equals(userDetails.getUsername()) && !isTokenExpired(token)); }
	 */

}
