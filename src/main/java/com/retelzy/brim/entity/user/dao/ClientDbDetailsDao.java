package com.retelzy.brim.entity.user.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.user.ClientDbDetails;

@Repository
public interface ClientDbDetailsDao extends CrudRepository<ClientDbDetails, Integer> {

	public Optional<ClientDbDetails> findByDbEnvironmentTypeIn(List<String> listOfEnvironmentType);

	@Query("from ClientDbDetails")
	public List<ClientDbDetails> getTheConfigurationAppDetails();

	public Optional<ClientDbDetails> findByDbEnvironmentType(String dbEnvName);

}
