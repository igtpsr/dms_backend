package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutI18nRes;
import com.retelzy.brim.entity.destination.LayoutI18nResPK;

@Repository
public interface DestinationLayoutI18nResDao extends CrudRepository<LayoutI18nRes, LayoutI18nResPK> {


}
