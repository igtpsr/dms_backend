package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.DocView;
import com.retelzy.brim.entity.destination.DocViewPK;

@Repository
public interface DestinationDocViewDao extends CrudRepository<DocView, DocViewPK> {


}
