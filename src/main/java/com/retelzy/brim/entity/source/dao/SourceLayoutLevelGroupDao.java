package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutLevelGroup;
import com.retelzy.brim.entity.source.LayoutLevelGroupPK;

@Repository
public interface SourceLayoutLevelGroupDao extends CrudRepository<LayoutLevelGroup, LayoutLevelGroupPK> {


	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);


}
