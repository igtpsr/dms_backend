package com.retelzy.brim.db.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.xerces.util.DOMUtil;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.entity.destination.DocViewElementD;
import com.retelzy.brim.entity.destination.DocViewElementDPK;
import com.retelzy.brim.entity.destination.SequenceNumbers;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.ExtValidation;
import com.retelzy.brim.entity.source.LayoutProfile;
import com.retelzy.brim.entity.source.QueryH;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.migration.model.MigrationId;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.Helper;

@Component
public class MigrationDbUtil {

	@Autowired
	private SourceTableDataRetrival sourceTableDataRetrival;

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private ConfigFileReader configFileReader;

	private static final Logger logger = LoggerFactory.getLogger(MigrationDbUtil.class);

	public List<Map<String, List<Object>>> generateInsertSQL(String[] securityTableInsertSeq, Integer roleId) {

		List<Map<String, List<Object>>> listOfInsertObjectByTableName = new ArrayList<Map<String, List<Object>>>();

		try {

			for (Integer index = 0; index < securityTableInsertSeq.length; index++) {
				String className = getTheEntityClassName(securityTableInsertSeq[index]);

				Map<String, List<Object>> insertObject = sourceTableDataRetrival.getTheDataFromSourceTable(className,
						roleId, null, null);
				listOfInsertObjectByTableName.add(insertObject);

			}

			logger.info("Before Calling Filter Method Total Size Of The List :" + listOfInsertObjectByTableName.size());

			// migrate security filters queries and resolve any query id confilt in
			// destination database
			listOfInsertObjectByTableName = migrateSecurityFilterQuery(listOfInsertObjectByTableName, roleId);

			logger.info("After Calling Filter Method Total Size Of The List :" + listOfInsertObjectByTableName.size());

			// migrate ALL custom process assigned to Role to destination database
			List<Integer> listOfRoleId = getDestRoleIdList(roleId);

			logger.info("Role Id List We Got :" + listOfRoleId.size());

			// migrate ALL custom process assigned to Role to destination database
			listOfInsertObjectByTableName = migrateAllCustomProcess(roleId, listOfRoleId,
					listOfInsertObjectByTableName);
			logger.info("After Calling migrateAllCustomProcess() method Total size of the List is :"
					+ listOfInsertObjectByTableName.size());

			// migrate ALL custom menues assigned to Role to destination database
			listOfInsertObjectByTableName = migrateAllCustomMenu(listOfInsertObjectByTableName, roleId, listOfRoleId);

			logger.info("After Calling migrateAllCustomMenu() method Total size of the List is :"
					+ listOfInsertObjectByTableName.size());

		} catch (Exception e) {
			logger.error("Exception Occured In generateInsertSQL() method of MigrationDbUtil class :" + e.getMessage());
		}

		return listOfInsertObjectByTableName;

	}

	private List<Map<String, List<Object>>> migrateSecurityFilterQuery(
			List<Map<String, List<Object>>> listOfInsertObjectByTableName, Integer roleId) {

		Map<Integer, String> sourceQueryIdMap = new TreeMap<Integer, String>();

		sourceQueryIdMap = getSecQueryIdMap(sourceQueryIdMap, "SEC_BO_FILTER", roleId, null);
		sourceQueryIdMap = getSecQueryIdMap(sourceQueryIdMap, "SEC_BOINT_FILTER", roleId, null);
		sourceQueryIdMap = getSecQueryIdMap(sourceQueryIdMap, "SEC_BO_FIELD_ACL", roleId, null);
		sourceQueryIdMap = getSecQueryIdMap(sourceQueryIdMap, "SEC_BO_LEVEL_ACL", roleId, null);

		logger.info("Total Key And Value Exist In sourceQueryIdMap :" + sourceQueryIdMap.size());

		// replace the query id - if the query id already exists in destination database
		Map<Integer, String> queryIdMap = replaceQueryIdIfExist(sourceQueryIdMap);

		if (!queryIdMap.isEmpty()) {
			Integer key = getKey(queryIdMap, "QUERY_ID");
			logger.info("key :" + key);
			Map<String, List<Object>> secuenceNumberMap = new LinkedHashMap<String, List<Object>>();
			List<Object> listOfSequenceNumberAsObject = new ArrayList<Object>();
			SequenceNumbers sequenceNumbers = new SequenceNumbers();
			sequenceNumbers.setCurrentnum(key);
			listOfSequenceNumberAsObject.add(sequenceNumbers);
			secuenceNumberMap.put("SequenceNumbers", listOfSequenceNumberAsObject);
			listOfInsertObjectByTableName.add(secuenceNumberMap);
		}

		logger.info("Source Query Map :" + sourceQueryIdMap);

		// get the Insert SQL statements for security tables
		String secBoFilter = getTheEntityClassName("SEC_BO_FILTER");
		String secBointFilter = getTheEntityClassName("SEC_BOINT_FILTER");
		String secBoFieldAcl = getTheEntityClassName("SEC_BO_FIELD_ACL");
		String secBoLevelAcl = getTheEntityClassName("SEC_BO_LEVEL_ACL");

		Map<String, List<Object>> secBoFilterInsertObject = sourceTableDataRetrival
				.getTheDataFromSourceTable(queryIdMap, secBoFilter, roleId, null);
		listOfInsertObjectByTableName.add(secBoFilterInsertObject);

		Map<String, List<Object>> secBointFilterInsertObject = sourceTableDataRetrival
				.getTheDataFromSourceTable(queryIdMap, secBointFilter, roleId, null);
		listOfInsertObjectByTableName.add(secBointFilterInsertObject);

		Map<String, List<Object>> secBoFieldAclInsertObject = sourceTableDataRetrival
				.getTheDataFromSourceTable(queryIdMap, secBoFieldAcl, roleId, null);
		listOfInsertObjectByTableName.add(secBoFieldAclInsertObject);

		Map<String, List<Object>> secBoLevelAclInsertObject = sourceTableDataRetrival
				.getTheDataFromSourceTable(queryIdMap, secBoLevelAcl, roleId, null);
		listOfInsertObjectByTableName.add(secBoLevelAclInsertObject);

		// this method generate XML files (query_h.xml and query_d.xml) which contains
		// validation queries for security
		try {
			generateQueryXML2ExportQueryData(roleId, sourceQueryIdMap, queryIdMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In generateQueryXML2ExportQueryData() method :" + e.getMessage());
		}

		return listOfInsertObjectByTableName;
	}

	public void generateQueryXML2ExportQueryData(Integer roleId, Map<Integer, String> sourceQueryIdMap,
			Map<Integer, String> queryIdMap) throws Exception {

		logger.info("sourceQueryIdMap :" + sourceQueryIdMap);
		logger.info("queryIdMap :" + queryIdMap);

		IDatabaseConnection connection = null;
		String queryList = ConfigFileReader.mapKeyToString(sourceQueryIdMap, ",");
		logger.info("Query List :" + queryList);

		if (queryList.length() > 0) {
			// Queries does not exists in destination database so create Queries XML to
			// insert queries
			String migrateQueryHSQL = "select * from query_h where query_id in (" + queryList + ") order by query_id";
			String migrateQueryDSQL = "select * from query_d where query_id in (" + queryList + ") order by query_id";
			System.out.println("\nExporting the security data for security role " + roleId
					+ " from source database.Please wait.........");
			connection = sourceDbService.getConnection();
			try {
				QueryDataSet partialDataSet = new QueryDataSet(connection);
				partialDataSet.addTable("query_h", migrateQueryHSQL);
				FlatXmlDataSet.write(partialDataSet, new FileOutputStream(
						configFileReader.getPropertyValue("dir.path.source") + "/query_h_insert" + roleId + ".xml"));
				// replace the query id - if the query id already exists in destination database
				replaceQueryId(queryIdMap, "/query_h_insert" + roleId + ".xml");

				QueryDataSet partialDataSet2 = new QueryDataSet(connection);
				partialDataSet2.addTable("query_d", migrateQueryDSQL);
				FlatXmlDataSet.write(partialDataSet2, new FileOutputStream(
						configFileReader.getPropertyValue("dir.path.source") + "/query_d_insert" + roleId + ".xml"));

				// replace the query id - if the query id already exists in destination database
				replaceQueryId(queryIdMap, "/query_d_insert" + roleId + ".xml");
			} catch (Exception e) {
				logger.error("Exception Occured while inserting Of QueryH And QueryD Table :" + e.getMessage());
				throw new MigrationException("Insertion Failed");
			} finally {
				connection.close();
			}
		}

	}

	/**
	 * Replace the query id in XML with new query id from the map The map will have
	 * new query id if the same query id is been used in destination database for
	 * some other query
	 *
	 * @param queryIdMap
	 * @param xmlFile
	 * @throws IOException
	 */
	private void replaceQueryId(Map<Integer, String> queryIdMap, String xmlFile) throws IOException {
		// conflict in Query Ids
		if (queryIdMap.size() > 0) {
			// read XML file with query and replace the query id in XML file
			FileReader fileRdr = new FileReader(configFileReader.getPropertyValue("dir.path.source") + xmlFile);

			if (fileRdr != null) {
				BufferedReader b = new BufferedReader(fileRdr);
				String xmlStr = "", temp = "";
				do {
					temp = b.readLine();
					if (temp != null)
						xmlStr = xmlStr + temp;
				} while (temp != null);

				// replace the QUERY_ID attribute in xml based on the query id map
				xmlStr = parseQueryXmlAndReplaceId(queryIdMap, xmlStr);

				FileWriter fw = new FileWriter(
						new File(configFileReader.getPropertyValue("dir.path.source") + xmlFile));
				PrintWriter pwQuery = new PrintWriter(fw);
				pwQuery.print(xmlStr);
				fw.close();
				b.close();
			}
		}
	}

	/**
	 * This method replace the QUERY_ID attribute in xml based on the query id map .
	 * The map contains the query id mapping fro source and destination database so
	 * if there confilts in query_id in source and destination database then source
	 * query_id will be mapped to next query_id from destination
	 *
	 * @param queryIdMap
	 * @param queryXml
	 * @return
	 */
	private String parseQueryXmlAndReplaceId(Map<Integer, String> queryIdMap, String queryXml) {
		String outputXml = "";
		try {
			Document document = ConfigFileReader.getXMLDocument(queryXml);
			Node documentNode = DOMUtil.getFirstChildElement(document, "dataset");

			if (documentNode.hasChildNodes()) {
				Element boElement = DOMUtil.getFirstChildElement(documentNode);
				while (boElement != null) {
					if (boElement.getNodeType() == Node.ELEMENT_NODE) {
						String queryId = boElement.getAttribute("QUERY_ID");

						Iterator<Integer> hkeySet = queryIdMap.keySet().iterator();
						while (hkeySet.hasNext()) {
							Integer queryKey = hkeySet.next();
							String queryVal = (String) queryIdMap.get(queryKey);
							if (queryKey.toString().equals(queryId)) {
								boElement.setAttribute("QUERY_ID", queryVal);
							}
						}

					}
					boElement = DOMUtil.getNextSiblingElement(boElement);
				}
				outputXml = ConfigFileReader.documentToString(document.getDocumentElement());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In parseQueryXmlAndReplaceId() method :" + e.getMessage());
		}
		return outputXml;
	}

	private Map<Integer, String> getSecQueryIdMap(Map<Integer, String> sourceQueryIdMap, String table, Integer roleId,
			Integer layoutId) {

		String className = getTheEntityClassName(table);

		switch (className) {
		case "SecBoFilter":
			List<Integer> listOfQueryIdOfSecBoFilter = sourceDbService.getAllQueryIdByRoleId(roleId, className);

			logger.info("Number Of Query Id We Got For SecBoFilter:" + listOfQueryIdOfSecBoFilter.size());

			List<QueryH> listOfQueryHForSecBoFilter = sourceDbService.getallTheQueryDetails(listOfQueryIdOfSecBoFilter);

			logger.info("Number Of QueryH table details we got For SecBoFilter:" + listOfQueryHForSecBoFilter.size());

			for (QueryH queryH : listOfQueryHForSecBoFilter) {
				if (!sourceQueryIdMap.containsKey(queryH.getQueryId()))
					sourceQueryIdMap.put(queryH.getQueryId(), queryH.getQueryName());
			}
			return sourceQueryIdMap;

		case "SecBointFilter":
			List<Integer> listOfQueryIdOfSecBointFilter = sourceDbService.getAllQueryIdByRoleId(roleId, className);

			logger.info("Number Of Query Id We Got For SecBointFilter:" + listOfQueryIdOfSecBointFilter.size());

			List<QueryH> listOfQueryHForSecBointFilter = sourceDbService
					.getallTheQueryDetails(listOfQueryIdOfSecBointFilter);

			logger.info(
					"Number Of QueryH table details we got For SecBointFilter:" + listOfQueryHForSecBointFilter.size());

			for (QueryH queryH : listOfQueryHForSecBointFilter) {
				if (!sourceQueryIdMap.containsKey(queryH.getQueryId()))
					sourceQueryIdMap.put(queryH.getQueryId(), queryH.getQueryName());
			}
			return sourceQueryIdMap;

		case "SecBoFieldAcl":
			List<Integer> listOfQueryIdOfSecBoFieldAcl = sourceDbService.getAllQueryIdByRoleId(roleId, className);

			logger.info("Number Of Query Id We Got For SecBoFieldAcl:" + listOfQueryIdOfSecBoFieldAcl.size());

			List<QueryH> listOfQueryHForSecBoFieldAcl = sourceDbService
					.getallTheQueryDetails(listOfQueryIdOfSecBoFieldAcl);

			logger.info(
					"Number Of QueryH table details we got For SecBoFieldAcl:" + listOfQueryHForSecBoFieldAcl.size());

			for (QueryH queryH : listOfQueryHForSecBoFieldAcl) {
				if (!sourceQueryIdMap.containsKey(queryH.getQueryId()))
					sourceQueryIdMap.put(queryH.getQueryId(), queryH.getQueryName());
			}
			return sourceQueryIdMap;

		case "SecBoLevelAcl":
			List<Integer> listOfQueryIdOfSecBoLevelAcl = sourceDbService.getAllQueryIdByRoleId(roleId, className);

			logger.info("Number Of Query Id We Got For SecBoFieldAcl:" + listOfQueryIdOfSecBoLevelAcl.size());

			List<QueryH> listOfQueryHForSecBoLevelAcl = sourceDbService
					.getallTheQueryDetails(listOfQueryIdOfSecBoLevelAcl);

			logger.info(
					"Number Of QueryH table details we got For SecBoFieldAcl:" + listOfQueryHForSecBoLevelAcl.size());

			for (QueryH queryH : listOfQueryHForSecBoLevelAcl) {
				if (!sourceQueryIdMap.containsKey(queryH.getQueryId()))
					sourceQueryIdMap.put(queryH.getQueryId(), queryH.getQueryName());
			}
			return sourceQueryIdMap;

		default:
			logger.error("Securiry Filter Table Not added in Switch Case :" + className);
			return sourceQueryIdMap;

		}

	}

	// replace the query id - if the query id already exists in destination database
	@SuppressWarnings("unlikely-arg-type")
	private Map<Integer, String> replaceQueryIdIfExist(Map<Integer, String> sourceQueryIdMap) {

		Map<Integer, String> queryIdMap = new TreeMap<Integer, String>();

		boolean flagUsed = false;
		Integer nextQueryId = null;

		// get the max query_id +1 from DESTINATION database to assign as new query_id
		// in case of conflict - same validation id been used in destination database
		Integer maxQueryId = destinationDbService.getTheMaxQueryIdFromQuerH();

		logger.info("Max Query Id We Got :" + maxQueryId);

		Iterator<Integer> sourceItr = sourceQueryIdMap.keySet().iterator();

		while (sourceItr.hasNext()) {

			// check the DESTINATION db if the validation query id exists - if yes then
			// there is conflict
			Integer queryId = sourceItr.next();
			String queryName = sourceQueryIdMap.get(queryId);

			// check the DESTINATION db if the validation query id exists - if yes then
			// there is conflict
			Integer checkQueryId = destinationDbService.getTheDestinationIdCounNumber(queryId, queryName);

			if (checkQueryId != null && checkQueryId >= 1) {
				continue;
			} else {
				checkQueryId = destinationDbService.getTheDestinationIdCounNumberByQueryId(queryId);

				if (checkQueryId != null && checkQueryId >= 1 || queryIdMap.containsValue(queryId)) {

					List<Integer> listOfQueryId = destinationDbService.getTheQueryIdByQueryName(queryName);

					if (listOfQueryId.isEmpty()) {
						queryIdMap.put(queryId, listOfQueryId.get(0).toString());
					} else {
						// flag is used when there is multiple queries id need to be migrated to
						// destination database
						if (!flagUsed && maxQueryId != null) {
							nextQueryId = maxQueryId;
							flagUsed = true;
						} else {
							nextQueryId = new Integer(nextQueryId.intValue() + 1);
						}
						queryIdMap.put(queryId, nextQueryId.toString());
					}

				}
			}
		}

		if (flagUsed) {
			// update sequnec numbers in destination database if the new query id is
			// assigned
			if (nextQueryId != null) {
				String sequenceName = "QUERY_ID";
//				Integer sequenceNoUpdateStatus = destinationDbService.updateTheSequnceNumberBySequenceName(nextQueryId,
//						sequenceName);
				queryIdMap.put(nextQueryId, sequenceName);
				// logger.info("Sequence Number Updated Status :" + sequenceNoUpdateStatus);

			}

		}

		return queryIdMap;

	}

	private List<Integer> getDestRoleIdList(Integer roleId) {

		List<Integer> listOfRoleIdsFromDestinationDB = destinationDbService.getTheRoleIdsFromDestinationDB(roleId);
		return listOfRoleIdsFromDestinationDB;
	}

	public String getTheEntityClassName(String tableName) {

		logger.info("Table Name We Got :" + tableName);

		String className = "";

		String[] splitClassName = tableName.split("_");

		for (String name : splitClassName) {

			for (Integer index = 0; index < name.length(); index++) {

				if (index == 0) {
					className += Character.toString(name.charAt(index));
				} else {
					className += Character.toString(name.charAt(index)).toLowerCase();
				}

			}
		}

		logger.info("Class Name We Sent :" + className);
		return className;
	}

	private List<Map<String, List<Object>>> migrateAllCustomProcess(Integer roleId, List<Integer> listOfRoleId,
			List<Map<String, List<Object>>> listOfInsertObjectByTableName) {

		Map<String, List<Object>> mapOfObject = new LinkedHashMap<String, List<Object>>();

		// get the Insert SQL statements export all custom process
		// All process above 10000 are custom process so we export all custom process
		String className = getTheEntityClassName("SEC_BO_PROCESS");
		mapOfObject = sourceTableDataRetrival.getTheDataFromSourceTable(className, roleId, null, null);
		listOfInsertObjectByTableName.add(mapOfObject);

		// SEC_BOVIEW_RES table contains dov_view_id and res_id mapping - copy all
		// entries for custom process
		className = getTheEntityClassName("SEC_BOVIEW_RES");
		Integer minProcessId = 10000;
		List<Integer> listOfProcessId = sourceDbService.getByIdProcessIdGreaterThanEqual(minProcessId);
		logger.info("List Of Process Id :" + listOfProcessId.size());
		Integer typeId = 1;
		List<Object> listOfSecBoviewResAsObject = sourceDbService.getTheDetailsOfSecBoviewTable(listOfProcessId,
				typeId);
		mapOfObject = new LinkedHashMap<String, List<Object>>();
		mapOfObject.put("CustomProcess" + className, listOfSecBoviewResAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		// SMU_RES_SHOW_ATTR contains attributes (URL , POSITION) for custom process
		className = getTheEntityClassName("SMU_RES_SHOW_ATTR");
		Integer smuTypeId = 1;
		List<Object> listOfSmuResShowAttrAsObject = sourceDbService.getTheSmuResShowAttrDetails(listOfProcessId,
				smuTypeId);
		mapOfObject = new LinkedHashMap<String, List<Object>>();
		mapOfObject.put("CustomProcess" + className, listOfSmuResShowAttrAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		/*
		 * By default new custom process is disabled for all roles and user has ability
		 * to enable and disable custom process for role- These Inserts cover that
		 * scenario
		 */
		className = getTheEntityClassName("SEC_ROLE_RES_CTL");
		Integer minRoleId = 100000;
		List<Object> listOfSecRoleResCtlAsObject = sourceDbService.getAllTheSecRoleResCtlDetails(minRoleId, roleId,
				listOfProcessId, listOfRoleId);
		mapOfObject = new LinkedHashMap<String, List<Object>>();
		;
		mapOfObject.put("CustomProcess" + className, listOfSecRoleResCtlAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		return listOfInsertObjectByTableName;
	}

	/**
	 * This method migrates ALL the custom menues -since we cannot easily get custom
	 * menu attached only to role Also it makes the source and destination database
	 * in SYNC with custom menus
	 *
	 * @param sourceCon
	 * @param pw
	 * @param securityInsertSql
	 * @param roleId
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private List<Map<String, List<Object>>> migrateAllCustomMenu(
			List<Map<String, List<Object>>> listOfInsertObjectByTableName, Integer roleId, List<Integer> listOfRoleId) {

		Map<String, List<Object>> mapOfObject = new LinkedHashMap<String, List<Object>>();
		Integer menuItemId = 5000000;
		Integer minRoleId = 100000;

		// get the Insert SQL statements export all custom menu
		// All menu above 5000000 are custom menu so we export all custom menu
		String className = getTheEntityClassName("SMU_COMMON");
		List<Object> listOfSmuCommonAsObject = sourceDbService.getSmuCommonAllDetails(menuItemId);
		mapOfObject.put("CustomMenu" + className, listOfSmuCommonAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		// SMU_ROLE_ROOT_MENU table contains role attached to menu
		className = getTheEntityClassName("SMU_ROLE_ROOT_MENU");
		List<Object> listOfSmuRoleRootMenuAsObject = sourceDbService.getSmuRoleRootMenuDetails(menuItemId, minRoleId,
				roleId, listOfRoleId);
		mapOfObject = new LinkedHashMap<String, List<Object>>();
		mapOfObject.put("CustomMenu" + className, listOfSmuRoleRootMenuAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		// SMU_ROLE_MENU_ITEM contains access right for menu
		className = getTheEntityClassName("SMU_ROLE_MENU_ITEM");
		List<Object> listOfSmuRoleRootMenuItemAsObject = sourceDbService.getSmuRoleRootMenuItemDetails(menuItemId,
				minRoleId, roleId, listOfRoleId);
		mapOfObject = new LinkedHashMap<String, List<Object>>();
		mapOfObject.put("CustomMenu" + className, listOfSmuRoleRootMenuItemAsObject);
		listOfInsertObjectByTableName.add(mapOfObject);

		return listOfInsertObjectByTableName;
	}

	@SuppressWarnings("hiding")
	private static <Integer, String> Integer getKey(Map<Integer, String> map, String value) {
		return map.entrySet().parallelStream().filter(entry -> value.equals(entry.getValue())).map(Map.Entry::getKey)
				.findFirst().get();
	}

	public Integer checkLayoutIdMap(Integer layoutId) {
		Integer newLayoutId = null;

		try {
			List<Object> sourceLayoutData = sourceDbService.getLayoutProfileInfo(layoutId);
			List<Object> destinationLayoutData = destinationDbService.getLayoutProfileInfo(layoutId);

			for (Object destobj : destinationLayoutData) {
				com.retelzy.brim.entity.destination.LayoutProfile destlp = (com.retelzy.brim.entity.destination.LayoutProfile) destobj;
				logger.info("Dest Layout ID: " + destlp.getLayoutId());
				logger.info("Dest Layout DESC: " + destlp.getLayoutDesc());
				for (Object srcobj : sourceLayoutData) {
					LayoutProfile srclp = (LayoutProfile) srcobj;
					logger.info("Src Layout ID: " + srclp.getLayoutId());
					logger.info("Src Layout DESC: " + srclp.getLayoutDesc());
					if (!destlp.getLayoutDesc().equalsIgnoreCase(srclp.getLayoutDesc())) {
						newLayoutId = destinationDbService.getLayoutID();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured in checkLayoutIdMap() method of MigrationDbUtil class :" + e.getMessage());
		}
		return newLayoutId;
	}

	public List<Map<java.lang.String, List<Object>>> generateLayoutInsertSQL(java.lang.String[] layoutTableInsertSeq,
			java.lang.Integer layoutId, java.lang.Integer newLayoutId, MigrationId migrationId) {

		List<Map<String, List<Object>>> listOfInsertObjectByTableName = new ArrayList<Map<String, List<Object>>>();

		try {

			for (Integer index = 0; index < layoutTableInsertSeq.length; index++) {
				String className = getTheEntityClassName(layoutTableInsertSeq[index]);
				Map<String, List<Object>> insertObject = sourceTableDataRetrival.getTheDataFromSourceTable(className,
						null, layoutId, migrationId);
				listOfInsertObjectByTableName.add(insertObject);

			}

			logger.info("Size of listOfInsertObjectByTableName-->" + listOfInsertObjectByTableName.size());

			// migrate validation queries and resolve any query id conflict in destination
			// database
			migrateValidationQuery(listOfInsertObjectByTableName, layoutId);

			Map<Integer, String> validationIdMap = null;

			// migrate PLM new validation to destination database
			validationIdMap = migrateNewValidation(listOfInsertObjectByTableName, layoutId);
			
			System.out.print(validationIdMap); // Added by LOKESH for testing

			// get the list of insert New Validation tables From properties
			String[] newValidationInsertSeq = ConfigFileReader
					.toArray(configFileReader.getInsertNewValidationTableSequence());

			System.out.print(newValidationInsertSeq); //Added by LOKESH for Testing
			
			if (validationIdMap != null && validationIdMap.size() > 0) {
				// get the where condition of the new validation

				for (Integer index = 0; index < newValidationInsertSeq.length; index++) {
					String className = getTheEntityClassName(newValidationInsertSeq[index]);
					Map<String, List<Object>> insertObject = sourceTableDataRetrival
							.getTheDataFromSourceTable(validationIdMap, className, null, migrationId);
					listOfInsertObjectByTableName.add(insertObject);
				}

			}

			if (newLayoutId != null && newLayoutId.intValue() >= 100000) {
				System.out.println(
						"NOTE: The input layout id with different layout description exists in destination database, use new layout id : "
								+ newLayoutId + " in destination database instead.\n");
				Map<String, List<Object>> secuenceNumberMap = new LinkedHashMap<String, List<Object>>();
				List<Object> listOfSequenceNumberAsObject = new ArrayList<Object>();
				SequenceNumbers sequenceNumbers = new SequenceNumbers();
				sequenceNumbers.setCurrentnum(newLayoutId);
				listOfSequenceNumberAsObject.add(sequenceNumbers);
				secuenceNumberMap.put("LayoutUpdateSequenceNumbers", listOfSequenceNumberAsObject);
				listOfInsertObjectByTableName.add(secuenceNumberMap);
			}

		} catch (Exception e) {
			logger.error("Exception Occured In generateLayoutInsertSQL() method of MigrationDbUtil class :"
					+ e.getMessage());
		}

		return listOfInsertObjectByTableName;
	}

	private void migrateValidationQuery(List<Map<String, List<Object>>> listOfInsertObjectByTableName,
			Integer layoutId) {

		Map<Integer, String> sourceQueryIdMap = new TreeMap<Integer, String>();

		sourceQueryIdMap = getLayoutQueryIdMap(sourceQueryIdMap, layoutId);

		logger.info("Total Key And Value Exist In sourceQueryIdMap :" + sourceQueryIdMap.size());

		// replace the query id - if the query id already exists in destination database
		Map<Integer, String> queryIdMap = replaceQueryIdIfExist(sourceQueryIdMap);

		if (!queryIdMap.isEmpty()) {

			Map<String, List<Object>> docViewElementDMap = new LinkedHashMap<String, List<Object>>();
			List<Object> listOfDocViewElementDList = new ArrayList<Object>();
			Iterator<Integer> itr = queryIdMap.keySet().iterator();
			while (itr.hasNext()) {
				Integer queryKey = (Integer) itr.next();
				String queryVal = (String) queryIdMap.get(queryKey);
				DocViewElementD docViewElement = new DocViewElementD();
				docViewElement.setPropValue(queryVal);

				DocViewElementDPK docViewElementPk = new DocViewElementDPK();
				docViewElementPk.setPropName(Integer.toString(queryKey));
				docViewElementPk.setLayoutId(layoutId);

				docViewElement.setId(docViewElementPk);

				listOfDocViewElementDList.add(docViewElement);
			}
			if (!listOfDocViewElementDList.isEmpty()) {
				docViewElementDMap.put("UpdateDocViewElementD", listOfDocViewElementDList);
				listOfInsertObjectByTableName.add(docViewElementDMap);
			}

			Integer key = getKey(queryIdMap, "QUERY_ID");
			logger.info("key :" + key);
			Map<String, List<Object>> secuenceNumberMap = new LinkedHashMap<String, List<Object>>();
			List<Object> listOfSequenceNumberAsObject = new ArrayList<Object>();
			SequenceNumbers sequenceNumbers = new SequenceNumbers();
			sequenceNumbers.setCurrentnum(key);
			listOfSequenceNumberAsObject.add(sequenceNumbers);
			secuenceNumberMap.put("UpdateSequenceNumbers", listOfSequenceNumberAsObject);
			listOfInsertObjectByTableName.add(secuenceNumberMap);

		}

		logger.info("Source Query Map :" + sourceQueryIdMap);

		// this method generate XML files (query_h.xml and query_d.xml) which contains
		// validation queries for security
		try {
			generateQueryXML2ExportQueryDataForLayout(layoutId, sourceQueryIdMap, queryIdMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In generateQueryXML2ExportQueryDataForLayout() method :" + e.getMessage());
		}

	}

	private Map<java.lang.Integer, java.lang.String> getLayoutQueryIdMap(
			Map<java.lang.Integer, java.lang.String> sourceQueryIdMap, java.lang.Integer layoutId) {
		Map<Integer, String> sourceIdMap = new TreeMap<Integer, String>();
		List<QueryH> queryInfo = sourceDbService.getQueryInfo(layoutId);
		for (QueryH queryH : queryInfo) {
			sourceIdMap.put(queryH.getQueryId(), queryH.getQueryName());
		}
		return sourceIdMap;
	}

	private Map<Integer, String> migrateNewValidation(List<Map<String, List<Object>>> listOfInsertObjectByTableName,
			Integer layoutId) {

		Map<Integer, String> sourceQueryIdMap = new TreeMap<Integer, String>();

		sourceQueryIdMap = getSourceNewValidationIdMap(sourceQueryIdMap, layoutId);

		logger.info("Total Key And Value Exist In sourceQueryIdMap :" + sourceQueryIdMap.size());

		// replace the validation id - if the validation id already exists in
		// destination database
		Map<Integer, String> queryIdMap = replaceValidationIdIfExist(sourceQueryIdMap);

		logger.info("Total Key And Value Exist In queryIdMap :" + queryIdMap.size());

		if (!queryIdMap.isEmpty()) {

			Map<String, List<Object>> docViewElementDMap = new LinkedHashMap<String, List<Object>>();
			List<Object> listOfDocViewElementDList = new ArrayList<Object>();
			Iterator<Integer> itr = queryIdMap.keySet().iterator();
			while (itr.hasNext()) {
				Integer queryKey = (Integer) itr.next();
				String queryVal = (String) queryIdMap.get(queryKey);
				DocViewElementD docViewElement = new DocViewElementD();
				docViewElement.setPropValue(queryVal);

				DocViewElementDPK docViewElementPk = new DocViewElementDPK();
				docViewElementPk.setPropName(Integer.toString(queryKey));
				docViewElementPk.setLayoutId(layoutId);

				docViewElement.setId(docViewElementPk);

				listOfDocViewElementDList.add(docViewElement);
			}
			docViewElementDMap.put("UpdateDocViewElementD", listOfDocViewElementDList);
			listOfInsertObjectByTableName.add(docViewElementDMap);
		}

		logger.info("Source Query Map :" + sourceQueryIdMap);

		// this method generate XML files (query_h.xml and query_d.xml) which contains
		// validation queries for security
		try {
			generateQueryXML2ExportQueryDataForLayout(layoutId, sourceQueryIdMap, queryIdMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In generateQueryXML2ExportQueryDataForLayout() method :" + e.getMessage());
		}
		return queryIdMap;
	}

	private Map<java.lang.Integer, java.lang.String> getSourceNewValidationIdMap(
			Map<java.lang.Integer, java.lang.String> sourceQueryIdMap, java.lang.Integer layoutId) {
		Map<Integer, String> sourceIdMap = new TreeMap<Integer, String>();
		List<ExtValidation> queryInfo = sourceDbService.getExtValidationInfo(layoutId);
		for (ExtValidation obj : queryInfo) {
			sourceIdMap.put(obj.getValidationId(), obj.getValidationName());
		}
		return sourceIdMap;
	}

	// replace the validation id - if the validation id already exists in
	// destination database
	@SuppressWarnings("unlikely-arg-type")
	private Map<Integer, String> replaceValidationIdIfExist(Map<Integer, String> sourceValidationIdMap) {

		Map<Integer, String> validationIdMap = new TreeMap<Integer, String>();

		Integer nextValidationId = null;

		// get the max validation +1 from DESTINATION database to assign as new
		// validation
		// in case of conflict - same validation id been used in destination database
		Integer maxValidationId = destinationDbService.getTheMaxValidationIdFromExtValidation();

		logger.info("Max Validation Id We Got :" + maxValidationId);

		Iterator<Integer> sourceItr = sourceValidationIdMap.keySet().iterator();

		while (sourceItr.hasNext()) {

			// check the DESTINATION db if the validation validation id exists - if yes then
			// there is conflict
			Integer validationId = sourceItr.next();
			String validationName = sourceValidationIdMap.get(validationId);

			// check the DESTINATION db if the validation validation id exists - if yes then
			// there is conflict
			Integer checkQueryId = destinationDbService.getTheValidationIdCountNumber(validationId, validationName);

			if ((checkQueryId != null && checkQueryId >= 1) || validationIdMap.containsValue(validationId)) {
				continue;
			} else {
				if (maxValidationId != null) {
					if ((new Integer(maxValidationId)).intValue() < 100000) {
						nextValidationId = new Integer(100000);
					} else {
						nextValidationId = (new Integer(maxValidationId));
					}
				} else {
					nextValidationId = new Integer(nextValidationId.intValue() + 1);
				}
				validationIdMap.put(validationId, nextValidationId.toString());

			}
		}
		return validationIdMap;

	}

	private void generateQueryXML2ExportQueryDataForLayout(Integer layoutId, Map<Integer, String> sourceQueryIdMap,
			Map<Integer, String> queryIdMap) throws Exception {
		// Queries does not exists in destination database so create Queries XML to
		// insert queries
		String migrateQueryHSQL = "select * from query_h where query_id in (select cast (prop_value as integer) from doc_view_element_d where layout_id ="
				+ layoutId + " and prop_name='LOOKUPFILTER') order by query_id";
		String migrateQueryDSQL = "select * from query_d where query_id in (select cast (prop_value as integer) from doc_view_element_d where layout_id ="
				+ layoutId + " and prop_name='LOOKUPFILTER') order by query_id";
		System.out.println(
				"\nExporting the layout data for layout id " + layoutId + " from source database.Please wait.........");

		IDatabaseConnection connection = sourceDbService.getConnection();
		try {
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("query_h", migrateQueryHSQL);
			FlatXmlDataSet.write(partialDataSet, new FileOutputStream(
					configFileReader.getPropertyValue("dir.path.source") + "/query_h_insert" + layoutId + ".xml"));
			// replace the query id - if the query id already exists in destination database
			replaceQueryId(queryIdMap, "/query_h_insert" + layoutId + ".xml");

			QueryDataSet partialDataSet2 = new QueryDataSet(connection);
			partialDataSet2.addTable("query_d", migrateQueryDSQL);
			FlatXmlDataSet.write(partialDataSet2, new FileOutputStream(
					configFileReader.getPropertyValue("dir.path.source") + "/query_d_insert" + layoutId + ".xml"));

			// replace the query id - if the query id already exists in destination database
			replaceQueryId(queryIdMap, "/query_d_insert" + layoutId + ".xml");
		} catch (Exception e) {
			logger.error("Exception Occured while inserting QueryH And QueryD Table :" + e.getMessage());
			throw new MigrationException("Insertion Failed");
		} finally {
			connection.close();
		}
	}

	/* Comparing Both Environment Profile Details */
	public Boolean compareBothProfileDetailsByModule(String moduleName) {

		logger.info("Comparing for Both Modules Started For :" + moduleName);

		try {

			switch (moduleName) {

			case "SECURITY":

				String[] securityTableDeleteSeq = ConfigFileReader
						.toArray(configFileReader.getDeleteSecurityTableSequenc());

				List<Integer> listOfRoleIdFromSource = sourceDbService
						.getAllRoleIdExistInSecRoleProfileTable(Helper.getListOfActiveRoleIdOrLayoutId(moduleName))
						.parallelStream().map(element -> element.getRoleId()).collect(Collectors.toList());

				List<Integer> listOfRoleIdFromDestination = destinationDbService
						.getAllRoleIdExistInSecRoleProfileTable(Helper.getListOfActiveRoleIdOrLayoutId(moduleName))
						.parallelStream().map(element -> element.getRoleId()).collect(Collectors.toList());

				listOfRoleIdFromDestination.forEach(destinationRoleId -> {

					if (!listOfRoleIdFromSource.contains(destinationRoleId)) {

						logger.info("This Role Id Not Available In Source Db :" + destinationRoleId);

						if (!destinationDbService.deleteSecurityData(securityTableDeleteSeq, destinationRoleId)) {

							/* If Delete Status Coming False, then It's not deleted properly */
							throw new MigrationException("Oops, Not able to delete");
						}

					}
				});
				logger.info("Comparing for Both Modules Completed For :" + moduleName);
				return true;

			case "LAYOUT":

				String[] layoutTableDeleteSeq = ConfigFileReader
						.toArray(configFileReader.getDeleteLayoutTableSHelperequenc());

				List<Integer> listOfLayoutIdFromSource = sourceDbService
						.getAllLayoutIdExistInLayoutProfile(Helper.getListOfActiveRoleIdOrLayoutId(moduleName))
						.parallelStream().map(element -> element.getLayoutId()).collect(Collectors.toList());

				List<Integer> listOfLayoutIdFromDestination = destinationDbService
						.getAllLayoutIdExistInLayoutProfile(Helper.getListOfActiveRoleIdOrLayoutId(moduleName))
						.parallelStream().map(element -> element.getLayoutId()).collect(Collectors.toList());

				listOfLayoutIdFromDestination.forEach(destinationLayoutId -> {

					if (!listOfLayoutIdFromSource.contains(destinationLayoutId)) {

						logger.info("This Layout Id Not Available In Source Db :" + destinationLayoutId);

						if (!destinationDbService.deleteLayoutData(layoutTableDeleteSeq, destinationLayoutId)) {

							/* If Delete Status Coming False, then It's not deleted properly */
							throw new MigrationException("Oops, Not able to delete");
						}

					}
				});
				logger.info("Comparing for Both Modules Completed For :" + moduleName);
				return true;

			case "CUSTOM_RULES_REPORTS":

				List<String> listOfReportsFromSource = sourceDbService.getTheListOfReportNamesWithExtension();

				List<String> listOfReportsFromDestination = destinationDbService.getTheListOfReportNamesWithExtension();

				listOfReportsFromDestination.forEach(destinationFileName -> {

					if (!listOfReportsFromSource.contains(destinationFileName)) {

						logger.info("This Report File Name Not Available In Source Db :" + destinationFileName);

						if (!destinationDbService.deleteTheReportFile(destinationFileName)) {
							/* If Delete Status Coming False, then It's not deleted properly */
							throw new MigrationException("Oops, Not able to delete");
						}

					}
				});

				

				logger.info("Comparing for Both Modules Completed For :" + moduleName);
				return true;

			case "RULES":
				List<String> listOfRulesFromSource = sourceDbService.getTheListOfRuleNames();

				List<String> listOfRulesFromDestination = destinationDbService.getTheListOfRuleNames();

				listOfRulesFromDestination.forEach(destinationFileName -> {

					if (!listOfRulesFromSource.contains(destinationFileName)) {

						logger.info("This Rules File Name Not Available In Source Db :" + destinationFileName);

						if (!destinationDbService.deleteTheRulesFile(destinationFileName)) {
							/* If Delete Status Coming False, then It's not deleted properly */
							throw new MigrationException("Oops, Not able to delete");
						}

					}
				});

				logger.info("Comparing for Both Modules Completed For :" + moduleName);
				return true;

			default:
				logger.error("Module Name Not Matching With Switch Case :" + moduleName);
				return false;
			}

		} catch (Exception e) {
			logger.error("Exception Occured While Comparing Both Environment Details :" + e.getCause());
			return false;
		}

	}

}
