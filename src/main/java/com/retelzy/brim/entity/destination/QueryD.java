package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Clob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "QUERY_D")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors
public class QueryD implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "QUERY_ID")
	private Integer queryId;

	@Column(name = "QUERY_XML")
	private Clob queryXML;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

}
