package com.retelzy.brim.migrationModules;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.retelzy.brim.db.util.MigrationBackupUtil;
import com.retelzy.brim.entity.destination.service.DestinationDbService;

@Component
public class MigrationBackupForAppConfigModule {

	private static final Logger logger = LoggerFactory.getLogger(MigrationBackupForAppConfigModule.class);

	@Autowired
	private Environment environment;

	@Autowired
	private MigrationBackupUtil migrationBackupUtil;

	@Autowired
	private DestinationDbService destinationDbService;

	public Boolean getBackupOfAppConfig(List<String> listOfTableName, String fromDate, String toDate, String clientSpec)
			throws IOException {

		logger.info("Backup Started For App Config Module....");

		Connection connection = null;
		Statement statement1 = null;
		Statement statement2 = null;
		String fileName = getSQLFileName();
		File file = new File(environment.getProperty("dir.path.backup"), fileName);
		FileWriter fw = new FileWriter(file, true);
		PrintWriter pw = new PrintWriter(fw);

		try {

			connection = destinationDbService.getTheDestinationDbConnection();
			String dbType = connection.getMetaData().getDatabaseProductName();

			for (Integer index = 0; index < listOfTableName.size(); index++) {

				String tableName = listOfTableName.get(index);

				if (clientSpec != null) {
					String where = "CONFIG_ID = " + 1 + " AND PROP_NAME ='clientspec'";
					migrationBackupUtil.writeQuriesInBackupFile(where, pw, connection, statement1, statement2, dbType,
							tableName, -1, null, null, file);
				} else {
					String where = "MODIFY_TS BetWeen '" + fromDate + "'" + " AND '" + toDate + "'";
					migrationBackupUtil.writeQuriesInBackupFile(where, pw, connection, statement1, statement2, dbType,
							tableName, -1, null, null, file);
				}

			}

			return true;
		} catch (Exception e) {
			System.gc();
			e.printStackTrace();
			return false;
		} finally {
			pw.close();
			fw.close();
			try {
				if (statement1 != null)
					statement1.close();
				if (statement2 != null)
					statement2.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get the file name for SQL file
	 *
	 * @param securityOption
	 * @param dateTime
	 * @return
	 */
	private String getSQLFileName() {
		SimpleDateFormat sdtf = new SimpleDateFormat("MMddyyHHmmss");
		String dateTime = sdtf.format(new java.util.Date());
		String fileName = "APPCONFIG_" + dateTime + ".sql";
		return fileName;
	}

}
