package com.retelzy.brim.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomExceptionHandler.ErrorResponse;
import com.retelzy.brim.entity.user.service.implementation.CustomUserdetilsService;
import com.retelzy.brim.util.GoogleGuavaCache;

@Component
public class SecurityFilter extends OncePerRequestFilter {

	@Autowired
	private CustomUserdetilsService customUserdetilsService;

	@Autowired
	private GoogleGuavaCache cache;

	private final static Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authorizationHeader = request.getHeader("Authorization");
		String sourceEnvironmentType = request.getHeader("sourceEnvironmentType");
		String destinationEnvironmentType = request.getHeader("destinationEnvironmentType");

		logger.info("Source Environment :" + sourceEnvironmentType);
		logger.info("Destination Environment :" + destinationEnvironmentType);
		String token = null;
		String username = null;

		String userAgent = request.getHeader("User-Agent");

		logger.info("User Agent :" + userAgent);
		logger.info("AuthorizationHeader :" + authorizationHeader);

		// && (userAgent.contains("Chrome") || userAgent.contains("Firefox")

		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			token = authorizationHeader.substring(7);
			username = cache.getToken(token);

			logger.info("username :" + username);

			if (username.equals("") || username == null) {
				logger.info("Token is not available");
				ErrorResponse errorResponse = new ErrorResponse();
				errorResponse.setErrorCode(403);
				errorResponse.setMessage("Token expired");
				response.setStatus(HttpStatus.FORBIDDEN.value());
				response.getWriter().write(convertObjectToJson(errorResponse));
				return;
			} else {
				logger.info("Inside else method");
				UserDetails userDetails = customUserdetilsService.loadUserByUsername(username);
				/* Validate the User Credential */
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());

				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

				cache.saveTheToken(token, username);

				logger.info("Is Migration Runnning :" + MigrationController.isMigrationRunning);

				if (MigrationController.isMigrationRunning == false) {
					if (sourceEnvironmentType != null) {
						logger.info("Source Environment Added");
						cache.saveTheToken("Source-" + sourceEnvironmentType, sourceEnvironmentType);
						cache.saveTheToken("SourceEnv", sourceEnvironmentType);
					}
					if (destinationEnvironmentType != null) {
						logger.info("Destination Environment Added");
						cache.saveTheToken("Destination-" + destinationEnvironmentType, destinationEnvironmentType);
						cache.saveTheToken("DestinationEnv", destinationEnvironmentType);
					}
				}

				filterChain.doFilter(request, response);
			}
		} else {
			filterChain.doFilter(request, response);
		}

	}

	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

}
