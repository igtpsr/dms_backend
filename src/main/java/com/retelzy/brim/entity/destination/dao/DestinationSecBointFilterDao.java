package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBointFilter;
import com.retelzy.brim.entity.destination.SecBointFilterPK;

@Repository
public interface DestinationSecBointFilterDao extends CrudRepository<SecBointFilter, SecBointFilterPK> {

	@Query("Select bo from SecBointFilter bo Where bo.id.roleId =:roleId")
	List<Object> getTheDataFromSecBointFilterTable(Integer roleId);

	@Query("Select bo.id.queryId from SecBointFilter bo Where bo.id.roleId =:roleId")
	List<Integer> getTheQueryIdByRoleId(Integer roleId);

}
