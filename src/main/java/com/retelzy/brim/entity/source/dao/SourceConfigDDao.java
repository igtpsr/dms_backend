package com.retelzy.brim.entity.source.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.ConfigD;
import com.retelzy.brim.entity.source.ConfigDPK;

@Repository
public interface SourceConfigDDao extends CrudRepository<ConfigD, ConfigDPK> {

	@Query("Select con From ConfigD con Where con.id.configId =:configId And con.id.propName =:propName")
	List<ConfigD> getTheConfigDTableDetails(String configId, String propName);

	@Query(nativeQuery = true, value = "SELECT DISTINCT PROP_VALUE FROM CONFIG_D WHERE CONFIG_ID = 1 AND PROP_VALUE LIKE '%.JAR'")
	List<String> getTheListOfRuleNames();

	@Query(value = "select * from CONFIG_D where CONFIG_ID = 1 AND PROP_VALUE like %:propValue%", nativeQuery = true)
	List<ConfigD> getRulesData(String propValue);
	
	@Query(value = "select PROP_NAME from CONFIG_D where CONFIG_ID = 1 AND PROP_VALUE like %:propValue%", nativeQuery = true)
	List<String> getPropName(String propValue);

	@Query("Select con From ConfigD con Where con.modifyTs BETWEEN :fromDate AND :toDate")
	List<ConfigD> getTheConfigDTableDetails(Date fromDate, Date toDate);

}
