package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the ExtValidationMetaDataPK database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class ExtValidationMetaDataPK implements Serializable {

	// default serial version id, required for serializable classes.
		private static final long serialVersionUID = 1L;

		@Column(name = "VALIDATION_ID", insertable = false, updatable = false, unique = true, nullable = false)
		private Integer validationId;

		@Column(name = "PROP_NAME", unique = true, nullable = false)
		private String propName;

}
