package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.SecQuery;
import com.retelzy.brim.entity.destination.SecQueryPK;

public interface DestinationSecQueryDao extends CrudRepository<SecQuery, SecQueryPK> {

	@Query("Select sec from SecQuery sec Where sec.id.roleId =:roleId")
	List<Object> getTheDataFromSecQueryTable(Integer roleId);

}
