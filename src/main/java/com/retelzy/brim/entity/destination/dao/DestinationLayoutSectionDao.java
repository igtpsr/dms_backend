package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutSection;
import com.retelzy.brim.entity.destination.LayoutSectionPK;

@Repository
public interface DestinationLayoutSectionDao extends CrudRepository<LayoutSection, LayoutSectionPK> {


}
