package com.retelzy.brim.entity.source.serviceImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.ext.db2.Db2DataTypeFactory;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retelzy.brim.entity.source.AdhocDocuments;
import com.retelzy.brim.entity.source.ConfigD;
import com.retelzy.brim.entity.source.ExtValidation;
import com.retelzy.brim.entity.source.LayoutProfile;
import com.retelzy.brim.entity.source.QueryH;
import com.retelzy.brim.entity.source.SecRoleProfile;
import com.retelzy.brim.entity.source.dao.SourceAdhocDocumentsDao;
import com.retelzy.brim.entity.source.dao.SourceCollabFieldsDao;
import com.retelzy.brim.entity.source.dao.SourceConfigDDao;
import com.retelzy.brim.entity.source.dao.SourceDashSectionHDao;
import com.retelzy.brim.entity.source.dao.SourceDocViewDao;
import com.retelzy.brim.entity.source.dao.SourceDocViewElementDDao;
import com.retelzy.brim.entity.source.dao.SourceExtValidationDao;
import com.retelzy.brim.entity.source.dao.SourceExtValidationMetaDataDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutDetailDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutElementDDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutI18NResDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutLevelGroupDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutPageDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutProfileDDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutProfileDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutSectionDDao;
import com.retelzy.brim.entity.source.dao.SourceLayoutSectionDao;
import com.retelzy.brim.entity.source.dao.SourceQueryHDao;
import com.retelzy.brim.entity.source.dao.SourceRoleResAttarDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoFieldAclDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoFilterDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoLevelAclDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoProcessDao;
import com.retelzy.brim.entity.source.dao.SourceSecBointFilterDao;
import com.retelzy.brim.entity.source.dao.SourceSecBoviewResDao;
import com.retelzy.brim.entity.source.dao.SourceSecQueryDao;
import com.retelzy.brim.entity.source.dao.SourceSecQueryGroupDao;
import com.retelzy.brim.entity.source.dao.SourceSecRoleProfileDao;
import com.retelzy.brim.entity.source.dao.SourceSecRoleResCtlDao;
import com.retelzy.brim.entity.source.dao.SourceSmuCommonDao;
import com.retelzy.brim.entity.source.dao.SourceSmuResShowAttrDao;
import com.retelzy.brim.entity.source.dao.SourceSmuRoleMenuItemDao;
import com.retelzy.brim.entity.source.dao.SourceSmuRoleRootMenuDao;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.migration.model.MigrationId;

@Service
public class SourceDbImplementation implements SourceDbService {

	private final static Logger logger = LoggerFactory.getLogger(SourceDbImplementation.class);

	@Autowired
	private SourceSecRoleProfileDao secRoleProfileDao;

	@Autowired
	private SourceDashSectionHDao dashSectionHDao;

	@Autowired
	private SourceSecRoleResCtlDao secRoleResCtlDao;

	@Autowired
	private SourceSmuRoleRootMenuDao smuRoleRootMenuDao;

	@Autowired
	private SourceSmuRoleMenuItemDao smuRoleMenuItemDao;

	@Autowired
	private SourceCollabFieldsDao collabFieldsDao;

	@Autowired
	private SourceRoleResAttarDao roleResAttarDao;

	@Autowired
	private SourceSecQueryDao secQueryDao;

	@Autowired
	private SourceSecQueryGroupDao secQueryGroupDao;

	@Autowired
	private SourceSecBoLevelAclDao secBoLevelAclDao;

	@Autowired
	private SourceSecBoFieldAclDao secBoFieldAclDao;

	@Autowired
	private SourceSecBoFilterDao secBoFilterDao;

	@Autowired
	private SourceSecBointFilterDao secBointFilterDao;

	@Autowired
	private SourceQueryHDao sourceQueryHDao;

	@Autowired
	private SourceSecBoProcessDao secBoProcessDao;

	@Autowired
	private SourceSecBoviewResDao secBoviewResDao;

	@Autowired
	private SourceSmuResShowAttrDao smuResShowAttrDao;

	@Autowired
	private SourceSmuCommonDao smuCommonDao;

	@Autowired
	private SourceLayoutProfileDao layoutProfileDao;

	@Autowired
	private SourceDocViewDao docViewDao;

	@Autowired
	private SourceDocViewElementDDao docViewElementDDao;

	@Autowired
	private SourceLayoutDetailDao layoutDetailDao;

	@Autowired
	private SourceLayoutElementDDao layoutElementDDao;

	@Autowired
	private SourceLayoutI18NResDao layoutI18NResDao;

	@Autowired
	private SourceLayoutLevelGroupDao layoutLevelGroupDao;

	@Autowired
	private SourceLayoutPageDao layoutPageDao;

	@Autowired
	private SourceLayoutProfileDDao layoutProfileDDao;

	@Autowired
	private SourceLayoutSectionDao layoutSectionDao;

	@Autowired
	private SourceLayoutSectionDDao layoutSectionDDao;

	@Autowired
	private SourceExtValidationDao extValidationDao;

	@Autowired
	private SourceExtValidationMetaDataDao extValidationMetaDataDao;

	@Autowired
	private SourceAdhocDocumentsDao adhocDocumentsDao;

	@Autowired
	private SourceConfigDDao configDDao;

	@PersistenceUnit(unitName = "Source")
	private EntityManagerFactory entityManagerFactory;

	@Override
	public IDatabaseConnection getConnection() throws Exception {

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Session session = entityManager.unwrap(Session.class);
		Connection jdbcConnection = session.doReturningWork(new ReturningWork<Connection>() {
			@Override
			public Connection execute(Connection conn) throws SQLException {
				return conn;
			}
		});

		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = connection.getConfig();
		String productName = jdbcConnection.getMetaData().getDatabaseProductName();

		logger.info("Source DB Product name :" + productName);

		if (productName.contains("Microsoft SQL Server")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		} else if (productName.contains("Oracle RDBMS")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
		} else if (productName.contains("MySQL")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		} else if (productName.contains("IBM DB2")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Db2DataTypeFactory());
		}
		return connection;
	}

	@Override
	public List<Integer> getAllTheRoleIdByRoleId(Integer roleId) {
		return secRoleProfileDao.getAllTheRoleIdByRoleId(roleId);
	}

	@Override
	public List<Object> getTheDataByRoleId(Integer roleId, String className) {
		SecRoleProfile secRoleProfile = new SecRoleProfile();
		secRoleProfile.setRoleId(roleId);

		switch (className) {

		case "SecRoleProfile":
			return secRoleProfileDao.getAllTheDataByRoleId(roleId);

		case "DashSectionH":
			return dashSectionHDao.findByIdRoleId(roleId);

		case "SecRoleResCtl":
			return secRoleResCtlDao.findByIdRoleId(roleId);

		case "SmuRoleRootMenu":
			return smuRoleRootMenuDao.findByIdRoleId(roleId);

		case "SmuRoleMenuItem":
			return smuRoleMenuItemDao.findByIdRoleId(roleId);

		case "CollabFields":
			return collabFieldsDao.findByIdRoleId(roleId);

		case "RoleResAttr":
			return roleResAttarDao.findByIdRoleId(roleId);

		case "SecQuery":
			return secQueryDao.findByIdRoleId(roleId);

		case "SecQueryGroup":
			return secQueryGroupDao.findByIdRoleId(roleId);

		case "SecBoLevelAcl":
			return secBoLevelAclDao.findBySecRoleProfile(secRoleProfile);

		case "SecBoFieldAcl":
			return secBoFieldAclDao.findByRoleId(roleId);

		case "SecBoFilter":
			return secBoFilterDao.findByIdRoleId(roleId);

		case "SecBointFilter":
			return secBointFilterDao.findByIdRoleId(roleId);

		case "SecBoProcess":
			return secBoProcessDao.findByIdProcessId(10000);

		default:
			return new ArrayList<Object>();

		}

	}

	@Override
	public List<Integer> getAllQueryIdByRoleId(Integer roleId, String className) {

		SecRoleProfile secRoleProfile = new SecRoleProfile();
		secRoleProfile.setRoleId(roleId);

		switch (className) {

		case "SecBoFilter":
			return secBoFilterDao.getAllQueryIdByRoleId(roleId);

		case "SecBointFilter":
			return secBointFilterDao.getAllTheQueryIdsByRoleId(roleId);

		case "SecBoFieldAcl":
			return secBoFieldAclDao.getAllTheQueryIdsByRoleId(roleId);

		case "SecBoLevelAcl":
			return secBoLevelAclDao.getAllTheQueryIdsByRoleId(secRoleProfile);

		default:
			logger.error("Fileter Table Not added in Service class Switch case condition :" + className);
			return new ArrayList<Integer>();
		}

	}

	@Override
	public List<QueryH> getallTheQueryDetails(List<Integer> listOfQueryId) {
		return sourceQueryHDao.getAllQueryHDetailByRoleIds(listOfQueryId);
	}

	@Override
	public List<Integer> getByIdProcessIdGreaterThanEqual(Integer processId) {
		return secBoProcessDao.getByIdProcessIdGreaterThanEqual(processId);
	}

	@Override
	public List<Object> getTheDetailsOfSecBoviewTable(List<Integer> listOfProcessId, Integer typeId) {
		return secBoviewResDao.getTheDetailsOfSecBoviewTable(listOfProcessId, typeId);
	}

	@Override
	public List<Object> getTheSmuResShowAttrDetails(List<Integer> listOfProcessId, Integer smuTypeId) {
		return smuResShowAttrDao.getTheSmuResShowAttrDetails(listOfProcessId, smuTypeId);
	}

	@Override
	public List<Object> getAllTheSecRoleResCtlDetails(Integer minRoleId, Integer roleId, List<Integer> listOfProcessId,
			List<Integer> listOfRoleId) {
		return secRoleResCtlDao.getAllTheSecRoleResCtlDetails(minRoleId, roleId, listOfProcessId, listOfRoleId);
	}

	@Override
	public List<Object> getSmuCommonAllDetails(Integer menuItemId) {
		return smuCommonDao.getSmuCommonAllDetails(menuItemId);
	}

	@Override
	public List<Object> getSmuRoleRootMenuDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId) {
		return smuRoleRootMenuDao.getSmuRoleRootMenuDetails(menuItemId, minRoleId, roleId, listOfRoleId);

	}

	@Override
	public List<Object> getSmuRoleRootMenuItemDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId) {
		return smuRoleMenuItemDao.getSmuRoleRootMenuItemDetails(menuItemId, minRoleId, roleId, listOfRoleId);
	}

	@Override
	public Optional<Integer> getTheRoleIdByRoleId(Integer roleId) {
		return secRoleProfileDao.getTheRoleIdByRoleId(roleId);
	}

	@Override
	public List<Integer> getAllTheLayoutIdByLayoutId() {
		return layoutProfileDao.getAllTheLayoutIdByLayoutId();
	}

	@Override
	public List<Object> getLayoutProfileInfo(Integer layoutId) {
		return layoutProfileDao.getAllTheDataByLayoutId(layoutId);
	}

	@Override
	public List<Object> getTheDataByLayoutId(Integer layoutId, String className, MigrationId migrationId) {

		try {
			long time = migrationId.getFromDate().getTime();
			Timestamp fromDate = new Timestamp(time);

			long time1 = migrationId.getToDate().getTime();
			Timestamp toDate = new Timestamp(time1);

			switch (className) {

			case "DocView":
				return docViewDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "DocViewElementD":
				return docViewElementDDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "LayoutDetail":

				return layoutDetailDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "LayoutElementD":
				return layoutElementDDao.findByIdLayoutId(layoutId);

			case "LayoutI18nRes":
				return layoutI18NResDao
						.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqualAndIdLanguageId(layoutId,
								fromDate, toDate, 0);

			case "LayoutLevelGroup":
				return layoutLevelGroupDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "LayoutPage":
				return layoutPageDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "LayoutProfile":
				return layoutProfileDao.getAllTheDataByLayoutId(layoutId);

			case "LayoutProfileD":
				return layoutProfileDDao.findByIdLayoutId(layoutId);

			case "LayoutSection":
				return layoutSectionDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			case "LayoutSectionD":
				return layoutSectionDDao.findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(layoutId,
						fromDate, toDate);

			default:
				return new ArrayList<Object>();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<Object>();
	}

	@Override
	public List<QueryH> getQueryInfo(Integer layoutId) {
		return sourceQueryHDao.getQueryInfo(layoutId);
	}

	@Override
	public List<ExtValidation> getExtValidationInfo(Integer layoutId) {
		return extValidationDao.getExtValidationInfo(layoutId);
	}

	@Override
	public List<Object> getTheDataOfExtValidation(List<Integer> whereList, String className, MigrationId migrationId) {
		switch (className) {

		case "ExtValidation":
			return extValidationDao.getTheDataOfExtValidation(whereList, migrationId.getFromDate(),
					migrationId.getToDate());

		case "ExtValidationMetadata":
			return extValidationMetaDataDao.getTheDataOfExtValidationMetaData(whereList, migrationId.getFromDate(),
					migrationId.getToDate());

		default:
			return new ArrayList<Object>();

		}

	}

	@Override
	public List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleId) {
		return secRoleProfileDao.getAllRoleIdExistInSecRoleProfileTable(listOfActiveRoleId);
	}

	@Override
	public List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveLayoutId) {
		return layoutProfileDao.getAllLayoutIdExistInLayoutProfile(listOfActiveLayoutId);
	}

	@Override
	public List<ConfigD> getTheConfigDTableDetails() {
		return configDDao.getTheConfigDTableDetails("1", "clientspec");
	}

	@Override
	public Optional<Integer> getTheLayoutIdByLayoutId(Integer layoutId) {
		return layoutProfileDao.getTheLayoutIdByLayoutId(layoutId);
	}

	@Override
	public List<AdhocDocuments> getTheAdhocDocumentsTableDetails(Date fromDate, Date toDate) {
		return adhocDocumentsDao.getTheAdhocDocumentsTableDetails(fromDate, toDate);
	}

	@Override
	public List<AdhocDocuments> getTheAdhocDocumentsData(List<String> fileNames) throws SQLException {
		// return adhocDocumentsDao.getTheAdhocDocumentsTableDetails(fileNames);

		List<AdhocDocuments> listOfAdhocDocFromSource = new ArrayList<AdhocDocuments>();

		fileNames.forEach(file -> {
			logger.info("File name :" + file);
			listOfAdhocDocFromSource.addAll(adhocDocumentsDao.getAdhocDocumnetsData(file));
		});

		return listOfAdhocDocFromSource;

	}

	@Override
	public List<String> getTheListOfAdhocDocumentsNames() {
		return adhocDocumentsDao.getTheListOfAdhocDocumentsNames();
	}

	@Override
	public List<String> getTheListOfRuleNames() {
		return configDDao.getTheListOfRuleNames();
	}

	@Override
	public List<ConfigD> getTheRulesData(List<String> fileNames) throws SQLException {
		List<ConfigD> listOfConfigDFromSource = new ArrayList<ConfigD>();

		fileNames.forEach(file -> {
			logger.info("File name :" + file);
			listOfConfigDFromSource.addAll(configDDao.getRulesData(file));
		});

		return listOfConfigDFromSource;
	}
	
	@Override
	public List<String> getPropName(List<String> fileNames) throws SQLException {
		List<String> listOfPropNamesFromSource = new ArrayList<String>();

		fileNames.forEach(file -> {
			logger.info("File name :" + file);
			listOfPropNamesFromSource.addAll(configDDao.getPropName(file));
		});

		return listOfPropNamesFromSource;

	}

	@Override
	public List<String> getTheListOfReportNames() {
		return adhocDocumentsDao.getTheListOfReportNames();
	}

	@Override
	public List<ConfigD> getTheConfigDTableDetails(Date fromDate, Date toDate) {
		return configDDao.getTheConfigDTableDetails(fromDate, toDate);
	}

	@Override
	public List<String> getTheListOfReportNamesWithExtension() {
		return adhocDocumentsDao.getTheListOfReportNamesWithExtension();
	}

}
