package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "SEC_BOINT_FILTER")
@DynamicUpdate
public class SecBointFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecBointFilterPK id;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	// bi-directional many-to-one association to SecRoleProfile
	@MapsId("ROLE_ID")
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private SecRoleProfile secRoleProfile;

	public SecBointFilter() {
		// TODO Auto-generated constructor stub
	}

	public SecBointFilterPK getId() {
		return id;
	}

	public void setId(SecBointFilterPK id) {
		this.id = id;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SecRoleProfile getSecRoleProfile() {
		return secRoleProfile;
	}

	public void setSecRoleProfile(SecRoleProfile secRoleProfile) {
		this.secRoleProfile = secRoleProfile;
	}

}
