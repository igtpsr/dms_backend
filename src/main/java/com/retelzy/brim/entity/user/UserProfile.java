package com.retelzy.brim.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Table(name = "USER_PROFILE")
@Entity
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class UserProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID")
	private Integer userId;

	@Column(name = "USER_NAME", unique = true)
	private String userName;

	@Column(name = "PSWD")
	private String password;

	@Column(name = "EMAIL", unique = true)
	public String email;

	@Basic
	@Temporal(TemporalType.DATE)
	@Column(name = "PSWD_EXP_DATE")
	private Date passwordExpDate;
	
	/*added by LOK11Feb2021 - Begin */
	@Column(name = "PSWD_NEVER_EXPIRES")
	private Boolean pswdNeverExpires;
	/*added by LOK11Feb2021 - End */
	
	@Column(name = "MODIFY_TS")
	private Date modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private RoleProfile roleProfile;

	@Transient
	private String token;


}
