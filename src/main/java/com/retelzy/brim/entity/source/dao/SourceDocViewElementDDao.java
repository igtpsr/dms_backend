package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.DocViewElementD;
import com.retelzy.brim.entity.source.DocViewElementDPK;

@Repository
public interface SourceDocViewElementDDao extends CrudRepository<DocViewElementD, DocViewElementDPK> {

	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);


}
