package com.retelzy.brim.entity.user.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;

public interface MigratedRoleIdDao extends CrudRepository<MigratedRoleIdDetails, Integer> {

	public List<MigratedRoleIdDetails> findByMigrationDetails(MigrationDetails migrationDetails);
}
