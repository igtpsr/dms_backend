// Generated with g9.

package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "DOC_VIEW")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class DocView implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DocViewPK id;

	@Column(name = "I18N_RES_ID", nullable = false)
	private Integer i18nResId;

	@Column(name = "ISBASEVIEW")
	private Integer isBaseView;

	@Column(name = "ISSTEPPROCESS")
	private Integer isStepProcess;

	@Column(name = "ISNEWVIEW")
	private Integer isNewView;

	@Column(name = "DOC_VIEW_TYPE")
	private Integer docViewType;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER", length = 18)
	private String modifyUser;

	@Column(name = "COMMENTS", length = 250)
	private String comments;

}
