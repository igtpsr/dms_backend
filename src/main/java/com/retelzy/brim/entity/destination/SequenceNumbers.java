package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "SEQUENCE_NUMBERS")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class SequenceNumbers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQUENCE_NAME")
	private String sequenceName;

	@Column(name = "CURRENTNUM")
	private Integer currentnum;

	@Column(name = "STARTNUM")
	private Integer startnum;

	@Column(name = "ENDNUM")
	private Integer endnum;

	@Column(name = "STEPNUM")
	private Integer stepnum;

	@Column(name = "PREFIX")
	private String prefix;

	@Column(name = "SUFFIX")
	private String suffix;

	@Column(name = "NO_OF_DIGITS")
	private Integer numberOfDigits;

	@Column(name = "ISALPHANUM")
	private String isAlphaNum;

	@Column(name = "SEQUENCE_TYPE")
	private String sequenceType;

	@Column(name = "ROLLOVER")
	private String rollOver;

	@Column(name = "ROLLOVER_TS")
	private Timestamp rollOverTs;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
