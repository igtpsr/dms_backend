package com.retelzy.brim.migrationModules;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.db.util.MigrationDbUtil;
import com.retelzy.brim.entity.destination.ConfigDPK;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.ConfigD;
import com.retelzy.brim.entity.source.service.SourceDbService;

@Component
public class MigrationRulesModule {

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private SimpMessagingTemplate template;
	
	@Autowired
	private MigrationDbUtil migrationDbUtil;

	private final static Logger logger = LoggerFactory.getLogger(MigrationRulesModule.class);

	public void migrateRulesModule(List<String> fileNames) {
		
		if (migrationDbUtil.compareBothProfileDetailsByModule("RULES") == false)
			throw new MigrationException("Something went wrong, Please try again later");

		List<ConfigD> listOfRulesFromSource = new ArrayList<ConfigD>();

		try {
			listOfRulesFromSource = sourceDbService.getTheRulesData(fileNames);
			logger.info("List of Rules We Got From Source :" + listOfRulesFromSource.size());
		} catch (Exception e) {
			logger.error("Exception Occured While Retrieving the Rules details from Source :" + e.getMessage());
			e.printStackTrace();
			throw new MigrationException("Migration terminated");
		}

		template.convertAndSend("/queue/percentage/others/", 35);

		

		List<com.retelzy.brim.entity.destination.ConfigD> listOfConfigDDetailsFromDestination = new ArrayList<com.retelzy.brim.entity.destination.ConfigD>();

		listOfRulesFromSource.forEach(configDData -> {
			com.retelzy.brim.entity.destination.ConfigD destinationConfigD = new com.retelzy.brim.entity.destination.ConfigD();
			ConfigDPK configDPK = new ConfigDPK();
			configDPK.setConfigId(configDData.getId().getConfigId());
			configDPK.setConfigType(configDData.getId().getConfigType());
			configDPK.setPropName(configDData.getId().getPropName());
			
			destinationConfigD.setId(configDPK);
			destinationConfigD.setPropValue(configDData.getPropValue());
			destinationConfigD.setDatatype(configDData.getDatatype());
			destinationConfigD.setFactoryName(configDData.getFactoryName());
			destinationConfigD.setMaxlength(configDData.getMaxlength());
			destinationConfigD.setEncrypted(configDData.getEncrypted());
			destinationConfigD.setDescription(configDData.getDescription());
			destinationConfigD.setComments(configDData.getComments());
			destinationConfigD.setDocIdList(configDData.getDocIdList());
			destinationConfigD.setClobValue(configDData.getClobValue());
			destinationConfigD.setBlobValue(configDData.getBlobValue());
			destinationConfigD.setFuncUrl1(configDData.getFuncUrl1());
			destinationConfigD.setFuncUrl2(configDData.getFuncUrl2());
			destinationConfigD.setUseClob(configDData.getUseClob());
			destinationConfigD.setCategoryId(configDData.getCategoryId());
			destinationConfigD.setLevelId(configDData.getLevelId());
			destinationConfigD.setPosition(configDData.getPosition());
			destinationConfigD.setModifyTs(configDData.getModifyTs());
			destinationConfigD.setModifyUser(configDData.getModifyUser());

			listOfConfigDDetailsFromDestination.add(destinationConfigD);
		});

		template.convertAndSend("/queue/percentage/others/", 65);

		destinationDbService.updateTheConigDTableDetails(listOfConfigDDetailsFromDestination);

		template.convertAndSend("/queue/percentage/others/", 100);

		MigrationController.isMigrationRunning = false;
		SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
		DestinationDBMultiRoutingDataSource.destinationLookupCode=0;

	}

}
