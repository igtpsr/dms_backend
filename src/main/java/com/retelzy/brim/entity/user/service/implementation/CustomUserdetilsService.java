package com.retelzy.brim.entity.user.service.implementation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.retelzy.brim.entity.user.ClientDbDetails;
import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.entity.user.RoleProfile;
import com.retelzy.brim.entity.user.UserProfile;
import com.retelzy.brim.entity.user.dao.AdhocDocMigrationStoredProcDao;
import com.retelzy.brim.entity.user.dao.AppConfig6RulesMigrationStoredProcDao;
import com.retelzy.brim.entity.user.dao.ClientDbDetailsDao;
import com.retelzy.brim.entity.user.dao.LayoutMigrationStoredProcDao;
import com.retelzy.brim.entity.user.dao.MigratedLayoutIdDao;
import com.retelzy.brim.entity.user.dao.MigratedRoleIdDao;
import com.retelzy.brim.entity.user.dao.MigrationDetailsDao;
import com.retelzy.brim.entity.user.dao.RoleProfileDao;
import com.retelzy.brim.entity.user.dao.SecurityMigrationStoredProcDao;
import com.retelzy.brim.entity.user.dao.UserProfileDao;
import com.retelzy.brim.entity.user.service.UserService;
import com.retelzy.brim.migration.model.MigrationId;

@Service
public class CustomUserdetilsService implements UserDetailsService, UserService {

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private RoleProfileDao roleProfileDao;

	@Autowired
	private ClientDbDetailsDao clientDbDetailsDao;

	@Autowired
	private MigrationDetailsDao migrationDetailsDao;
	
	@Autowired
	private MigratedRoleIdDao migratedRoleIdDao;
	
	@Autowired
	private MigratedLayoutIdDao migratedLayoutIdDao;
	
	
	//--------------------------Added by LOK28Apr2021 - Begin--------------------------
	@Autowired
	private LayoutMigrationStoredProcDao layoutMigrationStoredProcDao;
	
	@Autowired
	private SecurityMigrationStoredProcDao securityMigrationStoredProcDao;
	
	@Autowired
	private AdhocDocMigrationStoredProcDao adhocDocMigrationStoredProcDao;
	
	@Autowired
	private AppConfig6RulesMigrationStoredProcDao appConfig6RulesMigrationStoredProcDao;
	//--------------------------Added by LOK28Apr2021 - Begin--------------------------
	
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("Username :" + username);
		Optional<UserProfile> modifibleUsers = userProfileDao.findByUserName(username);

		if (modifibleUsers.isPresent()) {
			return new User(username, modifibleUsers.get().getPassword(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("Inavlid user credntials");
		}
	}

	@Override
	public Optional<UserProfile> getTheUserDetailsById(Integer modifibleUserId) {
		return userProfileDao.findById(modifibleUserId);
	}

	@Override
	public ClientDbDetails saveOrUpdateClientDbDetails(ClientDbDetails clientDbdDetails) {
		return clientDbDetailsDao.save(clientDbdDetails);
	}

	@Override
	public Optional<ClientDbDetails> getClientDbDetailsById(Integer clientDbCredentialId) {
		return clientDbDetailsDao.findById(clientDbCredentialId);
	}

	@Override
	public Optional<ClientDbDetails> getClientDbDetailsByEnvironmentType(List<String> listOfEnvironmentType) {
		return clientDbDetailsDao.findByDbEnvironmentTypeIn(listOfEnvironmentType);
	}

	@Override
	public void createMigrationReportDetails(MigrationDetails migrationDetails) {
		migrationDetailsDao.save(migrationDetails);
	}

	@Override
	public Optional<MigrationDetails> getTheLastMigratedTimePeriod() {
		return migrationDetailsDao.getTheLastMigratedTimePeriod();
	}

	@Override
	public List<ClientDbDetails> getTheConfigurationAppDetails() {
		return clientDbDetailsDao.getTheConfigurationAppDetails();
	}

	@Override
	public void createMigrationDetails(MigrationDetails migrationDetails) {
		migrationDetailsDao.save(migrationDetails);
	}

	@Override
	public void removeTheDBEnvironmentDetails(Integer clentDbDetailsId) {
		clientDbDetailsDao.deleteById(clentDbDetailsId);
	}

	@Override
	public Optional<MigrationDetails> getTheLatestMigrationDetailsByModuleName(String moduleName) {
		return migrationDetailsDao.getTheLatestMigrationDetailsByModuleName(moduleName);
	}

	@Override
	public UserProfile saveOrUpdate(UserProfile userProfile) {
		System.out.print("step8");
		return userProfileDao.save(userProfile);
	}

	@Override
	public RoleProfile saveOrUpdateRoleProfile(RoleProfile roleProfile) {
		return roleProfileDao.save(roleProfile);
	}

	@Override
	public List<RoleProfile> getAllAvailableRoleProfile() {
		return roleProfileDao.getAllAvailableRoleProfile();
	}

	@Override
	public Optional<RoleProfile> getTheRoleProfileDetails(Integer roleId) {
		return roleProfileDao.findById(roleId);
	}

	@Override
	public List<UserProfile> getUserProfileDetailsByRoleId(RoleProfile roleProfile) {
		return userProfileDao.findByRoleProfile(roleProfile);
	}

	@Override
	public void deleteRoleProfile(Integer roleId) {
		roleProfileDao.deleteById(roleId);
	}

	@Override
	public List<UserProfile> getAllAvailableUserProfile() {
		List<UserProfile> listOfUserProfile = new ArrayList<UserProfile>();
		listOfUserProfile = userProfileDao.getAllAvailableUserProfile(); 
		return listOfUserProfile;
	}
	
	@Override
	public List<String> getAllAvailableUserName() {
		return userProfileDao.getAllAvailableUserName();
	}

	@Override
	public UserProfile getTheUserProfile(String userName, String password) {
		return userProfileDao.findByUserNameAndPassword(userName, password);
	}

	@Override
	public Optional<ClientDbDetails> getTheClientDbDetailsByEnvName(String destinationEnvironmentType) {
		return clientDbDetailsDao.findByDbEnvironmentType(destinationEnvironmentType);
	}

	@Override
	public List<MigratedRoleIdDetails> getTheLatestMigratedRoleId(MigrationDetails migrationDetails) {
		return migratedRoleIdDao.findByMigrationDetails(migrationDetails);
	}

	@Override
	public List<MigratedLayoutIdDetails> getTheLatestMigratedLayoutId(MigrationDetails migrationDetails) {
		return migratedLayoutIdDao.findByMigrationDetails(migrationDetails);
	}
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	@Override
	public Map<String, Object> migrateLayoutStoredProc(MigrationId migrationId, String sourceEnvType, String destinationEnvType) {
		
		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		try {
			migrationStatusMap = layoutMigrationStoredProcDao.MigrateLayoutStoredProc(migrationId,sourceEnvType,destinationEnvType);
		} catch (Exception e) {
			migrationStatusMap.put("Status", e.getMessage());
			e.printStackTrace();
		}
		return migrationStatusMap;
	}
	
	@Override
	public Map<String, Object> migrateSecurityStoredProc(MigrationId migrationId, MigrationDetails migrationDetails) {
		
		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		try {
			migrationStatusMap = securityMigrationStoredProcDao.MigrateSecurityStoredProc(migrationId, migrationDetails);
		} catch (Exception e) {
			migrationStatusMap.put("Status", e.getMessage());
			e.printStackTrace();
		}
		return migrationStatusMap;
	}
	
	@Override
	public Map<String, Object> migrateTheAdhocDocStoredProc(MigrationId migrationId, String sourceEnvType, String destinationEnvType) {
		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		try {
			migrationStatusMap = adhocDocMigrationStoredProcDao.MigrateAdhocDocStoredProc(migrationId,sourceEnvType, destinationEnvType);
		} catch (Exception e) {
			migrationStatusMap.put("Status", e.getMessage());
			e.printStackTrace();
		}
		return migrationStatusMap;
	}
	
	@Override
	public Map<String, Object> appConfig6RulesMigrationStoredProc(MigrationId migrationId, List<String> listOfPropNames, String sourceEnvType, String destinationEnvType) {
		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		try {
			migrationStatusMap = appConfig6RulesMigrationStoredProcDao.migrateAppConfigorRulesStoredProc(migrationId, listOfPropNames, sourceEnvType, destinationEnvType);
		} catch (Exception e) {
			migrationStatusMap.put("Status", e.getMessage());
			e.printStackTrace();
		}
		return migrationStatusMap;
	}
	//--------------------------Added by LOK28APR2021 - End---------------------------------------

}
