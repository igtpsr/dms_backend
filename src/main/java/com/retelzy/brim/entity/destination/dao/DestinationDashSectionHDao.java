package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.DashSectionH;
import com.retelzy.brim.entity.destination.DashSectionHPK;

public interface DestinationDashSectionHDao extends CrudRepository<DashSectionH, DashSectionHPK> {

	@Query("Select dh from DashSectionH dh where dh.id.roleId =:roleId")
	public List<Object> getTheDataFromDashSectionHtable(Integer roleId);

}
