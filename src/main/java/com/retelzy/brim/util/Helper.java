package com.retelzy.brim.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.retelzy.brim.controller.MigrationController;

public class Helper {

	public static List<Integer> mapKeyToList(Map<Integer, String> map) {
		List<Integer> validationsIds = new ArrayList<Integer>();
		Iterator<Integer> i = map.keySet().iterator();
		while (i.hasNext()) {
			Integer element = (Integer) i.next();
			validationsIds.add(element);
		}
		return validationsIds;
	}

	public static String getDateTime() {
		SimpleDateFormat sdtf = new SimpleDateFormat("MMddyyHHmmss");
		String dateTime = sdtf.format(new java.util.Date());
		return dateTime;
	}

	public static void clearTheBackupFileNames() {
		MigrationController.queryHfileName = "";
		MigrationController.queryDfileName = "";
		MigrationController.backupModuleFileName = "";
	}

	public static List<Integer> getListOfActiveRoleIdOrLayoutId(String moduleName) {

		if (moduleName.equalsIgnoreCase("SECURITY")) {

			List<Integer> listOfActiveRoleId = new ArrayList<Integer>();
			listOfActiveRoleId.add(101);
			listOfActiveRoleId.add(102);
			listOfActiveRoleId.add(100037);
			listOfActiveRoleId.add(100113);
			listOfActiveRoleId.add(100120);
			listOfActiveRoleId.add(100122);
			listOfActiveRoleId.add(100123);
			listOfActiveRoleId.add(100124);
			listOfActiveRoleId.add(100125);
			listOfActiveRoleId.add(100126);
			listOfActiveRoleId.add(100128);
			listOfActiveRoleId.add(100129);
			listOfActiveRoleId.add(100130);
			listOfActiveRoleId.add(100131);
			listOfActiveRoleId.add(100132);
			listOfActiveRoleId.add(100150);
			listOfActiveRoleId.add(100151);
			listOfActiveRoleId.add(100153);
			listOfActiveRoleId.add(100155);

			return listOfActiveRoleId;

		} else if (moduleName.equalsIgnoreCase("LAYOUT")) {
			List<Integer> listOfActiveLayoutId = new ArrayList<Integer>();
			listOfActiveLayoutId.add(100093);
			listOfActiveLayoutId.add(100100);
			listOfActiveLayoutId.add(100102);
			listOfActiveLayoutId.add(100111);

			return listOfActiveLayoutId;
		} else {
			return null;
		}

	}

}
