package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecRoleProfile;

@Repository
public interface DestinationSecRoleProfileDao extends CrudRepository<SecRoleProfile, Integer> {

	@Query(value = "Select sf.roleId from SecRoleProfile sf where sf.roleId =:roleId")
	public List<Integer> getAllTheRoleIdByRoleId(Integer roleId);

	@Query(value = "Select sf from SecRoleProfile sf Where sf.roleId In (:listOfActiveRoleIdOrLayoutId)")
	public List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleIdOrLayoutId);

}
