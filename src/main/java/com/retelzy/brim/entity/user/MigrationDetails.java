package com.retelzy.brim.entity.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "MIGRATION_DETAILS")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class MigrationDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MIGRATION_DETAILS_ID")
	private Integer migrationDetailsId;

	@Column(name = "MIGRATION_MODULE_NAME")
	private String migrationModuleName;

	@Column(name = "MIGRATION_FROM_DATE")
	private Date migrationFromDate;

	@Column(name = "MIGRATION_TO_DATE")
	private Date migrationToDate;

	@Column(name = "MIGRATION_START_TIME")
	private Date migrationStartTime;

	@Column(name = "MIGRATION_END_TIME")
	private Date migrationEndTime;

	@Column(name = "SOURCE_ENVIRONMENT_TYPE")
	private String sourceEnvironmentType;

	@Column(name = "DESTINATION_ENVIRONMENT_TYPE")
	private String destinationEnvironmentType;

	@Column(name = "CREATE_TS")
	private Date createTs;

	@Column(name = "BACKUP_MODULE_FILE_NAME")
	private String backupModuleFileName;

	@Column(name = "QUERY_H_FILE_NAME")
	private String queryHfileName;

	@Column(name = "QUERY_D_FILE_NAME")
	private String queryDfileName;

	@Column(name = "DB_NAME")
	private String databaseName;

	@Column(name = "DB_SERVER_NAME")
	private String dbSeverName;
	
	@Column(name = "DB_TYPE")
	private String dbType;
	
	//Added by LOKESH -Begin
	@Column(name = "CR_NO")
	private String crNumber;
	
	@Column(name = "CR_DESCRIPTION")
	private String crDescription;
	//Added by LOKESH -End

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "migrationDetails")
	private List<MigratedRoleIdDetails> listOfMigratedRoleId = new ArrayList<MigratedRoleIdDetails>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "migrationDetails")
	private List<MigratedLayoutIdDetails> listOfMigratedLayoutId = new ArrayList<MigratedLayoutIdDetails>();

}