package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutProfileD;
import com.retelzy.brim.entity.destination.LayoutProfileDPK;

@Repository
public interface DestinationLayoutProfileDDao extends CrudRepository<LayoutProfileD, LayoutProfileDPK> {


}
