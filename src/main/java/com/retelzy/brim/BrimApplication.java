package com.retelzy.brim;

import java.util.Date;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.retelzy.brim.entity.user.RoleProfile;
import com.retelzy.brim.entity.user.UserProfile;
import com.retelzy.brim.entity.user.dao.RoleProfileDao;
import com.retelzy.brim.entity.user.dao.UserProfileDao;

@SpringBootApplication
public class BrimApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private UserProfileDao userPrfileDao;

	@Autowired
	private RoleProfileDao roleProfileDao;

	public static void main(String[] args) {
		SpringApplication.run(BrimApplication.class, args);

	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	@Override
	public void run(String... args) throws Exception {

		Optional<RoleProfile> isRoleProfileAvailable = roleProfileDao.findByRoleName("Default");
		Optional<UserProfile> isUserProfileAvailable = userPrfileDao.findByUserName("BRIM_ADMIN");
		DateTime dateTime = new DateTime();
		RoleProfile roleProfile = new RoleProfile();
		if (!isRoleProfileAvailable.isPresent()) {
			roleProfile.setModifyTs(new Date());
			roleProfile.setModifyUser("Default");
			roleProfile.setRoleName("Default");
			roleProfile = roleProfileDao.save(roleProfile);
		}

		if (!isUserProfileAvailable.isPresent()) {

			UserProfile userProfile = new UserProfile();
			userProfile.setEmail("manass@retelzy.com");
			userProfile.setModifyTs(new Date());
			userProfile.setModifyUser("Default");
			userProfile.setPasswordExpDate(dateTime.plusYears(100).toDate());
			userProfile.setUserName("BRIM_ADMIN");
			userProfile.setPassword("password");

			if (!isRoleProfileAvailable.isPresent())
				userProfile.setRoleProfile(roleProfile);

			userPrfileDao.save(userProfile);
		}

		System.out.println(
				"------------------------------>>>>>>>>>>>>>>>BRIM APPLICATION STARTED<<<<<<<<<<<<<---------------------------------");

	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BrimApplication.class);
	}

}
