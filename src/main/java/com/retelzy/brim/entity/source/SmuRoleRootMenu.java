package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the SMU_ROLE_ROOT_MENU database table.
 * 
 */
@Entity
@Table(name = "SMU_ROLE_ROOT_MENU")
@DynamicUpdate
public class SmuRoleRootMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SmuRoleRootMenuPK id;

	@Column(name = "GROUP_ID")
	private Integer groupId;

	@Column(name = "MENU_POS")
	private Integer menuPos;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@Column(name = "SORT_TYPE")
	private Integer sortType;

	public SmuRoleRootMenu() {
	}

	public SmuRoleRootMenuPK getId() {
		return id;
	}

	public void setId(SmuRoleRootMenuPK id) {
		this.id = id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getMenuPos() {
		return menuPos;
	}

	public void setMenuPos(Integer menuPos) {
		this.menuPos = menuPos;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Integer getSortType() {
		return sortType;
	}

	public void setSortType(Integer sortType) {
		this.sortType = sortType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}