package com.retelzy.brim.migration.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class MigrationId {

	private List<SecurityRoleIdDetais> listOfRoleId = new ArrayList<SecurityRoleIdDetais>();

	private Boolean isMigrateAllSecurityRoleId;

	private List<LayoutIdDetails> listOfLayoutId = new ArrayList<LayoutIdDetails>();

	private List<String> listOfCustomRules = new ArrayList<String>();

	private List<String> listOfRules = new ArrayList<String>();

	private List<String> listOfReports = new ArrayList<String>();

	private Boolean isMigrateAllLayoutId;

	private Date fromDate;

	private Date toDate;

	private String moduleName;

	private String clientSpec;
	
	private String crNumber;
	
	private String crDescription;

}
