package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "SMU_COMMON")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class SmuCommon implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "MENU_ITEM_ID")
	private Integer menuItemId;

	@Column(name = "MENU_ITEM_NAME")
	private String menuItemName;

	@Column(name = "I18N_RES_ID")
	private Integer i18nResId;

	@Column(name = "URL")
	private String url;

	@Column(name = "PARENT_ITEM_ID")
	private Integer parentItemId;

	@Column(name = "MENU_POS")
	private Integer menuPos;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
