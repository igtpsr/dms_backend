package com.retelzy.brim.cutomExceptionHandler;

import org.springframework.stereotype.Component;

@Component
public class ErrorResponse  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	Integer errorCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
