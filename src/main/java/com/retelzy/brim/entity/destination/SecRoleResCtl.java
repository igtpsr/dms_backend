package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * The persistent class for the SEC_ROLE_RES_CTL database table.
 * 
 */
@Entity
@Table(name = "SEC_ROLE_RES_CTL")
public class SecRoleResCtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecRoleResCtlPK id;

	@Column(name = "ENABLED")
	private Integer enabled;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	// bi-directional many-to-one association to SecRoleProfile
	@MapsId("ROLE_ID")
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private SecRoleProfile secRoleProfile;

	public SecRoleResCtl() {
	}

	public SecRoleResCtlPK getId() {
		return this.id;
	}

	public void setId(SecRoleResCtlPK id) {
		this.id = id;
	}

	public int getEnabled() {
		return this.enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public Timestamp getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public SecRoleProfile getSecRoleProfile() {
		return this.secRoleProfile;
	}

	public void setSecRoleProfile(SecRoleProfile secRoleProfile) {
		this.secRoleProfile = secRoleProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modifyTs == null) ? 0 : modifyTs.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((secRoleProfile == null) ? 0 : secRoleProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecRoleResCtl other = (SecRoleResCtl) obj;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modifyTs == null) {
			if (other.modifyTs != null)
				return false;
		} else if (!modifyTs.equals(other.modifyTs))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (secRoleProfile == null) {
			if (other.secRoleProfile != null)
				return false;
		} else if (!secRoleProfile.equals(other.secRoleProfile))
			return false;
		return true;
	}

}