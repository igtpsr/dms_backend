package com.retelzy.brim.entity.source;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the LAYOUT_ELEMENT_D database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutElementDPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="LAYOUT_ID", unique=true, nullable=false)
	private Integer layoutId;

	@Column(name="DOC_ID", unique=true, nullable=false)
	private Integer docId;

	@Column(name="LEVEL_ID", unique=true, nullable=false)
	private Integer levelId;

	@Column(name="FIELD_ID", unique=true, nullable=false)
	private Integer fieldId;

	@Column(name="PROP_NAME", unique=true, nullable=false, length=16)
	private String propName;

}