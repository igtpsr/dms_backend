package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBoProcess;
import com.retelzy.brim.entity.destination.SecBoProcessPK;

@Repository
public interface DestinationSecBoProcessDao extends CrudRepository<SecBoProcess, SecBoProcessPK> {

	@Query("Select sb.id.processId From SecBoProcess sb Where sb.id.processId >=:processId")
	public List<Integer> getByIdProcessIdGreaterThanEqual(Integer processId);

}
