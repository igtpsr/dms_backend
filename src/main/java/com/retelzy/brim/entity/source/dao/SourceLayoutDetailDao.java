package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutDetail;
import com.retelzy.brim.entity.source.LayoutDetailPK;

@Repository
public interface SourceLayoutDetailDao extends CrudRepository<LayoutDetail, LayoutDetailPK> {


	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);


}
