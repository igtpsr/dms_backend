package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "QUERY_H")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class QueryH implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "QUERY_ID")
	private Integer queryId;

	@Column(name = "DOC_ID")
	private Integer docId;

	@Column(name = "QUERY_NAME")
	private String queryName;

	@Column(name = "QUERY_DESC")
	private String queryDesc;

	@Column(name = "QUERY_TYPE")
	private String queryType;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@Column(name = "MODIFY_TS")
	private Timestamp modify_ts;

}
