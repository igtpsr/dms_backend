package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.QueryH;

@Repository
public interface DestinationQueryHDao extends CrudRepository<QueryH, Integer> {

	@Query("Select MAX(qh.queryId)+1 from QueryH qh")
	public Integer getTheMaximumQueryId();

	@Query("Select COUNT(*) from QueryH qh Where qh.queryId =:queryId And qh.queryName =:queryName")
	public Integer getTheDestinationId(Integer queryId, String queryName);

	@Query("Select COUNT(*) from QueryH qh Where qh.queryId =:queryId")
	public Integer getTheDestinationIdCounNumberByQueryId(Integer queryId);

	@Query("Select qh.queryId from QueryH qh where qh.queryName =:queryName")
	public List<Integer> getTheQueryIdByQueryName(String queryName);

}
