package com.retelzy.brim.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.retelzy.brim.cutomException.BasicAuthenticationException;
import com.retelzy.brim.cutomException.DuplicateRoleException;
import com.retelzy.brim.cutomException.DuplicateUserException;
import com.retelzy.brim.cutomException.UserNotFoundException;
import com.retelzy.brim.entity.user.RoleProfile;
import com.retelzy.brim.entity.user.UserProfile;
import com.retelzy.brim.entity.user.service.UserService;
import com.retelzy.brim.util.AES;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.GoogleGuavaCache;
import com.retelzy.brim.util.RetelzyTokenGenrator;

@RestController
@RequestMapping(value = "/authApi")
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private GoogleGuavaCache googleGuavaCache;
	
	@Autowired
	private ConfigFileReader configFileReader;

	@Autowired
	private UserService userService;

	private final Logger logger = LoggerFactory.getLogger(LoginController.class);

	/* This API working for Creating User Profile details */
	@PostMapping(value = "/createUserProfile")
	//Commented by LOK17May2021 bacause of facing error
	public Map<String, Object> createUserProfile(@RequestBody UserProfile userProfile) 
			throws InterruptedException, ExecutionException {
	//public List<UserProfile> createUserProfile(@RequestBody UserProfile userProfile) {

		try {
			userProfile.setModifyTs(new Date());
			/*Added by LOK24Feb2021 - Begin */
			/*Encrypting the frontend password and saving it in DB*/
			String originalString = userProfile.getPassword();
		    String encryptedString = AES.encrypt(originalString, configFileReader.getPropertyValue("secretKey"));
			userProfile.setPassword(encryptedString);
			/*Added by LOK24Feb2021 - End*/
			
			//List<UserProfile> listOfUserProfile = userService.getAllAvailableUserProfile();
			List<String> listOfUserProfile = userService.getAllAvailableUserName();
			System.out.print("listOfUserProfile : " + listOfUserProfile);
			for (String up : listOfUserProfile) {
				System.out.print("Each profile" + up);
				if (userProfile.getUserName().equalsIgnoreCase(up)) {
					throw new DuplicateUserException("Oops, Not able to create duplicate user profile");
				}
			}
			userService.saveOrUpdate(userProfile);
			
			//Commented by LOK17May2021 bacause of facing error - Begin
			Map<String, Object> usercreationStatusMap = new LinkedHashMap<String, Object>();
			usercreationStatusMap.put("Status", "Success");
			return usercreationStatusMap;
			//List<UserProfile> listOfUserProfileAfterSaving = userService.getAllAvailableUserProfile();
			//listOfUserProfileAfterSaving.sort(Comparator.comparing(UserProfile::getUserName));
			//return listOfUserProfileAfterSaving;
			//Commented by LOK17May2021 bcz of facing error - End
			
		} catch (DuplicateUserException e) {
			logger.error("Exception Occured While Creating User Profile :" + e.getMessage());
			throw new DuplicateUserException(e.getMessage());
		} catch (Exception e) {
			logger.error("Exception Occured While Creating User Profile :" + e.getMessage());
			throw new UserNotFoundException("Oops, Not able to create user profile");
		}

	}

	/* This API is Working for Updating user profile details */
	@PutMapping(value = "/updateUserProfile")
	public UserProfile updateUserProfile(@RequestBody UserProfile userProfile) {

		try {
			userProfile.setModifyTs(new Date());
			
			/*Added by LOK24Feb2021 - Begin */
			/*Encrypting the front-end password and saving it in DB*/
			String originalString = userProfile.getPassword();
		    String encryptedString = AES.encrypt(originalString, configFileReader.getPropertyValue("secretKey")) ;
			userProfile.setPassword(encryptedString);
			/*Added by LOK24Feb2021 - End */
			
			return userService.saveOrUpdate(userProfile);
		} catch (Exception e) {
			logger.error("Exception Occured While Updating User Profile :" + e.getMessage());
			throw new UserNotFoundException("Oops, Not able to update user profile");
		}

	}

	/*
	 * This API is Working for Creating Role like, Admin, Functiona Admin, Technical
	 * Admin
	 */
	@PostMapping(value = "/createRoleProfile")
	public List<RoleProfile> createRoleProfile(@RequestBody RoleProfile roleProfile) {

		try {
			roleProfile.setModifyTs(new Date());
			List<RoleProfile> listOfRoleProfile = userService.getAllAvailableRoleProfile();
			for (RoleProfile rp : listOfRoleProfile) {
				if (roleProfile.getRoleName().equalsIgnoreCase(rp.getRoleName())) {
					throw new DuplicateRoleException("Oops, Not able to create duplicate role profile");
				}
			}
			userService.saveOrUpdateRoleProfile(roleProfile);
			List<RoleProfile> listOfRoleProfileAfterSaving = userService.getAllAvailableRoleProfile();
			listOfRoleProfileAfterSaving.sort(Comparator.comparing(RoleProfile::getRoleName));
			return listOfRoleProfileAfterSaving;
		} catch (DuplicateRoleException e) {
			logger.error("Exception Occured While Creating Role Profile :" + e.getMessage());
			throw new DuplicateRoleException(e.getMessage());
		} catch (Exception e) {
			logger.error("Exception Occured While Creating Role Profile :" + e.getMessage());
			throw new UserNotFoundException("Oops, Not able to create role profile");
		}

	}

	/* This API is Wokring for updating Role Details */
	@PutMapping(value = "/updateRoleProfile")
	public RoleProfile updateRoleProfile(@RequestBody RoleProfile roleProfile) {

		try {
			roleProfile.setModifyTs(new Date());
			return userService.saveOrUpdateRoleProfile(roleProfile);
		} catch (Exception e) {
			logger.error("Exception Occured While updating Role Profile :" + e.getMessage());
			throw new UserNotFoundException("Oops, Not able to create role profile");
		}

	}

	@GetMapping(value = "/getAllRoleProfile")
	public List<RoleProfile> getAllRoleProfile() {
		List<RoleProfile> listOfRoleProfile = userService.getAllAvailableRoleProfile();
		listOfRoleProfile.sort(Comparator.comparing(RoleProfile::getRoleName));
		return listOfRoleProfile;
	}

	@GetMapping(value = "/getAllUserProfile")
	public List<UserProfile> getAllUserProfile() {
		List<UserProfile> listOfUserProfile = userService.getAllAvailableUserProfile();
		listOfUserProfile.sort(Comparator.comparing(UserProfile::getUserName));
		
		/*Added by LOK24Feb2021 - Begin */
		List<UserProfile> decryptedlistOfUserProfile = new ArrayList<UserProfile>();
		for(UserProfile currentUserProfile: listOfUserProfile) {
			String encryptedString = currentUserProfile.getPassword();
		    String decryptedString = AES.decrypt(encryptedString, configFileReader.getPropertyValue("secretKey")) ;
			currentUserProfile.setPassword(decryptedString);
			decryptedlistOfUserProfile.add(currentUserProfile);
		}
		return decryptedlistOfUserProfile;
		/*Added by LOK24Feb2021 - End */
		//return listOfUserProfile;
	}

	/*
	 * This API is Working for deleting Role details, here we are checking before
	 * deleting the roles as this role is associated with any User Profile, if
	 * associated, then we are throwing messsage, or else we are deleting the role.
	 */
	@DeleteMapping(value = "/deleteRoleProfile/{roleId}")
	public Map<String, Object> deleteRoleProfile(@PathVariable("roleId") Integer roleId) {

		Map<String, Object> map = new LinkedHashMap<String, Object>();
		Optional<RoleProfile> roleProfile = userService.getTheRoleProfileDetails(roleId);

		if (roleProfile.isPresent()) {
			List<UserProfile> userProfile = userService.getUserProfileDetailsByRoleId(roleProfile.get());

			if (userProfile.isEmpty()) {
				userService.deleteRoleProfile(roleId);
				map.put("Success", "Successfully deleted role profile");
				return map;
			} else {
				throw new UserNotFoundException("Oops, This role profile associted with user profile");
			}

		} else {
			throw new UserNotFoundException("Oops, Not able to find role profile details");
		}

	}

	/* This API is for Login */
	@PostMapping(value = "/brimLogin.do")
	public UserProfile authentication(@RequestBody UserProfile modifibleUsers) {

		logger.info("Login method called...");

		try {
			try {
				
				/*Added by LOK24Feb2021 - Begin */
				/*Encrypting the front-end password for comparing*/
				String originalString = modifibleUsers.getPassword(); 
				String encryptedString = AES.encrypt(originalString, configFileReader.getPropertyValue("secretKey")); 
				modifibleUsers.setPassword(encryptedString);
				/*Added by LOK24Feb2021 - End */
				
				
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(modifibleUsers.getUserName(),
						modifibleUsers.getPassword()));
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception Occured While Authenticate the User " + e.getMessage());
				throw new BasicAuthenticationException("Invalid Username/Password");
			}

			UserProfile userProfile = userService.getTheUserProfile(modifibleUsers.getUserName(),
					modifibleUsers.getPassword());
			System.out.println(userProfile.getEmail());
			if (userProfile != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date dateWithoutTime = sdf.parse(sdf.format(new Date()));
				
				/*PswdNeverExpires condition added by LOK11Feb2021 - Begin */
				if ((dateWithoutTime.compareTo(userProfile.getPasswordExpDate()) <= 0) || (userProfile.getPswdNeverExpires() == true)) {
					String token = RetelzyTokenGenrator.generateToken(modifibleUsers.getUserName());
					googleGuavaCache.saveTheToken(token, modifibleUsers.getUserName());
					userProfile.setPassword(null);
					userProfile.setToken(token);
				} else {
					throw new Exception("Password got expired. Please check with administrator.");
				}
				
			}
			return userProfile;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured While Authenticate the User " + e.getMessage());
			throw new BasicAuthenticationException(e.getMessage());
		}

	}

	/*
	 * This API is Working for updating expire time of token, basically we are
	 * giving token expire time as 15 minutes, after each updation expire time will
	 * increase by 15 minutes.
	 */
	@PutMapping(value = "/updateTheExpiryTimeOfToken")
	public Map<String, Object> updateTheExpiryTimeOfToken(@RequestBody UserProfile modifibleUsers) {

		String userName = googleGuavaCache.getToken(modifibleUsers.getToken());

		logger.info("Username :" + userName);

		if (userName != "") {
			googleGuavaCache.saveTheToken(modifibleUsers.getToken(), userName);

			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put("message", "Success");
			return map;
		} else {
			throw new BasicAuthenticationException("Token Expired");
		}

	}

	/*
	 * This API is Working for Logout from Application, basically we are clearing
	 * that particular token from cache.
	 */
	@PutMapping(value = "/logout.do")
	public Map<String, Object> invalidateTheToken(@RequestBody UserProfile modifibleUsers) {

		String userName = googleGuavaCache.getToken(modifibleUsers.getToken());

		logger.info("Username :" + userName);

		if (userName != "") {
			googleGuavaCache.clearToken(modifibleUsers.getToken());

			Map<String, Object> map = new LinkedHashMap<String, Object>();

			logger.info("<<<<<<<<<<<<<-------LOGGED OUT---------->>>>>>>>>");

			map.put("message", "Success");
			return map;
		} else {
			throw new BasicAuthenticationException("Token Expired");
		}

	}

}
