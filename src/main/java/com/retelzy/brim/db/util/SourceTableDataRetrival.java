package com.retelzy.brim.db.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.retelzy.brim.entity.source.ExtValidation;
import com.retelzy.brim.entity.source.ExtValidationMetaData;
import com.retelzy.brim.entity.source.SecBoFieldAcl;
import com.retelzy.brim.entity.source.SecBoFilter;
import com.retelzy.brim.entity.source.SecBoLevelAcl;
import com.retelzy.brim.entity.source.SecBointFilter;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.migration.model.MigrationId;
import com.retelzy.brim.util.Helper;

@Component
public class SourceTableDataRetrival {

	private final static Logger logger = LoggerFactory.getLogger(SourceTableDataRetrival.class);

	@Autowired
	private SourceDbService sourceDbService;

	public Map<String, List<Object>> getTheDataFromSourceTable(String className, Integer roleId, Integer layoutId, MigrationId migrationId) {
		Map<String, List<Object>> mapOfObject = new LinkedHashMap<String, List<Object>>();
		switch (className) {

		case "SecRoleProfile":
			List<Object> listOfObject = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfObject);
			return mapOfObject;

		case "DashSectionH":
			List<Object> listOfDashSectionH = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfDashSectionH);
			return mapOfObject;

		case "SecRoleResCtl":
			List<Object> listOfSecRoleCtl = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecRoleCtl);
			return mapOfObject;

		case "SmuRoleRootMenu":
			List<Object> listOfSmuRoleRootMenu = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSmuRoleRootMenu);
			return mapOfObject;

		case "SmuRoleMenuItem":
			List<Object> listOfSmuRoleMenuItem = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSmuRoleMenuItem);
			return mapOfObject;

		case "CollabFields":
			List<Object> listOfCollabFields = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfCollabFields);
			return mapOfObject;

		case "RoleResAttr":
			List<Object> listOfRoleResAttr = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfRoleResAttr);
			return mapOfObject;

		case "SecQuery":
			List<Object> listOfSecQueryAsObject = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecQueryAsObject);
			return mapOfObject;

		case "SecQueryGroup":
			List<Object> listOfSecQueryGroup = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecQueryGroup);
			return mapOfObject;

		case "SecBoLevelAcl":
			List<Object> listOfSecBoLevelAcl = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecBoLevelAcl);
			return mapOfObject;

		case "SecBoFieldAcl":
			List<Object> listOfSecBoFieldAcl = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecBoFieldAcl);
			return mapOfObject;

		case "SecBoFilter":
			List<Object> listOfSecBoFilter = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecBoFilter);
			return mapOfObject;

		case "SecBointFilter":
			List<Object> listOfSecBointFilter = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put(className, listOfSecBointFilter);
			return mapOfObject;

		case "SecBoProcess":
			List<Object> listOfSecBoProcessAsObject = sourceDbService.getTheDataByRoleId(roleId, className);
			mapOfObject.put("CustomProcess" + className, listOfSecBoProcessAsObject);
			return mapOfObject;

		case "DocView":
			List<Object> listOfDocViewAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfDocViewAsObject);
			return mapOfObject;
		case "DocViewElementD":
			List<Object> listOfDocViewElementDAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfDocViewElementDAsObject);
			return mapOfObject;
		case "LayoutDetail":
			List<Object> listOfLayoutDetailAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutDetailAsObject);
			return mapOfObject;
		case "LayoutElementD":
			List<Object> listOfLayoutElementDObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutElementDObject);
			return mapOfObject;
		case "LayoutI18nRes":
			List<Object> listOfLayout19NResAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayout19NResAsObject);
			return mapOfObject;
		case "LayoutLevelGroup":
			List<Object> listOfLayoutLevelGroupAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutLevelGroupAsObject);
			return mapOfObject;
		case "LayoutPage":
			List<Object> listOfLayoutPageAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutPageAsObject);
			return mapOfObject;
		case "LayoutProfile":
			List<Object> listOfLayoutProfileAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutProfileAsObject);
			return mapOfObject;
		case "LayoutProfileD":
			List<Object> listOfLayoutProfileDAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutProfileDAsObject);
			return mapOfObject;
		case "LayoutSection":
			List<Object> listOfLayoutSectionAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutSectionAsObject);
			return mapOfObject;
		case "LayoutSectionD":
			List<Object> listOfLayoutSectionDAsObject = sourceDbService.getTheDataByLayoutId(layoutId, className, migrationId);
			mapOfObject.put(className, listOfLayoutSectionDAsObject);
			return mapOfObject;

		default:
			logger.error("This Class is Not Added in Swich Case Statemnt :" + className);
			return new LinkedHashMap<String, List<Object>>();

		}

	}

	/* This method execute for Security Filter Table */
	public Map<String, List<Object>> getTheDataFromSourceTable(Map<Integer, String> queryIdMap, String className,
			Integer roleId, MigrationId migrationId) {
		Map<String, List<Object>> mapOfObject = new LinkedHashMap<String, List<Object>>();

		switch (className.trim()) {

		case "SecBoLevelAcl":
			List<Object> listOfSecBoLevelAclAsObject = sourceDbService.getTheDataByRoleId(roleId, className);

			List<SecBoLevelAcl> listOfSecBoLevel = listOfSecBoLevelAclAsObject.parallelStream()
					.map(element -> (SecBoLevelAcl) element).collect(Collectors.toList());

			// repalce query id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfSecBoLevel.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {
						Integer queryKey = itr.next();
						String queryVal = (String) queryIdMap.get(queryKey);
						if (queryKey.equals(listOfSecBoLevel.get(index).getId().getQueryId())) {
							listOfSecBoLevel.get(index).getId().setQueryId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put("Security" + className,
					listOfSecBoLevel.parallelStream().map(element -> (Object) element).collect(Collectors.toList()));
			return mapOfObject;

		case "SecBoFieldAcl":
			List<Object> listOfSecBoFieldAclAsObject = sourceDbService.getTheDataByRoleId(roleId, className);

			List<SecBoFieldAcl> listOfSecBoFieldAcl = listOfSecBoFieldAclAsObject.parallelStream()
					.map(element -> (SecBoFieldAcl) element).collect(Collectors.toList());

			// repalce query id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfSecBoFieldAcl.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {

						Integer queryKey = itr.next();

						String queryVal = (String) queryIdMap.get(queryKey);

						if (queryKey.equals(listOfSecBoFieldAcl.get(index).getId().getQueryId())) {
							listOfSecBoFieldAcl.get(index).getId().setQueryId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put("Security" + className,
					listOfSecBoFieldAcl.parallelStream().map(element -> (Object) element).collect(Collectors.toList()));

			return mapOfObject;

		case "SecBoFilter":
			List<Object> listOfSecBoFilterAsObject = sourceDbService.getTheDataByRoleId(roleId, className);

			List<SecBoFilter> listOfSecBoFilter = listOfSecBoFilterAsObject.parallelStream()
					.map(element -> (SecBoFilter) element).collect(Collectors.toList());

			// repalce query id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfSecBoFilter.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {

						Integer queryKey = itr.next();

						String queryVal = (String) queryIdMap.get(queryKey);

						if (queryKey.equals(listOfSecBoFilter.get(index).getId().getQueryId())) {
							listOfSecBoFilter.get(index).getId().setQueryId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put("Security" + className,
					listOfSecBoFilter.parallelStream().map(element -> (Object) element).collect(Collectors.toList()));

			return mapOfObject;

		case "SecBointFilter":
			List<Object> listOfSecBointFilterAsObject = sourceDbService.getTheDataByRoleId(roleId, className);

			List<SecBointFilter> listOfSecBointFilter = listOfSecBointFilterAsObject.parallelStream()
					.map(element -> (SecBointFilter) element).collect(Collectors.toList());

			// repalce query id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfSecBointFilter.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {

						Integer queryKey = itr.next();

						String queryVal = (String) queryIdMap.get(queryKey);

						if (queryKey.equals(listOfSecBointFilter.get(index).getId().getQueryId())) {
							listOfSecBointFilter.get(index).getId().setQueryId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put("Security" + className, listOfSecBointFilter.parallelStream()
					.map(element -> (Object) element).collect(Collectors.toList()));

			return mapOfObject;

		case "ExtValidation":

			List<Integer> whereList = Helper.mapKeyToList(queryIdMap);
			List<Object> listOfExtValidationAsObject = sourceDbService.getTheDataOfExtValidation(whereList, className, migrationId);

			List<ExtValidation> listOfExtValidation = listOfExtValidationAsObject.parallelStream()
					.map(element -> (ExtValidation) element).collect(Collectors.toList());

			// repalce validation id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfExtValidation.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {

						Integer queryKey = itr.next();

						String queryVal = (String) queryIdMap.get(queryKey);

						if (queryKey.equals(listOfExtValidation.get(index).getValidationId())) {
							listOfExtValidation.get(index).setValidationId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put(className,
					listOfExtValidation.parallelStream().map(element -> (Object) element).collect(Collectors.toList()));

			return mapOfObject;

			
		case "ExtValidationMetadata":

			List<Integer> whereList2 = Helper.mapKeyToList(queryIdMap);
			List<Object> listOfExtValidationMetaDataAsObject = sourceDbService.getTheDataOfExtValidation(whereList2, className, migrationId);

			List<ExtValidationMetaData> listOfExtValidationMetaData = listOfExtValidationMetaDataAsObject.parallelStream()
					.map(element -> (ExtValidationMetaData) element).collect(Collectors.toList());

			// repalce validation id value based on the Map value in Insert SQL
			if (!queryIdMap.isEmpty()) {

				for (Integer index = 0; index < listOfExtValidationMetaData.size(); index++) {

					Iterator<Integer> itr = queryIdMap.keySet().iterator();
					while (itr.hasNext()) {

						Integer queryKey = itr.next();

						String queryVal = (String) queryIdMap.get(queryKey);

						if (queryKey.equals(listOfExtValidationMetaData.get(index).getId().getValidationId())) {
							listOfExtValidationMetaData.get(index).getId().setValidationId(Integer.valueOf(queryVal));
						}
					}

				}
			}

			mapOfObject.put(className,
					listOfExtValidationMetaData.parallelStream().map(element -> (Object) element).collect(Collectors.toList()));

			return mapOfObject;
		default:
			logger.error("This Class is Not Added in Swich Case Statemnt :" + className);
			return new LinkedHashMap<String, List<Object>>();

		}

	}

}
