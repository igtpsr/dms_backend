USE [DMS]
GO

IF OBJECT_ID ( 'RIM_MIGRATION_FROM_TEST_TO_DEV_SECURITY', 'P' ) IS NOT NULL   
    DROP PROCEDURE RIM_MIGRATION_FROM_TEST_TO_DEV_SECURITY;  
GO

CREATE PROCEDURE [dbo].[RIM_MIGRATION_FROM_TEST_TO_DEV_SECURITY]
	  @roleIdList 			VARCHAR(500) = ''
	, @totalCount 			INT OUTPUT
	, @result				VARCHAR(500) OUTPUT

AS 
BEGIN

		SET NOCOUNT ON;
		DECLARE @rowcount					INT;
		DECLARE @ROLE_ID				    INT;
		DECLARE @ROLE_NAME			    	VARCHAR(50);
		DECLARE @MODIFY_USER			    VARCHAR(18);
		DECLARE @MODIFY_TS 				    datetime;
		DECLARE @PROP_NAME 					VARCHAR(20);
		DECLARE @PROP_VALUE 				VARCHAR(250);
		DECLARE @DOC_VIEW_ID 				INT;
		DECLARE @SECTION_ID 				INT;
		DECLARE @I18N_RES_ID          		INT;
		DECLARE @ACL                  		INT;
		DECLARE @SECTION_TYPE               INT;
		DECLARE @COL_POS                    INT;
		DECLARE @ROW_POS                    INT;
		DECLARE @BACKGROUND                 VARCHAR(35);
		DECLARE @HEIGHT                     INT;
		DECLARE @WIDTH	                    INT;
		DECLARE @GROUP_ID                   INT;
		DECLARE @SMU_RES_ID                 INT;
		DECLARE @SMU_TYPE_ID                INT;
		DECLARE @ENABLED                    INT;
		DECLARE @PARENT_DOC_ID              INT;
		DECLARE @PARENT_LEVEL_ID            INT;
		DECLARE @RELATION_ID                INT;
		DECLARE @MENU_ITEM_ID               INT;
		DECLARE @MENU_ITEM_TYPE             INT;
		DECLARE @MENU_POS                   INT;
		DECLARE @DOC_VIEW_REL_ID            INT;
		DECLARE @SORT_TYPE                  INT;
		DECLARE @TYPE                       VARCHAR(1);
		DECLARE @QUERY_ID                   INT;
		DECLARE @POS                        INT;
		DECLARE @QUERY_GROUP_ID             INT;
		DECLARE @SECTION_CLASS        		VARCHAR(150);
		DECLARE @DOC_ID						INT;
		DECLARE @LEVEL_ID                   INT;
		DECLARE @FIELD_ID                   INT;	

		DECLARE @PROCESS_ID                 INT;
		DECLARE @PROCESS_NAME               VARCHAR(50);
		DECLARE @ACTION_CLASS               VARCHAR(250);
		DECLARE @PARAMS                     VARCHAR(250);
        DECLARE @PRES_ID                    INT;
        DECLARE @PTYPE_ID                   INT;
		DECLARE @MENU_ITEM_NAME             VARCHAR(50);
		DECLARE @URL                        VARCHAR(250);
		DECLARE @PARENT_ITEM_ID             INT;
	    DECLARE @SEQUENCE_NAME              VARCHAR(16);
        DECLARE @CURRENTNUM                 INT;
        DECLARE @STARTNUM                   INT;
        DECLARE @ENDNUM                     INT;
        DECLARE @STEPNUM                    INT;
        DECLARE @PREFIX                     VARCHAR(16);
        DECLARE @SUFFIX                     VARCHAR(16);
        DECLARE @NO_OF_DIGITS               INT;
        DECLARE @ISALPHANUM                 VARCHAR(1);
        DECLARE @SEQUENCE_TYPE              VARCHAR(6);
        DECLARE @ROLLOVER                   VARCHAR(1);
        DECLARE @ROLLOVER_TS                datetime;
        DECLARE @QUERY_NAME                 VARCHAR(30);
        DECLARE @QUERY_DESC                 VARCHAR(250);
        DECLARE @QUERY_TYPE                 VARCHAR(1);
		DECLARE @QUERY_XML                  NVARCHAR(MAX);
		DECLARE @DEFN_ID                    INT;
        DECLARE @PARENT_DEFN_ID             INT;
        DECLARE @DEPEND_DOC_ID              INT;
        DECLARE @DEPEND_PROC_ID             INT;
	    DECLARE @WS_NAME                    VARCHAR(250);
        DECLARE @WS_ID                      INT;
        DECLARE @SECTION_TAG_NAME           VARCHAR(30);
        DECLARE @TYPE_ID                    INT;
		DECLARE @RES_ID                     INT;
		DECLARE @GROUP_NAME                 VARCHAR(30);
		DECLARE @GROUP_DESC                 VARCHAR(250);
		DECLARE @DISP_COUNT                 VARCHAR(6);

	-- Spliting list of layoutId's
	-- dbo.SplitString function getting call
	SELECT * INTO #rolelist 
	FROM dbo.SplitString(@roleIdList, ',')
	
	--Cursor for SEC_ROLE_PROFILE Table -Begin
	IF OBJECT_ID ( 'CR_SEC_ROLE_PROFILE', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_ROLE_PROFILE;
	
	DECLARE CR_SEC_ROLE_PROFILE CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_ROLE_PROFILE]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_PROFILE]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_ROLE_PROFILE Table -End
	
	--Cursor for QUERY_H Table -Begin
	IF OBJECT_ID ( 'CR_QUERY_H', 'C' ) IS NOT NULL   
		DEALLOCATE CR_QUERY_H;
	
	DECLARE CR_QUERY_H CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[QUERY_H]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[QUERY_H]		
	--Cursor for QUERY_H Table -End
	
	--Cursor for QUERY_GROUP_H Table -Begin
	IF OBJECT_ID ( 'CR_QUERY_GROUP_H', 'C' ) IS NOT NULL   
		DEALLOCATE CR_QUERY_GROUP_H;
	
	DECLARE CR_QUERY_GROUP_H CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[QUERY_GROUP_H]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[QUERY_GROUP_H]
	--Cursor for QUERY_GROUP_H Table -End
	
	--Cursor for SEC_QUERY_GROUP Table -Begin
	IF OBJECT_ID ( 'CR_SEC_QUERY_GROUP', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_QUERY_GROUP;
	
	DECLARE CR_SEC_QUERY_GROUP CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_QUERY_GROUP]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY_GROUP]
			WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_QUERY_GROUP Table -End
	
	--Cursor for SEC_BO_FIELD_ACL Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BO_FIELD_ACL', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_FIELD_ACL;
	
	DECLARE CR_SEC_BO_FIELD_ACL CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_FIELD_ACL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)	
--	EXCEPT
--		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FIELD_ACL]
--				WHERE ROLE_ID 
--					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_BO_FIELD_ACL Table -End
	
	-- Cursor for SEC_BO_FILTER Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BO_FILTER', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_FILTER;
	
	DECLARE CR_SEC_BO_FILTER CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_FILTER]
				WHERE ROLE_ID 
					IN (SELECT VALUE FROM #rolelist)
--	EXCEPT
--		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FILTER]
--				WHERE ROLE_ID 
--					IN (SELECT VALUE FROM #rolelist)
	-- Cursor for SEC_BO_FILTER Table -End
	
	--Cursor for SEC_BO_LEVEL_ACL Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BO_LEVEL_ACL', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_LEVEL_ACL;
	
	DECLARE CR_SEC_BO_LEVEL_ACL CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_LEVEL_ACL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)	
--	EXCEPT
--		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_LEVEL_ACL]
--				WHERE ROLE_ID  
--					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_BO_LEVEL_ACL Table -End
	
	-- Cursor for SEC_BOINT_FILTER Table - Begin
	IF OBJECT_ID ( 'CR_SEC_BOINT_FILTER', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BOINT_FILTER;
	
	DECLARE CR_SEC_BOINT_FILTER CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BOINT_FILTER]
				WHERE ROLE_ID 
					IN (SELECT VALUE FROM #rolelist)
--	EXCEPT
--		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BOINT_FILTER]
--				WHERE ROLE_ID 
--					IN (SELECT VALUE FROM #rolelist)
	-- Cursor for SEC_BOINT_FILTER Table - End
	
	--Cursor for SEC_ROLE_RES_CTL Table -Begin
	IF OBJECT_ID ( 'CR_SEC_ROLE_RES_CTL', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_ROLE_RES_CTL;
	
	DECLARE CR_SEC_ROLE_RES_CTL CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_ROLE_RES_CTL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
--	EXCEPT
--		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_RES_CTL]
--				WHERE ROLE_ID  
--					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_ROLE_RES_CTL Table -End
	
	--Cursor for SEC_QUERY Table -Begin
	IF OBJECT_ID ( 'CR_SEC_QUERY', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_QUERY;
	
	DECLARE CR_SEC_QUERY CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_QUERY]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SEC_QUERY Table -End
	
	--Cursor for QUERY_D Table -Begin
	IF OBJECT_ID ( 'CR_QUERY_D', 'C' ) IS NOT NULL   
		DEALLOCATE CR_QUERY_D;
	
	DECLARE CR_QUERY_D CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[QUERY_D]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[QUERY_D]
	--Cursor for QUERY_D Table -End
	
	--Cursor for DASH_SECTION_H Table -Begin
	IF OBJECT_ID ( 'CR_DASH_SECTION_H', 'C' ) IS NOT NULL   
		DEALLOCATE CR_DASH_SECTION_H;
	
	DECLARE CR_DASH_SECTION_H CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[DASH_SECTION_H]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[DASH_SECTION_H]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for DASH_SECTION_H Table -End
	
	--Cursor for SMU_ROLE_ROOT_MENU Table -Begin
	IF OBJECT_ID ( 'CR_SMU_ROLE_ROOT_MENU', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SMU_ROLE_ROOT_MENU;
	
	DECLARE CR_SMU_ROLE_ROOT_MENU CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SMU_ROLE_ROOT_MENU]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_ROOT_MENU]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SMU_ROLE_ROOT_MENU Table -End

	--Cursor for SMU_ROLE_MENU_ITEM Table -Begin
	IF OBJECT_ID ( 'CR_SMU_ROLE_MENU_ITEM', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SMU_ROLE_MENU_ITEM;
	
	DECLARE CR_SMU_ROLE_MENU_ITEM CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SMU_ROLE_MENU_ITEM]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_MENU_ITEM]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for SMU_ROLE_MENU_ITEM Table -End

	--Cursor for COLLAB_FIELDS Table -Begin
	IF OBJECT_ID ( 'CR_COLLAB_FIELDS', 'C' ) IS NOT NULL   
		DEALLOCATE CR_COLLAB_FIELDS;
	
	DECLARE CR_COLLAB_FIELDS CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[COLLAB_FIELDS]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[COLLAB_FIELDS]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for COLLAB_FIELDS Table -End

	--Cursor for ROLE_RES_ATTR Table -Begin
	IF OBJECT_ID ( 'CR_ROLE_RES_ATTR', 'C' ) IS NOT NULL   
		DEALLOCATE CR_ROLE_RES_ATTR;
	
	DECLARE CR_ROLE_RES_ATTR CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[ROLE_RES_ATTR]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[ROLE_RES_ATTR]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	--Cursor for ROLE_RES_ATTR Table -End

	--Cursor for SEC_BO_PROCESS Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BO_PROCESS', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_PROCESS;
	
	DECLARE CR_SEC_BO_PROCESS CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_PROCESS]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROCESS]
	--Cursor for SEC_BO_PROCESS Table -End
	
	-- Cursor for SEC_BO_PROC_DPD Table - Begin
	IF OBJECT_ID ( 'CR_SEC_BO_PROC_DPD', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_PROC_DPD;
	
	DECLARE CR_SEC_BO_PROC_DPD CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_PROC_DPD]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROC_DPD]
	-- Cursor for SEC_BO_PROC_DPD Table - End
	
	--Cursor for SEC_BOVIEW_RES Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BOVIEW_RES', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BOVIEW_RES;
	
	DECLARE CR_SEC_BOVIEW_RES CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BOVIEW_RES]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BOVIEW_RES]
	--Cursor for SEC_BOVIEW_RES Table -End
	
	--Cursor for SMU_RES_SHOW_ATTR Table -Begin
	IF OBJECT_ID ( 'CR_SMU_RES_SHOW_ATTR', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SMU_RES_SHOW_ATTR;
	
	DECLARE CR_SMU_RES_SHOW_ATTR CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SMU_RES_SHOW_ATTR]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SMU_RES_SHOW_ATTR]
	--Cursor for SMU_RES_SHOW_ATTR Table -End
	
	--Cursor for SMU_COMMON Table -Begin
	IF OBJECT_ID ( 'CR_SMU_COMMON', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SMU_COMMON;
	
	DECLARE CR_SMU_COMMON CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SMU_COMMON]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SMU_COMMON]
	--Cursor for SMU_COMMON Table -End

	--Cursor for SEQUENCE_NUMBERS Table -Begin
	IF OBJECT_ID ( 'CR_SEQUENCE_NUMBERS', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEQUENCE_NUMBERS;
	
	DECLARE CR_SEQUENCE_NUMBERS CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEQUENCE_NUMBERS]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEQUENCE_NUMBERS]
	--Cursor for SEQUENCE_NUMBERS Table -End
	
	--Cursor for SEC_DOC_VIEW_DEFN Table -Begin
	IF OBJECT_ID ( 'CR_SEC_DOC_VIEW_DEFN', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_DOC_VIEW_DEFN;
	
	DECLARE CR_SEC_DOC_VIEW_DEFN CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_DOC_VIEW_DEFN]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW_DEFN]
	--Cursor for SEC_DOC_VIEW_DEFN Table -End
	
	--Cursor for SEC_DOC_VIEW Table -Begin
	IF OBJECT_ID ( 'CR_SEC_DOC_VIEW', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_DOC_VIEW;
	
	DECLARE CR_SEC_DOC_VIEW CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_DOC_VIEW]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW]
	--Cursor for SEC_DOC_VIEW Table -End
	
	-- Cursor for SEC_BO_WS Table -Begin
	IF OBJECT_ID ( 'CR_SEC_BO_WS', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_BO_WS;
	
	DECLARE CR_SEC_BO_WS CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_BO_WS]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_BO_WS]
	-- Cursor for SEC_BO_WS Table -End
	
	-- Cursor for SEC_SECTION_MENU Table - Begin
	IF OBJECT_ID ( 'CR_SEC_SECTION_MENU', 'C' ) IS NOT NULL   
		DEALLOCATE CR_SEC_SECTION_MENU;
	
	DECLARE CR_SEC_SECTION_MENU CURSOR FOR 
		SELECT * FROM [DMS_TEMP].[dbo].[SEC_SECTION_MENU]
	EXCEPT
		SELECT * FROM [TSSDEV02SQL01].[dbo].[SEC_SECTION_MENU]
	-- Cursor for SEC_SECTION_MENU Table - End
	
	BEGIN TRANSACTION
	BEGIN TRY

	DELETE FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_RES_CTL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)

	DELETE FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FILTER]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)

	DELETE FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FIELD_ACL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)

	DELETE FROM [TSSDEV02SQL01].[dbo].[SEC_BO_LEVEL_ACL]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)
	DELETE FROM [TSSDEV02SQL01].[dbo].[SEC_BOINT_FILTER]
				WHERE ROLE_ID  
					IN (SELECT VALUE FROM #rolelist)

	SET @rowcount = 0;
	-- Start the execution of Cursor CR_SEC_ROLE_PROFILE.
	OPEN CR_SEC_ROLE_PROFILE
		FETCH NEXT FROM  CR_SEC_ROLE_PROFILE 
			INTO @ROLE_ID, @ROLE_NAME, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
					IF EXISTS (	
						SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_PROFILE]
							WHERE [ROLE_ID]      =  @ROLE_ID
							)
				BEGIN
						UPDATE SRP
							SET  ROLE_NAME         = @ROLE_NAME
								,I18N_RES_ID       = @I18N_RES_ID
								,MODIFY_TS         = @MODIFY_TS
								,MODIFY_USER       = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_PROFILE] SRP
							WHERE [ROLE_ID]      =  @ROLE_ID

					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_ROLE_PROFILE]	 ([ROLE_ID]
								,[ROLE_NAME]
								,[I18N_RES_ID]
								,[MODIFY_TS]
								,[MODIFY_USER])
					VALUES (@ROLE_ID
						  , @ROLE_NAME
						  , @I18N_RES_ID
						  , @MODIFY_TS
						  , @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_ROLE_PROFILE INTO @ROLE_ID, @ROLE_NAME, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_ROLE_PROFILE
		DEALLOCATE CR_SEC_ROLE_PROFILE;
	-- End the execution of Cursor CR_SEC_ROLE_PROFILE.
	
	-- Start the execution of Cursor CR_QUERY_H.
	OPEN CR_QUERY_H
		FETCH NEXT FROM  CR_QUERY_H 
			INTO @QUERY_ID, @DOC_ID, @QUERY_NAME, @QUERY_DESC, @QUERY_TYPE, @MODIFY_USER, @MODIFY_TS
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[QUERY_H]
							WHERE [QUERY_ID]            = @QUERY_ID
							AND [DOC_ID]                = @DOC_ID
							)
				BEGIN
						UPDATE QH
							SET   QUERY_NAME            = @QUERY_NAME
								, QUERY_DESC            = @QUERY_DESC
								, QUERY_TYPE            = @QUERY_TYPE
								, MODIFY_USER           = @MODIFY_USER
								, MODIFY_TS             = @MODIFY_TS
							FROM [TSSDEV02SQL01].[dbo].[QUERY_H] QH
							WHERE [QUERY_ID]            = @QUERY_ID
							AND [DOC_ID]                = @DOC_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[QUERY_H]	 ([QUERY_ID]
										,[DOC_ID]
										,[QUERY_NAME]
										,[QUERY_DESC]
										,[QUERY_TYPE]
										,[MODIFY_USER]
										,[MODIFY_TS]
								  )
					VALUES (  @QUERY_ID
							, @DOC_ID
							, @QUERY_NAME
							, @QUERY_DESC
							, @QUERY_TYPE
							, @MODIFY_USER
							, @MODIFY_TS
						  )
					    
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_QUERY_H INTO @QUERY_ID, @DOC_ID, @QUERY_NAME, @QUERY_DESC, @QUERY_TYPE, @MODIFY_USER, @MODIFY_TS
			END;
	CLOSE  CR_QUERY_H
	DEALLOCATE CR_QUERY_H;
	-- End the execution of Cursor CR_QUERY_H.

	-- Start the execution of Cursor CR_QUERY_GROUP_H.
	OPEN CR_QUERY_GROUP_H
		FETCH NEXT FROM  CR_QUERY_GROUP_H
			INTO @GROUP_ID, @GROUP_NAME, @GROUP_DESC, @DISP_COUNT, @MODIFY_USER, @MODIFY_TS
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[QUERY_GROUP_H]
							WHERE  [GROUP_ID]	   =  @GROUP_ID
							)
				BEGIN
						UPDATE QGH
							SET   GROUP_NAME           = @GROUP_NAME
								, GROUP_DESC           = @GROUP_DESC
								, DISP_COUNT           = @DISP_COUNT
								, MODIFY_USER          = @MODIFY_USER
								, MODIFY_TS            = @MODIFY_TS
							FROM [TSSDEV02SQL01].[dbo].[QUERY_GROUP_H] QGH
							WHERE [GROUP_ID]		   =  @GROUP_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[QUERY_GROUP_H]	 ([GROUP_ID]
									,[GROUP_NAME]
									,[GROUP_DESC]
									,[DISP_COUNT]
									,[MODIFY_USER]
									,[MODIFY_TS]
								  )
					VALUES (  @GROUP_ID
							, @GROUP_NAME
							, @GROUP_DESC
							, @DISP_COUNT
							, @MODIFY_USER
							, @MODIFY_TS
						  )
					    
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_QUERY_GROUP_H INTO @GROUP_ID, @GROUP_NAME, @GROUP_DESC, @DISP_COUNT, @MODIFY_USER, @MODIFY_TS
			END;
	CLOSE  CR_QUERY_GROUP_H
	DEALLOCATE CR_QUERY_GROUP_H;
	-- End the execution of Cursor CR_QUERY_GROUP_H.
	
	-- Start the execution of Cursor CR_SEC_QUERY_GROUP.
	OPEN CR_SEC_QUERY_GROUP
		FETCH NEXT FROM  CR_SEC_QUERY_GROUP 
			INTO  @ROLE_ID, @QUERY_GROUP_ID, @POS, @MODIFY_USER, @MODIFY_TS
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY_GROUP]
							WHERE [ROLE_ID] 		=  @ROLE_ID
							AND [QUERY_GROUP_ID]    =  @QUERY_GROUP_ID
							 )
				BEGIN
						UPDATE SQG
							SET   POS 				= @POS
								 ,MODIFY_TS 		= @MODIFY_TS
								 ,MODIFY_USER 		= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY_GROUP] SQG
							WHERE [ROLE_ID] 		=  @ROLE_ID
							AND [QUERY_GROUP_ID]    =  @QUERY_GROUP_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_QUERY_GROUP]	 ([ROLE_ID]
								  ,[QUERY_GROUP_ID]
								  ,[POS]
								  ,[MODIFY_USER]
								  ,[MODIFY_TS]
								  )
					VALUES (  @ROLE_ID
							, @QUERY_GROUP_ID
							, @POS
							, @MODIFY_USER
							, @MODIFY_TS
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_QUERY_GROUP INTO @ROLE_ID, @QUERY_GROUP_ID, @POS, @MODIFY_USER, @MODIFY_TS
			END;
	CLOSE  CR_SEC_QUERY_GROUP
	DEALLOCATE CR_SEC_QUERY_GROUP;
    -- End the execution of Cursor CR_SEC_QUERY_GROUP.
	
	-- Start the execution of Cursor CR_SEC_BO_FIELD_ACL
	OPEN CR_SEC_BO_FIELD_ACL
		FETCH NEXT FROM  CR_SEC_BO_FIELD_ACL 
			INTO @ROLE_ID, @DOC_ID, @ACL, @QUERY_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
			IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FIELD_ACL]
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [ACL]                   =  @ACL
							AND [QUERY_ID]              =  @QUERY_ID
							)
				BEGIN
						UPDATE SBFA
							SET   MODIFY_TS 		= @MODIFY_TS
								 ,MODIFY_USER 		= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FIELD_ACL] SBFA
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [ACL]                   =  @ACL
							AND [QUERY_ID]              =  @QUERY_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_FIELD_ACL]	 ([ROLE_ID]
								  ,[DOC_ID]
								  ,[ACL]
								  ,[QUERY_ID]
								  ,[MODIFY_TS]
								  ,[MODIFY_USER]
								)
						VALUES (	@ROLE_ID
								  , @DOC_ID
								  , @ACL
								  , @QUERY_ID
								  , @MODIFY_TS
								  , @MODIFY_USER
								)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_FIELD_ACL INTO @ROLE_ID, @DOC_ID, @ACL, @QUERY_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_FIELD_ACL
	DEALLOCATE CR_SEC_BO_FIELD_ACL;
	-- End the execution of Cursor CR_SEC_BO_FIELD_ACL.

	-- Start the execution of Cursor CR_SEC_BO_FILTER.
	OPEN CR_SEC_BO_FILTER
		FETCH NEXT FROM  CR_SEC_BO_FILTER 
			INTO @ROLE_ID, @DOC_ID, @QUERY_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FILTER]
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [QUERY_ID]              =  @QUERY_ID
							)
				BEGIN
						UPDATE SBF
							SET  MODIFY_TS             = @MODIFY_TS
								,MODIFY_USER           = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_FILTER] SBF
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [QUERY_ID]              =  @QUERY_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_FILTER]	 ([ROLE_ID]
							,[DOC_ID]
							,[QUERY_ID]
							,[MODIFY_TS]
							,[MODIFY_USER]
						)
					VALUES (@ROLE_ID
							, @DOC_ID
							, @QUERY_ID
							, @MODIFY_TS
							, @MODIFY_USER
					)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_FILTER INTO @ROLE_ID, @DOC_ID, @QUERY_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_FILTER
	DEALLOCATE CR_SEC_BO_FILTER;
	
	-- End the execution of Cursor CR_SEC_BO_FILTER.
	
	-- Start the execution of Cursor CR_SEC_BO_LEVEL_ACL.
	OPEN CR_SEC_BO_LEVEL_ACL
		FETCH NEXT FROM  CR_SEC_BO_LEVEL_ACL 
			INTO @ROLE_ID, @DOC_ID, @LEVEL_ID, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @ACL, @QUERY_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_LEVEL_ACL]
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [LEVEL_ID]              =  @LEVEL_ID
							AND [PARENT_DOC_ID]         =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]       =  @PARENT_LEVEL_ID
							AND [ACL]                   =  @ACL
							AND [QUERY_ID]              =  @QUERY_ID
							AND [RELATION_ID]           =  @RELATION_ID
							)
				BEGIN
						UPDATE SBLA
							SET    MODIFY_TS = @MODIFY_TS
								 , MODIFY_USER = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_LEVEL_ACL] SBLA
							WHERE [ROLE_ID]             =  @ROLE_ID
							AND [DOC_ID]                =  @DOC_ID
							AND [LEVEL_ID]              =  @LEVEL_ID
							AND [PARENT_DOC_ID]         =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]       =  @PARENT_LEVEL_ID
							AND [ACL]                   =  @ACL
							AND [QUERY_ID]              =  @QUERY_ID
							AND [RELATION_ID]           =  @RELATION_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_LEVEL_ACL]	 ([ROLE_ID]
								  ,[DOC_ID]
								  ,[LEVEL_ID]
								  ,[PARENT_DOC_ID]
								  ,[PARENT_LEVEL_ID]
								  ,[ACL]
								  ,[QUERY_ID]
								  ,[RELATION_ID]
								  ,[MODIFY_TS]
								  ,[MODIFY_USER]
								)
					VALUES (	    @ROLE_ID
								  , @DOC_ID
								  , @LEVEL_ID
								  , @PARENT_DOC_ID
								  , @PARENT_LEVEL_ID
								  , @ACL
								  , @QUERY_ID
								  , @RELATION_ID
								  , @MODIFY_TS
								  , @MODIFY_USER
								 )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_LEVEL_ACL INTO @ROLE_ID, @DOC_ID, @LEVEL_ID, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @ACL, @QUERY_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_LEVEL_ACL
	DEALLOCATE CR_SEC_BO_LEVEL_ACL;
	-- End the execution of Cursor CR_SEC_BO_LEVEL_ACL.

	-- Start the execution of Cursor CR_SEC_BOINT_FILTER.
	OPEN CR_SEC_BOINT_FILTER
		FETCH NEXT FROM  CR_SEC_BOINT_FILTER 
			INTO  @ROLE_ID, @DOC_ID, @LEVEL_ID, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @QUERY_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BOINT_FILTER]
							WHERE [ROLE_ID]            =  @ROLE_ID
							AND [DOC_ID]               =  @DOC_ID
							AND [LEVEL_ID]             =  @LEVEL_ID
							AND [PARENT_DOC_ID]        =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]      =  @PARENT_LEVEL_ID
								)
				BEGIN
						UPDATE SBF
							SET  QUERY_ID 			   = @QUERY_ID
								,RELATION_ID 		   = @RELATION_ID
								,MODIFY_TS             = @MODIFY_TS
								,MODIFY_USER           = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BOINT_FILTER] SBF
							WHERE [ROLE_ID]            =  @ROLE_ID
							AND [DOC_ID]               =  @DOC_ID
							AND [LEVEL_ID]             =  @LEVEL_ID
							AND [PARENT_DOC_ID]        =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]      =  @PARENT_LEVEL_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BOINT_FILTER]	 ([ROLE_ID]
								,[DOC_ID]
								,[LEVEL_ID]
								,[PARENT_DOC_ID]
								,[PARENT_LEVEL_ID]
								,[QUERY_ID]
								,[RELATION_ID]
								,[MODIFY_TS]
								,[MODIFY_USER]
					)
					VALUES (  @ROLE_ID
							, @DOC_ID
							, @LEVEL_ID
							, @PARENT_DOC_ID
							, @PARENT_LEVEL_ID
							, @QUERY_ID
							, @RELATION_ID
							, @MODIFY_TS
							, @MODIFY_USER
					)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BOINT_FILTER INTO @ROLE_ID, @DOC_ID, @LEVEL_ID, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @QUERY_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BOINT_FILTER
	DEALLOCATE CR_SEC_BOINT_FILTER;
	-- End the execution of Cursor CR_SEC_BOINT_FILTER.
	
	-- Start the execution of Cursor CR_SEC_ROLE_RES_CTL.
	OPEN CR_SEC_ROLE_RES_CTL
		FETCH NEXT FROM  CR_SEC_ROLE_RES_CTL 
			INTO @ROLE_ID, @DOC_ID, @SMU_RES_ID, @SMU_TYPE_ID, @ENABLED, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_RES_CTL]
							WHERE [ROLE_ID]         =  @ROLE_ID 
							AND [DOC_ID]      	    =  @DOC_ID 
							AND [SMU_RES_ID]        =  @SMU_RES_ID 
							AND [SMU_TYPE_ID] 	    =  @SMU_TYPE_ID
							AND [PARENT_DOC_ID]     =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]   =  @PARENT_LEVEL_ID
							AND [RELATION_ID]       =  @RELATION_ID
							)
				BEGIN
						UPDATE SRRC               
							SET  ENABLED            =  @ENABLED
								, MODIFY_TS         =  @MODIFY_TS
								, MODIFY_USER       =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_ROLE_RES_CTL] SRRC
							WHERE [ROLE_ID]         =  @ROLE_ID 
							AND [DOC_ID]      	    =  @DOC_ID 
							AND [SMU_RES_ID]        =  @SMU_RES_ID 
							AND [SMU_TYPE_ID] 	    =  @SMU_TYPE_ID
							AND [PARENT_DOC_ID]     =  @PARENT_DOC_ID
							AND [PARENT_LEVEL_ID]   =  @PARENT_LEVEL_ID
							AND [RELATION_ID]       =  @RELATION_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_ROLE_RES_CTL]	 ([ROLE_ID]
								,[DOC_ID]
								,[SMU_RES_ID]
								,[SMU_TYPE_ID]
								,[ENABLED]
								,[PARENT_DOC_ID]
								,[PARENT_LEVEL_ID]
								,[RELATION_ID]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (	 @ROLE_ID
								, @DOC_ID
								, @SMU_RES_ID
								, @SMU_TYPE_ID
								, @ENABLED
								, @PARENT_DOC_ID
								, @PARENT_LEVEL_ID
								, @RELATION_ID
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_ROLE_RES_CTL INTO @ROLE_ID, @DOC_ID, @SMU_RES_ID, @SMU_TYPE_ID, @ENABLED, @PARENT_DOC_ID, @PARENT_LEVEL_ID, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_ROLE_RES_CTL
	DEALLOCATE CR_SEC_ROLE_RES_CTL;
	-- End the execution of Cursor CR_SEC_ROLE_RES_CTL.
	
		
	-- Start the execution of Cursor CR_SEC_QUERY.
	OPEN CR_SEC_QUERY
		FETCH NEXT FROM  CR_SEC_QUERY 
			INTO @ROLE_ID, @QUERY_ID, @POS, @MODIFY_USER, @MODIFY_TS
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY]
							WHERE [ROLE_ID] 			=  @ROLE_ID
							AND [QUERY_ID]              =  @QUERY_ID
							)
				BEGIN
						UPDATE SQ
							SET	   POS 					= @POS
								 , MODIFY_TS 			= @MODIFY_TS
								 , MODIFY_USER 			= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_QUERY] SQ
							WHERE [ROLE_ID] 			=  @ROLE_ID
							AND [QUERY_ID]              =  @QUERY_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_QUERY]	 ([ROLE_ID]
								  ,[QUERY_ID]
								  ,[POS]
								  ,[MODIFY_USER]
								  ,[MODIFY_TS]
								  )
					VALUES ( @ROLE_ID
							, @QUERY_ID
							, @POS
							, @MODIFY_USER
							, @MODIFY_TS
							)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_QUERY INTO @ROLE_ID, @QUERY_ID, @POS, @MODIFY_USER, @MODIFY_TS
			END;
	CLOSE  CR_SEC_QUERY
	DEALLOCATE CR_SEC_QUERY;
	-- End the execution of Cursor CR_SEC_QUERY.

	-- Start the execution of Cursor CR_QUERY_D.
	OPEN CR_QUERY_D
		FETCH NEXT FROM  CR_QUERY_D 
			INTO @QUERY_ID, @QUERY_XML, @MODIFY_USER, @MODIFY_TS
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[QUERY_D]
							WHERE [QUERY_ID]		=  @QUERY_ID
							)
				BEGIN
						UPDATE QD
							SET    	  QUERY_XML     =  @QUERY_XML
									, MODIFY_USER   =  @MODIFY_USER
									, MODIFY_TS     =  @MODIFY_TS
							FROM [TSSDEV02SQL01].[dbo].[QUERY_D] QD
							WHERE [QUERY_ID]		=  @QUERY_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[QUERY_D]	 ([QUERY_ID]
									,[QUERY_XML]
									,[MODIFY_USER]
									,[MODIFY_TS]
								  )
					VALUES (   @QUERY_ID
							 , @QUERY_XML
							 , @MODIFY_USER
							 , @MODIFY_TS
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_QUERY_D INTO @QUERY_ID, @QUERY_XML, @MODIFY_USER, @MODIFY_TS
			END;
	CLOSE  CR_QUERY_D
	DEALLOCATE CR_QUERY_D;
	-- End the execution of Cursor CR_QUERY_D.

	-- Start the execution of Cursor CR_DASH_SECTION_H.
	OPEN CR_DASH_SECTION_H
		FETCH NEXT FROM  CR_DASH_SECTION_H 
			INTO @ROLE_ID, @SECTION_ID, @I18N_RES_ID, @SECTION_TYPE, @COL_POS, @ROW_POS, @ACL, @SECTION_CLASS,
				@BACKGROUND, @HEIGHT, @WIDTH, @GROUP_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
					IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[DASH_SECTION_H]
							WHERE [ROLE_ID]      =  @ROLE_ID
							AND [SECTION_ID]     =  @SECTION_ID
							)
				BEGIN
						UPDATE DSH
							SET   I18N_RES_ID    =  @I18N_RES_ID
								, SECTION_TYPE   =  @SECTION_TYPE
								, COL_POS        =  @COL_POS
								, ROW_POS        =  @ROW_POS
								, ACL            =  @ACL
								, SECTION_CLASS  =  @SECTION_CLASS
								, BACKGROUND     =  @BACKGROUND
								, HEIGHT         =  @HEIGHT
								, WIDTH          =  @WIDTH
								, GROUP_ID       =  @GROUP_ID
								, MODIFY_TS      =  @MODIFY_TS
								, MODIFY_USER    =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[DASH_SECTION_H] DSH
							WHERE [ROLE_ID]      =  @ROLE_ID
							AND [SECTION_ID]     =  @SECTION_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[DASH_SECTION_H]	 ([ROLE_ID]
								,[SECTION_ID]
								,[I18N_RES_ID]
								,[SECTION_TYPE]
								,[COL_POS]
								,[ROW_POS]
								,[ACL]
								,[SECTION_CLASS]
								,[BACKGROUND]
								,[HEIGHT]
								,[WIDTH]
								,[GROUP_ID]
								,[MODIFY_TS]
								,[MODIFY_USER]
								  )
					VALUES (	@ROLE_ID
								, @SECTION_ID
								, @I18N_RES_ID
								, @SECTION_TYPE
								, @COL_POS
								, @ROW_POS
								, @ACL
								, @SECTION_CLASS
								, @BACKGROUND
								, @HEIGHT
								, @WIDTH
								, @GROUP_ID
								, @MODIFY_TS
								, @MODIFY_USER
								)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_DASH_SECTION_H 	INTO @ROLE_ID, @SECTION_ID, @I18N_RES_ID, @SECTION_TYPE, @COL_POS, @ROW_POS, @ACL, @SECTION_CLASS,
				@BACKGROUND, @HEIGHT, @WIDTH, @GROUP_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_DASH_SECTION_H
	DEALLOCATE CR_DASH_SECTION_H;
	-- End the execution of Cursor CR_DASH_SECTION_H.


	-- Start the execution of Cursor CR_SMU_ROLE_ROOT_MENU.
	OPEN CR_SMU_ROLE_ROOT_MENU
		FETCH NEXT FROM  CR_SMU_ROLE_ROOT_MENU 
		INTO @ROLE_ID, @DOC_VIEW_ID, @MENU_ITEM_ID, @MENU_ITEM_TYPE, @MENU_POS, @DOC_VIEW_REL_ID, @GROUP_ID, @SORT_TYPE, @MODIFY_TS, @MODIFY_USER
		WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_ROOT_MENU]
							WHERE [ROLE_ID] 	    = @ROLE_ID 
							AND [DOC_VIEW_ID]       = @DOC_VIEW_ID 
							AND [MENU_ITEM_ID]      = @MENU_ITEM_ID 
							AND [MENU_ITEM_TYPE]    = @MENU_ITEM_TYPE
							AND [MENU_POS]         	= @MENU_POS
							AND [DOC_VIEW_REL_ID]  = @DOC_VIEW_REL_ID
							)
				BEGIN
						UPDATE SRRM
							SET   GROUP_ID         = @GROUP_ID
								 , SORT_TYPE 		= @SORT_TYPE
								 , MODIFY_TS 		= @MODIFY_TS
								 , MODIFY_USER 		= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_ROOT_MENU] SRRM
							WHERE [ROLE_ID] 	    = @ROLE_ID 
							AND [DOC_VIEW_ID]       = @DOC_VIEW_ID 
							AND [MENU_ITEM_ID]      = @MENU_ITEM_ID 
							AND [MENU_ITEM_TYPE]    = @MENU_ITEM_TYPE 
							AND [MENU_POS]         	= @MENU_POS
							AND [DOC_VIEW_REL_ID]   = @DOC_VIEW_REL_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SMU_ROLE_ROOT_MENU]	 ([ROLE_ID]
								,[DOC_VIEW_ID]
								,[MENU_ITEM_ID]
								,[MENU_ITEM_TYPE]
								,[MENU_POS]
								,[DOC_VIEW_REL_ID]
								,[GROUP_ID]
								,[SORT_TYPE]
								,[MODIFY_TS]
								,[MODIFY_USER]
								  )
					VALUES (  @ROLE_ID
							, @DOC_VIEW_ID
							, @MENU_ITEM_ID
							, @MENU_ITEM_TYPE
							, @MENU_POS
							, @DOC_VIEW_REL_ID
							, @GROUP_ID
							, @SORT_TYPE
							, @MODIFY_TS
							, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SMU_ROLE_ROOT_MENU INTO @ROLE_ID, @DOC_VIEW_ID, @MENU_ITEM_ID, @MENU_ITEM_TYPE, @MENU_POS, @DOC_VIEW_REL_ID, @GROUP_ID, @SORT_TYPE, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SMU_ROLE_ROOT_MENU
	DEALLOCATE CR_SMU_ROLE_ROOT_MENU;
	-- End the execution of Cursor CR_SMU_ROLE_ROOT_MENU.

	-- Start the execution of Cursor CR_SMU_ROLE_MENU_ITEM.
	OPEN CR_SMU_ROLE_MENU_ITEM
		FETCH NEXT FROM  CR_SMU_ROLE_MENU_ITEM 
			INTO @ROLE_ID, @DOC_VIEW_ID, @MENU_ITEM_ID, @ENABLED, @MENU_POS, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_MENU_ITEM]
							WHERE [ROLE_ID]			=  @ROLE_ID
							AND [DOC_VIEW_ID]		=  @DOC_VIEW_ID
							AND [MENU_ITEM_ID]		=  @MENU_ITEM_ID
							)
				BEGIN
						UPDATE SRMI
							SET    ENABLED          =  @ENABLED
								 , MENU_POS         =  @MENU_POS
								 , MODIFY_TS 		=  @MODIFY_TS
								 , MODIFY_USER 		=  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SMU_ROLE_MENU_ITEM] SRMI
							WHERE [ROLE_ID]			=  @ROLE_ID
							AND [DOC_VIEW_ID]		=  @DOC_VIEW_ID
							AND [MENU_ITEM_ID]		=  @MENU_ITEM_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SMU_ROLE_MENU_ITEM]	 ([ROLE_ID]
								,[DOC_VIEW_ID]
								,[MENU_ITEM_ID]
								,[ENABLED]
								,[MENU_POS]
								,[MODIFY_TS]
								,[MODIFY_USER]
								  )
					VALUES (  @ROLE_ID
							, @DOC_VIEW_ID
							, @MENU_ITEM_ID
							, @ENABLED
							, @MENU_POS
							, @MODIFY_TS
							, @MODIFY_USER
												)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SMU_ROLE_MENU_ITEM INTO @ROLE_ID, @DOC_VIEW_ID, @MENU_ITEM_ID, @ENABLED, @MENU_POS, @MODIFY_TS, @MODIFY_USER
			END;

	CLOSE  CR_SMU_ROLE_MENU_ITEM
	DEALLOCATE CR_SMU_ROLE_MENU_ITEM;
	-- End the execution of Cursor CR_SMU_ROLE_MENU_ITEM.

	-- Start the execution of Cursor CR_COLLAB_FIELDS.
	OPEN CR_COLLAB_FIELDS
		FETCH NEXT FROM  CR_COLLAB_FIELDS 
			INTO @ROLE_ID, @DOC_ID, @LEVEL_ID, @FIELD_ID, @TYPE, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[COLLAB_FIELDS]
							WHERE [ROLE_ID]				=  @ROLE_ID
							AND [DOC_ID]				=  @DOC_ID
							AND [LEVEL_ID]				=  @LEVEL_ID
							AND [FIELD_ID]				=  @FIELD_ID
							)
				BEGIN
						UPDATE CF
							SET    TYPE					= @TYPE
								 , MODIFY_TS			= @MODIFY_TS
								 , MODIFY_USER			= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[COLLAB_FIELDS] CF
							WHERE [ROLE_ID]				=  @ROLE_ID
							AND [DOC_ID]				=  @DOC_ID
							AND [LEVEL_ID]				=  @LEVEL_ID
							AND [FIELD_ID]				=  @FIELD_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[COLLAB_FIELDS]	 ([ROLE_ID]
								 ,[DOC_ID]
								 ,[LEVEL_ID]
								 ,[FIELD_ID]
								 ,[TYPE]
								 ,[MODIFY_TS]
								 ,[MODIFY_USER]
								  )
					VALUES (  @ROLE_ID
							, @DOC_ID
							, @LEVEL_ID
							, @FIELD_ID
							, @TYPE
							, @MODIFY_TS
							, @MODIFY_USER
						  )
					    
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_COLLAB_FIELDS INTO @ROLE_ID, @DOC_ID, @LEVEL_ID, @FIELD_ID, @TYPE, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_COLLAB_FIELDS
	DEALLOCATE CR_COLLAB_FIELDS;
	-- End the execution of Cursor CR_COLLAB_FIELDS.

	-- Start the execution of Cursor CR_ROLE_RES_ATTR.
	OPEN CR_ROLE_RES_ATTR
		FETCH NEXT FROM  CR_ROLE_RES_ATTR 
			INTO @ROLE_ID, @DOC_VIEW_ID, @SMU_RES_ID, @SMU_TYPE_ID, @PROP_NAME, @PROP_VALUE, @RELATION_ID, @MODIFY_TS, @MODIFY_USER 
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[ROLE_RES_ATTR]
							WHERE [ROLE_ID]			=  @ROLE_ID
							AND [DOC_VIEW_ID]		=  @DOC_VIEW_ID
							AND [SMU_RES_ID]		=  @SMU_RES_ID
							AND [SMU_TYPE_ID]		=  @SMU_TYPE_ID
							AND [PROP_NAME]			=  @PROP_NAME
							AND [RELATION_ID]		=  @RELATION_ID
							)
				BEGIN
						UPDATE RRA
							SET     PROP_VALUE		= @PROP_VALUE
								  , MODIFY_TS		= @MODIFY_TS
								  , MODIFY_USER		= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[ROLE_RES_ATTR] RRA
							WHERE [ROLE_ID]			=  @ROLE_ID
							AND [DOC_VIEW_ID]		=  @DOC_VIEW_ID
							AND [SMU_RES_ID]		=  @SMU_RES_ID
							AND [SMU_TYPE_ID]		=  @SMU_TYPE_ID
							AND [PROP_NAME]			=  @PROP_NAME
							AND [RELATION_ID]		=  @RELATION_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[ROLE_RES_ATTR]	 ([ROLE_ID]
								  ,[DOC_VIEW_ID]
								  ,[SMU_RES_ID]
								  ,[SMU_TYPE_ID]
								  ,[PROP_NAME]
								  ,[PROP_VALUE]
								  ,[RELATION_ID]
								  ,[MODIFY_TS]
								  ,[MODIFY_USER]
								  )
					VALUES (  @ROLE_ID
							, @DOC_VIEW_ID
							, @SMU_RES_ID
							, @SMU_TYPE_ID
							, @PROP_NAME
							, @PROP_VALUE
							, @RELATION_ID
							, @MODIFY_TS
							, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_ROLE_RES_ATTR INTO @ROLE_ID, @DOC_VIEW_ID, @SMU_RES_ID, @SMU_TYPE_ID, @PROP_NAME, @PROP_VALUE, @RELATION_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_ROLE_RES_ATTR
	DEALLOCATE CR_ROLE_RES_ATTR;
	-- End the execution of Cursor CR_ROLE_RES_ATTR.

	-- Start the execution of Cursor CR_SEC_BO_PROCESS.
	OPEN CR_SEC_BO_PROCESS
		FETCH NEXT FROM  CR_SEC_BO_PROCESS 
			INTO @DOC_ID, @PROCESS_ID, @PROCESS_NAME, @ACTION_CLASS, @PARAMS, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
					IF EXISTS (	
						SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROCESS]
							WHERE [DOC_ID]         =  @DOC_ID
							AND [PROCESS_ID]       =  @PROCESS_ID
							)
				BEGIN
						UPDATE SBP
							SET  PROCESS_NAME      =  @PROCESS_NAME
								, ACTION_CLASS     =  @ACTION_CLASS
								, PARAMS           =  @PARAMS
								, I18N_RES_ID      =  @I18N_RES_ID
								, MODIFY_TS        =  @MODIFY_TS
								, MODIFY_USER      =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROCESS] SBP
							WHERE [DOC_ID]         =  @DOC_ID
							AND [PROCESS_ID]       =  @PROCESS_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_PROCESS]	 ([DOC_ID]
									,[PROCESS_ID]
									,[PROCESS_NAME]
									,[ACTION_CLASS]
									,[PARAMS]
									,[I18N_RES_ID]
									,[MODIFY_TS]
									,[MODIFY_USER]
							)
					VALUES (  @DOC_ID
							, @PROCESS_ID
							, @PROCESS_NAME
							, @ACTION_CLASS
							, @PARAMS
							, @I18N_RES_ID
							, @MODIFY_TS
							, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_PROCESS INTO @DOC_ID, @PROCESS_ID, @PROCESS_NAME, @ACTION_CLASS, @PARAMS, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_PROCESS
		DEALLOCATE CR_SEC_BO_PROCESS;
	-- End the execution of Cursor CR_SEC_BO_PROCESS.

	-- Start the execution of Cursor CR_SEC_BO_PROC_DPD.
	OPEN CR_SEC_BO_PROC_DPD
		FETCH NEXT FROM  CR_SEC_BO_PROC_DPD 
			INTO @DOC_ID, @PROCESS_ID, @DEPEND_DOC_ID, @DEPEND_PROC_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROC_DPD]
							WHERE [DOC_ID]             =  @DOC_ID
							AND [PROCESS_ID]           =  @PROCESS_ID
							AND [DEPEND_DOC_ID]        =  @DEPEND_DOC_ID
							AND [DEPEND_PROC_ID]       =  @DEPEND_PROC_ID
								)
				BEGIN
						UPDATE SBPD
							SET  MODIFY_TS             = @MODIFY_TS
								,MODIFY_USER           = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_PROC_DPD] SBPD
							WHERE [DOC_ID]             =  @DOC_ID
							AND [PROCESS_ID]           =  @PROCESS_ID
							AND [DEPEND_DOC_ID]        =  @DEPEND_DOC_ID
							AND [DEPEND_PROC_ID]       =  @DEPEND_PROC_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_PROC_DPD]	 ([DOC_ID]
									,[PROCESS_ID]
									,[DEPEND_DOC_ID]
									,[DEPEND_PROC_ID]
									,[MODIFY_TS]
									,[MODIFY_USER]
								)
					VALUES (  @DOC_ID
							, @PROCESS_ID
							, @DEPEND_DOC_ID
							, @DEPEND_PROC_ID
							, @MODIFY_TS
							, @MODIFY_USER
					)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_PROC_DPD INTO @DOC_ID, @PROCESS_ID, @DEPEND_DOC_ID, @DEPEND_PROC_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_PROC_DPD
	DEALLOCATE CR_SEC_BO_PROC_DPD;
	-- End the execution of Cursor CR_SEC_BO_PROC_DPD.
	
	-- Start the execution of Cursor CR_SEC_BOVIEW_RES.
	OPEN CR_SEC_BOVIEW_RES
		FETCH NEXT FROM  CR_SEC_BOVIEW_RES 
			INTO @DOC_VIEW_ID, @RES_ID, @TYPE_ID, @PROP_NAME, @PROP_VALUE, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
					IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BOVIEW_RES]
							WHERE [DOC_VIEW_ID]      =  @DOC_VIEW_ID
							AND [RES_ID]    		 =  @RES_ID
							AND [TYPE_ID]    		 =  @TYPE_ID
							)
				BEGIN
						UPDATE SBR
							SET   PROP_NAME          =  @PROP_NAME  
								, PROP_VALUE         =  @PROP_VALUE 
								, MODIFY_TS          =  @MODIFY_TS  
								, MODIFY_USER        =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BOVIEW_RES] SBR
							WHERE [DOC_VIEW_ID]      =  @DOC_VIEW_ID
							AND [RES_ID]    		 =  @RES_ID
							AND [TYPE_ID]    		 =  @TYPE_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BOVIEW_RES]	 ([DOC_VIEW_ID]
									,[RES_ID]
									,[TYPE_ID]
									,[PROP_NAME]
									,[PROP_VALUE]
									,[MODIFY_TS]
									,[MODIFY_USER]
								  )
					VALUES (	  @DOC_VIEW_ID
								, @RES_ID
								, @TYPE_ID
								, @PROP_NAME
								, @PROP_VALUE
								, @MODIFY_TS
								, @MODIFY_USER
								)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BOVIEW_RES 	INTO @DOC_VIEW_ID, @RES_ID, @TYPE_ID, @PROP_NAME, @PROP_VALUE, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BOVIEW_RES
	DEALLOCATE CR_SEC_BOVIEW_RES;
	-- End the execution of Cursor CR_SEC_BOVIEW_RES.

	-- Start the execution of Cursor CR_SMU_RES_SHOW_ATTR.
	OPEN CR_SMU_RES_SHOW_ATTR
		FETCH NEXT FROM  CR_SMU_RES_SHOW_ATTR 
			INTO @DOC_ID, @SMU_RES_ID, @SMU_TYPE_ID, @PROP_NAME, @PROP_VALUE, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SMU_RES_SHOW_ATTR]
							WHERE [DOC_ID]      	=  @DOC_ID 
							AND [SMU_RES_ID]        =  @SMU_RES_ID 
							AND [SMU_TYPE_ID] 	    =  @SMU_TYPE_ID
							AND [PROP_NAME]     	=  @PROP_NAME
							)
				BEGIN
						UPDATE SRSA              
							SET   PROP_VALUE        =  @PROP_VALUE
								, MODIFY_TS         =  @MODIFY_TS
								, MODIFY_USER       =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SMU_RES_SHOW_ATTR] SRSA
							WHERE [DOC_ID]      	=  @DOC_ID 
							AND [SMU_RES_ID]        =  @SMU_RES_ID 
							AND [SMU_TYPE_ID] 	    =  @SMU_TYPE_ID
							AND [PROP_NAME]     	=  @PROP_NAME
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SMU_RES_SHOW_ATTR]	 ([DOC_ID]
									,[SMU_RES_ID]
									,[SMU_TYPE_ID]
									,[PROP_NAME]
									,[PROP_VALUE]
									,[MODIFY_TS]
									,[MODIFY_USER]
								)
					VALUES (	  @DOC_ID
								, @SMU_RES_ID
								, @SMU_TYPE_ID
								, @PROP_NAME
								, @PROP_VALUE
								, @MODIFY_TS
								, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SMU_RES_SHOW_ATTR INTO @DOC_ID, @SMU_RES_ID, @SMU_TYPE_ID, @PROP_NAME, @PROP_VALUE, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SMU_RES_SHOW_ATTR
	DEALLOCATE CR_SMU_RES_SHOW_ATTR;
	-- End the execution of Cursor CR_SMU_RES_SHOW_ATTR.

	-- Start the execution of Cursor CR_SMU_COMMON.
	OPEN CR_SMU_COMMON
		FETCH NEXT FROM  CR_SMU_COMMON 
		INTO @MENU_ITEM_ID, @MENU_ITEM_NAME, @I18N_RES_ID, @URL, @PARENT_ITEM_ID, @MENU_POS, @MODIFY_TS, @MODIFY_USER
		WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SMU_COMMON]
							WHERE[MENU_ITEM_ID]      = @MENU_ITEM_ID 
							)
				BEGIN
						UPDATE SC
							SET  MENU_ITEM_NAME     = @MENU_ITEM_NAME
								, I18N_RES_ID        = @I18N_RES_ID
								, URL                = @URL
								, PARENT_ITEM_ID     = @PARENT_ITEM_ID
								, MENU_POS           = @MENU_POS
								, MODIFY_TS          = @MODIFY_TS
								, MODIFY_USER        = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SMU_COMMON] SC
							WHERE[MENU_ITEM_ID]      = @MENU_ITEM_ID 
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SMU_COMMON]	 ([MENU_ITEM_ID]
									,[MENU_ITEM_NAME]
									,[I18N_RES_ID]
									,[URL]
									,[PARENT_ITEM_ID]
									,[MENU_POS]
									,[MODIFY_TS]
									,[MODIFY_USER]
								  )
					VALUES (  @MENU_ITEM_ID
							, @MENU_ITEM_NAME
							, @I18N_RES_ID
							, @URL
							, @PARENT_ITEM_ID
							, @MENU_POS
							, @MODIFY_TS
							, @MODIFY_USER
						  )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SMU_COMMON INTO @MENU_ITEM_ID, @MENU_ITEM_NAME, @I18N_RES_ID, @URL, @PARENT_ITEM_ID, @MENU_POS, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SMU_COMMON
	DEALLOCATE CR_SMU_COMMON;
	-- End the execution of Cursor CR_SMU_COMMON.

	-- Start the execution of Cursor CR_SEQUENCE_NUMBERS.
	OPEN CR_SEQUENCE_NUMBERS
		FETCH NEXT FROM  CR_SEQUENCE_NUMBERS 
			INTO @SEQUENCE_NAME, @CURRENTNUM, @STARTNUM, @ENDNUM, @STEPNUM, @PREFIX, @SUFFIX, @NO_OF_DIGITS, @ISALPHANUM
					, @SEQUENCE_TYPE, @ROLLOVER, @ROLLOVER_TS, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEQUENCE_NUMBERS]
							WHERE [SEQUENCE_NAME]	=  @SEQUENCE_NAME
							)
				BEGIN
						UPDATE SN
							SET       CURRENTNUM    =  @CURRENTNUM
									, STARTNUM      =  @STARTNUM
									, ENDNUM        =  @ENDNUM
									, STEPNUM       =  @STEPNUM
									, PREFIX        =  @PREFIX
									, SUFFIX        =  @SUFFIX
									, NO_OF_DIGITS  =  @NO_OF_DIGITS
									, ISALPHANUM    =  @ISALPHANUM
									, SEQUENCE_TYPE =  @SEQUENCE_TYPE
									, ROLLOVER      =  @ROLLOVER
									, ROLLOVER_TS   =  @ROLLOVER_TS
									, MODIFY_TS     =  @MODIFY_TS
									, MODIFY_USER   =  @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEQUENCE_NUMBERS] SN
							WHERE [SEQUENCE_NAME]	=  @SEQUENCE_NAME
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEQUENCE_NUMBERS]	 ([SEQUENCE_NAME]
									,[CURRENTNUM]
									,[STARTNUM]
									,[ENDNUM]
									,[STEPNUM]
									,[PREFIX]
									,[SUFFIX]
									,[NO_OF_DIGITS]
									,[ISALPHANUM]
									,[SEQUENCE_TYPE]
									,[ROLLOVER]
									,[ROLLOVER_TS]
									,[MODIFY_TS]
									,[MODIFY_USER]
								  )
					VALUES (   @SEQUENCE_NAME
							 , @CURRENTNUM
							 , @STARTNUM
							 , @ENDNUM
							 , @STEPNUM
							 , @PREFIX
							 , @SUFFIX
							 , @NO_OF_DIGITS
							 , @ISALPHANUM
							 , @SEQUENCE_TYPE
							 , @ROLLOVER
							 , @ROLLOVER_TS
							 , @MODIFY_TS
							 , @MODIFY_USER
						)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEQUENCE_NUMBERS INTO @SEQUENCE_NAME, @CURRENTNUM, @STARTNUM, @ENDNUM, @STEPNUM, @PREFIX, @SUFFIX, @NO_OF_DIGITS, @ISALPHANUM
					, @SEQUENCE_TYPE, @ROLLOVER, @ROLLOVER_TS, @MODIFY_TS, @MODIFY_USER
			END;

	CLOSE  CR_SEQUENCE_NUMBERS
	DEALLOCATE CR_SEQUENCE_NUMBERS;
	-- End the execution of Cursor CR_SEQUENCE_NUMBERS.
	
	-- Start the execution of Cursor CR_SEC_DOC_VIEW_DEFN.
	OPEN CR_SEC_DOC_VIEW_DEFN
		FETCH NEXT FROM  CR_SEC_DOC_VIEW_DEFN 
			INTO @DEFN_ID, @DOC_VIEW_ID, @LEVEL_ID, @PARENT_DEFN_ID
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW_DEFN]
							WHERE [DEFN_ID]             =  @DEFN_ID
							)
				BEGIN
						UPDATE SDVD
							SET    DOC_VIEW_ID 			= @DOC_VIEW_ID
								 , LEVEL_ID 			= @LEVEL_ID
								 , PARENT_DEFN_ID 		= @PARENT_DEFN_ID
							FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW_DEFN] SDVD
							WHERE [DEFN_ID]             =  @DEFN_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW_DEFN]	 ([DEFN_ID]
									,[DOC_VIEW_ID]
									,[LEVEL_ID]
									,[PARENT_DEFN_ID]
								)
					VALUES (	      @DEFN_ID
									, @DOC_VIEW_ID
									, @LEVEL_ID
									, @PARENT_DEFN_ID
								 )
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_DOC_VIEW_DEFN INTO @DEFN_ID, @DOC_VIEW_ID, @LEVEL_ID, @PARENT_DEFN_ID
			END;
	CLOSE  CR_SEC_DOC_VIEW_DEFN
	DEALLOCATE CR_SEC_DOC_VIEW_DEFN;
-- End the execution of Cursor CR_SEC_DOC_VIEW_DEFN.

-- Start the execution of Cursor CR_SEC_DOC_VIEW
	OPEN CR_SEC_DOC_VIEW
		FETCH NEXT FROM  CR_SEC_DOC_VIEW 
			INTO @DOC_VIEW_ID, @DOC_ID,@MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
			IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW]
							WHERE [DOC_VIEW_ID]         =  @DOC_VIEW_ID
							AND [DOC_ID]                =  @DOC_ID
							)
				BEGIN
						UPDATE SDV
							SET   MODIFY_TS 			= @MODIFY_TS
								 ,MODIFY_USER 			= @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW] SDV
							WHERE [DOC_VIEW_ID]         =  @DOC_VIEW_ID
							AND [DOC_ID]                =  @DOC_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_DOC_VIEW]	 ([DOC_VIEW_ID]
									,[DOC_ID]
									,[MODIFY_TS]
									,[MODIFY_USER]
								)
						VALUES (	  @DOC_VIEW_ID
									, @DOC_ID
									, @MODIFY_TS
									, @MODIFY_USER
								)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_DOC_VIEW INTO @DOC_VIEW_ID, @DOC_ID,@MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_DOC_VIEW
	DEALLOCATE CR_SEC_DOC_VIEW;
-- End the execution of Cursor CR_SEC_DOC_VIEW.

-- Start the execution of Cursor CR_SEC_BO_WS.

	OPEN CR_SEC_BO_WS
		FETCH NEXT FROM  CR_SEC_BO_WS 
			INTO @WS_NAME, @DOC_ID, @WS_ID, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_WS]
							WHERE [WS_NAME]             =  @WS_NAME
							)
				BEGIN
						UPDATE SBW
							SET  DOC_ID              	= @DOC_ID
								, WS_ID             	= @WS_ID
								, I18N_RES_ID           = @I18N_RES_ID
								, MODIFY_TS             = @MODIFY_TS
								, MODIFY_USER           = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_BO_WS] SBW
							WHERE [WS_NAME]             =  @WS_NAME
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_BO_WS]	 ([WS_NAME]
								,[DOC_ID]
								,[WS_ID]
								,[I18N_RES_ID]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (  @WS_NAME
							, @DOC_ID
							, @WS_ID
							, @I18N_RES_ID
							, @MODIFY_TS
							, @MODIFY_USER
					)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_BO_WS INTO @WS_NAME, @DOC_ID, @WS_ID, @I18N_RES_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_BO_WS
	DEALLOCATE CR_SEC_BO_WS;
	
	-- End the execution of Cursor CR_SEC_BO_WS.
	
	-- Start the execution of Cursor CR_SEC_SECTION_MENU.
	
	OPEN CR_SEC_SECTION_MENU
		FETCH NEXT FROM  CR_SEC_SECTION_MENU
			INTO @DOC_VIEW_ID, @SECTION_TAG_NAME, @SMU_RES_ID, @TYPE_ID, @MODIFY_TS, @MODIFY_USER
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS (SELECT 1 
							FROM [TSSDEV02SQL01].[dbo].[SEC_SECTION_MENU]
							WHERE [DOC_VIEW_ID]          =  @DOC_VIEW_ID
							AND [SECTION_TAG_NAME]       =  @SECTION_TAG_NAME
							AND [SMU_RES_ID]             =  @SMU_RES_ID
								)
				BEGIN
						UPDATE SSM
							SET  TYPE_ID 			   = @TYPE_ID
								,MODIFY_TS             = @MODIFY_TS
								,MODIFY_USER           = @MODIFY_USER
							FROM [TSSDEV02SQL01].[dbo].[SEC_SECTION_MENU] SSM
							WHERE [DOC_VIEW_ID]          =  @DOC_VIEW_ID
							AND [SECTION_TAG_NAME]       =  @SECTION_TAG_NAME
							AND [SMU_RES_ID]             =  @SMU_RES_ID
					SET @rowcount = @rowcount+1;
				END
			ELSE 
				BEGIN
					INSERT INTO [TSSDEV02SQL01].[dbo].[SEC_SECTION_MENU]	 ([DOC_VIEW_ID]
								,[SECTION_TAG_NAME]
								,[SMU_RES_ID]
								,[TYPE_ID]
								,[MODIFY_TS]
								,[MODIFY_USER]
								)
					VALUES (   @DOC_VIEW_ID
							 , @SECTION_TAG_NAME
							 , @SMU_RES_ID
							 , @TYPE_ID
							 , @MODIFY_TS
							 , @MODIFY_USER
							)
					SET @rowcount = @rowcount+1;
				END
				FETCH NEXT FROM  CR_SEC_SECTION_MENU INTO @DOC_VIEW_ID, @SECTION_TAG_NAME, @SMU_RES_ID, @TYPE_ID, @MODIFY_TS, @MODIFY_USER
			END;
	CLOSE  CR_SEC_SECTION_MENU
	DEALLOCATE CR_SEC_SECTION_MENU;
	-- End the execution of Cursor CR_SEC_SECTION_MENU.
	
	SET @result = 'MIGRATION SUCCESS'
	SET @totalCount = @rowcount;

	COMMIT TRANSACTION

	END TRY

	BEGIN CATCH

	ROLLBACK TRANSACTION
		 if (ERROR_NUMBER() = 1222 )
		  BEGIN
		     PRINT ERROR_MESSAGE()
			 SET @result  = 'FAILED DUE TO DEADLOCK.  ERROR = ' + ERROR_MESSAGE()
		  END else 
		  BEGIN 
		   SET @result  = 'Failed Due to Error = ' + ERROR_MESSAGE()
		  END
	END CATCH
END