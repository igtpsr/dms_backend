package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "ADHOC_DOCUMENTS")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
@DynamicUpdate
public class AdhocDocuments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private AdhocDocumentsPK id;
	
	@Column(name = "CLOB_VALUE")
	private Clob clobValue;
	
	@Column(name = "BLOB_VALUE")
	private Blob blobValue;
	
	@Column(name = "CREATE_TS")
	private Timestamp createTs;
	
	@Column(name = "CREATE_USER")
	private String createUser;
	
	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;
	
	@Column(name = "MODIFY_USER")
	private String modifyUser;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
