package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBoLevelAcl;
import com.retelzy.brim.entity.destination.SecBoLevelAclPK;

@Repository
public interface DestinationSecBoLevelAclDao extends CrudRepository<SecBoLevelAcl, SecBoLevelAclPK> {

	@Query("Select bo from SecBoLevelAcl bo Where bo.id.roleId =:roleId")
	List<Object> getTheDataFromSecBoLevelAclTable(Integer roleId);

	@Query("Select bo.id.queryId from SecBoLevelAcl bo Where bo.id.roleId =:roleId")
	List<Integer> getTheQueryIdByRoleId(Integer roleId);

}
