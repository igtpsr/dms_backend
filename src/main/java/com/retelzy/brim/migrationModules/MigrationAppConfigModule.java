package com.retelzy.brim.migrationModules;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.entity.destination.ConfigDPK;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.ConfigD;
import com.retelzy.brim.entity.source.service.SourceDbService;

@Component
public class MigrationAppConfigModule {

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private MigrationBackupForAppConfigModule backupForAppConfigModule;

	@Autowired
	private SimpMessagingTemplate template;

	private final static Logger logger = LoggerFactory.getLogger(MigrationAppConfigModule.class);

	public void migrateAppConfigModule(Date fromDate, Date toDate, String clientSpec)
			throws IOException, ParseException {

		List<String> listOfTableName = new ArrayList<String>();

		logger.info("Client Spec Property :" + clientSpec);

		listOfTableName.add("CONFIG_D");

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		String fDate = formatter.format(fromDate);
		String tDate = formatter.format(toDate);

		Boolean backupStatus = backupForAppConfigModule.getBackupOfAppConfig(listOfTableName, fDate, tDate, clientSpec);

		if (backupStatus == true) {

			template.convertAndSend("/queue/percentage/others/", 35);

			List<ConfigD> listOfConfigDDetailsFromSource = new ArrayList<ConfigD>();

			if (clientSpec != null) {
				/* This Code for retrieving for only client spec property migration */
				listOfConfigDDetailsFromSource = sourceDbService.getTheConfigDTableDetails();
			} else {
				/* This Code for retrieving for All property migration */
				listOfConfigDDetailsFromSource = sourceDbService.getTheConfigDTableDetails(fromDate, toDate);

			}

			logger.info("List Of Config Details We Got From Source :" + listOfConfigDDetailsFromSource.size());

			List<com.retelzy.brim.entity.destination.ConfigD> listOfConfigDDetailsFromDestination = new ArrayList<com.retelzy.brim.entity.destination.ConfigD>();

			listOfConfigDDetailsFromSource.forEach(sourceConfigDetails -> {
				com.retelzy.brim.entity.destination.ConfigD destinationConfigD = new com.retelzy.brim.entity.destination.ConfigD();
				ConfigDPK configDPK = new ConfigDPK();
				configDPK.setConfigId(sourceConfigDetails.getId().getConfigId());
				configDPK.setConfigType(sourceConfigDetails.getId().getConfigType());
				configDPK.setPropName(sourceConfigDetails.getId().getPropName());

				destinationConfigD.setId(configDPK);
				destinationConfigD.setBlobValue(sourceConfigDetails.getBlobValue());
				destinationConfigD.setCategoryId(sourceConfigDetails.getCategoryId());
				destinationConfigD.setClobValue(sourceConfigDetails.getClobValue());
				destinationConfigD.setComments(sourceConfigDetails.getComments());
				destinationConfigD.setDatatype(sourceConfigDetails.getDatatype());
				destinationConfigD.setDescription(sourceConfigDetails.getDescription());
				destinationConfigD.setDocIdList(sourceConfigDetails.getDocIdList());
				destinationConfigD.setEncrypted(sourceConfigDetails.getEncrypted());
				destinationConfigD.setFactoryName(sourceConfigDetails.getFactoryName());
				destinationConfigD.setFuncUrl1(sourceConfigDetails.getFuncUrl1());
				destinationConfigD.setFuncUrl2(sourceConfigDetails.getFuncUrl2());
				destinationConfigD.setLevelId(sourceConfigDetails.getLevelId());
				destinationConfigD.setMaxlength(sourceConfigDetails.getMaxlength());
				destinationConfigD.setModifyTs(sourceConfigDetails.getModifyTs());
				destinationConfigD.setModifyUser(sourceConfigDetails.getModifyUser());
				destinationConfigD.setPosition(sourceConfigDetails.getPosition());
				destinationConfigD.setPropValue(sourceConfigDetails.getPropValue());
				destinationConfigD.setUseClob(sourceConfigDetails.getUseClob());

				listOfConfigDDetailsFromDestination.add(destinationConfigD);
			});

			template.convertAndSend("/queue/percentage/others/", 65);
			destinationDbService.updateTheConigDTableDetails(listOfConfigDDetailsFromDestination);
			template.convertAndSend("/queue/percentage/others/", 100);
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
		} else {
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
			throw new MigrationException("Backup failed, migration terminated");
		}

	}

}
