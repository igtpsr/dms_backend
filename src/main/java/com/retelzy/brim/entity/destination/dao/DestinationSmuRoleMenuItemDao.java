package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.SmuRoleMenuItem;
import com.retelzy.brim.entity.destination.SmuRoleMenuItemPK;

public interface DestinationSmuRoleMenuItemDao extends CrudRepository<SmuRoleMenuItem, SmuRoleMenuItemPK> {

	@Query("Select sm from SmuRoleMenuItem sm Where sm.id.roleId =:roleId")
	List<Object> getTheDataFromSmuRoleMenuItemTable(Integer roleId);

}
