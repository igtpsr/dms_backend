package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the ROLE_RES_ATTR database table.
 * 
 */
@Embeddable
public class RoleResAttrPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "DOC_VIEW_ID")
	private Integer docViewId;

	@Column(name = "SMU_RES_ID")
	private Integer smuResId;

	@Column(name = "SMU_TYPE_ID")
	private Integer smuTypeId;

	@Column(name = "PROP_NAME")
	private String propName;

	@Column(name = "RELATION_ID")
	private Integer relationId;

	public RoleResAttrPK() {
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getDocViewId() {
		return docViewId;
	}

	public void setDocViewId(Integer docViewId) {
		this.docViewId = docViewId;
	}

	public Integer getSmuResId() {
		return smuResId;
	}

	public void setSmuResId(Integer smuResId) {
		this.smuResId = smuResId;
	}

	public Integer getSmuTypeId() {
		return smuTypeId;
	}

	public void setSmuTypeId(Integer smuTypeId) {
		this.smuTypeId = smuTypeId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docViewId == null) ? 0 : docViewId.hashCode());
		result = prime * result + ((propName == null) ? 0 : propName.hashCode());
		result = prime * result + ((relationId == null) ? 0 : relationId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((smuResId == null) ? 0 : smuResId.hashCode());
		result = prime * result + ((smuTypeId == null) ? 0 : smuTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleResAttrPK other = (RoleResAttrPK) obj;
		if (docViewId == null) {
			if (other.docViewId != null)
				return false;
		} else if (!docViewId.equals(other.docViewId))
			return false;
		if (propName == null) {
			if (other.propName != null)
				return false;
		} else if (!propName.equals(other.propName))
			return false;
		if (relationId == null) {
			if (other.relationId != null)
				return false;
		} else if (!relationId.equals(other.relationId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (smuResId == null) {
			if (other.smuResId != null)
				return false;
		} else if (!smuResId.equals(other.smuResId))
			return false;
		if (smuTypeId == null) {
			if (other.smuTypeId != null)
				return false;
		} else if (!smuTypeId.equals(other.smuTypeId))
			return false;
		return true;
	}

}