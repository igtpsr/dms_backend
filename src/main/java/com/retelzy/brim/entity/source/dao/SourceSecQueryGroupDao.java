package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecQueryGroup;
import com.retelzy.brim.entity.source.SecQueryGroupPK;

@Repository
public interface SourceSecQueryGroupDao extends CrudRepository<SecQueryGroup, SecQueryGroupPK> {

	public List<Object> findByIdRoleId(Integer roleId);

}
