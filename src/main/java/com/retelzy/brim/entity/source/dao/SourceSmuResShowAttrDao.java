package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.SmuResShowAttr;
import com.retelzy.brim.entity.source.SmuResShowAttrPK;

public interface SourceSmuResShowAttrDao extends CrudRepository<SmuResShowAttr, SmuResShowAttrPK> {

	@Query("Select srs From SmuResShowAttr srs Where srs.id.smuResId in (:listOfProcessId) AND srs.id.smuTypeId =:smuTypeId")
	List<Object> getTheSmuResShowAttrDetails(List<Integer> listOfProcessId, Integer smuTypeId);

}
