package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.QueryH;

@Repository
public interface SourceQueryHDao extends CrudRepository<QueryH, Integer> {

	@Query("Select qh from QueryH qh Where qh.queryId in (:listOfQueryId)")
	public List<QueryH> getAllQueryHDetailByRoleIds(List<Integer> listOfQueryId);

	@Query("SELECT qh FROM QueryH qh WHERE qh.queryId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='LOOKUPFILTER') ORDER BY qh.queryId")
	public List<QueryH> getQueryInfo(Integer layoutId);

}
