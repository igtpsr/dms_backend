package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the LAYOUT_PAGE database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutPagePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="LAYOUT_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer layoutId;

	@Column(name="DOC_VIEW_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer docViewId;

	@Column(name="SECTION_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer sectionId;

	@Column(name="LAYOUT_LEVEL", unique=true, nullable=false)
	private Integer layoutLevel;


}