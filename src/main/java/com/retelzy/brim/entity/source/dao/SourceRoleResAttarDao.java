package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.RoleResAttr;
import com.retelzy.brim.entity.source.RoleResAttrPK;

public interface SourceRoleResAttarDao extends CrudRepository<RoleResAttr, RoleResAttrPK> {

	public List<Object> findByIdRoleId(Integer roleId);

}
