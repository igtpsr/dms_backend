package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBoviewRes;
import com.retelzy.brim.entity.source.SecBoviewResPK;

@Repository
public interface SourceSecBoviewResDao extends CrudRepository<SecBoviewRes, SecBoviewResPK> {

	@Query("Select sbr from SecBoviewRes sbr where sbr.id.resId in (:listOfProcessId) AND sbr.id.typeId =:typeId")
	List<Object> getTheDetailsOfSecBoviewTable(List<Integer> listOfProcessId, Integer typeId);

}
