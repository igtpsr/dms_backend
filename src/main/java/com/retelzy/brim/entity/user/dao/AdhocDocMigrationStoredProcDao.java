package com.retelzy.brim.entity.user.dao;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.retelzy.brim.migration.model.MigrationId;

@Service
public class AdhocDocMigrationStoredProcDao {
private static final Logger log = LoggerFactory.getLogger(AdhocDocMigrationStoredProcDao.class);

	
	@PersistenceUnit(unitName = "User")
	private EntityManagerFactory emf;
	
	
	public Map<String, Object> MigrateAdhocDocStoredProc(MigrationId migrationId, String sourceEnvType, String destinationEnvType) throws Exception {
		try {
			return this.callStoredProcedureToMigrateAdhocDoc(migrationId, sourceEnvType, destinationEnvType);
		} catch (Exception e) {
			log.error(" Error while running migration Adhoc Doc stored procedure --> " + e.getMessage());
			throw e;
		}
	}
	
	
	private Map<String, Object> callStoredProcedureToMigrateAdhocDoc(MigrationId migrationId, String sourceEnvType, String destinationEnvType) throws Exception {
		log.info("Stored procedure migration of --> \n " + migrationId.getModuleName());
		
		String customeRul6Reports = "";
		if (migrationId.getModuleName().equalsIgnoreCase("CUSTOMRULES")) {
			//--------------------------------------------------------
			//Converting list of values to Comma separated String of values
			StringBuilder strbul=new StringBuilder();
	        for(String str : migrationId.getListOfCustomRules())
	        {
	            strbul.append(str);
	            strbul.append(",");
	        }
	        strbul.setLength(strbul.length()-1);
	        customeRul6Reports = strbul.toString();
	        System.out.print("Custom Rules list : " + customeRul6Reports);
			//--------------------------------------------------------
		}else if (migrationId.getModuleName().equalsIgnoreCase("REPORTS")) {
			//--------------------------------------------------------
			//Converting list of values to Comma separated String of values
			StringBuilder strbul=new StringBuilder();
	        for(String str : migrationId.getListOfReports())
	        {
	            strbul.append(str);
	            strbul.append(",");
	        }
	        strbul.setLength(strbul.length()-1);
	        customeRul6Reports = strbul.toString();
	        System.out.print("Reports list : " + customeRul6Reports);
			//--------------------------------------------------------
		}
			
		if (!customeRul6Reports.isEmpty()) {
			EntityManager em = emf.createEntityManager();
			try {
				
				Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
				
				String storedProcName = "";
				if (sourceEnvType.equalsIgnoreCase("DEV") && destinationEnvType.equalsIgnoreCase("TEST")) {
					storedProcName = "RIM_MIGRATION_FROM_DEV_TO_TEST_CUSTOM_RULES_AND_REPORTS";
				} else if (sourceEnvType.equalsIgnoreCase("TEST") && destinationEnvType.equalsIgnoreCase("DEV")) {
					storedProcName = "RIM_MIGRATION_FROM_TEST_TO_DEV_CUSTOM_RULES_AND_REPORTS";
				}
				
				StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcName)
											 .registerStoredProcedureParameter("adhocDocNameList", String.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("totalCount", Integer.class, ParameterMode.OUT)
											 .registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
											 .setParameter("adhocDocNameList", customeRul6Reports);
				
				query.execute();
				String result = (String) query.getOutputParameterValue("result");
				Integer totalCount = (Integer) query.getOutputParameterValue("totalCount");
				
				log.info("  Result  : " + result);
				log.info("  Total count  : " + totalCount);
				
				migrationStatusMap.put("Status", result);
				migrationStatusMap.put("Count", totalCount);
				return migrationStatusMap;
			} catch (Exception e) {
				log.error(String.format("Data has not been updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
		return null;
	}

}
