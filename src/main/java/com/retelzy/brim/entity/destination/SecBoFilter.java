package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * The persistent class for the SEC_BO_FILTER database table.
 * 
 */
@Entity
@Table(name = "SEC_BO_FILTER")
public class SecBoFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecBoFilterPK id;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	// bi-directional many-to-one association to SecRoleProfile
	@MapsId("ROLE_ID")
	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private SecRoleProfile secRoleProfile;

	public SecBoFilter() {
	}

	public SecBoFilterPK getId() {
		return this.id;
	}

	public void setId(SecBoFilterPK id) {
		this.id = id;
	}

	public Timestamp getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public SecRoleProfile getSecRoleProfile() {
		return this.secRoleProfile;
	}

	public void setSecRoleProfile(SecRoleProfile secRoleProfile) {
		this.secRoleProfile = secRoleProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modifyTs == null) ? 0 : modifyTs.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((secRoleProfile == null) ? 0 : secRoleProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecBoFilter other = (SecBoFilter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modifyTs == null) {
			if (other.modifyTs != null)
				return false;
		} else if (!modifyTs.equals(other.modifyTs))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (secRoleProfile == null) {
			if (other.secRoleProfile != null)
				return false;
		} else if (!secRoleProfile.equals(other.secRoleProfile))
			return false;
		return true;
	}

}