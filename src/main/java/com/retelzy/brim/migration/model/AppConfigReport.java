package com.retelzy.brim.migration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class AppConfigReport {

	private String categoryName;
	private String description;
	private String propValue;
}
