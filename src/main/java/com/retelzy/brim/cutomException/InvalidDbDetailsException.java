package com.retelzy.brim.cutomException;

public class InvalidDbDetailsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDbDetailsException(String message) {
		super(message);
	}

}
