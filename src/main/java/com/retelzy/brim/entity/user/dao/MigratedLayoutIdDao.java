package com.retelzy.brim.entity.user.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;

public interface MigratedLayoutIdDao extends CrudRepository<MigratedLayoutIdDetails, Integer> {

	List<MigratedLayoutIdDetails> findByMigrationDetails(MigrationDetails migrationDetails);

}
