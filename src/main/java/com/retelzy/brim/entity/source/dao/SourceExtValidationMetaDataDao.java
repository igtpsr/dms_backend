package com.retelzy.brim.entity.source.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.ExtValidationMetaData;
import com.retelzy.brim.entity.source.ExtValidationMetaDataPK;

@Repository
public interface SourceExtValidationMetaDataDao extends CrudRepository<ExtValidationMetaData, ExtValidationMetaDataPK> {

	@Query("SELECT ev from ExtValidationMetaData ev where ev.id.validationId in (:whereList) and modifyTs>=:fromDate and modifyTs<=:toDate ")
	public List<Object> getTheDataOfExtValidationMetaData(List<Integer> whereList, Date fromDate, Date toDate);

}
