package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.source.SecQuery;
import com.retelzy.brim.entity.source.SecQueryPK;

public interface SourceSecQueryDao extends CrudRepository<SecQuery, SecQueryPK> {

	public List<Object> findByIdRoleId(Integer roleId);

}
