package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the SMU_ROLE_MENU_ITEM database table.
 * 
 */
@Entity
@Table(name = "SMU_ROLE_MENU_ITEM")
public class SmuRoleMenuItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SmuRoleMenuItemPK id;

	@Column(name = "ENABLED")
	private Integer enabled;

	@Column(name = "MENU_POS")
	private Integer menuPos;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	public SmuRoleMenuItem() {
	}

	public SmuRoleMenuItemPK getId() {
		return id;
	}

	public void setId(SmuRoleMenuItemPK id) {
		this.id = id;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getMenuPos() {
		return menuPos;
	}

	public void setMenuPos(Integer menuPos) {
		this.menuPos = menuPos;
	}

	public Timestamp getModifyTs() {
		return modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((menuPos == null) ? 0 : menuPos.hashCode());
		result = prime * result + ((modifyTs == null) ? 0 : modifyTs.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmuRoleMenuItem other = (SmuRoleMenuItem) obj;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (menuPos == null) {
			if (other.menuPos != null)
				return false;
		} else if (!menuPos.equals(other.menuPos))
			return false;
		if (modifyTs == null) {
			if (other.modifyTs != null)
				return false;
		} else if (!modifyTs.equals(other.modifyTs))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		return true;
	}

}