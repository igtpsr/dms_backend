package com.retelzy.brim.migration.model;

public class LayoutIdDetails {

	private Integer layoutId;
	private String layouDesc;

	public Integer getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}

	public String getLayouDesc() {
		return layouDesc;
	}

	public void setLayouDesc(String layouDesc) {
		this.layouDesc = layouDesc;
	}

}
