package com.retelzy.brim.entity.source.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecRoleProfile;

@Repository
public interface SourceSecRoleProfileDao extends CrudRepository<SecRoleProfile, Integer> {

	@Query(value = "Select sf.roleId from SecRoleProfile sf where sf.roleId >=:roleId")
	public List<Integer> getAllTheRoleIdByRoleId(Integer roleId);

	@Query(value = "Select sf.roleId from SecRoleProfile sf where sf.roleId =:roleId")
	public Optional<Integer> getTheRoleIdByRoleId(Integer roleId);

	@Query(value = "Select sf from SecRoleProfile sf where sf.roleId =:roleId")
	public List<Object> getAllTheDataByRoleId(Integer roleId);

	@Query(value = "Select sf from SecRoleProfile sf Where sf.roleId In (:listOfActiveRoleId)")
	public List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleId);

}
