package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SmuResShowAttr;
import com.retelzy.brim.entity.destination.SmuResShowAttrPK;

@Repository
public interface DestinationSmuResShowAttrDao extends CrudRepository<SmuResShowAttr, SmuResShowAttrPK> {

}
