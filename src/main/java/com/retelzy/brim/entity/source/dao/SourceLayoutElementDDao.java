package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutElementD;
import com.retelzy.brim.entity.source.LayoutElementDPK;

@Repository
public interface SourceLayoutElementDDao extends CrudRepository<LayoutElementD, LayoutElementDPK> {

	List<Object> findByIdLayoutId(Integer layoutId);


}
