package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the SEC_ROLE_RES_CTL database table.
 * 
 */
@Embeddable
public class SecRoleResCtlPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID", insertable = false, updatable = false)
	private Integer roleId;

	@Column(name = "DOC_ID")
	private Integer docId;

	@Column(name = "SMU_RES_ID")
	private Integer smuResId;

	@Column(name = "SMU_TYPE_ID")
	private Integer smuTypeId;

	@Column(name = "PARENT_DOC_ID")
	private Integer parentDocId;

	@Column(name = "PARENT_LEVEL_ID")
	private Integer parentLevelId;

	@Column(name = "RELATION_ID")
	private Integer relationId;

	public SecRoleResCtlPK() {
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getDocId() {
		return docId;
	}

	public void setDocId(Integer docId) {
		this.docId = docId;
	}

	public Integer getSmuResId() {
		return smuResId;
	}

	public void setSmuResId(Integer smuResId) {
		this.smuResId = smuResId;
	}

	public Integer getSmuTypeId() {
		return smuTypeId;
	}

	public void setSmuTypeId(Integer smuTypeId) {
		this.smuTypeId = smuTypeId;
	}

	public Integer getParentDocId() {
		return parentDocId;
	}

	public void setParentDocId(Integer parentDocId) {
		this.parentDocId = parentDocId;
	}

	public Integer getParentLevelId() {
		return parentLevelId;
	}

	public void setParentLevelId(Integer parentLevelId) {
		this.parentLevelId = parentLevelId;
	}

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((parentDocId == null) ? 0 : parentDocId.hashCode());
		result = prime * result + ((parentLevelId == null) ? 0 : parentLevelId.hashCode());
		result = prime * result + ((relationId == null) ? 0 : relationId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((smuResId == null) ? 0 : smuResId.hashCode());
		result = prime * result + ((smuTypeId == null) ? 0 : smuTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecRoleResCtlPK other = (SecRoleResCtlPK) obj;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (parentDocId == null) {
			if (other.parentDocId != null)
				return false;
		} else if (!parentDocId.equals(other.parentDocId))
			return false;
		if (parentLevelId == null) {
			if (other.parentLevelId != null)
				return false;
		} else if (!parentLevelId.equals(other.parentLevelId))
			return false;
		if (relationId == null) {
			if (other.relationId != null)
				return false;
		} else if (!relationId.equals(other.relationId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (smuResId == null) {
			if (other.smuResId != null)
				return false;
		} else if (!smuResId.equals(other.smuResId))
			return false;
		if (smuTypeId == null) {
			if (other.smuTypeId != null)
				return false;
		} else if (!smuTypeId.equals(other.smuTypeId))
			return false;
		return true;
	}

}