package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the COLLAB_FIELDS database table.
 * 
 */
@Entity
@Table(name="COLLAB_FIELDS")
@DynamicUpdate
public class CollabFields implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CollabFieldPK id;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER")
	private String modifyUser;

	@Column(name="[TYPE]")
	private String type;

	public CollabFields() {
	}

	public CollabFieldPK getId() {
		return this.id;
	}

	public void setId(CollabFieldPK id) {
		this.id = id;
	}

	public Timestamp getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}