package com.retelzy.brim.migrationModules;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.db.util.MigrationDbUtil;
import com.retelzy.brim.entity.destination.AdhocDocumentsPK;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.AdhocDocuments;
import com.retelzy.brim.entity.source.service.SourceDbService;

@Component
public class MigrationAdhocDocumentsModule {

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private MigrationDbUtil migrationDbUtil;

	@Autowired
	private SimpMessagingTemplate template;

	private final static Logger logger = LoggerFactory.getLogger(MigrationAdhocDocumentsModule.class);

	public void migrateAdhocDocumentsModule(List<String> fileNames) {

		if (migrationDbUtil.compareBothProfileDetailsByModule("CUSTOM_RULES_REPORTS") == false)
			throw new MigrationException("Something went wrong, Please try again later");

		List<AdhocDocuments> listOfAdhocDocumentsFromSource = new ArrayList<AdhocDocuments>();

		try {
			listOfAdhocDocumentsFromSource = sourceDbService.getTheAdhocDocumentsData(fileNames);
			logger.info("List of AdhocDocuments We Got From Source :" + listOfAdhocDocumentsFromSource.size());
		} catch (Exception e) {
			logger.error("Exception Occured While Retrieving the Adhoc File details from Source :" + e.getMessage());
			e.printStackTrace();
			throw new MigrationException("Migration terminated");
		}

		template.convertAndSend("/queue/percentage/others/", 35);

		List<com.retelzy.brim.entity.destination.AdhocDocuments> listOfAdhocDocumentsDetailsFromDestination = new ArrayList<com.retelzy.brim.entity.destination.AdhocDocuments>();

		listOfAdhocDocumentsFromSource.forEach(adhocDocuments -> {
			com.retelzy.brim.entity.destination.AdhocDocuments destinationAdhocDocuments = new com.retelzy.brim.entity.destination.AdhocDocuments();
			AdhocDocumentsPK adhocDocumentsPK = new AdhocDocumentsPK();
			adhocDocumentsPK.setAdhocDocName(adhocDocuments.getId().getAdhocDocName());
			adhocDocumentsPK.setCategoryID(adhocDocuments.getId().getCategoryID());
			destinationAdhocDocuments.setId(adhocDocumentsPK);
			destinationAdhocDocuments.setBlobValue(adhocDocuments.getBlobValue());
			destinationAdhocDocuments.setClobValue(adhocDocuments.getClobValue());
			destinationAdhocDocuments.setCreateTs(adhocDocuments.getCreateTs());
			destinationAdhocDocuments.setCreateUser(adhocDocuments.getCreateUser());
			destinationAdhocDocuments.setModifyTs(adhocDocuments.getModifyTs());
			destinationAdhocDocuments.setModifyUser(adhocDocuments.getModifyUser());

			listOfAdhocDocumentsDetailsFromDestination.add(destinationAdhocDocuments);
		});

		template.convertAndSend("/queue/percentage/others/", 65);

		destinationDbService.updateTheAdhocDocumentsTableDetails(listOfAdhocDocumentsDetailsFromDestination);

		template.convertAndSend("/queue/percentage/others/", 100);

		MigrationController.isMigrationRunning = false;
		SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
		DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;

	}

}
