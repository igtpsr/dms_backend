package com.retelzy.brim.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * The persistent class for the LAYOUT_SECTION database table.
 * 
 */
@Entity
@Table(name="LAYOUT_SECTION")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutSection implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutSectionPK id;

	@Column(name="ACL")
	private Integer acl;

	@Column(name="CELL_PADDING")
	private Integer cellPadding;

	@Column(name="CELL_SPACING")
	private Integer cellSpacing;

	@Column(name="COLS_FREEZE")
	private Integer colsFreeze;

	@Column(name="CUSTOM_SECTION", length=1)
	private String customSection;

	@Column(name="DISPLAY_TYPE", length=8)
	private String displayType;

	@Column(name="I18N_RES_ID", nullable=false)
	private Integer i18nResId;

	@Column(name="LABEL_DISPLAY")
	private Integer labelDisplay;

	@Column(name="MODEL_VAL_ID")
	private Integer modelValId;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER", length=18)
	private String modifyUser;

	@Column(name="MULTIPOST")
	private Integer multipost;

	@Column(name="NO_OF_COLS")
	private Integer noOfCols;

	@Column(name="REPEATING")
	private Integer repeating;

	@Column(name="ROWS_TO_DISPLAY")
	private Integer rowsToDisplay;

	@Column(name="SECTION_CLASS", length=150)
	private String sectionClass;

	@Column(name="SECTION_TAG", length=30)
	private String sectionTag;

	@Column(name="SELECT_WIDGET_TYPE")
	private Integer selectWidgetType;

	@Column(name="SHOW_ASSOC_MENU", length=1)
	private String showAssocMenu;

	@Column(name="SHOW_CHECKBOX", length=1)
	private String showCheckbox;

	@Column(name="TITLE_FREEZE")
	private Integer titleFreeze;

	//bi-directional many-to-one association to LayoutPage
	/*
	 * @OneToMany(mappedBy="layoutSection") private List<LayoutPage> layoutPages;
	 */

	//bi-directional many-to-one association to LayoutSectionD
	/*
	 * @OneToMany(mappedBy="layoutSection") private List<LayoutSectionD>
	 * layoutSectionDs;
	 */

}