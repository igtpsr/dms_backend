package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the SMU_ROLE_MENU_ITEM database table.
 * 
 */
@Embeddable
public class SmuRoleMenuItemPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "DOC_VIEW_ID")
	private Integer docViewId;

	@Column(name = "MENU_ITEM_ID")
	private Integer menuItemId;

	public SmuRoleMenuItemPK() {
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getDocViewId() {
		return docViewId;
	}

	public void setDocViewId(Integer docViewId) {
		this.docViewId = docViewId;
	}

	public Integer getMenuItemId() {
		return menuItemId;
	}

	public void setMenuItemId(Integer menuItemId) {
		this.menuItemId = menuItemId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docViewId == null) ? 0 : docViewId.hashCode());
		result = prime * result + ((menuItemId == null) ? 0 : menuItemId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmuRoleMenuItemPK other = (SmuRoleMenuItemPK) obj;
		if (docViewId == null) {
			if (other.docViewId != null)
				return false;
		} else if (!docViewId.equals(other.docViewId))
			return false;
		if (menuItemId == null) {
			if (other.menuItemId != null)
				return false;
		} else if (!menuItemId.equals(other.menuItemId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}

}