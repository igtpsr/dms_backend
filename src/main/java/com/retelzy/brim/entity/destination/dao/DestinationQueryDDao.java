package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.QueryD;

public interface DestinationQueryDDao extends CrudRepository<QueryD, Integer> {

	

}
