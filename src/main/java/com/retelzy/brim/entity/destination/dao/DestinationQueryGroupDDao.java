package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.QueryGroupD;
import com.retelzy.brim.entity.destination.QueryGroupDPrimaryPK;

@Repository
public interface DestinationQueryGroupDDao extends CrudRepository<QueryGroupD, QueryGroupDPrimaryPK> {

	
}
