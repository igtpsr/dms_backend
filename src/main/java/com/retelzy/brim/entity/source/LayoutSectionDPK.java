package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The primary key class for the LAYOUT_SECTION_D database table.
 * 
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutSectionDPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="LAYOUT_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer layoutId;

	@Column(name="DOC_VIEW_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer docViewId;

	@Column(name="SECTION_ID", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer sectionId;

	@Column(name="ROW_NO", unique=true, nullable=false)
	private Integer rowNo;

	@Column(name="DOC_ID", unique=true, nullable=false)
	private Integer docId;

	@Column(name="DOC_LEVEL_ID", unique=true, nullable=false)
	private Integer docLevelId;

	@Column(name="DOC_FIELD_ID", unique=true, nullable=false)
	private Integer docFieldId;

}