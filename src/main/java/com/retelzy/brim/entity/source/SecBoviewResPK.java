package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class SecBoviewResPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "DOC_VIEW_ID")
	private Integer docViewId;

	@Column(name = "RES_ID")
	private Integer resId;

	@Column(name = "TYPE_ID")
	private Integer typeId;

}
