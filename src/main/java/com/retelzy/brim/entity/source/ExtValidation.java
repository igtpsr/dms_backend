package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The persistent class for the EXT_VALIDATION database table.
 * 
 */
@Entity
@Table(name="EXT_VALIDATION")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class ExtValidation  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "VALIDATION_ID")
	private Integer validationId;

	@Column(name = "VALIDATION_NAME", nullable = false, length = 80)
	private String validationName;
	
	@Column(name = "VALIDATION_RULE", nullable = false, length = 3000)
	private String validationRule;
	
	@Column(name = "VALIDATION_DESC", nullable = false, length = 250)
	private String validationDesc;
	
	@Column(name = "CREATE_TS")
	private Timestamp createTs;

	@Column(name = "CREATE_USER", length = 18)
	private String createUser;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER", length = 18)
	private String modifyUser;

}
