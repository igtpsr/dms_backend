package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SmuCommon;

@Repository
public interface DestinationSmuCommonDao extends CrudRepository<SmuCommon, Integer> {


}
