package com.retelzy.brim.entity.user.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.user.RoleProfile;

public interface RoleProfileDao extends CrudRepository<RoleProfile, Integer> {

	@Query("Select role from RoleProfile role")
	public List<RoleProfile> getAllAvailableRoleProfile();

	public Optional<RoleProfile> findByRoleName(String string);

}
