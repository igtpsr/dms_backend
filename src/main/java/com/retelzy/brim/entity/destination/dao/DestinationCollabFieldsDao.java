package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.CollabField;
import com.retelzy.brim.entity.destination.CollabFieldPK;

public interface DestinationCollabFieldsDao extends CrudRepository<CollabField, CollabFieldPK> {

	@Query("Select co from CollabField co Where co.id.roleId =:roleId")
	List<Object> getTheDataFromCollabFieldsTable(Integer roleId);

}
