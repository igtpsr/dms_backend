package com.retelzy.brim.cutomException;

public class MigrationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MigrationException(String message) {
		super(message);
	}

}
