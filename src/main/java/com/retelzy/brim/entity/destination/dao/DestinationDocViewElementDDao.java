package com.retelzy.brim.entity.destination.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.DocViewElementD;
import com.retelzy.brim.entity.destination.DocViewElementDPK;

@Repository
public interface DestinationDocViewElementDDao extends CrudRepository<DocViewElementD, DocViewElementDPK> {

	@Modifying
	@Transactional
	@Query("Update DocViewElementD dve set dve.propValue=:propValue Where dve.propValue=:propName and dve.id.layoutId=:layoutId")
	void updateTheDocViewElementData(String propValue, String propName, Integer layoutId);


}
