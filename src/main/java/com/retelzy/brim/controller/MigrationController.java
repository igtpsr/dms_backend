package com.retelzy.brim.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.retelzy.brim.cutomException.InvalidDbDetailsException;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.LayoutProfile;
import com.retelzy.brim.entity.source.SecRoleProfile;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.entity.user.ClientDbDetails;
import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.entity.user.service.UserService;
import com.retelzy.brim.migration.model.AdhocDocumentReport;
import com.retelzy.brim.migration.model.AppConfigReport;
import com.retelzy.brim.migration.model.DashBoardChartColumns;
import com.retelzy.brim.migration.model.DashboardReports;
import com.retelzy.brim.migration.model.DbEnvironmentModel;
import com.retelzy.brim.migration.model.LayoutIdDetails;
import com.retelzy.brim.migration.model.MigrationId;
import com.retelzy.brim.migration.model.SecurityRoleIdDetais;
import com.retelzy.brim.migrationModules.MigrationAdhocDocumentsModule;
import com.retelzy.brim.migrationModules.MigrationAppConfigModule;
import com.retelzy.brim.migrationModules.MigrationLayoutModule;
import com.retelzy.brim.migrationModules.MigrationRulesModule;
import com.retelzy.brim.migrationModules.MigrationSecurityModule;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.GoogleGuavaCache;
import com.retelzy.brim.util.Helper;
import com.zaxxer.hikari.HikariDataSource;

@RestController
@RequestMapping(value = "/migration")
public class MigrationController {

	private final static Logger logger = LoggerFactory.getLogger(MigrationController.class);

	public static Boolean isMigrationRunning = false;

	public static String backupModuleFileName = "";

	public static String queryHfileName = "";

	public static String queryDfileName = "";

	@Autowired
	private MigrationSecurityModule migrationSecurityModule;

	@Autowired
	private MigrationLayoutModule migrationLayoutModule;

	@Autowired
	private UserService userService;

	@Autowired
	private Environment environment;

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private DestinationDbService destDbService;

	@Autowired
	private MigrationAppConfigModule migartionAppConfigModule;

	@Autowired
	private MigrationAdhocDocumentsModule migrationAdhocDocumentsModule;

	@Autowired
	private MigrationRulesModule migrationRulesModule;

	@PersistenceUnit(unitName = "Destination")
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private GoogleGuavaCache cache;

	@Autowired
	private ConfigFileReader configFileReader;

	/*
	 * This API is working for creating Database environment details like DEV, TEST,
	 * PROD, QA by user.
	 */
	@PostMapping(value = "/createTheClientDbDetails")
	public ClientDbDetails createTheClientDbDetails(@RequestBody ClientDbDetails clientDbdDetails) {

		if (clientDbdDetails.getDbType().equalsIgnoreCase("SQLSERVER")) {

			clientDbdDetails.setDbDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));

		} else if (clientDbdDetails.getDbType().equalsIgnoreCase("ORACLE")) {

			clientDbdDetails.setDbDriverClassName(environment.getProperty("db.oracle.driverClassName"));

		} else if (clientDbdDetails.getDbType().equalsIgnoreCase("MYSQL")) {

			clientDbdDetails.setDbDriverClassName(environment.getProperty("db.mysql.driverClassName"));

		} else if (clientDbdDetails.getDbType().equalsIgnoreCase("DB2")) {

			clientDbdDetails.setDbDriverClassName(environment.getProperty("db.db2.driverClassName"));
		}
		return userService.saveOrUpdateClientDbDetails(clientDbdDetails);

	}

	/* This API is Working for updating db environment details */
	@PutMapping(value = "/updateTheClientDbDetails")
	public Map<String, Object> updateTheClientDbDetails(@RequestBody ClientDbDetails clientDbdDetails) {

		Map<String, Object> message = new LinkedHashMap<String, Object>();

		if (isMigrationRunning == false) {

			Optional<ClientDbDetails> isCLientDbDetailsAvailable = userService
					.getClientDbDetailsById(clientDbdDetails.getClientDbCredentialId());

			if (isCLientDbDetailsAvailable.isPresent()) {

				if (clientDbdDetails.getDbType().equalsIgnoreCase("SQLSERVER")) {

					isCLientDbDetailsAvailable.get()
							.setDbDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));

				} else if (clientDbdDetails.getDbType().equalsIgnoreCase("ORACLE")) {

					isCLientDbDetailsAvailable.get()
							.setDbDriverClassName(environment.getProperty("db.oracle.driverClassName"));

				} else if (clientDbdDetails.getDbType().equalsIgnoreCase("MYSQL")) {

					isCLientDbDetailsAvailable.get()
							.setDbDriverClassName(environment.getProperty("db.mysql.driverClassName"));

				} else if (clientDbdDetails.getDbType().equalsIgnoreCase("DB2")) {

					isCLientDbDetailsAvailable.get()
							.setDbDriverClassName(environment.getProperty("db.db2.driverClassName"));

				} else if (clientDbdDetails.getDbType().equalsIgnoreCase("SYBASE")) {

					isCLientDbDetailsAvailable.get()
							.setDbDriverClassName(environment.getProperty("db.sybase.driverClassName"));
				}

				isCLientDbDetailsAvailable.get().setDbEnvironmentType(clientDbdDetails.getDbEnvironmentType());
				isCLientDbDetailsAvailable.get().setDbUsername(clientDbdDetails.getDbUsername());
				isCLientDbDetailsAvailable.get().setDbPassword(clientDbdDetails.getDbPassword());
				isCLientDbDetailsAvailable.get().setDbServerName(clientDbdDetails.getDbServerName());
				isCLientDbDetailsAvailable.get().setDbType(clientDbdDetails.getDbType());
				isCLientDbDetailsAvailable.get().setDbName(clientDbdDetails.getDbName());
				isCLientDbDetailsAvailable.get().setPortNumber(clientDbdDetails.getPortNumber());
				isCLientDbDetailsAvailable.get().setIsThisDbEnvVerified(clientDbdDetails.getIsThisDbEnvVerified());

				userService.saveOrUpdateClientDbDetails(isCLientDbDetailsAvailable.get());

				message.put("message", clientDbdDetails.getDbEnvironmentType() + " successfully updated");
				return message;
			} else {
				throw new InvalidDbDetailsException("Invalid Database details");
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}

	/* This API For get the All Layout Id and Security Role Id From DB */
	@GetMapping(value = "/getAllTheIdsAndRuleFiles/{moduleName}")
	public MigrationId getAllRoleAndLayoutId(@PathVariable("moduleName") String moduleName) {

		if (moduleName.equalsIgnoreCase("Security")) {
			List<Integer> listOfActiveRoleId = Helper.getListOfActiveRoleIdOrLayoutId("SECURITY");
			
			//System.out.print("Source DB in backend" + cache.getToken("SourceEnv"));
			//System.out.print("Destination DB in backend" + cache.getToken("DestinationEnv"));
			
			List<SecRoleProfile> listOfRole = sourceDbService
					.getAllRoleIdExistInSecRoleProfileTable(listOfActiveRoleId);

			List<SecurityRoleIdDetais> listOfRoleIdDetails = new ArrayList<SecurityRoleIdDetais>();
			listOfRole.forEach(secRolePfrofile -> {
				SecurityRoleIdDetais securityRoleIdDetais = new SecurityRoleIdDetais();
				securityRoleIdDetais.setRoleId(secRolePfrofile.getRoleId());
				securityRoleIdDetais.setRoleName(secRolePfrofile.getRoleName());
				listOfRoleIdDetails.add(securityRoleIdDetais);
			});

			MigrationId migrationId = new MigrationId();
			migrationId.setListOfRoleId(listOfRoleIdDetails);
			migrationSecurityModule.clearCacheDbEnvironmentType();

			return migrationId;

		} else if (moduleName.equalsIgnoreCase("Layout")) {

			List<Integer> listOfActiveLayoutId = Helper.getListOfActiveRoleIdOrLayoutId("LAYOUT");

			List<LayoutProfile> listOfLayout = sourceDbService.getAllLayoutIdExistInLayoutProfile(listOfActiveLayoutId);

			List<LayoutIdDetails> listOfLayoutDetails = new ArrayList<LayoutIdDetails>();

			listOfLayout.forEach(layoutProfile -> {
				LayoutIdDetails layoutIdDetails = new LayoutIdDetails();
				layoutIdDetails.setLayoutId(layoutProfile.getLayoutId());
				layoutIdDetails.setLayouDesc(layoutProfile.getLayoutDesc());
				listOfLayoutDetails.add(layoutIdDetails);
			});

			MigrationId migrationId = new MigrationId();
			migrationId.setListOfLayoutId(listOfLayoutDetails);
			migrationSecurityModule.clearCacheDbEnvironmentType();

			return migrationId;
		} else if (moduleName.equalsIgnoreCase("CustomRules")) {
			List<String> getTheListOfCustomeRules = sourceDbService.getTheListOfAdhocDocumentsNames();

			MigrationId migrationId = new MigrationId();
			migrationId.setListOfCustomRules(getTheListOfCustomeRules);
			migrationSecurityModule.clearCacheDbEnvironmentType();

			return migrationId;
		} else if (moduleName.equalsIgnoreCase("Reports")) {
			List<String> getTheListOfReportNames = sourceDbService.getTheListOfReportNames();
			logger.info("size :" + getTheListOfReportNames.size());
			MigrationId migrationId = new MigrationId();
			migrationId.setListOfReports(getTheListOfReportNames);
			migrationSecurityModule.clearCacheDbEnvironmentType();

			return migrationId;
		} else if (moduleName.equalsIgnoreCase("Rules")) {

			List<String> listOfRules = sourceDbService.getTheListOfRuleNames();

			MigrationId migrationId = new MigrationId();
			migrationId.setListOfRules(listOfRules);
			migrationSecurityModule.clearCacheDbEnvironmentType();

			return migrationId;
		} else {
			throw new MigrationException("Please provide the module name");
		}

	}
	
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
		@PutMapping(value = "/migrateTheSecurityModuleUsingStoredProcedure")
		public Map<String, Object> migrateTheSecurityModuleStoredProc(@RequestBody MigrationId migrationId)
				throws InterruptedException, ExecutionException {
			logger.info("Inside migrateTheSecurityModuleStoredProc api");
			
			Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
			MigrationDetails migrationDetails = new MigrationDetails();
			
			
			if (isMigrationRunning == false) {
				isMigrationRunning = true;
				try {
					//-----------------------------------------------------------------------------
					if (!migrationId.getListOfRoleId().isEmpty()) {
						template.convertAndSend("/queue/percentage/", 10);
						List<Integer> listOfRoleId = migrationId.getListOfRoleId().parallelStream()
								.map(element -> element.getRoleId()).collect(Collectors.toList());

						migrationDetails.setMigrationStartTime(new Date());
						migrationDetails.setMigrationModuleName("SECURITY");
						
						//Added by LOKESH -Begin
						migrationDetails.setCrNumber(migrationId.getCrNumber());
						migrationDetails.setCrDescription(migrationId.getCrDescription());
						//Added by LOKESH -End
						
						migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
						migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

						Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
								.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

						if (isClientDbDetailsAvailable.isPresent()) {
							migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
							migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
							migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
						}

						migrationDetails.setCreateTs(new Date());
						//-----------------------------------------------------------------------------
						logger.info("Migration Details  :  " + migrationDetails);
						migrationStatusMap = userService.migrateSecurityStoredProc(migrationId, migrationDetails);
						//-----------------------------------------------------------------------------
						createMigrationReport4rSecStordProc(migrationStatusMap.get("Status").toString(), migrationDetails,"SECURITY",listOfRoleId);
						migrationDetails.setMigrationEndTime(new Date());
						migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
						migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
						migrationDetails.setQueryDfileName(MigrationController.queryDfileName);
						userService.createMigrationDetails(migrationDetails);
						logger.info("Migration Report Created");
						template.convertAndSend("/queue/percentage/", 75);

						Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

						logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

						if (isMigrationDetailsAvailble.isPresent()) {
							migrationStatusMap.put("lastMigratedTime",
									cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
						} else {
							migrationStatusMap.put("lastMigratedTime", null);
						}
						template.convertAndSend("/queue/percentage/", 100);
						System.gc();
						isMigrationRunning = false;
						SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
						DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
						Helper.clearTheBackupFileNames();
						return migrationStatusMap;
					
					} else {

						Map<String, Object> message = new LinkedHashMap<String, Object>();
						message.put("Status", "Please provide atleast one role id");
						isMigrationRunning = false;
						SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
						DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
						return message;
					}
					//-----------------------------------------------------------------------------
				} catch (Exception e) {
					isMigrationRunning = false;
					e.printStackTrace();
				}
				
				return migrationStatusMap;
			} else {
				throw new MigrationException("Please wait for migration to complete");
			}
		}
		//--------------------------Added by LOK28APR2021 - End---------------------------------------

	@PutMapping(value = "/migrateTheSecurityModule")
	public Map<String, Object> migrateTheDB(@RequestBody MigrationId migrationId)
			throws InterruptedException, ExecutionException {

		if (isMigrationRunning == false) {

			isMigrationRunning = true;
			logger.info("Inside migrateTheSecurityModule api");
			MigrationDetails migrationDetails = new MigrationDetails();
			Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();

			if (migrationId.getIsMigrateAllSecurityRoleId()) {
				template.convertAndSend("/queue/percentage/", 10);
				migrationDetails.setMigrationStartTime(new Date());
				migrationDetails.setMigrationModuleName("SECURITY");
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				migrationDetails.setCreateTs(new Date());
				CompletableFuture<Map<String, Object>> migrationStatus = migrationSecurityModule
						.migrateSecurityOption("ALL_SECURITY", 100000, null);
				template.convertAndSend("/queue/percentage/", 10);
				createMigrationReport(migrationStatus.get(), migrationDetails);
				migrationDetails.setMigrationEndTime(new Date());
				migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
				migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
				migrationDetails.setQueryDfileName(MigrationController.queryDfileName);
				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");
				template.convertAndSend("/queue/percentage/", 75);

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					migrationStatusMap.put("lastMigratedTime",
							cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					migrationStatusMap.put("lastMigratedTime", null);
				}
				template.convertAndSend("/queue/percentage/", 100);
				migrationStatusMap.put("Status", migrationStatus.get().get("Status"));
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				Helper.clearTheBackupFileNames();
				return migrationStatusMap;

			} else {

				if (!migrationId.getListOfRoleId().isEmpty()) {
					template.convertAndSend("/queue/percentage/", 10);
					List<Integer> listOfRoleId = migrationId.getListOfRoleId().parallelStream()
							.map(element -> element.getRoleId()).collect(Collectors.toList());

					migrationDetails.setMigrationStartTime(new Date());
					migrationDetails.setMigrationModuleName("SECURITY");
					
					//Added by LOKESH -Begin
					migrationDetails.setCrNumber(migrationId.getCrNumber());
					migrationDetails.setCrDescription(migrationId.getCrDescription());
					//Added by LOKESH -End
					
					migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
					migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

					Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
							.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

					if (isClientDbDetailsAvailable.isPresent()) {
						migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
						migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
						migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
					}

					migrationDetails.setCreateTs(new Date());
					CompletableFuture<Map<String, Object>> migrationStatus = migrationSecurityModule
							.migrateSecurityOption("listOfRoleId", null, listOfRoleId);

					createMigrationReport(migrationStatus.get(), migrationDetails);
					migrationDetails.setMigrationEndTime(new Date());
					migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
					migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
					migrationDetails.setQueryDfileName(MigrationController.queryDfileName);
					userService.createMigrationDetails(migrationDetails);
					logger.info("Migration Report Created");
					template.convertAndSend("/queue/percentage/", 75);

					Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

					logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

					if (isMigrationDetailsAvailble.isPresent()) {
						migrationStatusMap.put("lastMigratedTime",
								cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
					} else {
						migrationStatusMap.put("lastMigratedTime", null);
					}
					template.convertAndSend("/queue/percentage/", 100);
					migrationStatusMap.put("Status", migrationStatus.get().get("Status"));
					System.gc();
					isMigrationRunning = false;
					SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
					DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
					Helper.clearTheBackupFileNames();
					return migrationStatusMap;
				} else {

					Map<String, Object> message = new LinkedHashMap<String, Object>();
					message.put("Status", "Please provide atleast one role id");
					isMigrationRunning = false;
					SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
					DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
					return message;
				}

			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}

	/*
	 * This method is working for migration report for Security and Layout module.
	 * Basically this method is setting the values in POJO class fields for each
	 * role id and layout id status.
	 */
	private void createMigrationReport(Map<String, Object> migrationStatusMap, MigrationDetails MigrationDetails) {

		List<MigratedRoleIdDetails> listOfMigratedRoleIdDetails = new ArrayList<MigratedRoleIdDetails>();
		List<MigratedLayoutIdDetails> listOfMigratedLayoutIdDetails = new ArrayList<MigratedLayoutIdDetails>();

		for (Map.Entry<String, Object> map : migrationStatusMap.entrySet()) {

			if (map.getKey().startsWith("Security-")) {

				Integer roleId = Integer.parseInt(map.getKey().substring(9));
				MigratedRoleIdDetails migratedRoleIdDetails = new MigratedRoleIdDetails();
				migratedRoleIdDetails.setRoleId(roleId);
				migratedRoleIdDetails.setRoleIdMigrationStatus(migrationStatusMap.get(map.getKey()).toString());
				migratedRoleIdDetails.setMigrationDetails(MigrationDetails);

				listOfMigratedRoleIdDetails.add(migratedRoleIdDetails);

			} else if (map.getKey().startsWith("Layout-")) {

				Integer layoutId = Integer.parseInt(map.getKey().substring(7));
				MigratedLayoutIdDetails migratedLayoutIdDetails = new MigratedLayoutIdDetails();
				migratedLayoutIdDetails.setLayoutId(layoutId);
				migratedLayoutIdDetails.setLayoutIdMigrationStatus(migrationStatusMap.get(map.getKey()).toString());
				migratedLayoutIdDetails.setMigrationDetails(MigrationDetails);

				listOfMigratedLayoutIdDetails.add(migratedLayoutIdDetails);
			}
		}
		logger.info("List Of Role Id Migrated :" + listOfMigratedRoleIdDetails.size());
		logger.info("List Of Layout Id Migrated :" + listOfMigratedLayoutIdDetails.size());

		if (!listOfMigratedRoleIdDetails.isEmpty()) {
			MigrationDetails.setListOfMigratedRoleId(listOfMigratedRoleIdDetails);
		}

		if (!listOfMigratedLayoutIdDetails.isEmpty()) {
			MigrationDetails.setListOfMigratedLayoutId(listOfMigratedLayoutIdDetails);
		}

	}
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	private void createMigrationReport4rSecStordProc(String status, MigrationDetails migrationDetails, String moduleName, List<Integer> listOfSecRoleIDs) {

		List<MigratedRoleIdDetails> listOfMigratedRoleIdDetails = new ArrayList<MigratedRoleIdDetails>();

		System.out.print("Printing Listofsecroleids : \n");
		System.out.print("\n" + listOfSecRoleIDs);
		System.out.print("Printing ModuleName : \n");
		System.out.print("\n" + moduleName);
		System.out.print("Printing MIgrationDetails : \n");
		System.out.print("\n" + migrationDetails);
		System.out.print("Step1 \n");
		if (moduleName.equalsIgnoreCase("SECURITY")) {
			System.out.print("Step2 \n");
			for (Integer roleId : listOfSecRoleIDs) {
				System.out.print("Step3");
				MigratedRoleIdDetails migratedRoleIdDetails = new MigratedRoleIdDetails();
				System.out.print("Step4");
				migratedRoleIdDetails.setRoleId(roleId);
				System.out.print("Step5");
				migratedRoleIdDetails.setRoleIdMigrationStatus(status);
				System.out.print("Step6");
				migratedRoleIdDetails.setMigrationDetails(migrationDetails);

				System.out.print("Step7");
				listOfMigratedRoleIdDetails.add(migratedRoleIdDetails);
				System.out.print("Step8");
			}
		}
		logger.info("List Of Role Id Migrated :" + listOfMigratedRoleIdDetails.size());

		if (!listOfMigratedRoleIdDetails.isEmpty()) {
			migrationDetails.setListOfMigratedRoleId(listOfMigratedRoleIdDetails);
		}

	}
	
	
	private void createMigrationReport4rLayStordProc(String status, MigrationDetails migrationDetails, String moduleName, List<LayoutIdDetails> listOfLayoutIDs) {

		List<MigratedLayoutIdDetails> listOfMigratedLayoutIdDetails = new ArrayList<MigratedLayoutIdDetails>();

		System.out.print("Printing Status : \n");
		System.out.print(status);
		//System.out.print("Printing MIgrationDetails : \n");
		//System.out.print(migrationDetails);
		System.out.print("Step1");
		if (moduleName.equalsIgnoreCase("LAYOUT")) {

			for (LayoutIdDetails layoutIdDetails : listOfLayoutIDs) {
				Integer layoutId = layoutIdDetails.getLayoutId();
				MigratedLayoutIdDetails migratedLayoutIdDetails = new MigratedLayoutIdDetails();
				migratedLayoutIdDetails.setLayoutId(layoutId);
				migratedLayoutIdDetails.setLayoutIdMigrationStatus(status);
				migratedLayoutIdDetails.setMigrationDetails(migrationDetails);

				listOfMigratedLayoutIdDetails.add(migratedLayoutIdDetails);
			}
		}
		logger.info("List Of Layout Id Migrated :" + listOfMigratedLayoutIdDetails.size());

		if (!listOfMigratedLayoutIdDetails.isEmpty()) {
			migrationDetails.setListOfMigratedLayoutId(listOfMigratedLayoutIdDetails);
		}

	}
	//--------------------------Added by LOK28APR2021 - END---------------------------------------
	
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	@PutMapping(value = "/migrateTheLayoutModuleUsingStoredProcedure")
	public Map<String, Object> migrateTheLayoutModuleStoredProc(@RequestBody MigrationId migrationId)
			throws InterruptedException, ExecutionException {
		logger.info("Inside migrateTheLayoutModuleStoredProc api");
		
		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		MigrationDetails migrationDetails = new MigrationDetails();
		
		
		if (isMigrationRunning == false) {
			isMigrationRunning = true;
			try {
				String sourceEnvType = cache.getToken("SourceEnv");
				String destinationEnvType = cache.getToken("DestinationEnv");
				logger.info("Source Env Type : " + sourceEnvType);
				logger.info("Destination Env Type : "+ destinationEnvType);
				migrationStatusMap = userService.migrateLayoutStoredProc(migrationId,sourceEnvType,destinationEnvType);
				//-----------------------------------------------------------------------------
				if (migrationStatusMap.get("Status").equals("MIGRATION SUCCESS")) {
					System.out.print("AFter success migration");
					createMigrationReport4rLayStordProc(migrationStatusMap.get("Status").toString(), migrationDetails,"LAYOUT",migrationId.getListOfLayoutId());
					migrationDetails.setMigrationEndTime(new Date());
					migrationDetails.setMigrationModuleName(migrationId.getModuleName());
					
					//Added by LOKESH -Begin
					migrationDetails.setCrNumber(migrationId.getCrNumber());
					migrationDetails.setCrDescription(migrationId.getCrDescription());
					//Added by LOKESH -End
					
					migrationDetails.setMigrationFromDate(migrationId.getFromDate());
					migrationDetails.setMigrationToDate(migrationId.getToDate());
					migrationDetails.setCreateTs(new Date());
					migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
					migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

					Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
							.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

					if (isClientDbDetailsAvailable.isPresent()) {
						migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
						migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
						migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
					}

					migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
					migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
					migrationDetails.setQueryDfileName(MigrationController.queryDfileName);

					userService.createMigrationDetails(migrationDetails);
					logger.info("Migration Details Inserted");

					cache.clearToken("SourceEnv");
					cache.clearToken("DestinationEnv");
				}

				logger.info("Migration Report Created");
				template.convertAndSend("/queue/percentage/", 75);

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					migrationStatusMap.put("lastMigratedTime",
							cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					migrationStatusMap.put("lastMigratedTime", null);
				}
				template.convertAndSend("/queue/percentage/", 100);
				//migrationStatusMap.put("Status", migrationStatus.get().get("Status"));
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				Helper.clearTheBackupFileNames();
				return migrationStatusMap;
				//-----------------------------------------------------------------------------
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return migrationStatusMap;
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}
	//--------------------------Added by LOK28APR2021 - End---------------------------------------
	
	
	
	
	

	@PutMapping(value = "/migrateTheLayoutModule")
	public Map<String, Object> migrateTheLayoutModule(@RequestBody MigrationId migrationId)
			throws InterruptedException, ExecutionException {
		logger.info("Inside migrateTheLayoutModule api");

		Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
		MigrationDetails migrationDetails = new MigrationDetails();
		if (isMigrationRunning == false) {

			isMigrationRunning = true;

			if (migrationId.getIsMigrateAllLayoutId()) {
				template.convertAndSend("/queue/percentage/", 10);
				migrationDetails.setMigrationStartTime(new Date());
				CompletableFuture<Map<String, Object>> migrationStatus = migrationLayoutModule
						.migrateLayoutOption("ALL_LAYOUT", null, migrationId);

				if (migrationStatus.get().get("Status").equals("Success")) {
					createMigrationReport(migrationStatus.get(), migrationDetails);
					migrationDetails.setMigrationEndTime(new Date());
					migrationDetails.setMigrationModuleName(migrationId.getModuleName());
					
					//Added by LOKESH -Begin
					migrationDetails.setCrNumber(migrationId.getCrNumber());
					migrationDetails.setCrDescription(migrationId.getCrDescription());
					//Added by LOKESH -End
					
					migrationDetails.setMigrationFromDate(migrationId.getFromDate());
					migrationDetails.setMigrationToDate(migrationId.getToDate());
					migrationDetails.setCreateTs(new Date());
					migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
					migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

					Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
							.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

					if (isClientDbDetailsAvailable.isPresent()) {
						migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
						migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
						migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
					}

					migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
					migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
					migrationDetails.setQueryDfileName(MigrationController.queryDfileName);

					userService.createMigrationDetails(migrationDetails);
					logger.info("Migration Details Inserted");

					cache.clearToken("SourceEnv");
					cache.clearToken("DestinationEnv");
				}

				logger.info("Migration Report Created");
				template.convertAndSend("/queue/percentage/", 75);

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					migrationStatusMap.put("lastMigratedTime",
							cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					migrationStatusMap.put("lastMigratedTime", null);
				}
				template.convertAndSend("/queue/percentage/", 100);
				migrationStatusMap.put("Status", migrationStatus.get().get("Status"));
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				Helper.clearTheBackupFileNames();
				return migrationStatusMap;

			} else {

				if (!migrationId.getListOfLayoutId().isEmpty()) {
					template.convertAndSend("/queue/percentage/", 10);
					List<Integer> listOfLayoutId = migrationId.getListOfLayoutId().parallelStream()
							.map(element -> element.getLayoutId()).collect(Collectors.toList());

					migrationDetails.setMigrationStartTime(new Date());
					CompletableFuture<Map<String, Object>> migrationStatus = migrationLayoutModule
							.migrateLayoutOption("listOfLayoutId", listOfLayoutId, migrationId);

					if (migrationStatus.get().get("Status").equals("Success")) {
						createMigrationReport(migrationStatus.get(), migrationDetails);
						migrationDetails.setMigrationEndTime(new Date());
						migrationDetails.setMigrationModuleName(migrationId.getModuleName());
						
						//Added by LOKESH -Begin
						migrationDetails.setCrNumber(migrationId.getCrNumber());
						migrationDetails.setCrDescription(migrationId.getCrDescription());
						//Added by LOKESH -End
						
						migrationDetails.setMigrationFromDate(migrationId.getFromDate());
						migrationDetails.setMigrationToDate(migrationId.getToDate());
						migrationDetails.setCreateTs(new Date());
						migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
						migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));

						Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
								.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

						if (isClientDbDetailsAvailable.isPresent()) {
							migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
							migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
							migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
						}
						migrationDetails.setBackupModuleFileName(MigrationController.backupModuleFileName);
						migrationDetails.setQueryHfileName(MigrationController.queryHfileName);
						migrationDetails.setQueryDfileName(MigrationController.queryDfileName);

						userService.createMigrationDetails(migrationDetails);
						logger.info("Migration Details Inserted");

						cache.clearToken("SourceEnv");
						cache.clearToken("DestinationEnv");
					}

					template.convertAndSend("/queue/percentage/", 75);

					Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

					logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

					if (isMigrationDetailsAvailble.isPresent()) {
						migrationStatusMap.put("lastMigratedTime",
								cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
					} else {
						migrationStatusMap.put("lastMigratedTime", null);
					}
					template.convertAndSend("/queue/percentage/", 100);
					migrationStatusMap.put("Status", migrationStatus.get().get("Status"));
					System.gc();
					isMigrationRunning = false;
					SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
					DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
					Helper.clearTheBackupFileNames();
					return migrationStatusMap;
				} else {
					isMigrationRunning = false;
					SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
					DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
					Map<String, Object> message = new LinkedHashMap<String, Object>();
					message.put("Status", "Please provide atleast one layout id");
					return message;
				}

			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}

	/* This API is Working for giving latest migration time. */
	@GetMapping(value = "/getTheLastMigratedTimePeriod")
	public Map<String, Object> getTheLastMigratedTimePeriod() {

		Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();
		Map<String, Object> message = new LinkedHashMap<String, Object>();

		logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

		if (isMigrationDetailsAvailble.isPresent()) {

			message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
		} else {
			message.put("lastMigratedTime", null);
		}

		return message;
	}

	/*
	 * This API is Working for checking or testing db connection, When user will
	 * create a DB environment like DEV, TEST, QA, and PROD, after creation user
	 * will go for a check, wheather this entered db details is correct or not. for
	 * that purpose we made this API. If db details is correct we are updating
	 * status as 1 or else 0. If your db environment details not verified, then this
	 * environment details will not appeared in migration page of UI. only you will
	 * get the there verfied db details.
	 */
	@PostMapping(value = "/testTheDBConnection")
	public Map<String, Object> testTheDBConnection(@RequestBody ClientDbDetails clientDbDetails) {

		Map<String, Object> message = new LinkedHashMap<String, Object>();
		HikariDataSource hikariDataSource = new HikariDataSource();
		Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
				.getClientDbDetailsById(clientDbDetails.getClientDbCredentialId());
		try {
			String serverPrefixURL = "";
			String jdbcURL = "";
			if (clientDbDetails.getDbType().equalsIgnoreCase("SQLSERVER")) {

				hikariDataSource.setDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));
				serverPrefixURL = "jdbc:sqlserver://";
				jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":" + clientDbDetails.getPortNumber()
						+ ";database=" + clientDbDetails.getDbName();
				hikariDataSource.setJdbcUrl(jdbcURL);

			} else if (clientDbDetails.getDbType().equalsIgnoreCase("ORACLE")) {

				hikariDataSource.setDriverClassName(environment.getProperty("db.oracle.driverClassName"));
				hikariDataSource.setDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));
				serverPrefixURL = "jdbc:oracle:thin:@";
				jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":" + clientDbDetails.getPortNumber()
						+ ":xe";
				hikariDataSource.setJdbcUrl(jdbcURL);

			} else if (clientDbDetails.getDbType().equalsIgnoreCase("MYSQL")) {

				hikariDataSource.setDriverClassName(environment.getProperty("db.mysql.driverClassName"));
				hikariDataSource.setDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));
				serverPrefixURL = "jdbc:mysql://";
				jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":" + clientDbDetails.getPortNumber()
						+ "/" + clientDbDetails.getDbName();
				hikariDataSource.setJdbcUrl(jdbcURL);

			} else if (clientDbDetails.getDbType().equalsIgnoreCase("DB2")) {

				hikariDataSource.setDriverClassName(environment.getProperty("db.mysql.driverClassName"));
				hikariDataSource.setDriverClassName(environment.getProperty("db.sqlserver.driverClassName"));
				serverPrefixURL = "jdbc:db2://";
				jdbcURL = serverPrefixURL + clientDbDetails.getDbServerName() + ":" + clientDbDetails.getPortNumber()
						+ "/" + clientDbDetails.getDbName();
				hikariDataSource.setJdbcUrl(jdbcURL);

			}

			hikariDataSource.setUsername(clientDbDetails.getDbUsername());
			hikariDataSource.setPassword(clientDbDetails.getDbPassword());
			hikariDataSource.getConnection();
			hikariDataSource.close();

			isClientDbDetailsAvailable.get().setIsThisDbEnvVerified(1);
			userService.saveOrUpdateClientDbDetails(isClientDbDetailsAvailable.get());

			message.put("Status", "Success");
			return message;
		} catch (Exception e) {
			hikariDataSource.close();
			message.put("Status", "Failed");
			return message;
		}

	}
	
	
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	@PutMapping(value = "/migrateTheAppConfigModuleStoredProc")
	public Map<String, Object> migrateTheAppConfigModuleStoredProc(@RequestBody MigrationId migrationId) {
		Map<String, Object> message = new LinkedHashMap<String, Object>();

		logger.info("from date :" + migrationId.getFromDate());
		logger.info("to date :" + migrationId.getToDate());

		if (isMigrationRunning == false) {
			isMigrationRunning = true;

			try {
				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());
				
				//-------------------------------------------------------------------------
				String sourceEnvType = cache.getToken("SourceEnv");
				String destinationEnvType = cache.getToken("DestinationEnv");
				logger.info("Source Env Type : " + sourceEnvType);
				logger.info("Destination Env Type : "+ destinationEnvType);
				message = userService.appConfig6RulesMigrationStoredProc(migrationId, null, sourceEnvType, destinationEnvType);
				//-------------------------------------------------------------------------
				
				logger.info("App Config Module Migation Completed");

				migrationDetails.setMigrationModuleName(migrationId.getModuleName());
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setMigrationFromDate(migrationId.getFromDate());
				migrationDetails.setMigrationToDate(migrationId.getToDate());
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}
				//message.put("Status", "Success");
				System.gc();
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				// e.printStackTrace();
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				logger.info("Exception Occured While Migrating The App Config Module :" + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}
	
	
	//--------------------------Added by LOK28APR2021 - End---------------------------------------
	
	

	@PutMapping(value = "/migrateTheAppConfigModule")
	public Map<String, Object> migrateTheAppConfigModule(@RequestBody MigrationId migrationId) {
		Map<String, Object> message = new LinkedHashMap<String, Object>();

		logger.info("from date :" + migrationId.getFromDate());
		logger.info("to date :" + migrationId.getToDate());

		if (isMigrationRunning == false) {
			isMigrationRunning = true;

			try {
				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());
				migartionAppConfigModule.migrateAppConfigModule(migrationId.getFromDate(), migrationId.getToDate(),
						migrationId.getClientSpec());
				logger.info("App Config Module Migation Completed");

				migrationDetails.setMigrationModuleName(migrationId.getModuleName());
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setMigrationFromDate(migrationId.getFromDate());
				migrationDetails.setMigrationToDate(migrationId.getToDate());
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}
				message.put("Status", "Success");
				System.gc();
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				// e.printStackTrace();
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				logger.info("Exception Occured While Migrating The App Config Module :" + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}

	@GetMapping(value = "/getTheConfigurationAppDetails")
	public List<ClientDbDetails> getTheConfigurationAppDetails() {

		List<ClientDbDetails> listOfClientDbDetails = userService.getTheConfigurationAppDetails();
		listOfClientDbDetails.forEach(dbDetails -> {
			if (dbDetails.getDbEnvironmentType().equalsIgnoreCase("DEV")) {
				dbDetails.setPriority(1);
			} else if (dbDetails.getDbEnvironmentType().equalsIgnoreCase("TEST")) {
				dbDetails.setPriority(2);
			} else if (dbDetails.getDbEnvironmentType().equalsIgnoreCase("QA")) {
				dbDetails.setPriority(3);
			} else if (dbDetails.getDbEnvironmentType().equalsIgnoreCase("PROD")) {
				dbDetails.setPriority(4);
			} /*
				 * else { dbDetails.setPriority(1); }
				 */
		});
		logger.info("List Of Environment Details Available :" + listOfClientDbDetails.size());
		return listOfClientDbDetails;
	}

	@DeleteMapping(value = "/removeTheDBEnvironmentDetails/{Id}")
	public Map<String, Object> removeTheDBEnvironmentDetails(@PathVariable("Id") Integer clentDbDetailsId) {

		Map<String, Object> message = new LinkedHashMap<String, Object>();
		if (isMigrationRunning == false) {
			logger.info("Client DB Details Id :" + clentDbDetailsId);

			try {
				userService.removeTheDBEnvironmentDetails(clentDbDetailsId);
				message.put("Status", "Success");
				return message;
			} catch (Exception e) {
				logger.error("Exception Occured While Deleting DB Environment details :" + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}

	@GetMapping(value = "/testing")
	public String testing() throws Exception {
		return "Token Not Expired";
	}

	@GetMapping(value = "/getTheDashboardReports")
	public DashboardReports getTheDashboardReports() {
		logger.info("Entering into getTheDashboardReports()");
		DashboardReports dashboardReports = new DashboardReports();
		String destinationEnvironmentType = null;
		try {
			
			// For Layout Report
			Optional<MigrationDetails> isMigrationDetailsAvailable = userService
					.getTheLatestMigrationDetailsByModuleName("LAYOUT");
			
			if (isMigrationRunning == true) {

				throw new MigrationException("Can not load the dashboard details, because data migration is running");
			}
			
			destinationEnvironmentType = isMigrationDetailsAvailable.get().getDestinationEnvironmentType();

			if (destinationEnvironmentType != null) {
				logger.info("Destination Environment Added");
				cache.saveTheToken("Destination-" + destinationEnvironmentType, destinationEnvironmentType);
			}
			
			logger.info("From Date :" + isMigrationDetailsAvailable.get().getMigrationFromDate());
			logger.info("To Date :" + isMigrationDetailsAvailable.get().getMigrationToDate());

			List<Object[]> listOfLayoutColumns = destDbService.getTheLayoutColumnsByModifiedTime(
					isMigrationDetailsAvailable.get().getMigrationFromDate(),
					isMigrationDetailsAvailable.get().getMigrationToDate());
			List<Integer> listOfLayoutIds = new ArrayList<>();
			listOfLayoutColumns.forEach(objectArray -> {

				Integer layoutId = null;
				try {
					layoutId = (Integer) objectArray[0];
					listOfLayoutIds.add(layoutId);
				} catch (Exception e) {
					e.printStackTrace();
					layoutId = 0;
				}
			});
			if (!listOfLayoutIds.isEmpty()) {
				
				// Changed by LOK03Mar2021 - Begin
				/*List<LayoutReport> templistOfLayoutReport = new ArrayList<LayoutReport>();
				templistOfLayoutReport = destDbService.getTheLayoutReport(listOfLayoutIds,
						isMigrationDetailsAvailable.get().getMigrationFromDate(),
						isMigrationDetailsAvailable.get().getMigrationToDate());
				if (templistOfLayoutReport != null) {
					dashboardReports.setListOflayoutReportDetails(templistOfLayoutReport);
				} */

				dashboardReports.setListOflayoutReportDetails(destDbService.getTheLayoutReport(listOfLayoutIds,
						isMigrationDetailsAvailable.get().getMigrationFromDate(),
						isMigrationDetailsAvailable.get().getMigrationToDate()));
				// Changed by LOK03Mar2021 - End
				
			} else {
				logger.error(
						"Error occured in getTheLayoutReport() because no data of layoutIDs " + listOfLayoutIds.size());
			}
			
			
			// For AppConfig Report
			
			Optional<MigrationDetails> isAppConfigMigrationDetailsAvailable = userService
					.getTheLatestMigrationDetailsByModuleName("APP CONFIG");
			
			destinationEnvironmentType = isAppConfigMigrationDetailsAvailable.get().getDestinationEnvironmentType();

			if (destinationEnvironmentType != null) {
				logger.info("Destination Environment Added");
				cache.saveTheToken("Destination-" + destinationEnvironmentType, destinationEnvironmentType);
			}
			
			logger.info("From Date :" + isAppConfigMigrationDetailsAvailable.get().getMigrationFromDate());
			logger.info("To Date :" + isAppConfigMigrationDetailsAvailable.get().getMigrationToDate());
			
			dashboardReports.setListOfAppConfigDetails(destDbService.getTheAppConfigReport(isAppConfigMigrationDetailsAvailable.get().getMigrationFromDate(),
					isAppConfigMigrationDetailsAvailable.get().getMigrationToDate()));
			
			if (isMigrationRunning == false) {
				cache.clearToken("Destination-" + destinationEnvironmentType);
			}
		} catch (Exception e) {
			logger.error("Error occured in getTheDashboardReports() " + e.getMessage());
			if (isMigrationRunning == false) {
				cache.clearToken("Destination-" + destinationEnvironmentType);
			}
		}
		return dashboardReports;
	}

	@GetMapping(value = "/getTheAppConfigReport")
	public List<AppConfigReport> getTheAppConfigReport(@RequestParam String fromDate, @RequestParam String toDate) {
		logger.info("Entering into getTheAppConfigReport()");
		List<AppConfigReport> listOfAppConfigReportDetails = new ArrayList<AppConfigReport>();
		try {
			listOfAppConfigReportDetails = destDbService.getTheAppConfigReport(fromDate, toDate);
		} catch (Exception e) {
			logger.error("Error occured in getTheAppConfigReport() " + e.getMessage());
		}
		return listOfAppConfigReportDetails;
	}

	@GetMapping(value = "/getTheAdhocDocumentsReport")
	public List<AdhocDocumentReport> getTheAdhocDocumentsReport(@RequestParam String fromDate,
			@RequestParam String toDate) {
		logger.info("Entering into getTheAdhocDocumentsReport()");
		List<AdhocDocumentReport> listOfAdhocDocumentReportDetails = new ArrayList<AdhocDocumentReport>();
		try {
			listOfAdhocDocumentReportDetails = destDbService.getTheAdhocDocumentsReport(fromDate, toDate);
		} catch (Exception e) {
			logger.error("Error occured in getTheAdhocDocumentsReport() " + e.getMessage());
		}
		return listOfAdhocDocumentReportDetails;
	}

	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	@PutMapping(value = "/migrateTheAdhocDocumentsStoredProc")
	public Map<String, Object> migrateTheAdhocDocumentsStoredProc(@RequestBody MigrationId migrationId) {

		logger.info("Entering into migrateTheAdhocDocuments()");
		

		Map<String, Object> message = new LinkedHashMap<String, Object>();

		if (isMigrationRunning == false) {

			isMigrationRunning = true;

			try {

				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());
				
				//---------------------------------------------------------------------
				String sourceEnvType = cache.getToken("SourceEnv");
				String destinationEnvType = cache.getToken("DestinationEnv");
				logger.info("Source Env Type : " + sourceEnvType);
				logger.info("Destination Env Type : "+ destinationEnvType);
				message = userService.migrateTheAdhocDocStoredProc(migrationId, sourceEnvType, destinationEnvType);
				//---------------------------------------------------------------------				
				
				migrationDetails.setMigrationModuleName("CustomRulesOrReport");
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}
				//message.put("Status", "Success");
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				e.printStackTrace();
				logger.error("Error occured in migrateTheAdhocDocuments() " + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}
	//--------------------------Added by LOK28APR2021 - End---------------------------------------
	
	
	@PutMapping(value = "/migrateTheAdhocDocuments")
	public Map<String, Object> migrateTheAdhocDocuments(@RequestBody MigrationId migrationId) {

		logger.info("Entering into migrateTheAdhocDocuments()");
		
		//Added by LOKESH -Begin
		List<String> fileNames = new ArrayList<>();
		if (migrationId.getModuleName().equals("CUSTOMRULES")) {
			fileNames = migrationId.getListOfCustomRules();
		}
		if (migrationId.getModuleName().equals("REPORTS")) {
			fileNames = migrationId.getListOfReports();
		}
		//Added by LOKESH -End

		Map<String, Object> message = new LinkedHashMap<String, Object>();

		if (isMigrationRunning == false) {

			isMigrationRunning = true;

			try {

				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());
				migrationAdhocDocumentsModule.migrateAdhocDocumentsModule(fileNames);
				migrationDetails.setMigrationModuleName("CustomRulesOrReport");
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}
				message.put("Status", "Success");
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				e.printStackTrace();
				logger.error("Error occured in migrateTheAdhocDocuments() " + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}

	/*
	 * @GetMapping(value = "/getTheListOfAdhocDocumentsNames") public List<String>
	 * getTheListOfAdhocDocumentsNames() {
	 * logger.info("Entering into getTheListOfAdhocDocumentsNames()"); List<String>
	 * adhocDocumentNames = new ArrayList<String>(); try { adhocDocumentNames =
	 * sourceDbService.getTheListOfAdhocDocumentsNames(); } catch (Exception e) {
	 * logger.error("Error occured in getTheListOfAdhocDocumentsNames() " +
	 * e.getMessage()); } return adhocDocumentNames; }
	 */

	/*
	 * @GetMapping(value = "/getTheListOfRuleNames") public List<String>
	 * getTheListOfRuleNames() {
	 * logger.info("Entering into getTheListOfRuleNames()"); List<String> ruleNames
	 * = new ArrayList<String>(); try { ruleNames =
	 * sourceDbService.getTheListOfRuleNames(); } catch (Exception e) {
	 * logger.error("Error occured in getTheListOfRuleNames() " + e.getMessage()); }
	 * return ruleNames; }
	 */
	
	
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	@PutMapping(value = "/migrateTheRulesStoredProc")
	public Map<String, Object> migrateTheRulesStoredProc(@RequestBody MigrationId migrationId) {

		logger.info("Entering into migrateTheRules()");
		//Added by LOKESH -Begin
		List<String> fileNames = new ArrayList<>();
		fileNames = migrationId.getListOfRules();
		//Added by LOKESH -End

		Map<String, Object> message = new LinkedHashMap<String, Object>();

		if (isMigrationRunning == false) {

			isMigrationRunning = true;

			try {

				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());
				
				List<String> listOfPropNames = sourceDbService.getPropName(fileNames);
				System.out.print("List of Prop Names : " + listOfPropNames);
				System.out.print("Size of PropNames : " + listOfPropNames.size());
				
				//------------------------------------------------------------------------------------
				String sourceEnvType = cache.getToken("SourceEnv");
				String destinationEnvType = cache.getToken("DestinationEnv");
				logger.info("Source Env Type : " + sourceEnvType);
				logger.info("Destination Env Type : "+ destinationEnvType);
				message = userService.appConfig6RulesMigrationStoredProc(migrationId,listOfPropNames, sourceEnvType, destinationEnvType);
				//------------------------------------------------------------------------------------
				
				migrationDetails.setMigrationModuleName("Rules");
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}

				//message.put("Status", "Success");
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				e.printStackTrace();
				logger.error("Error occured in migrateTheRules() " + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}
	//--------------------------Added by LOK28APR2021 - End---------------------------------------

	
	
	@PutMapping(value = "/migrateTheRules")
	public Map<String, Object> migrateTheRules(@RequestBody MigrationId migrationId) {

		logger.info("Entering into migrateTheRules()");
		//Added by LOKESH -Begin
		List<String> fileNames = new ArrayList<>();
		fileNames = migrationId.getListOfRules();
		//Added by LOKESH -End

		Map<String, Object> message = new LinkedHashMap<String, Object>();

		if (isMigrationRunning == false) {

			isMigrationRunning = true;

			try {

				MigrationDetails migrationDetails = new MigrationDetails();
				migrationDetails.setMigrationStartTime(new Date());

				migrationRulesModule.migrateRulesModule(fileNames);

				migrationDetails.setMigrationModuleName("Rules");
				
				//Added by LOKESH -Begin
				migrationDetails.setCrNumber(migrationId.getCrNumber());
				migrationDetails.setCrDescription(migrationId.getCrDescription());
				//Added by LOKESH -End
				
				migrationDetails.setCreateTs(new Date());
				migrationDetails.setSourceEnvironmentType(cache.getToken("SourceEnv"));
				migrationDetails.setDestinationEnvironmentType(cache.getToken("DestinationEnv"));
				migrationDetails.setMigrationEndTime(new Date());

				Optional<ClientDbDetails> isClientDbDetailsAvailable = userService
						.getTheClientDbDetailsByEnvName(cache.getToken("DestinationEnv"));

				if (isClientDbDetailsAvailable.isPresent()) {
					migrationDetails.setDatabaseName(isClientDbDetailsAvailable.get().getDbName());
					migrationDetails.setDbSeverName(isClientDbDetailsAvailable.get().getDbServerName());
					migrationDetails.setDbType(isClientDbDetailsAvailable.get().getDbType());
				}

				userService.createMigrationDetails(migrationDetails);
				logger.info("Migration Report Created");

				Optional<MigrationDetails> isMigrationDetailsAvailble = userService.getTheLastMigratedTimePeriod();

				logger.info("Latest Migrated Id :" + isMigrationDetailsAvailble.get().getMigrationDetailsId());

				if (isMigrationDetailsAvailble.isPresent()) {
					message.put("lastMigratedTime", cvtToGmt(isMigrationDetailsAvailble.get().getMigrationEndTime()));
				} else {
					message.put("lastMigratedTime", null);
				}
				message.put("Status", "Success");
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				return message;
			} catch (Exception e) {
				isMigrationRunning = false;
				SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
				DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
				migrationSecurityModule.clearCacheDbEnvironmentType();
				e.printStackTrace();
				logger.error("Error occured in migrateTheRules() " + e.getMessage());
				message.put("Status", "Failed");
				return message;
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}
	}

	@GetMapping(value = "/getTheDashBoardChartDetails")
	public List<DashBoardChartColumns> getTheDashBoardChartDetails() {

		Optional<MigrationDetails> isMigrationDetailsAvailable = userService
				.getTheLatestMigrationDetailsByModuleName("LAYOUT");

		if (isMigrationRunning == true) {

			throw new MigrationException("Can not load the dashboard details, because data migration is running");
		}

		List<DashBoardChartColumns> listOfChartColumns = new LinkedList<DashBoardChartColumns>();

		if (isMigrationDetailsAvailable.isPresent()) {

			String destinationEnvironmentType = isMigrationDetailsAvailable.get().getDestinationEnvironmentType();

			if (destinationEnvironmentType != null) {
				logger.info("Destination Environment Added");
				cache.saveTheToken("Destination-" + destinationEnvironmentType, destinationEnvironmentType);
			}

			logger.info("From Date :" + isMigrationDetailsAvailable.get().getMigrationFromDate());
			logger.info("To Date :" + isMigrationDetailsAvailable.get().getMigrationToDate());

			List<Object[]> listOfLayoutColumns = destDbService.getTheLayoutColumnsByModifiedTime(
					isMigrationDetailsAvailable.get().getMigrationFromDate(),
					isMigrationDetailsAvailable.get().getMigrationToDate());

			listOfLayoutColumns.forEach(objectArray -> {

				Integer layoutId = null;
				Integer docId = null;
				try {
					layoutId = (Integer) objectArray[0];
					docId = (Integer) objectArray[1];
				} catch (Exception e) {
					e.printStackTrace();
					layoutId = 0;
					docId = 0;

				}

				Integer count = destDbService.getTheCountOfLayoutColumn(layoutId, docId,
						isMigrationDetailsAvailable.get().getMigrationFromDate(),
						isMigrationDetailsAvailable.get().getMigrationToDate());

				logger.info("Count :" + count);

				DashBoardChartColumns boardChartColumns = new DashBoardChartColumns();
				boardChartColumns.setLayoutId(layoutId);
				boardChartColumns.setDocId(docId);
				boardChartColumns.setCount(count);

				listOfChartColumns.add(boardChartColumns);

			});

			if (isMigrationRunning == false) {
				cache.clearToken("Destination-" + destinationEnvironmentType);
			}
			return listOfChartColumns;

		} else {
			throw new MigrationException("Oops, No migration details available");
		}

	}

	/*
	 * This API is Working for Restore the data in Destination or Targeted db after
	 * the last migration. This feature is now only available for Security and
	 * Layout module.
	 */
	@PutMapping(value = "/restoreTheDataByModule/{moduleName}")
	public Map<String, Object> restoreTheDataByModule(@PathVariable("moduleName") String moduleName) {

		logger.info("Restore Module Name :" + moduleName);

		if (MigrationController.isMigrationRunning == false) {

			Optional<MigrationDetails> isMigrationDetailsAvailable = userService
					.getTheLatestMigrationDetailsByModuleName(moduleName);

			File secFile = new File(environment.getProperty("dir.path.backup")
					+ isMigrationDetailsAvailable.get().getBackupModuleFileName());
			
			System.out.println(environment.getProperty("dir.path.backup")
					+ isMigrationDetailsAvailable.get().getBackupModuleFileName());
			
			try {
				if (!secFile.exists()) {
					logger.error("File is not exist");
					throw new MigrationException("Oops, No data to restore..");
				}
			} catch (Exception e) {
				throw new MigrationException("Oops, No data to restore..");
			}

			Map<String, Object> message = new LinkedHashMap<String, Object>();

			if (isMigrationDetailsAvailable.isPresent()) {

				Optional<ClientDbDetails> isEnvironmentDetailsAvailable = userService.getTheClientDbDetailsByEnvName(
						isMigrationDetailsAvailable.get().getDestinationEnvironmentType());

				if (isEnvironmentDetailsAvailable.isPresent()) {

					Boolean envStatus = checkTheDbEnvironmentDetails(isEnvironmentDetailsAvailable.get(),
							isMigrationDetailsAvailable.get());

					if (envStatus) {

						try {
							switch (moduleName) {

							case "SECURITY":

								// get the list of security tables From DELETE sequence
								String[] securityTableDeleteSeq = ConfigFileReader
										.toArray(configFileReader.getDeleteSecurityTableSequenc());

								setTheDbEnvironment(isMigrationDetailsAvailable.get());

								List<MigratedRoleIdDetails> listOfRoleId = userService
										.getTheLatestMigratedRoleId(isMigrationDetailsAvailable.get());

								logger.info("Size :" + listOfRoleId.size());

								Boolean deleteStatus = destDbService
										.deleteSecurityDataForRestoringData(securityTableDeleteSeq, listOfRoleId);

								if (deleteStatus == false)
									throw new MigrationException("Oops, Unable to restore");

								StringBuilder secQuery = new StringBuilder();

								BufferedReader br = new BufferedReader(new FileReader(secFile));
								String line = "";
								while ((line = br.readLine()) != null) {
									secQuery.append(line);
								}
								destDbService.restoreTheModuleData(secQuery);
								destDbService.restoreQueryHandQueryDTableData(isMigrationDetailsAvailable.get());
								br.close();

								resetTheDetails(isMigrationDetailsAvailable.get());
								message.put("Status", "Successfully restored the data");
								return message;

							case "LAYOUT":

								// get the list of security tables From DELETE sequence
								String[] layoutTableDeleteSeq = ConfigFileReader
										.toArray(configFileReader.getDeleteLayoutTableSHelperequenc());

								setTheDbEnvironment(isMigrationDetailsAvailable.get());

								List<MigratedLayoutIdDetails> listOfLayoutId = userService
										.getTheLatestMigratedLayoutId(isMigrationDetailsAvailable.get());

								logger.info("Size :" + listOfLayoutId.size());

								Boolean laoutdDeleteStatus = destDbService
										.deleteLayoutDataForRestoringData(layoutTableDeleteSeq, listOfLayoutId);

								if (laoutdDeleteStatus == false)
									throw new MigrationException("Oops, Unable to restore");

								StringBuilder query = new StringBuilder();
								File file = new File(environment.getProperty("dir.path.backup")
										+ isMigrationDetailsAvailable.get().getBackupModuleFileName());

								BufferedReader br1 = new BufferedReader(new FileReader(file));
								String line1 = "";
								while ((line1 = br1.readLine()) != null) {
									query.append(line1);
								}
								destDbService.restoreTheModuleData(query);
								destDbService.restoreQueryHandQueryDTableData(isMigrationDetailsAvailable.get());

								br1.close();

								resetTheDetails(isMigrationDetailsAvailable.get());
								message.put("Status", "Successfully restored the data");
								return message;

							default:
								logger.error(
										"Module name not matched with switch case Of Restore Service :" + moduleName);
								throw new MigrationException("Please provide the module name");
							}
						} catch (Exception e) {
							e.printStackTrace();
							resetTheDetails(isMigrationDetailsAvailable.get());
							logger.error("Exception Occured In Restore Service :" + e.getCause());
							throw new MigrationException("Sorry, Unable to restore");
						}

					} else {
						throw new MigrationException("Oops, Destination DB environment details changed ");
					}

				} else {
					throw new MigrationException("Oops, Destination environment details not available ");
				}

			} else {
				throw new MigrationException("Oops, No migration details available for " + moduleName + " module");
			}
		} else {
			throw new MigrationException("Please wait for migration to complete");
		}

	}
	
	/*Added by LOKESH -Begin*/
	/*----------------------------------------------------------------------------------*/
	/*This method is to take backup modules data*/
	@PutMapping(value = "/backUpEnvDB")
	public Map<String, Object> backUpDevDB(@RequestBody String envName) {
		Map<String, Object> message = new LinkedHashMap<String, Object>();
		try {
			template.convertAndSend("/queue/percentage/", 10);
			Optional<ClientDbDetails> envDetails = userService.getTheClientDbDetailsByEnvName(envName);
			template.convertAndSend("/queue/percentage/", 20);
			if (envDetails.isPresent()) {
				System.out.print("envDetails are present for backup of Environment: " + envName + "; ");
				String data_path = "sqlfiles";
				String logPath="export_logs";
				String dataBase_Name = envDetails.get().getDbName();
				String dataBase_Schema = "dbo";
				String server_Name = envDetails.get().getDbServerName();
				String port_Number = envDetails.get().getPortNumber().toString();
				String user_Name = envDetails.get().getDbUsername();
				String password = envDetails.get().getDbPassword();
				template.convertAndSend("/queue/percentage/", 30);
				
				if (envName.toUpperCase().equalsIgnoreCase("DEV")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Dev"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("exportFileName"), data_path, logPath, dataBase_Name, dataBase_Schema, server_Name, port_Number, user_Name, password);
					/*--------------------------------------------------------------------------------------------------------1----------2-----------3---------------4----------------5---------6-----------7---------8----*/
			        pb.directory(dir);
			        Process p = pb.start();
			        template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if (envName.toUpperCase().equalsIgnoreCase("TEST")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Test"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("exportFileName"), data_path, logPath, dataBase_Name, dataBase_Schema, server_Name, port_Number, user_Name, password);
					/*--------------------------------------------------------------------------------------------------------1----------2-----------3---------------4----------------5---------6-----------7---------8----*/
					pb.directory(dir);
			        Process p = pb.start();
			        template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if (envName.toUpperCase().equalsIgnoreCase("QA")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Qa"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("exportFileName"), data_path, logPath, dataBase_Name, dataBase_Schema, server_Name, port_Number, user_Name, password);
					/*--------------------------------------------------------------------------------------------------------1----------2-----------3---------------4----------------5---------6-----------7---------8----*/
			        pb.directory(dir);
			        Process p = pb.start();
			        template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if(envName.toUpperCase().equalsIgnoreCase("PROD")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Prod"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("exportFileName"), data_path, logPath, dataBase_Name, dataBase_Schema, server_Name, port_Number, user_Name, password);
					/*--------------------------------------------------------------------------------------------------------1----------2-----------3---------------4----------------5---------6-----------7---------8----*/
			        pb.directory(dir);
			        Process p = pb.start();
			        template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				}
			} else {
				message.put("Status", "Oops, Destination environment details not available ");
			}
    	} catch (Exception e) {
    	message.put("Status", "Failed");
    	System.out.print(e.getMessage());
    	}
		return message;
	}
	/*----------------------------------------------------------------------------------*/
	/*This method is to take Restore modules data*/
	@PutMapping(value = "/restoreEnvDB")
	public Map<String, Object> RestoreDevDB(@RequestBody String envName) {
		Map<String, Object> message = new LinkedHashMap<String, Object>();
		try {
			template.convertAndSend("/queue/percentage/", 10);
			Optional<ClientDbDetails> envDetails = userService.getTheClientDbDetailsByEnvName(envName);
			template.convertAndSend("/queue/percentage/", 20);
			if (envDetails.isPresent()) {
				System.out.print("envDetails are present for Rstore of Environment: " + envName);
				String data_path = "sqlfiles";
				String logPath4rDel = "delete_logs";
				String logPath4rImp = "import_logs";
				String dataBase_Name = envDetails.get().getDbName();
				String dataBase_Schema = "dbo";
				String server_Name = envDetails.get().getDbServerName();
				String port_Number = envDetails.get().getPortNumber().toString();
				String user_Name = envDetails.get().getDbUsername();
				String password = envDetails.get().getDbPassword();
				template.convertAndSend("/queue/percentage/", 30);
				System.out.print(data_path + " " + logPath4rDel + " " + dataBase_Name + " " + dataBase_Schema + " " + server_Name + " " + user_Name + " " + password);
				
				if (envName.toUpperCase().equalsIgnoreCase("DEV")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Dev"));
			        ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("del_Imp_FileName"), data_path, logPath4rDel, logPath4rImp, dataBase_Name, dataBase_Schema, server_Name, port_Number,user_Name, password);
			        /*----------------------------------------------------------------------------------------------------------1------------2------------3----------------4--------------5--------------6-----------7----------8----------9---*/
			        pb.directory(dir);
			        Process p = pb.start();
					template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if(envName.toUpperCase().equalsIgnoreCase("TEST")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Test"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("del_Imp_FileName"), data_path, logPath4rDel, logPath4rImp, dataBase_Name, dataBase_Schema, server_Name, port_Number,user_Name, password);
			        /*----------------------------------------------------------------------------------------------------------1------------2------------3----------------4--------------5--------------6-----------7----------8----------9---*/
			        pb.directory(dir);
			        Process p = pb.start();
					template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if (envName.toUpperCase().equalsIgnoreCase("QA")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Qa"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("del_Imp_FileName"), data_path, logPath4rDel, logPath4rImp, dataBase_Name, dataBase_Schema, server_Name, port_Number,user_Name, password);
			        /*----------------------------------------------------------------------------------------------------------1------------2------------3----------------4--------------5--------------6-----------7----------8----------9---*/
			        pb.directory(dir);
			        Process p = pb.start();
					template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				} else if(envName.toUpperCase().equalsIgnoreCase("PROD")) {
					File dir = new File(environment.getProperty("exp_Del_Imp_FilePath_Prod"));
					ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", "",environment.getProperty("del_Imp_FileName"), data_path, logPath4rDel, logPath4rImp, dataBase_Name, dataBase_Schema, server_Name, port_Number,user_Name, password);
			        /*----------------------------------------------------------------------------------------------------------1------------2------------3----------------4--------------5--------------6-----------7----------8----------9---*/
			        pb.directory(dir);
			        Process p = pb.start();
					template.convertAndSend("/queue/percentage/", 40);
			        if(p.waitFor() == 0) {
			        	template.convertAndSend("/queue/percentage/", 100);
			        	message.put("Status", "Success");
			        } else {
			        	message.put("Status", "Failed");
			        }
				}
			} else {
				message.put("Status", "Oops, Destination environment details not available ");
			}
	    } catch (Exception e) {
	    	message.put("Status", "Failed");
	    	System.out.print(e.getMessage());
	    }
		return message;
	}
	/*----------------------------------------------------------------------------------*/
	/*Added by LOKESH -End*/
	

	/*
	 * This method is working for set the environment details before restore the db
	 */
	private void setTheDbEnvironment(MigrationDetails migrationDetails) {
		cache.saveTheToken("Destination-" + migrationDetails.getDestinationEnvironmentType(),
				migrationDetails.getDestinationEnvironmentType());
		cache.saveTheToken("Source-" + migrationDetails.getSourceEnvironmentType(),
				migrationDetails.getSourceEnvironmentType());
	}

	/*
	 * This method is working for reset the environment details after restore
	 * completed
	 */
	private void resetTheDetails(MigrationDetails migrationDetails) {
		cache.clearToken("Destination-" + migrationDetails.getDestinationEnvironmentType());
		cache.clearToken("Source-" + migrationDetails.getSourceEnvironmentType());
		MigrationController.isMigrationRunning = false;
	}

	/*
	 * This method is working for checking the environment details.When user wants
	 * to restore the detsination db, that time we are checking last migration db
	 * details and current db details both are same or not, based on that we are
	 * returning status.
	 */
	private Boolean checkTheDbEnvironmentDetails(ClientDbDetails clientDbDetails, MigrationDetails migrationDetails) {

		DbEnvironmentModel dbDetailsAvailbleInClientTab = new DbEnvironmentModel();
		dbDetailsAvailbleInClientTab.setDestDbName(clientDbDetails.getDbName());
		dbDetailsAvailbleInClientTab.setDestDbServerName(clientDbDetails.getDbServerName());
		dbDetailsAvailbleInClientTab.setDestDbType(clientDbDetails.getDbType());
		dbDetailsAvailbleInClientTab.setDestEnvType(clientDbDetails.getDbEnvironmentType());

		DbEnvironmentModel dbDetailsAvailbleInMigrationDetTab = new DbEnvironmentModel();
		dbDetailsAvailbleInMigrationDetTab.setDestDbName(migrationDetails.getDatabaseName());
		dbDetailsAvailbleInMigrationDetTab.setDestDbServerName(migrationDetails.getDbSeverName());
		dbDetailsAvailbleInMigrationDetTab.setDestDbType(migrationDetails.getDbType());
		dbDetailsAvailbleInMigrationDetTab.setDestEnvType(migrationDetails.getDestinationEnvironmentType());

		if (dbDetailsAvailbleInClientTab.equals(dbDetailsAvailbleInMigrationDetTab)) {
			return true;
		} else {
			return false;
		}

	}

	private Date cvtToGmt(Date date) {
		return date;
	}
}
