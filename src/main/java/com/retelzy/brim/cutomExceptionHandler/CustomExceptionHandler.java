package com.retelzy.brim.cutomExceptionHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.retelzy.brim.cutomException.BasicAuthenticationException;
import com.retelzy.brim.cutomException.DuplicateRoleException;
import com.retelzy.brim.cutomException.DuplicateUserException;
import com.retelzy.brim.cutomException.InvalidDbDetailsException;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.cutomException.UserNotFoundException;
import com.retelzy.brim.util.GoogleGuavaCache;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private GoogleGuavaCache googleCache;

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorResponse> basicExceptionHandler(Exception ex) {

		googleCache.clearToken("Source-DEV");
		googleCache.clearToken("Source-TEST");
		googleCache.clearToken("Source-QA");
		googleCache.clearToken("Source-STAGING");
		googleCache.clearToken("Source-PROD");
		googleCache.clearToken("Destination-DEV");
		googleCache.clearToken("Destination-TEST");
		googleCache.clearToken("Destination-QA");
		googleCache.clearToken("Destination-STAGING");
		googleCache.clearToken("Destination-PROD");

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorResponse.setMessage("Something went wrong, Please try again later");
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<ErrorResponse> userNotFoundExceptionHandler(UserNotFoundException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.EXPECTATION_FAILED.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.EXPECTATION_FAILED);

	}

	@ExceptionHandler(BasicAuthenticationException.class)
	public final ResponseEntity<ErrorResponse> basicExceptionHandler(BasicAuthenticationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.FORBIDDEN.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(InvalidDbDetailsException.class)
	public final ResponseEntity<ErrorResponse> invalidDetailsExceptionHandler(BasicAuthenticationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.FAILED_DEPENDENCY.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.FAILED_DEPENDENCY);
	}

	@ExceptionHandler(MigrationException.class)
	public final ResponseEntity<ErrorResponse> mifrationExceptionHandler(MigrationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.FAILED_DEPENDENCY.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.FAILED_DEPENDENCY);
	}
	
	@ExceptionHandler(DuplicateRoleException.class)
	public final ResponseEntity<ErrorResponse> duplicateRoleExceptionExceptionHandler(DuplicateRoleException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.EXPECTATION_FAILED.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.EXPECTATION_FAILED);

	}
	
	@ExceptionHandler(DuplicateUserException.class)
	public final ResponseEntity<ErrorResponse> duplicateUserExceptionExceptionHandler(DuplicateUserException ex) {

		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.EXPECTATION_FAILED.value());
		errorResponse.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.EXPECTATION_FAILED);

	}

}
