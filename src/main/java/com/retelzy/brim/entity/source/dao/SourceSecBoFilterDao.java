package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBoFilter;
import com.retelzy.brim.entity.source.SecBoFilterPK;

@Repository
public interface SourceSecBoFilterDao extends CrudRepository<SecBoFilter, SecBoFilterPK> {

	public List<Object> findByIdRoleId(Integer roleId);

	@Query("Select sb.id.queryId From SecBoFilter sb Where sb.id.roleId =:roleId")
	public List<Integer> getAllQueryIdByRoleId(Integer roleId);

}
