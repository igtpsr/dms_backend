package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBointFilter;
import com.retelzy.brim.entity.source.SecBointFilterPK;

@Repository
public interface SourceSecBointFilterDao extends CrudRepository<SecBointFilter, SecBointFilterPK> {

	public List<Object> findByIdRoleId(Integer roleId);

	@Query("Select sb.id.queryId from SecBointFilter sb where sb.id.roleId =:roleId")
	public List<Integer> getAllTheQueryIdsByRoleId(Integer roleId);

}
