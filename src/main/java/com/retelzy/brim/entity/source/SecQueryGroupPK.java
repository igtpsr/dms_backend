package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SecQueryGroupPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "QUERY_GROUP_ID")
	private Integer queryGroupId;

	public SecQueryGroupPK() {
		// TODO Auto-generated constructor stub
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getQueryGroupId() {
		return queryGroupId;
	}

	public void setQueryGroupId(Integer queryGroupId) {
		this.queryGroupId = queryGroupId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((queryGroupId == null) ? 0 : queryGroupId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecQueryGroupPK other = (SecQueryGroupPK) obj;
		if (queryGroupId == null) {
			if (other.queryGroupId != null)
				return false;
		} else if (!queryGroupId.equals(other.queryGroupId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}

}
