package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the SEC_BO_FILTER database table.
 * 
 */
@Entity
@Table(name="SEC_BO_FILTER")
@DynamicUpdate
public class SecBoFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SecBoFilterPK id;

	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER")
	private String modifyUser;

	//bi-directional many-to-one association to SecRoleProfile
	@MapsId("ROLE_ID")
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private SecRoleProfile secRoleProfile;

	public SecBoFilter() {
	}

	public SecBoFilterPK getId() {
		return this.id;
	}

	public void setId(SecBoFilterPK id) {
		this.id = id;
	}

	public Timestamp getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Timestamp modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public SecRoleProfile getSecRoleProfile() {
		return this.secRoleProfile;
	}

	public void setSecRoleProfile(SecRoleProfile secRoleProfile) {
		this.secRoleProfile = secRoleProfile;
	}

}