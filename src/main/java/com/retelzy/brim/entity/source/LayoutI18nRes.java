// Generated with g9.

package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="LAYOUT_I18N_RES")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutI18nRes implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutI18nResPK id;

	@Column(name="LABEL", nullable=false, length=200)
	private String label;
	
	@Column(name="SHORT_DESC",length=80)
	private String shortDesc;
	
	@Column(name="LONG_DESC",  length=250)
	private String longDesc;
	
	@Column(name="MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name="MODIFY_USER", length=18)
	private String modifyUser;

}
