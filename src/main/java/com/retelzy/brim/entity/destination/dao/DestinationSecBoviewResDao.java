package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.SecBoviewRes;
import com.retelzy.brim.entity.destination.SecBoviewResPK;

@Repository
public interface DestinationSecBoviewResDao extends CrudRepository<SecBoviewRes, SecBoviewResPK> {

}
