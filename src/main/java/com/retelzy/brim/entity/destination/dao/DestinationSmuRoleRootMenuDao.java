package com.retelzy.brim.entity.destination.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.retelzy.brim.entity.destination.SmuRoleRootMenu;
import com.retelzy.brim.entity.destination.SmuRoleRootMenuPK;

public interface DestinationSmuRoleRootMenuDao extends CrudRepository<SmuRoleRootMenu, SmuRoleRootMenuPK> {

	@Query("Select smu from SmuRoleRootMenu smu Where smu.id.roleId =:roleId")
	List<Object> getTheDataFromSmuRoleRootMenuTable(Integer roleId);

}
