package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutSectionD;
import com.retelzy.brim.entity.source.LayoutSectionDPK;

@Repository
public interface SourceLayoutSectionDDao extends CrudRepository<LayoutSectionD, LayoutSectionDPK> {

	List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);


}
