package com.retelzy.brim.db.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.retelzy.brim.util.ConfigFileReader;

@Component
public class MigrationBackupUtil {

	public void writeQuriesInBackupFile(String where, PrintWriter pw, Connection connection, Statement statement1,
			Statement statement2, String dbType, String table, Integer nMaxRows, Map<Integer, String> queryIdMap,
			Integer newLayoutId, File f) throws SQLException, IOException {

		String owner = null;
		int clobCount = 0;

		statement1 = connection.createStatement();
		statement1.executeQuery("SELECT * FROM " + table);
		ResultSet resultSet = statement1.getResultSet();
		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

		ArrayList<String> columnsList = new ArrayList<String>();

		for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
			columnsList.add(resultSetMetaData.getColumnName(i));
		}

		resultSet.close();

		String sqlIns = "INSERT INTO " + table + "(";
		for (int i = 0; i < columnsList.size(); i++) {
			if (i != 0)
				sqlIns += ", ";
			if (dbType.equalsIgnoreCase("Microsoft SQL Server")) {
				sqlIns += "\"" + columnsList.get(i) + "\"";
			} else {
				sqlIns += columnsList.get(i);
			}
		}

		sqlIns += ") VALUES (";

		statement2 = connection.createStatement();
		String sqlSel = "select * from " + table;

		if (where != null) {
			// Add where
			sqlSel += " where " + where;
		}
		System.err.println("SQL :"+ sqlSel);
		ResultSet rs = statement2.executeQuery(sqlSel);
		String sql = "";
		int nCount = 0;
		while (rs.next()) {
			String clobs = "";

			nCount++;
			if (nMaxRows > 0 && nCount > nMaxRows) {
				break;
			}

			sql = sqlIns;
			for (int j = 0; j < columnsList.size(); j++) {

				boolean clob = false;

				if (j != 0) {
					sql += ", ";
				}

				String colName = (String) columnsList.get(j);
				Object val = rs.getObject(colName);

				/*
				 * if (val instanceof oracle.sql.TIMESTAMP) { val = rs.getTimestamp(colName); }
				 */

				if (colName.equalsIgnoreCase("OWNER") && owner != null) {
					// System.out.println("SQL :" + sql);
					val = owner;
					// break;
				}

				if (val instanceof java.sql.Clob) {
					clobCount++;
					String clobCountString = "C" + clobCount;
					if ("".equals(clobs)) {
						clobs = "{CLOBS:" + clobCountString;
					} else {
						clobs += ":clobCountString";
					}
					val = getClobValue(rs, val);
					// set the clob aside in a file
					File f1 = new File(f.getParent(), clobCountString);
					FileWriter fw = new FileWriter(f1);
					PrintWriter pw1 = new PrintWriter(fw);
					pw1.print(val);
					pw1.close();
					fw.close();
					val = "?";
					clob = true;
				}

				if (val == null) {
					sql += "null";
				} else if (val instanceof String && !clob) {
					val = ConfigFileReader.replaceAll(val.toString(), "'", "''");
					sql += "'" + val + "'";
				} else if (val instanceof java.util.Date) {

					if ("Oracle RDBMS".equalsIgnoreCase(dbType)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String toDate = "to_date('" + sdf.format(val) + "','yyyy-mm-dd hh24:mi:ss')";
						sql += toDate;
					} else if ("Microsoft SQL Server".equalsIgnoreCase(dbType)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String toDate = "convert(datetime,'" + sdf.format(val) + "',120)";
						sql += toDate;
					} else {
						sql += "'" + val + "'";
					}

				} else {
					// repalce query id value based on the Map value in Insert SQL
					if (queryIdMap != null && (colName.equals("QUERY_ID") || colName.equals("VALIDATION_ID"))) {
						Iterator<Integer> itr = queryIdMap.keySet().iterator();
						while (itr.hasNext()) {
							Integer queryKey = itr.next();
							String queryVal = (String) queryIdMap.get(queryKey);
							if (queryKey.equals(val)) {
								val = queryVal;
							}
						}
					}

					// replace layout id if new layoud id is used for migration
					if (newLayoutId != null && newLayoutId.intValue() >= 100000 && colName.equals("LAYOUT_ID")) {
						val = newLayoutId.toString();
					}

					sql += val;
				}
			}
			sql += ")";
			pw.println(sql + ";");
			sql = "";
		} /* While Loop Ending */

		System.gc();
	}

	/**
	 * temporary method to get the clob value for query_xml column todo remove this
	 * method and reimplement it for all the columns
	 *
	 * @param rs
	 * @param colName
	 * @param val
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private Object getClobValue(ResultSet rs, Object val) throws SQLException, IOException {
		// temporary code to get value of clob
		Clob clob = (Clob) val;
		if (clob != null) {
			StringBuffer buffer = new StringBuffer();
			Reader reader = clob.getCharacterStream();
			char[] bytes = new char[2024];
			int length;
			while ((length = reader.read(bytes)) != -1) {
				buffer.append(bytes, 0, length);
			}

			val = buffer.toString();
			reader.close();
		}
		return val;
	}

}
