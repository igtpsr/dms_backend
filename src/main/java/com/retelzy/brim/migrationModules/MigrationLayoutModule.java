package com.retelzy.brim.migrationModules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.db.util.MigrationDbUtil;
import com.retelzy.brim.entity.destination.DocView;
import com.retelzy.brim.entity.destination.DocViewElementD;
import com.retelzy.brim.entity.destination.DocViewElementDPK;
import com.retelzy.brim.entity.destination.DocViewPK;
import com.retelzy.brim.entity.destination.ExtValidation;
import com.retelzy.brim.entity.destination.ExtValidationMetaData;
import com.retelzy.brim.entity.destination.ExtValidationMetaDataPK;
import com.retelzy.brim.entity.destination.LayoutDetail;
import com.retelzy.brim.entity.destination.LayoutDetailPK;
import com.retelzy.brim.entity.destination.LayoutElementD;
import com.retelzy.brim.entity.destination.LayoutElementDPK;
import com.retelzy.brim.entity.destination.LayoutI18nRes;
import com.retelzy.brim.entity.destination.LayoutI18nResPK;
import com.retelzy.brim.entity.destination.LayoutLevelGroup;
import com.retelzy.brim.entity.destination.LayoutLevelGroupPK;
import com.retelzy.brim.entity.destination.LayoutPage;
import com.retelzy.brim.entity.destination.LayoutPagePK;
import com.retelzy.brim.entity.destination.LayoutProfile;
import com.retelzy.brim.entity.destination.LayoutProfileD;
import com.retelzy.brim.entity.destination.LayoutProfileDPK;
import com.retelzy.brim.entity.destination.LayoutSection;
import com.retelzy.brim.entity.destination.LayoutSectionD;
import com.retelzy.brim.entity.destination.LayoutSectionDPK;
import com.retelzy.brim.entity.destination.LayoutSectionPK;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.entity.user.service.UserService;
import com.retelzy.brim.migration.model.MigrationId;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.GoogleGuavaCache;

@Component
public class MigrationLayoutModule {

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private MigrationDbUtil migrationDbUtil;

	@Autowired
	private ConfigFileReader configFileReader;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private GoogleGuavaCache cache;

	@Autowired
	private Environment env;

	@Autowired
	private MigrationBackupForLayoutModule backupForLayoutModule;

	@Autowired
	private SimpMessagingTemplate template;

	private final static Logger logger = LoggerFactory.getLogger(MigrationLayoutModule.class);
	
	@Async
	public CompletableFuture<Map<String, Object>> migrateLayoutOption(String layoutOption,
			List<Integer> listOfLayoutIdSentByUser, MigrationId migrationId) {

		CompletableFuture<Map<String, Object>> migrationStatus = new CompletableFuture<Map<String, Object>>();

		Map<String, Object> mapOfStatus = new LinkedHashMap<String, Object>();

		logger.info("migrate migrateLayoutOption method called");
		FileWriter fw = null;
		PrintWriter pw = null;
		try {

			if (migrationDbUtil.compareBothProfileDetailsByModule("LAYOUT") == false)
				throw new MigrationException("Something went wrong, Please try again later");

			if (layoutOption.equals("ALL_LAYOUT")) {

				List<Integer> listOfLayoutId = sourceDbService.getAllTheLayoutIdByLayoutId();
				logger.info("List Of Layout ID Available :" + listOfLayoutId.size());

				ListIterator<Integer> listItr = listOfLayoutId.listIterator();

				String fileName = getSQLFileName(layoutOption);
				File file = new File(env.getProperty("dir.path.backup"), fileName);
				fw = new FileWriter(file, true);
				pw = new PrintWriter(fw);

				MigrationController.backupModuleFileName = fileName;

				Boolean backupStatus = backupForLayoutModule.getBackupLayoutInsertSql(listOfLayoutId, pw, file);

				if (backupStatus == true) {
					template.convertAndSend("/queue/percentage/", 35);
					// get the list of layout tables From INSERT sequence
					String[] layoutTableInsertSeq = ConfigFileReader
							.toArray(configFileReader.getInsertLayoutTableSequence());

					// get the list of layout tables From DELETE sequence
					String[] layoutTableDeleteSeq = ConfigFileReader
							.toArray(configFileReader.getDeleteLayoutTableSHelperequenc());

					while (listItr.hasNext()) {
						Integer layoutId = listItr.next();
						Integer newLayoutId = migrationDbUtil.checkLayoutIdMap(layoutId);

						List<Map<String, List<Object>>> listOfInsertQuery = migrationDbUtil
								.generateLayoutInsertSQL(layoutTableInsertSeq, layoutId, newLayoutId, migrationId);

						logger.info("Total Insert Scripts-->" + listOfInsertQuery.size());

						logger.info("Total Insert Scripts-->" + listOfInsertQuery);

						if (newLayoutId != null && newLayoutId.intValue() >= 100000) {
							layoutId = newLayoutId;
						}
						boolean deleteStatus = destinationDbService.deleteLayoutData(layoutTableDeleteSeq, layoutId);

						logger.info("Is Delete Completed :" + deleteStatus);

						if (deleteStatus) {
							boolean insertStatus = insertTheLayoutData(listOfInsertQuery, layoutId);
							if (insertStatus == true) {
								mapOfStatus.put(layoutId.toString(), "Success");
								logger.info("For layoutId " + layoutId + " insertion is completed successfully");
							} else {
								logger.info("Insert Data From Destination Tables Failed");
								mapOfStatus.put(layoutId.toString(), "Failed");
							}
							try {
								insertQueryHandQueryDtableData(layoutId);
							} catch (Exception e) {
								// e.printStackTrace();
								logger.error("Exception Occured For QueryH and QueryD Table :" + e.getMessage());
							}

						} else {
							logger.info("Delete Data From Destination Tables Failed");
						}

					}
					logger.info("migrateLayoutOption() method while loop completed");
					template.convertAndSend("/queue/percentage/", 50);
				} else {
					file.deleteOnExit();
					throw new MigrationException("Backup Failed, migration terminated");
				}
			} else {
				File file = null;
				System.out.print(listOfLayoutIdSentByUser); // Added by LOKESH for Testing
				if (listOfLayoutIdSentByUser.size() == 1) {
					String fileName = getSQLFileName(listOfLayoutIdSentByUser.get(0).toString());
					file = new File(env.getProperty("dir.path.backup"), fileName);
					MigrationController.backupModuleFileName = fileName;
				} else {
					String fileName = getSQLFileName("MULTIPLE");
					file = new File(env.getProperty("dir.path.backup"), fileName);
					MigrationController.backupModuleFileName = fileName;
				}

				fw = new FileWriter(file, true);
				pw = new PrintWriter(fw);

				Boolean backupStatus = backupForLayoutModule.getBackupLayoutInsertSql(listOfLayoutIdSentByUser, pw,
						file);

				if (backupStatus == true) {
					template.convertAndSend("/queue/percentage/", 35);
					// get the list of layout tables From INSERT sequence
					String[] layoutTableInsertSeq = ConfigFileReader
							.toArray(configFileReader.getInsertLayoutTableSequence());

					// get the list of layout tables From DELETE sequence
					String[] layoutTableDeleteSeq = ConfigFileReader
							.toArray(configFileReader.getDeleteLayoutTableSHelperequenc());

					listOfLayoutIdSentByUser.forEach(layoutId -> {

						try {
							Optional<Integer> isLayoutIdDetailsAvailable = sourceDbService
									.getTheLayoutIdByLayoutId(layoutId);

							if (isLayoutIdDetailsAvailable.isPresent()) {

								Integer newLayoutId = migrationDbUtil.checkLayoutIdMap(layoutId);

								List<Map<String, List<Object>>> listOfInsertQuery = migrationDbUtil
										.generateLayoutInsertSQL(layoutTableInsertSeq, layoutId, newLayoutId,
												migrationId);

								logger.info("Total Insert Scripts-->" + listOfInsertQuery.size());

								logger.info("Total Insert Scripts-->" + listOfInsertQuery);

								if (newLayoutId != null && newLayoutId.intValue() >= 100000) {
									layoutId = newLayoutId;
								}
								boolean deleteStatus = destinationDbService.deleteLayoutData(layoutTableDeleteSeq,
										layoutId);

								logger.info("Is Delete Completed :" + deleteStatus);

								if (deleteStatus) {
									boolean insertStatus = insertTheLayoutData(listOfInsertQuery, layoutId);
									if (insertStatus == true) {
										mapOfStatus.put("Layout-" + layoutId.toString(), "Success");
										logger.info(
												"For LayoutId " + layoutId + " insertion is completed successfully");

									} else {
										logger.info("Insert Data From Destination Tables Failed");
										mapOfStatus.put("Layout-" + layoutId.toString(), "Failed");

									}
									try {
										insertQueryHandQueryDtableData(layoutId);
									} catch (Exception e) {
										// e.printStackTrace();
										logger.error(
												"Exception Occured For QueryH and QueryD Table :" + e.getMessage());
									}

								} else {
									logger.info("Delete Data From Destination Tables Failed");
									logger.info("Delete Data From Destination Tables Failed");
									mapOfStatus.put("Layout-" + layoutId.toString(), "Failed");
								}
							} else {
								mapOfStatus.put("Layout-" + isLayoutIdDetailsAvailable.get().toString(),
										"LayoutId Id Not exist");
							}
						} catch (Exception e) {
							mapOfStatus.put("Layout-" + layoutId.toString(), "Failed");
						}
					});
					template.convertAndSend("/queue/percentage/", 50);
				} else {
					file.deleteOnExit();
					throw new MigrationException("Backup Failed, migration terminated");
				}
			}

			mapOfStatus.put("Status", "Success");
			migrationStatus.complete(mapOfStatus);
			clearCacheDbEnvironmentType();
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
			return migrationStatus;
		} catch (Exception e) {
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
			e.printStackTrace();
			logger.error("Exception Occured On Layout Migration Method :" + e.getMessage());
			mapOfStatus.put("Status", "Failed");
			migrationStatus.complete(mapOfStatus);
			clearCacheDbEnvironmentType();
			return migrationStatus;
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void clearCacheDbEnvironmentType() {
		cache.clearToken("Source-DEV");
		cache.clearToken("Source-TEST");
		cache.clearToken("Source-QA");
		cache.clearToken("Source-STAGING");
		cache.clearToken("Source-PROD");
		cache.clearToken("Destination-DEV");
		cache.clearToken("Destination-TEST");
		cache.clearToken("Destination-QA");
		cache.clearToken("Destination-STAGING");
		cache.clearToken("Destination-PROD");

	}

	/**
	 * @param listOfInsertQuery
	 * @return
	 */
	
	private boolean insertTheLayoutData(List<Map<String, List<Object>>> listOfInsertQuery, Integer layoutId) {

		logger.info("InsertTheLayoutData Called");
		Map<String, List<Object>> insertQueryMap = new LinkedHashMap<String, List<Object>>();

		Iterator<Map<String, List<Object>>> listIterator = listOfInsertQuery.listIterator();

		List<DocView> listOfDestinationDocView = new ArrayList<DocView>();
		List<DocViewElementD> listOfDestinationDocViewElementD = new ArrayList<DocViewElementD>();
		List<LayoutDetail> listOfDestinationLayoutDetail = new ArrayList<LayoutDetail>();
		List<LayoutElementD> listOfDestinationLayoutElementD = new ArrayList<LayoutElementD>();
		List<LayoutI18nRes> listOfDestinationLayoutI18nRes = new ArrayList<LayoutI18nRes>();
		List<LayoutLevelGroup> listOfDestinationLayoutLevelGroup = new ArrayList<LayoutLevelGroup>();
		List<LayoutPage> listOfDestinationLayoutPage = new ArrayList<LayoutPage>();
		List<LayoutProfile> listOfDestinationLayoutProfile = new ArrayList<LayoutProfile>();
		List<LayoutProfileD> listOfDestinationLayoutProfileD = new ArrayList<LayoutProfileD>();
		List<LayoutSection> listOfDestinationLayoutSection = new ArrayList<LayoutSection>();
		List<LayoutSectionD> listOfDestinationLayoutSectionD = new ArrayList<LayoutSectionD>();
		List<ExtValidation> listOfDestinationExtValidation = new ArrayList<ExtValidation>();
		List<ExtValidationMetaData> listOfDestinationExtValidationMetaData = new ArrayList<ExtValidationMetaData>();

		while (listIterator.hasNext()) {

			Map<String, List<Object>> mapOfObject = listIterator.next();

			for (Entry<String, List<Object>> entry : mapOfObject.entrySet()) {

				switch (entry.getKey()) {

				case "DocView":
					if (!entry.getValue().isEmpty()) {

						List<com.retelzy.brim.entity.source.DocView> listOfSourceDocView = entry.getValue()
								.parallelStream().map(element -> (com.retelzy.brim.entity.source.DocView) element)
								.collect(Collectors.toList());

						for (Integer index = 0; index < listOfSourceDocView.size(); index++) {

							DocView destinationDocView = new DocView();
							destinationDocView.setI18nResId(listOfSourceDocView.get(index).getI18nResId());
							destinationDocView.setIsBaseView(listOfSourceDocView.get(index).getIsBaseView());
							destinationDocView.setIsStepProcess(listOfSourceDocView.get(index).getIsStepProcess());
							destinationDocView.setIsNewView(listOfSourceDocView.get(index).getIsNewView());
							destinationDocView.setDocViewType(listOfSourceDocView.get(index).getDocViewType());
							destinationDocView.setComments(listOfSourceDocView.get(index).getComments());
							destinationDocView.setModifyTs(listOfSourceDocView.get(index).getModifyTs());
							destinationDocView.setModifyUser(listOfSourceDocView.get(index).getModifyUser());

							DocViewPK destinationDocViewPk = new DocViewPK();
							destinationDocViewPk.setDocId(listOfSourceDocView.get(index).getId().getDocId());
							destinationDocViewPk.setDocViewId(listOfSourceDocView.get(index).getId().getDocViewId());
							destinationDocViewPk.setLayoutId(layoutId);
							
							destinationDocView.setId(destinationDocViewPk);
							logger.info("DocViewID :" + destinationDocView.getId());
							

							listOfDestinationDocView.add(destinationDocView);
						}

						if (!listOfDestinationDocView.isEmpty()) {

							List<Object> listOfDocViewAsObject = listOfDestinationDocView.parallelStream()
									.map(element -> (Object) element).collect(Collectors.toList());
							insertQueryMap.put("DocView", listOfDocViewAsObject);

						}

					}

					break;

				case "DocViewElementD":

					List<com.retelzy.brim.entity.source.DocViewElementD> listOfSourceDocViewElementD = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.DocViewElementD) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceDocViewElementD.size(); index++) {

						DocViewElementD docViewElementD = new DocViewElementD();
						docViewElementD.setPropValue(listOfSourceDocViewElementD.get(index).getPropValue());
						docViewElementD.setModifyTs(listOfSourceDocViewElementD.get(index).getModifyTs());
						docViewElementD.setModifyUser(listOfSourceDocViewElementD.get(index).getModifyUser());

						DocViewElementDPK docViewElementDPK = new DocViewElementDPK();
						docViewElementDPK
								.setAssocLevelId(listOfSourceDocViewElementD.get(index).getId().getAssocLevelId());
						docViewElementDPK.setDocId(listOfSourceDocViewElementD.get(index).getId().getDocId());
						docViewElementDPK.setDocViewId(listOfSourceDocViewElementD.get(index).getId().getDocViewId());
						docViewElementDPK.setFieldId(listOfSourceDocViewElementD.get(index).getId().getFieldId());
						docViewElementDPK.setLayoutId(listOfSourceDocViewElementD.get(index).getId().getLayoutId());
						docViewElementDPK.setLevelId(listOfSourceDocViewElementD.get(index).getId().getLevelId());
						docViewElementDPK.setPropName(listOfSourceDocViewElementD.get(index).getId().getPropName());

						docViewElementD.setId(docViewElementDPK);
						logger.info("docViewElementD :" + docViewElementD.getId());

						listOfDestinationDocViewElementD.add(docViewElementD);
					}

					if (!listOfDestinationDocViewElementD.isEmpty()) {

						List<Object> listOfDocViewElementDAsObject = listOfDestinationDocViewElementD.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("DocViewElementD", listOfDocViewElementDAsObject);

					} else {
						logger.info("DocViewElementD Table Don't have data to insert");
					}

					break;

				case "LayoutDetail":

					List<com.retelzy.brim.entity.source.LayoutDetail> listOfSourceLayoutDetail = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutDetail) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutDetail.size(); index++) {

						LayoutDetail destLayoutDetail = new LayoutDetail();
						destLayoutDetail.setAssocLevelId(listOfSourceLayoutDetail.get(index).getAssocLevelId());
						destLayoutDetail.setFieldPosition(listOfSourceLayoutDetail.get(index).getFieldPosition());
						destLayoutDetail.setAcl(listOfSourceLayoutDetail.get(index).getAcl());
						destLayoutDetail.setColspan(listOfSourceLayoutDetail.get(index).getColspan());
						destLayoutDetail.setRowspan(listOfSourceLayoutDetail.get(index).getRowspan());
						destLayoutDetail.setSearchPos(listOfSourceLayoutDetail.get(index).getSearchPos());
						destLayoutDetail.setSearchListPos(listOfSourceLayoutDetail.get(index).getSearchListPos());
						destLayoutDetail.setLinkUrl(listOfSourceLayoutDetail.get(index).getLinkUrl());
						destLayoutDetail.setIsRef(listOfSourceLayoutDetail.get(index).getIsRef());
						destLayoutDetail.setIsExt(listOfSourceLayoutDetail.get(index).getIsExt());
						destLayoutDetail.setRefLevelId(listOfSourceLayoutDetail.get(index).getRefLevelId());
						destLayoutDetail.setModifyTs(listOfSourceLayoutDetail.get(index).getModifyTs());
						destLayoutDetail.setModifyUser(listOfSourceLayoutDetail.get(index).getModifyUser());

						LayoutDetailPK layoutDetailPK = new LayoutDetailPK();
						layoutDetailPK.setLayoutId(listOfSourceLayoutDetail.get(index).getId().getLayoutId());
						layoutDetailPK.setDocViewId(listOfSourceLayoutDetail.get(index).getId().getDocViewId());
						layoutDetailPK.setSectionId(listOfSourceLayoutDetail.get(index).getId().getSectionId());
						layoutDetailPK.setDocId(listOfSourceLayoutDetail.get(index).getId().getDocId());
						layoutDetailPK.setDocLevelId(listOfSourceLayoutDetail.get(index).getId().getDocLevelId());
						layoutDetailPK.setDocFieldId(listOfSourceLayoutDetail.get(index).getId().getDocFieldId());

						destLayoutDetail.setId(layoutDetailPK);
						logger.info("destLayoutDetail :" + destLayoutDetail.getId());

						listOfDestinationLayoutDetail.add(destLayoutDetail);

					}

					if (!listOfDestinationLayoutDetail.isEmpty()) {

						List<Object> listOfLayoutDetailAsObject = listOfDestinationLayoutDetail.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("LayoutDetail", listOfLayoutDetailAsObject);

					} else {
						logger.info("LayoutDetail Table Don't have data to insert");
					}

					break;

				case "LayoutElementD":
					List<com.retelzy.brim.entity.source.LayoutElementD> listOfSourceLayoutElementD = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutElementD) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutElementD.size(); index++) {

						LayoutElementD layoutElementD = new LayoutElementD();
						layoutElementD.setPropValue(listOfSourceLayoutElementD.get(index).getPropValue());
						layoutElementD.setModifyTs(listOfSourceLayoutElementD.get(index).getModifyTs());
						layoutElementD.setModifyUser(listOfSourceLayoutElementD.get(index).getModifyUser());

						LayoutElementDPK layoutElementDPK = new LayoutElementDPK();
						layoutElementDPK.setDocId(listOfSourceLayoutElementD.get(index).getId().getDocId());
						layoutElementDPK.setFieldId(listOfSourceLayoutElementD.get(index).getId().getFieldId());
						layoutElementDPK.setLayoutId(listOfSourceLayoutElementD.get(index).getId().getLayoutId());
						layoutElementDPK.setLevelId(listOfSourceLayoutElementD.get(index).getId().getLevelId());
						layoutElementDPK.setPropName(listOfSourceLayoutElementD.get(index).getId().getPropName());

						layoutElementD.setId(layoutElementDPK);
						logger.info("layoutElementD :" + layoutElementD.getId());

						listOfDestinationLayoutElementD.add(layoutElementD);

						if (!listOfDestinationLayoutElementD.isEmpty()) {
							List<Object> listOfLayoutElementDAsObject = listOfDestinationLayoutElementD.parallelStream()
									.map(element -> (Object) element).collect(Collectors.toList());

							insertQueryMap.put("LayoutElementD", listOfLayoutElementDAsObject);

						} else {
							logger.info("LayoutElementD Table Don't have data to Insert");
						}

					}

					break;

				case "LayoutI18nRes":

					List<com.retelzy.brim.entity.source.LayoutI18nRes> listOfSourceLayoutI8nRes = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutI18nRes) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutI8nRes.size(); index++) {

						LayoutI18nRes layoutI18nRes = new LayoutI18nRes();
						layoutI18nRes.setLabel(listOfSourceLayoutI8nRes.get(index).getLabel());
						layoutI18nRes.setShortDesc(listOfSourceLayoutI8nRes.get(index).getShortDesc());
						layoutI18nRes.setLongDesc(listOfSourceLayoutI8nRes.get(index).getLongDesc());
						layoutI18nRes.setModifyTs(listOfSourceLayoutI8nRes.get(index).getModifyTs());
						layoutI18nRes.setModifyUser(listOfSourceLayoutI8nRes.get(index).getModifyUser());

						LayoutI18nResPK layoutI18nResPK = new LayoutI18nResPK();
						layoutI18nResPK.setLanguageId(listOfSourceLayoutI8nRes.get(index).getId().getLanguageId());
						layoutI18nResPK.setLayoutId(listOfSourceLayoutI8nRes.get(index).getId().getLayoutId());
						layoutI18nResPK.setI18NResId(listOfSourceLayoutI8nRes.get(index).getId().getI18NResId());

						layoutI18nRes.setId(layoutI18nResPK);
						logger.info("layoutI18nRes :" + layoutI18nRes.getId());

						listOfDestinationLayoutI18nRes.add(layoutI18nRes);

					}

					if (!listOfDestinationLayoutI18nRes.isEmpty()) {

						List<Object> listOfLayoutI18nResAsObject = listOfDestinationLayoutI18nRes.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("LayoutI18nRes", listOfLayoutI18nResAsObject);

					} else {
						logger.info("LayoutI18nRes Table Don't have data to insert");
					}

					break;

				case "LayoutLevelGroup":

					List<com.retelzy.brim.entity.source.LayoutLevelGroup> listOfSourceLayoutLevelGroup = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.LayoutLevelGroup) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutLevelGroup.size(); index++) {

						LayoutLevelGroup layoutLevelGroup = new LayoutLevelGroup();
						layoutLevelGroup.setPropValue(listOfSourceLayoutLevelGroup.get(index).getPropValue());
						layoutLevelGroup.setModifyTs(listOfSourceLayoutLevelGroup.get(index).getModifyTs());
						layoutLevelGroup.setModifyUser(listOfSourceLayoutLevelGroup.get(index).getModifyUser());

						LayoutLevelGroupPK layoutLevelGroupPK = new LayoutLevelGroupPK();
						layoutLevelGroupPK.setDocViewId(listOfSourceLayoutLevelGroup.get(index).getId().getDocViewId());
						layoutLevelGroupPK
								.setLevelGroupId(listOfSourceLayoutLevelGroup.get(index).getId().getLevelGroupId());
						layoutLevelGroupPK.setLayoutId(listOfSourceLayoutLevelGroup.get(index).getId().getLayoutId());
						layoutLevelGroupPK.setPropName(listOfSourceLayoutLevelGroup.get(index).getId().getPropName());

						layoutLevelGroup.setId(layoutLevelGroupPK);
						logger.info("layoutLevelGroup :" + layoutLevelGroup.getId());

						listOfDestinationLayoutLevelGroup.add(layoutLevelGroup);
					}

					if (!listOfDestinationLayoutLevelGroup.isEmpty()) {

						List<Object> listOfLayoutLevelGroupAsObject = listOfDestinationLayoutLevelGroup.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("LayoutLevelGroup", listOfLayoutLevelGroupAsObject);
					} else {
						logger.info("LayoutLevelGroup Table Don't have to insert");
					}

					break;

				case "LayoutPage":
					List<com.retelzy.brim.entity.source.LayoutPage> listOfSourceLayoutPage = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutPage) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutPage.size(); index++) {

						LayoutPage layoutPage = new LayoutPage();
						layoutPage.setLevelGroupId(listOfSourceLayoutPage.get(index).getLevelGroupId());
						layoutPage.setSectionPosition(listOfSourceLayoutPage.get(index).getSectionPosition());
						layoutPage.setSectionStackPos(listOfSourceLayoutPage.get(index).getSectionStackPos());
						layoutPage.setModifyTs(listOfSourceLayoutPage.get(index).getModifyTs());
						layoutPage.setModifyUser(listOfSourceLayoutPage.get(index).getModifyUser());

						LayoutPagePK layoutPagePK = new LayoutPagePK();
						layoutPagePK.setDocViewId(listOfSourceLayoutPage.get(index).getId().getDocViewId());
						layoutPagePK.setLayoutId(listOfSourceLayoutPage.get(index).getId().getLayoutId());
						layoutPagePK.setSectionId(listOfSourceLayoutPage.get(index).getId().getSectionId());
						layoutPagePK.setLayoutLevel(listOfSourceLayoutPage.get(index).getId().getLayoutLevel());

						layoutPage.setId(layoutPagePK);
						logger.info("layoutPage :" + layoutPage.getId());

						listOfDestinationLayoutPage.add(layoutPage);

					}

					if (!listOfDestinationLayoutPage.isEmpty()) {

						List<Object> listOfLayoutPageAsObject = listOfDestinationLayoutPage.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("LayoutPage", listOfLayoutPageAsObject);

					} else {
						logger.info("LayoutPage Table don't have data to insert");
					}

					break;

				case "LayoutProfile":

					List<com.retelzy.brim.entity.source.LayoutProfile> listOfSourceLayoutProfile = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutProfile) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutProfile.size(); index++) {

						LayoutProfile layoutProfile = new LayoutProfile();
						layoutProfile.setModifyTs(listOfSourceLayoutProfile.get(index).getModifyTs());
						layoutProfile.setModifyUser(listOfSourceLayoutProfile.get(index).getModifyUser());
						layoutProfile.setLayoutId(listOfSourceLayoutProfile.get(index).getLayoutId());
						layoutProfile.setLayoutDesc(listOfSourceLayoutProfile.get(index).getLayoutDesc());

						listOfDestinationLayoutProfile.add(layoutProfile);
					}

					if (!listOfDestinationLayoutProfile.isEmpty()) {

						List<Object> listOfLayoutProfileAsObject = listOfDestinationLayoutProfile.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("LayoutProfile", listOfLayoutProfileAsObject);

					} else {
						logger.info("LayoutProfile Table Don't have data to insert");
					}

					break;

				case "LayoutProfileD":

					List<com.retelzy.brim.entity.source.LayoutProfileD> listOfSourceLayoutProfileD = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutProfileD) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutProfileD.size(); index++) {

						LayoutProfileD layoutProfileD = new LayoutProfileD();
						layoutProfileD.setModifyTs(listOfSourceLayoutProfileD.get(index).getModifyTs());
						layoutProfileD.setModifyUser(listOfSourceLayoutProfileD.get(index).getModifyUser());
						layoutProfileD.setPropValue(listOfSourceLayoutProfileD.get(index).getPropValue());

						LayoutProfileDPK layoutProfileDPK = new LayoutProfileDPK();
						layoutProfileDPK.setLayoutId(listOfSourceLayoutProfileD.get(index).getId().getLayoutId());
						layoutProfileDPK.setPropName(listOfSourceLayoutProfileD.get(index).getId().getPropName());

						layoutProfileD.setId(layoutProfileDPK);
						
						logger.info("layoutProfileD :" + layoutProfileD.getId());

						listOfDestinationLayoutProfileD.add(layoutProfileD);

					}

					if (!listOfDestinationLayoutProfileD.isEmpty()) {

						List<Object> listOfLayoutProfileDAsObject = listOfDestinationLayoutProfileD.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("LayoutProfileD", listOfLayoutProfileDAsObject);

					} else {
						logger.info("LayoutProfileD Table Don't have data to insert");
					}

					break;

				case "LayoutSection":

					List<com.retelzy.brim.entity.source.LayoutSection> listOfSourceLayoutSection = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutSection) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutSection.size(); index++) {

						LayoutSection layoutSection = new LayoutSection();
						layoutSection.setAcl(listOfSourceLayoutSection.get(index).getAcl());
						layoutSection.setCellPadding(listOfSourceLayoutSection.get(index).getCellPadding());
						layoutSection.setCellSpacing(listOfSourceLayoutSection.get(index).getCellSpacing());
						layoutSection.setColsFreeze(listOfSourceLayoutSection.get(index).getColsFreeze());
						layoutSection.setCustomSection(listOfSourceLayoutSection.get(index).getCustomSection());
						layoutSection.setDisplayType(listOfSourceLayoutSection.get(index).getDisplayType());
						layoutSection.setI18nResId(listOfSourceLayoutSection.get(index).getI18nResId());
						layoutSection.setLabelDisplay(listOfSourceLayoutSection.get(index).getLabelDisplay());
						layoutSection.setModelValId(listOfSourceLayoutSection.get(index).getModelValId());
						layoutSection.setMultipost(listOfSourceLayoutSection.get(index).getMultipost());
						layoutSection.setNoOfCols(listOfSourceLayoutSection.get(index).getNoOfCols());
						layoutSection.setRepeating(listOfSourceLayoutSection.get(index).getRepeating());
						layoutSection.setRowsToDisplay(listOfSourceLayoutSection.get(index).getRowsToDisplay());
						layoutSection.setSectionClass(listOfSourceLayoutSection.get(index).getSectionClass());
						layoutSection.setSectionTag(listOfSourceLayoutSection.get(index).getSectionTag());
						layoutSection.setSelectWidgetType(listOfSourceLayoutSection.get(index).getSelectWidgetType());
						layoutSection.setShowAssocMenu(listOfSourceLayoutSection.get(index).getShowAssocMenu());
						layoutSection.setShowCheckbox(listOfSourceLayoutSection.get(index).getShowCheckbox());
						layoutSection.setTitleFreeze(listOfSourceLayoutSection.get(index).getTitleFreeze());
						layoutSection.setModifyTs(listOfSourceLayoutSection.get(index).getModifyTs());
						layoutSection.setModifyUser(listOfSourceLayoutSection.get(index).getModifyUser());

						LayoutSectionPK layoutSectionPK = new LayoutSectionPK();
						layoutSectionPK.setLayoutId(listOfSourceLayoutSection.get(index).getId().getLayoutId());
						layoutSectionPK.setDocViewId(listOfSourceLayoutSection.get(index).getId().getDocViewId());
						layoutSectionPK.setSectionId(listOfSourceLayoutSection.get(index).getId().getSectionId());

						layoutSection.setId(layoutSectionPK);
						
						logger.info("layoutSection :" + layoutSection.getId());

						listOfDestinationLayoutSection.add(layoutSection);

					}

					if (!listOfDestinationLayoutSection.isEmpty()) {
						List<Object> listOfLayoutSectionAsObject = listOfDestinationLayoutSection.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("LayoutSection", listOfLayoutSectionAsObject);

					} else {
						logger.info("LayoutSection Table Don't have data to Insert");
					}

					break;

				case "LayoutSectionD":

					List<com.retelzy.brim.entity.source.LayoutSectionD> listOfSourceLayoutSectionD = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.LayoutSectionD) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceLayoutSectionD.size(); index++) {

						LayoutSectionD layoutSectionD = new LayoutSectionD();
						layoutSectionD.setAssocLevelId(listOfSourceLayoutSectionD.get(index).getAssocLevelId());
						layoutSectionD.setValue(listOfSourceLayoutSectionD.get(index).getValue());
						layoutSectionD.setModifyTs(listOfSourceLayoutSectionD.get(index).getModifyTs());
						layoutSectionD.setModifyUser(listOfSourceLayoutSectionD.get(index).getModifyUser());

						LayoutSectionDPK layoutSectionDPK = new LayoutSectionDPK();
						layoutSectionDPK.setDocId(listOfSourceLayoutSectionD.get(index).getId().getDocId());
						layoutSectionDPK.setLayoutId(listOfSourceLayoutSectionD.get(index).getId().getLayoutId());
						layoutSectionDPK.setDocViewId(listOfSourceLayoutSectionD.get(index).getId().getDocViewId());
						layoutSectionDPK.setSectionId(listOfSourceLayoutSectionD.get(index).getId().getSectionId());
						layoutSectionDPK.setRowNo(listOfSourceLayoutSectionD.get(index).getId().getRowNo());
						layoutSectionDPK.setDocLevelId(listOfSourceLayoutSectionD.get(index).getId().getDocLevelId());
						layoutSectionDPK.setDocFieldId(listOfSourceLayoutSectionD.get(index).getId().getDocFieldId());

						layoutSectionD.setId(layoutSectionDPK);
						logger.info("layoutSectionD :" + layoutSectionD.getId());

						listOfDestinationLayoutSectionD.add(layoutSectionD);

					}

					if (!listOfDestinationLayoutSectionD.isEmpty()) {

						List<Object> listOfLayoutSectionDAsObject = listOfDestinationLayoutSectionD.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("LayoutSectionD", listOfLayoutSectionDAsObject);

					} else {
						logger.info("LayoutSectionD Table Don't have data to insert");
					}

					break;

				case "LayoutSequenceNumbers":
					if (!entry.getValue().isEmpty()) {
						insertQueryMap.put("LayoutSequenceNumbers", entry.getValue());

					} else {
						logger.info("LayoutSequenceNumbers Table don't have data to insert");
					}
					break;

				case "UpdateSequenceNumbers":
					if (!entry.getValue().isEmpty()) {
						insertQueryMap.put("UpdateSequenceNumbers", entry.getValue());

					} else {
						logger.info("UpdateSequenceNumbers Table don't have data to insert");
					}
					break;

				case "UpdateDocViewElementD":
					if (!entry.getValue().isEmpty()) {
						insertQueryMap.put("UpdateDocViewElementD", entry.getValue());

					} else {
						logger.info("UpdateDocViewElementD Table don't have data to insert");
					}
					break;

				case "ExtValidation":
					List<com.retelzy.brim.entity.source.ExtValidation> listOfSourceExtValidation = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.ExtValidation) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceExtValidation.size(); index++) {

						ExtValidation extValidation = new ExtValidation();
						extValidation.setValidationId(listOfSourceExtValidation.get(index).getValidationId());
						extValidation.setValidationName(listOfSourceExtValidation.get(index).getValidationName());
						extValidation.setValidationRule(listOfSourceExtValidation.get(index).getValidationRule());
						extValidation.setValidationDesc(listOfSourceExtValidation.get(index).getValidationDesc());
						extValidation.setCreateTs(listOfSourceExtValidation.get(index).getCreateTs());
						extValidation.setCreateUser(listOfSourceExtValidation.get(index).getCreateUser());
						extValidation.setModifyTs(listOfSourceExtValidation.get(index).getModifyTs());
						extValidation.setModifyUser(listOfSourceExtValidation.get(index).getModifyUser());

						listOfDestinationExtValidation.add(extValidation);

					}

					if (!listOfDestinationExtValidation.isEmpty()) {

						List<Object> listOfExtValidationAsObject = listOfDestinationExtValidation.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("ExtValidation", listOfExtValidationAsObject);

					} else {
						logger.info("ExtValidation Table Don't have data to insert");
					}

					break;

				case "ExtValidationMetadata":
					List<com.retelzy.brim.entity.source.ExtValidationMetaData> listOfSourceExtValidationMetaData = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.ExtValidationMetaData) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceExtValidationMetaData.size(); index++) {

						ExtValidationMetaData extValidationMetaData = new ExtValidationMetaData();
						extValidationMetaData.setPropValue(listOfSourceExtValidationMetaData.get(index).getPropValue());
						extValidationMetaData.setModifyTs(listOfSourceExtValidationMetaData.get(index).getModifyTs());
						extValidationMetaData
								.setModifyUser(listOfSourceExtValidationMetaData.get(index).getModifyUser());

						ExtValidationMetaDataPK extValidationMetaDataPK = new ExtValidationMetaDataPK();
						extValidationMetaDataPK.setValidationId(
								listOfSourceExtValidationMetaData.get(index).getId().getValidationId());
						extValidationMetaDataPK
								.setPropName(listOfSourceExtValidationMetaData.get(index).getId().getPropName());

						extValidationMetaData.setId(extValidationMetaDataPK);
						
						logger.info("extValidationMetaData :" + extValidationMetaData.getId());

						listOfDestinationExtValidationMetaData.add(extValidationMetaData);
					}

					if (!listOfDestinationExtValidationMetaData.isEmpty()) {

						List<Object> listOfDestinationExtValidationMetaDataAsObject = listOfDestinationExtValidationMetaData
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("ExtValidationMetaData", listOfDestinationExtValidationMetaDataAsObject);

					} else {
						logger.info("ExtValidationMetaData Table Don't have data to insert");
					}

					break;

				default:
					logger.error(
							"Table name not added in Switch Statement of insertTheLayoutData() method of MigationSecuirtyModule class :"
									+ entry.getKey());
					return false;
				}

			}

		}

		CompletableFuture<Boolean> futureStatus = destinationDbService
				.saveAllTheDestinationLayoutTableDetails(insertQueryMap);

		Boolean insertStatus = false;

		try {
			insertStatus = futureStatus.get();

			/*
			 * if (insertStatus) { insertQueryHandQueryDtableData(layoutId); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In insertTheLayoutData() method Of MigrationLayoutModule Class :"
					+ e.getMessage());
			insertStatus = false;
		}

		return insertStatus;

		/*
		 * Boolean insertStatus =
		 * destinationDbService.saveAllTheDestinationTableDetails(insertQueryMap);
		 * 
		 * logger.info("Insert Status :" + insertStatus);
		 * 
		 * if (insertStatus == true) { return true; } else { return false; }
		 */

	}

	@SuppressWarnings("deprecation")
	public void insertQueryHandQueryDtableData(Integer layoutId) {
		logger.info("Entering into insertQueryHandQueryDtableData()..... ");
		File queryHInsertfile = new File(
				configFileReader.getPropertyValue("dir.path.source") + "/query_h_insert" + layoutId + ".xml");
		File queryDInsertfile = new File(
				configFileReader.getPropertyValue("dir.path.source") + "/query_d_insert" + layoutId + ".xml");

		IDatabaseConnection connection = null;

		if (queryHInsertfile.exists() && queryDInsertfile.exists()) {
			try {
				connection = destinationDbService.getConnection();

				try {
					logger.info("Inserting QueryHandQueryDtableData()..... ");
					DatabaseOperation.INSERT.execute(connection,
							new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));
					DatabaseOperation.INSERT.execute(connection,
							new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
					connection.getConnection().commit();
					logger.info("Completed Inserting QueryHandQueryDtableData()..... ");
				} catch (Exception e) {
					logger.info("Updating QueryHandQueryDtableData()..... ");
					DatabaseOperation.UPDATE.execute(connection,
							new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));
					DatabaseOperation.UPDATE.execute(connection,
							new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
					connection.getConnection().commit();
					logger.info("Updating QueryHandQueryDtableData() is completed..... ");
				} finally {
					// delete the files
					connection.close();
					System.gc();
					queryHInsertfile.delete();
					queryDInsertfile.delete();
					try {
						Files.deleteIfExists(Paths.get(queryHInsertfile.getAbsolutePath()));
						Files.deleteIfExists(Paths.get(queryDInsertfile.getAbsolutePath()));
					} catch (NoSuchFileException e) {
						logger.error("No such file/directory exists");
					} catch (DirectoryNotEmptyException e) {
						logger.error("Directory is not empty.");
					} catch (IOException e) {
						logger.error("Invalid permissions.");

					}

					logger.info("Deletion successful.");
				}
			} catch (Exception e) {
				logger.error("Error occured while Inserting QueryHandQueryDtableData() " + e.getMessage());
			}
		}

	}

	/**
	 * Get the file name for SQL file
	 *
	 * @param layoutOption
	 * @param dateTime
	 * @return
	 */
	private String getSQLFileName(String layoutOption) {
		SimpleDateFormat sdtf = new SimpleDateFormat("MMddyyHHmmss");
		String dateTime = sdtf.format(new java.util.Date());
		String fileName = "";
		if (layoutOption.equals("ALL_LAYOUT")) {
			fileName = "Layout_ALL_LAYOUT-" + dateTime + ".sql";
		} else if (layoutOption.equals("MULTIPLE")) {
			fileName = "Layout_MULTIPLE-" + dateTime + ".sql";
		} else {
			fileName = "Layout_" + layoutOption + "-" + dateTime + ".sql";
		}
		return fileName;
	}

}
