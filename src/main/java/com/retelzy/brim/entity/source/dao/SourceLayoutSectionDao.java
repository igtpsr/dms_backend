package com.retelzy.brim.entity.source.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutSection;
import com.retelzy.brim.entity.source.LayoutSectionPK;

@Repository
public interface SourceLayoutSectionDao extends CrudRepository<LayoutSection, LayoutSectionPK> {

	public List<Object> findByIdLayoutIdAndModifyTsGreaterThanEqualAndModifyTsLessThanEqual(Integer layoutId,
			Timestamp fromDate, Timestamp toDate);

}
