package com.retelzy.brim.entity.user.dao;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.retelzy.brim.migration.model.MigrationId;

@Service
public class AppConfig6RulesMigrationStoredProcDao {
	
private static final Logger log = LoggerFactory.getLogger(AppConfig6RulesMigrationStoredProcDao.class);

	
	@PersistenceUnit(unitName = "User")
	private EntityManagerFactory emf;
	
	
	public Map<String, Object> migrateAppConfigorRulesStoredProc(MigrationId migrationId, List<String> listOfPropNames, String sourceEnvType, String destinationEnvType) throws Exception {
		try {
			System.out.print(listOfPropNames);
			return this.callStoredProcedureToMigrateAppConfigorRules(migrationId, listOfPropNames, sourceEnvType, destinationEnvType);
		} catch (Exception e) {
			log.error(" Error while running migration stored procedure --> " + e.getMessage());
			throw e;
		}
	}
	
	
	private Map<String, Object> callStoredProcedureToMigrateAppConfigorRules(MigrationId migrationId, List<String> listOfPropNames, String sourceEnvType, String destinationEnvType) throws Exception {
		log.info("Stored procedure migration of --> \n " + migrationId.getModuleName());
		
		if (migrationId.getModuleName().equalsIgnoreCase("APP CONFIG") || migrationId.getModuleName().equalsIgnoreCase("RULES")) {
		
			//Input parameters
			String moduletype = migrationId.getModuleName();
			String clientSpec = migrationId.getClientSpec();
			Date startdate = migrationId.getFromDate();
			Date enddate = migrationId.getToDate();
			Integer configID = 0;
			String propnames = "";
			String propvalues = "";
			
			if (moduletype.equalsIgnoreCase("APP CONFIG") && clientSpec != null ) {
				configID = 1;
				propnames = "clientspec";
			} else if (moduletype.equalsIgnoreCase("RULES")) {
				configID = 1;
				//--------------------------------------------------------
				//Converting list of values to Comma separated String of values
				StringBuilder strbul=new StringBuilder();
		        for(String str : listOfPropNames)
		        {
		            strbul.append(str);
		            strbul.append(",");
		        }
		        strbul.setLength(strbul.length()-1);
		        propvalues = strbul.toString();
				//--------------------------------------------------------
			}
			
			
			EntityManager em = emf.createEntityManager();
			try {
				
				Map<String, Object> migrationStatusMap = new LinkedHashMap<String, Object>();
				
				String storedProcName = "";
				if (sourceEnvType.equalsIgnoreCase("DEV") && destinationEnvType.equalsIgnoreCase("TEST")) {
					storedProcName = "RIM_MIGRATION_FROM_DEV_TO_TEST_APP_CONFIG_AND_RULES";
				} else if (sourceEnvType.equalsIgnoreCase("TEST") && destinationEnvType.equalsIgnoreCase("DEV")) {
					storedProcName = "RIM_MIGRATION_FROM_TEST_TO_DEV_APP_CONFIG_AND_RULES";
				}
				
				StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcName)
											 .registerStoredProcedureParameter("moduletype", String.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("configid", Integer.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("propname", String.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("propvalues", String.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("startdate", Date.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("enddate", Date.class, ParameterMode.IN)
											 .registerStoredProcedureParameter("totalCount", Integer.class, ParameterMode.OUT)
											 .registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
											 .setParameter("moduletype",moduletype).setParameter("configid",configID )
											 .setParameter("propname",propnames ).setParameter("propvalues",propnames )
											 .setParameter("startdate",startdate ).setParameter("enddate", enddate);
				
				query.execute();
				String result = (String) query.getOutputParameterValue("result");
				Integer totalCount = (Integer) query.getOutputParameterValue("totalCount");
				
				log.info("  Result  : " + result);
				log.info("  Total count  : " + totalCount);
				
				migrationStatusMap.put("Status", result);
				migrationStatusMap.put("Count", totalCount);
				return migrationStatusMap;
			} catch (Exception e) {
				log.error(String.format("Data has not been updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
		return null;
	}
}
