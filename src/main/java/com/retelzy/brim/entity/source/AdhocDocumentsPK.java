package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Accessors
@Data
public class AdhocDocumentsPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "ADHOC_DOC_NAME")
	private String adhocDocName;
	
	@Column(name = "CATEGORY_ID")
	private String categoryID;

}
