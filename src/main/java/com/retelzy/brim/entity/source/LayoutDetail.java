// Generated with g9.

package com.retelzy.brim.entity.source;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "LAYOUT_DETAIL")
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors
public class LayoutDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LayoutDetailPK id;

	@Column(name = "ASSOC_LEVEL_ID")
	private Integer assocLevelId;

	@Column(name = "FIELD_POSITION")
	private Integer fieldPosition;

	@Column(name = "ACL")
	private Integer acl;

	@Column(name = "COLSPAN")
	private Integer colspan;

	@Column(name = "ROWSPAN")
	private Integer rowspan;

	@Column(name = "SEARCHPOS")
	private Integer searchPos;

	@Column(name = "SEARCHLISTPOS")
	private Integer searchListPos;

	@Column(name = "LINKURL", length = 250)
	private String linkUrl;

	@Column(name = "ISREF", length = 1)
	private String isRef;

	@Column(name = "ISEXT", length = 1)
	private String isExt;

	@Column(name = "REF_LEVEL_ID")
	private Integer refLevelId;

	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

}
