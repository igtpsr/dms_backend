package com.retelzy.brim.entity.user.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.retelzy.brim.entity.user.ClientDbDetails;
import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.entity.user.RoleProfile;
import com.retelzy.brim.entity.user.UserProfile;
import com.retelzy.brim.migration.model.MigrationId;

public interface UserService {

	public Optional<UserProfile> getTheUserDetailsById(Integer modifibleUserId);

	public ClientDbDetails saveOrUpdateClientDbDetails(ClientDbDetails clientDbdDetails);

	public Optional<ClientDbDetails> getClientDbDetailsById(Integer clientDbCredentialId);

	public Optional<ClientDbDetails> getClientDbDetailsByEnvironmentType(List<String> listOfEnvironmentType);

	public void createMigrationReportDetails(MigrationDetails migrationReportDetails);

	public Optional<MigrationDetails> getTheLastMigratedTimePeriod();

	public List<ClientDbDetails> getTheConfigurationAppDetails();

	public void createMigrationDetails(MigrationDetails migrationDetails);

	public void removeTheDBEnvironmentDetails(Integer clentDbDetailsId);

	public Optional<MigrationDetails> getTheLatestMigrationDetailsByModuleName(String moduleName);

	public UserProfile saveOrUpdate(UserProfile userProfile);

	public RoleProfile saveOrUpdateRoleProfile(RoleProfile roleProfile);

	public List<RoleProfile> getAllAvailableRoleProfile();

	public Optional<RoleProfile> getTheRoleProfileDetails(Integer roleId);

	public List<UserProfile> getUserProfileDetailsByRoleId(RoleProfile roleProfile);

	public void deleteRoleProfile(Integer roleId);

	public List<UserProfile> getAllAvailableUserProfile();

	public List<String> getAllAvailableUserName();
	
	public UserProfile getTheUserProfile(String userName, String password);

	public Optional<ClientDbDetails> getTheClientDbDetailsByEnvName(String destinationEnvironmentType);

	public List<MigratedRoleIdDetails> getTheLatestMigratedRoleId(MigrationDetails migrationDetails);

	public List<MigratedLayoutIdDetails> getTheLatestMigratedLayoutId(MigrationDetails migrationDetails);
	
	//--------------------------Added by LOK28APR2021 - Begin---------------------------------------
	public Map<String, Object> migrateLayoutStoredProc(MigrationId migrationId, String sourceEnvType, String destinationEnvType);
	
	public Map<String, Object> migrateSecurityStoredProc(MigrationId migrationId, MigrationDetails migrationDetails);
	
	public Map<String, Object> migrateTheAdhocDocStoredProc(MigrationId migrationId, String sourceEnvType, String destinationEnvType);
	
	public Map<String, Object> appConfig6RulesMigrationStoredProc(MigrationId migrationId, List<String> listOfPropNames, String sourceEnvType, String destinationEnvType);
	//--------------------------Added by LOK28APR2021 - End---------------------------------------
}
