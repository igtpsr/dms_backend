package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.SecBoFieldAcl;

@Repository
public interface SourceSecBoFieldAclDao extends CrudRepository<SecBoFieldAcl, Integer> {

	@Query("Select sb from SecBoFieldAcl sb Where sb.id.roleId =:roleId")
	public List<Object> findByRoleId(Integer roleId);

	@Query("Select sb.id.queryId from SecBoFieldAcl sb where sb.id.roleId =:roleId")
	public List<Integer> getAllTheQueryIdsByRoleId(Integer roleId);

}
