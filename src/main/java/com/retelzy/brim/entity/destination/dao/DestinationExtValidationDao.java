package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.ExtValidation;

@Repository
public interface DestinationExtValidationDao extends CrudRepository<ExtValidation, Integer> {
	
	@Query("select max(ev.validationId)+1 from ExtValidation ev")
	public Integer getTheMaxValidationIdFromExtValidation();

	@Query("SELECT count(*) FROM ExtValidation ev where ev.validationId=:validationId and ev.validationName=:validationName")
	public Integer getTheValidationIdCountNumber(Integer validationId, String validationName);

}
