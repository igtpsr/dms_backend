package com.retelzy.brim.entity.user.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.user.RoleProfile;
import com.retelzy.brim.entity.user.UserProfile;

@Repository
public interface UserProfileDao extends CrudRepository<UserProfile, Integer> {

	public Optional<UserProfile> findByUserName(String userName);

	@Query("Select user from UserProfile user Where user.roleProfile =:roleProfile")
	public List<UserProfile> findByRoleProfile(RoleProfile roleProfile);

	@Query("Select user from UserProfile user")
	public List<UserProfile> getAllAvailableUserProfile();
	
	@Query("Select userName from UserProfile")
	public List<String> getAllAvailableUserName();
	

	public UserProfile findByUserNameAndPassword(String userName, String password);

}
