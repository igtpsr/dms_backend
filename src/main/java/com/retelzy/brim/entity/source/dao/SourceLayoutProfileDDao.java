package com.retelzy.brim.entity.source.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.LayoutProfileD;
import com.retelzy.brim.entity.source.LayoutProfileDPK;

@Repository
public interface SourceLayoutProfileDDao extends CrudRepository<LayoutProfileD, LayoutProfileDPK> {

	List<Object> findByIdLayoutId(Integer layoutId);


}
