package com.retelzy.brim.migration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Accessors
@Data
public class DashBoardChartColumns {

	private Integer layoutId;

	private Integer docId;

	private Integer count;

}
