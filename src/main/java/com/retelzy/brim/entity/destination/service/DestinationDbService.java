package com.retelzy.brim.entity.destination.service;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.dbunit.database.IDatabaseConnection;

import com.retelzy.brim.entity.destination.AdhocDocuments;
import com.retelzy.brim.entity.destination.ConfigD;
import com.retelzy.brim.entity.destination.DashSectionH;
import com.retelzy.brim.entity.destination.LayoutProfile;
import com.retelzy.brim.entity.destination.SecRoleProfile;
import com.retelzy.brim.entity.user.MigratedLayoutIdDetails;
import com.retelzy.brim.entity.user.MigratedRoleIdDetails;
import com.retelzy.brim.entity.user.MigrationDetails;
import com.retelzy.brim.migration.model.AdhocDocumentReport;
import com.retelzy.brim.migration.model.AppConfigReport;
import com.retelzy.brim.migration.model.LayoutReport;

public interface DestinationDbService {

	public void TestingMethod();

	public IDatabaseConnection getConnection() throws Exception;

	public Integer getTheMaxQueryIdFromQuerH();

	public Integer getTheDestinationIdCounNumber(Integer queryId, String queryName);

	public Integer getTheDestinationIdCounNumberByQueryId(Integer queryId);

	public List<Integer> getTheQueryIdByQueryName(String queryName);

	public Integer updateTheSequnceNumberBySequenceName(Integer nextQueryId, String sequenceName);

	public List<Integer> getTheRoleIdsFromDestinationDB(Integer roleId);

	public List<Integer> getAllTheProcessId(Integer processId);

	public void saveAllSecRoleProfileDetails(List<SecRoleProfile> listOfSecRoleProfile);

	public void saveAllDestinationDashSectionHDetails(List<DashSectionH> listOfDestinationDashSectionH);

	public CompletableFuture<Boolean> saveAllTheDestinationTableDetails(Map<String, List<Object>> insertQueryMap);

	public boolean deleteSecurityData(String[] delTables, Integer roleId);

	public List<Object> getLayoutProfileInfo(Integer layoutId);

	public Integer getLayoutID();

	public Integer getTheMaxValidationIdFromExtValidation();

	public Integer getTheValidationIdCountNumber(Integer validationId, String validationName);

	public boolean deleteLayoutData(String[] layoutTableDeleteSeq, Integer layoutId);

	public CompletableFuture<Boolean> saveAllTheDestinationLayoutTableDetails(Map<String, List<Object>> insertQueryMap);

	public void updateTheConigDTableDetails(List<ConfigD> listOfConfigDDetailsFromDestination);

	public void updateTheAdhocDocumentsTableDetails(List<AdhocDocuments> listOfAdhocDocumentsDetailsFromDestination);

	public List<AppConfigReport> getTheAppConfigReport(String fromDate, String toDate) throws Exception;

	public List<AdhocDocumentReport> getTheAdhocDocumentsReport(String fromDate, String toDate) throws Exception;

	public Optional<SecRoleProfile> isRoleIdAvailable(Integer roleId);

	public Connection getTheDestinationDbConnection();

	public List<Object> getTheDataFromDestinationDbByTableName(String className, Integer roleId);

	public List<Object[]> getTheLayoutColumnsByModifiedTime(Date migrationFromDate, Date migrationToDate);

	public Integer getTheCountOfLayoutColumn(Integer layoutId, Integer docId, Date migrationFromDate,
			Date migrationToDate);

	public List<LayoutReport> getTheLayoutReport(List<Integer> listOfLayoutIds, Date migrationFromDate,
			Date migrationToDate);

	public void restoreTheModuleData(StringBuilder query);

	public void restoreQueryHandQueryDTableData(MigrationDetails migrationDetails) throws Exception;

	public Boolean deleteSecurityDataForRestoringData(String[] securityTableDeleteSeq,
			List<MigratedRoleIdDetails> listOfRoleId);

	public Boolean deleteLayoutDataForRestoringData(String[] layoutTableDeleteSeq,
			List<MigratedLayoutIdDetails> listOfLayoutId);

	public List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleIdOrLayoutId);

	public List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveRoleIdOrLayoutId);

	public List<String> getTheListOfReportNamesWithExtension();

	public Boolean deleteTheReportFile(String destinationFileName);

	public List<String> getTheListOfAdhocDocumentsNames();

	public List<String> getTheListOfRuleNames();

	public boolean deleteTheRulesFile(String destinationFileName);

	public List<ConfigD> getTheAppConfigReport(Date migrationFromDate, Date migrationToDate);

	// public boolean saveAllTheDestinationTableDetails(Map<String, List<Object>>
	// insertQueryMap);

}
