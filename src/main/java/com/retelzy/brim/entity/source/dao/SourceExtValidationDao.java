package com.retelzy.brim.entity.source.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.source.ExtValidation;

@Repository
public interface SourceExtValidationDao extends CrudRepository<ExtValidation, Integer> {
	
	@Query("SELECT ev FROM ExtValidation ev WHERE ev.validationId IN (SELECT dv.propValue FROM DocViewElementD dv WHERE dv.id.layoutId = :layoutId AND dv.id.propName ='NEW_REF_ID') ORDER BY ev.validationId")
	public List<ExtValidation> getExtValidationInfo(Integer layoutId);

	@Query("SELECT ev from ExtValidation ev where ev.validationId in (:whereList) and modifyTs>=:fromDate and modifyTs<=:toDate")
	public List<Object> getTheDataOfExtValidation(List<Integer> whereList, Date fromDate, Date toDate);

}
