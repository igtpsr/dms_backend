package com.retelzy.brim.entity.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Table(name = "ROLE_PROFILE")
@Entity
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
public class RoleProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "ROLE_NAME", unique = true)
	private String roleName;

	@Column(name = "MODIFY_TS")
	private Date modifyTs;

	@Column(name = "MODIFY_USER")
	private String modifyUser;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "roleProfile", fetch = FetchType.LAZY)
	private List<UserProfile> listOfUserProfile = new ArrayList<UserProfile>();

}
