package com.retelzy.brim.entity.source;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
@Embeddable
public class SecBoLevelAclPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "ROLE_ID")
	private Integer roleId;
	
	@Column(name = "DOC_ID")
	private Integer docId;
	
	@Column(name = "LEVEL_ID")
	private Integer levelId;
	
	@Column(name = "PARENT_DOC_ID")
	private Integer parentDocId;
	
	@Column(name = "PARENT_LEVEL_ID")
	private Integer parentLevelId;

	@Column(name = "QUERY_ID")
	private Integer queryId;

	@Column(name = "RELATION_ID")
	private Integer relationId;

}
