package com.retelzy.brim.entity.source.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.dbunit.database.IDatabaseConnection;

import com.retelzy.brim.entity.source.AdhocDocuments;
import com.retelzy.brim.entity.source.ConfigD;
import com.retelzy.brim.entity.source.ExtValidation;
import com.retelzy.brim.entity.source.LayoutProfile;
import com.retelzy.brim.entity.source.QueryH;
import com.retelzy.brim.entity.source.SecRoleProfile;
import com.retelzy.brim.migration.model.MigrationId;

public interface SourceDbService {

	List<Integer> getAllTheRoleIdByRoleId(Integer roleId);

	List<Object> getTheDataByRoleId(Integer roleId, String className);

	List<Integer> getAllQueryIdByRoleId(Integer roleId, String className);

	List<QueryH> getallTheQueryDetails(List<Integer> listOfQueryId);

	List<Integer> getByIdProcessIdGreaterThanEqual(Integer processId);

	List<Object> getTheDetailsOfSecBoviewTable(List<Integer> listOfProcessId, Integer typeId);

	List<Object> getTheSmuResShowAttrDetails(List<Integer> listOfProcessId, Integer smuTypeId);

	List<Object> getAllTheSecRoleResCtlDetails(Integer minRoleId, Integer roleId, List<Integer> listOfProcessId,
			List<Integer> listOfRoleId);

	List<Object> getSmuCommonAllDetails(Integer menuItemId);

	List<Object> getSmuRoleRootMenuDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId);

	List<Object> getSmuRoleRootMenuItemDetails(Integer menuItemId, Integer minRoleId, Integer roleId,
			List<Integer> listOfRoleId);

	public IDatabaseConnection getConnection() throws Exception;

	List<Integer> getAllTheLayoutIdByLayoutId();

	List<Object> getLayoutProfileInfo(Integer layoutId);

	List<Object> getTheDataByLayoutId(Integer layoutId, String className, MigrationId migrationId);

	List<QueryH> getQueryInfo(Integer layoutId);

	List<ExtValidation> getExtValidationInfo(Integer layoutId);

	List<Object> getTheDataOfExtValidation(List<Integer> whereList, String className, MigrationId migrationId);

	Optional<Integer> getTheRoleIdByRoleId(Integer roleId);

	List<SecRoleProfile> getAllRoleIdExistInSecRoleProfileTable(List<Integer> listOfActiveRoleId);

	List<LayoutProfile> getAllLayoutIdExistInLayoutProfile(List<Integer> listOfActiveLayoutId);

	List<ConfigD> getTheConfigDTableDetails();
	
	Optional<Integer> getTheLayoutIdByLayoutId(Integer layoutId);

	List<AdhocDocuments> getTheAdhocDocumentsTableDetails(Date fromDate, Date toDate);

	List<AdhocDocuments> getTheAdhocDocumentsData(List<String> fileNames) throws SQLException;

	List<String> getTheListOfAdhocDocumentsNames();

	List<String> getTheListOfRuleNames();

	List<ConfigD> getTheRulesData(List<String> fileNames) throws SQLException;
	
	List<String> getPropName(List<String> fileNames) throws SQLException;

	List<String> getTheListOfReportNames();

	List<ConfigD> getTheConfigDTableDetails(Date fromDate, Date toDate);

	List<String> getTheListOfReportNamesWithExtension();

}
