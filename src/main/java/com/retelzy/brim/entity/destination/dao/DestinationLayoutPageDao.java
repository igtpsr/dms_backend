package com.retelzy.brim.entity.destination.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.retelzy.brim.entity.destination.LayoutPage;
import com.retelzy.brim.entity.destination.LayoutPagePK;

@Repository
public interface DestinationLayoutPageDao extends CrudRepository<LayoutPage, LayoutPagePK> {


}
