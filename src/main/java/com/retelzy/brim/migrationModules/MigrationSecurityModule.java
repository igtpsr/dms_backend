package com.retelzy.brim.migrationModules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.retelzy.brim.controller.MigrationController;
import com.retelzy.brim.cutomException.MigrationException;
import com.retelzy.brim.db.config.DestinationDBMultiRoutingDataSource;
import com.retelzy.brim.db.config.SourceDBMultiRoutingDataSource;
import com.retelzy.brim.db.util.MigrationDbUtil;
import com.retelzy.brim.entity.destination.CollabField;
import com.retelzy.brim.entity.destination.CollabFieldPK;
import com.retelzy.brim.entity.destination.DashSectionH;
import com.retelzy.brim.entity.destination.DashSectionHPK;
import com.retelzy.brim.entity.destination.RoleResAttr;
import com.retelzy.brim.entity.destination.RoleResAttrPK;
import com.retelzy.brim.entity.destination.SecBoFieldAcl;
import com.retelzy.brim.entity.destination.SecBoFieldAclPK;
import com.retelzy.brim.entity.destination.SecBoFilter;
import com.retelzy.brim.entity.destination.SecBoFilterPK;
import com.retelzy.brim.entity.destination.SecBoLevelAcl;
import com.retelzy.brim.entity.destination.SecBoLevelAclPK;
import com.retelzy.brim.entity.destination.SecBoProcess;
import com.retelzy.brim.entity.destination.SecBoProcessPK;
import com.retelzy.brim.entity.destination.SecBointFilter;
import com.retelzy.brim.entity.destination.SecBointFilterPK;
import com.retelzy.brim.entity.destination.SecBoviewRes;
import com.retelzy.brim.entity.destination.SecBoviewResPK;
import com.retelzy.brim.entity.destination.SecQuery;
import com.retelzy.brim.entity.destination.SecQueryGroup;
import com.retelzy.brim.entity.destination.SecQueryGroupPK;
import com.retelzy.brim.entity.destination.SecQueryPK;
import com.retelzy.brim.entity.destination.SecRoleProfile;
import com.retelzy.brim.entity.destination.SecRoleResCtl;
import com.retelzy.brim.entity.destination.SecRoleResCtlPK;
import com.retelzy.brim.entity.destination.SmuCommon;
import com.retelzy.brim.entity.destination.SmuResShowAttr;
import com.retelzy.brim.entity.destination.SmuResShowAttrPK;
import com.retelzy.brim.entity.destination.SmuRoleMenuItem;
import com.retelzy.brim.entity.destination.SmuRoleMenuItemPK;
import com.retelzy.brim.entity.destination.SmuRoleRootMenu;
import com.retelzy.brim.entity.destination.SmuRoleRootMenuPK;
import com.retelzy.brim.entity.destination.service.DestinationDbService;
import com.retelzy.brim.entity.source.CollabFields;
import com.retelzy.brim.entity.source.service.SourceDbService;
import com.retelzy.brim.util.ConfigFileReader;
import com.retelzy.brim.util.GoogleGuavaCache;

@Component
public class MigrationSecurityModule {

	@Autowired
	private SourceDbService sourceDbService;

	@Autowired
	private MigrationDbUtil migrationDbUtil;

	@Autowired
	private ConfigFileReader configFileReader;

	@Autowired
	private DestinationDbService destinationDbService;

	@Autowired
	private Environment env;

	@Autowired
	private GoogleGuavaCache cache;

	@Autowired
	private MigrationBackupForSecurityModule backupForSecurityModule;

	@Autowired
	private SimpMessagingTemplate template;

	private final static Logger logger = LoggerFactory.getLogger(MigrationSecurityModule.class);

	@Async
	public CompletableFuture<Map<String, Object>> migrateSecurityOption(String securityOption, Integer minRoleId,
			List<Integer> listOfRoleIdSentByUser) {

		CompletableFuture<Map<String, Object>> migrationStatus = new CompletableFuture<Map<String, Object>>();

		Map<String, Object> mapOfStatus = new LinkedHashMap<String, Object>();

		logger.info("migrate secuirty option method called");

		// get the list of security tables From INSERT sequence
		String[] securityTableInsertSeq = ConfigFileReader.toArray(configFileReader.getInsertSecurityTableSequence());

		// get the list of security tables From DELETE sequence
		String[] securityTableDeleteSeq = ConfigFileReader.toArray(configFileReader.getDeleteSecurityTableSequenc());
		FileWriter fw = null;
		PrintWriter pw = null;
		try {
			if (migrationDbUtil.compareBothProfileDetailsByModule("SECURITY") == false)
				throw new MigrationException("Something went wrong, Please try again later");
			
			if (securityOption.equals("ALL_SECURITY")) {

				List<Integer> listOfRoleId = sourceDbService.getAllTheRoleIdByRoleId(minRoleId);
				logger.info("List Of Role Id Available :" + listOfRoleId.size());

				ListIterator<Integer> listItr = listOfRoleId.listIterator();

				String fileName = getSQLFileName(securityOption);
				File file = new File(env.getProperty("dir.path.backup"), fileName);
				fw = new FileWriter(file, true);
				pw = new PrintWriter(fw);

				MigrationController.backupModuleFileName = fileName;

				Boolean backupStatus = backupForSecurityModule.getBackupSecurityInsertSql(listOfRoleId, pw, file);

				if (backupStatus == true) {
					template.convertAndSend("/queue/percentage/", 35);

					while (listItr.hasNext()) {

						Integer roleId = listItr.next();
						try {
							List<Map<String, List<Object>>> listOfInsertQuery = migrationDbUtil
									.generateInsertSQL(securityTableInsertSeq, roleId);

							boolean deleteStatus = destinationDbService.deleteSecurityData(securityTableDeleteSeq,
									roleId);

							logger.info("Is Delete Completed :" + deleteStatus);

							if (deleteStatus) {
								boolean insertStatus = insertTheSecurityData(listOfInsertQuery, roleId);
								if (insertStatus == true) {
									mapOfStatus.put("Security-" + roleId.toString(), "Success");
									logger.info("For RoleId " + roleId + " insertion is completed successfully");
								} else {
									logger.info("Insert Data From Destination Tables Failed");
									mapOfStatus.put("Security-" + roleId.toString(), "Failed");
								}
								try {
									insertQueryHandQueryDtableData(roleId);
								} catch (Exception e) {
									// e.printStackTrace();
									logger.error("Exception Occured For QueryH and QueryD Table :" + e.getMessage());
								}

							} else {
								logger.info("Delete Data From Destination Tables Failed");
								mapOfStatus.put("Security-" + roleId.toString(), "Failed");
							}
						} catch (Exception e) {
							mapOfStatus.put("Security-" + roleId.toString(), "Failed");
						}
					}
					logger.info("migrateSecurityOption() method while loop completed");
					template.convertAndSend("/queue/percentage/", 50);
				} else {
					file.deleteOnExit();
					throw new MigrationException("Backup Failed, migration terminated");
				}
			} else {

				File file = null;
				if (listOfRoleIdSentByUser.size() == 1) {
					String fileName = getSQLFileName(listOfRoleIdSentByUser.get(0).toString());
					file = new File(env.getProperty("dir.path.backup"), fileName);
					MigrationController.backupModuleFileName = fileName;
				} else {
					String fileName = getSQLFileName("MULTIPLE");
					file = new File(env.getProperty("dir.path.backup"), fileName);
					MigrationController.backupModuleFileName = fileName;
				}

				fw = new FileWriter(file, true);
				pw = new PrintWriter(fw);

				Boolean backupStatus = backupForSecurityModule.getBackupSecurityInsertSql(listOfRoleIdSentByUser, pw,
						file);

				if (backupStatus) {
					template.convertAndSend("/queue/percentage/", 35);
					listOfRoleIdSentByUser.forEach(roleId -> {
						try {
							Optional<Integer> isRoleIdDetailsAvailable = sourceDbService.getTheRoleIdByRoleId(roleId);

							if (isRoleIdDetailsAvailable.isPresent()) {

								List<Map<String, List<Object>>> listOfInsertQuery = migrationDbUtil
										.generateInsertSQL(securityTableInsertSeq, roleId);

								boolean deleteStatus = destinationDbService.deleteSecurityData(securityTableDeleteSeq,
										roleId);

								logger.info("Is Delete Completed :" + deleteStatus);

								if (deleteStatus) {
									boolean insertStatus = insertTheSecurityData(listOfInsertQuery, roleId);
									if (insertStatus == true) {
										mapOfStatus.put("Security-" + roleId.toString(), "Success");
										logger.info("For RoleId " + roleId + " insertion is completed successfully");
									} else {
										logger.info("Insert Data From Destination Tables Failed");
										mapOfStatus.put("Security-" + roleId.toString(), "Failed");
									}

									try {
										insertQueryHandQueryDtableData(roleId);
									} catch (Exception e) {
										// e.printStackTrace();
										logger.error(
												"Exception Occured For QueryH and QueryD Table :" + e.getMessage());
										logger.error("Exception Occured For Cause :" + e.getCause());
									}

								} else {
									logger.info("Delete Data From Destination Tables Failed");
									mapOfStatus.put("Security-" + roleId.toString(), "Failed");
								}

							} else {
								mapOfStatus.put("Security-" + isRoleIdDetailsAvailable.get().toString(),
										"Role Id Not exist");
							}
						} catch (Exception e) {
							mapOfStatus.put("Security-" + roleId.toString(), "Failed");
						}
					});
					template.convertAndSend("/queue/percentage/", 50);
				} else {
					file.deleteOnExit();
					throw new MigrationException("Backup Failed, migration terminated");
				}
			}

			mapOfStatus.put("Status", "Success");
			migrationStatus.complete(mapOfStatus);
			clearCacheDbEnvironmentType();
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
			return migrationStatus;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured On Security migration Method :" + e.getMessage());
			mapOfStatus.put("Status", "Failed");
			migrationStatus.complete(mapOfStatus);
			clearCacheDbEnvironmentType();
			MigrationController.isMigrationRunning = false;
			SourceDBMultiRoutingDataSource.sourceLookupCode = 0;
			DestinationDBMultiRoutingDataSource.destinationLookupCode = 0;
			return migrationStatus;
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void clearCacheDbEnvironmentType() {
		cache.clearToken("Source-DEV");
		cache.clearToken("Source-TEST");
		cache.clearToken("Source-QA");
		cache.clearToken("Source-STAGING");
		cache.clearToken("Source-PROD");
		cache.clearToken("Destination-DEV");
		cache.clearToken("Destination-TEST");
		cache.clearToken("Destination-QA");
		cache.clearToken("Destination-STAGING");
		cache.clearToken("Destination-PROD");
	}

	/**
	 * @param listOfInsertQuery
	 * @return
	 */
	private boolean insertTheSecurityData(List<Map<String, List<Object>>> listOfInsertQuery, Integer roleId) {

		logger.info("Inserty Security Query Method Called");
		Map<String, List<Object>> insertQueryMap = new LinkedHashMap<String, List<Object>>();

		Iterator<Map<String, List<Object>>> listIterator = listOfInsertQuery.listIterator();

		List<SecRoleProfile> listOfDestinationSecRoleProfile = new ArrayList<SecRoleProfile>();
		List<DashSectionH> listOfDestinationDashSectionH = new ArrayList<DashSectionH>();
		List<SecRoleResCtl> listOfDestionationSecRoleResCtl = new ArrayList<SecRoleResCtl>();
		List<SmuRoleRootMenu> listOfDestinationSmuRoleRootMenu = new ArrayList<SmuRoleRootMenu>();
		List<SmuRoleMenuItem> listOfDestinationSmuRoleMenuItem = new ArrayList<SmuRoleMenuItem>();
		List<CollabField> listOfDestinationCollabFields = new ArrayList<CollabField>();
		List<RoleResAttr> listOfDestinationRoleResAttr = new ArrayList<RoleResAttr>();
		List<SecQuery> listOfDestinationSecQuery = new ArrayList<SecQuery>();
		List<SecQueryGroup> listOfDestinationSecQueryGroups = new ArrayList<SecQueryGroup>();
		List<SecBoLevelAcl> listOfDestinationSecBoLevelAcls = new ArrayList<SecBoLevelAcl>();
		List<SecBoFieldAcl> listOfDestinationSecBoFieldAcls = new ArrayList<SecBoFieldAcl>();
		List<SecBoFilter> listOfDestinationSecBoFilters = new ArrayList<SecBoFilter>();
		List<SecBointFilter> listOfDestinationSecBointFilters = new ArrayList<SecBointFilter>();
		List<SecBoFilter> listOfDestinationSecurityBoFilters = new ArrayList<SecBoFilter>();
		List<SecBointFilter> listOfDestinationSecurityBointFilters = new ArrayList<SecBointFilter>();
		List<SecBoFieldAcl> listOfDestinationSecuritySecBoFieldAcls = new ArrayList<SecBoFieldAcl>();
		List<SecBoLevelAcl> listOfDestinationSecuritySecBoLevelAcls = new ArrayList<SecBoLevelAcl>();
		List<SecBoProcess> listOfDestinationCustProcSecBoProcess = new ArrayList<SecBoProcess>();
		List<SecBoviewRes> listOfDestinationCustomProcSecBoviewRes = new ArrayList<SecBoviewRes>();
		List<SmuResShowAttr> listOfDestinationCustProcSmuResShowAttr = new ArrayList<SmuResShowAttr>();
		List<SecRoleResCtl> listOfDestinationCustProcSecRoleResCtl = new ArrayList<SecRoleResCtl>();
		List<SmuCommon> listOfDestinationCustMenuSmuCommon = new ArrayList<SmuCommon>();
		List<SmuRoleRootMenu> listOfDestinationCustMenuSmuRoleRootMenu = new ArrayList<SmuRoleRootMenu>();
		List<SmuRoleMenuItem> listOfDestinationCustMenuSmuRoleMenuItem = new ArrayList<SmuRoleMenuItem>();

		while (listIterator.hasNext()) {

			Map<String, List<Object>> mapOfObject = listIterator.next();

			for (Entry<String, List<Object>> entry : mapOfObject.entrySet()) {

				switch (entry.getKey()) {

				case "SecRoleProfile":
					if (!entry.getValue().isEmpty()) {

						List<com.retelzy.brim.entity.source.SecRoleProfile> listOfSourceSecRoleProfile = entry
								.getValue().parallelStream()
								.map(element -> (com.retelzy.brim.entity.source.SecRoleProfile) element)
								.collect(Collectors.toList());

						for (Integer index = 0; index < listOfSourceSecRoleProfile.size(); index++) {

							SecRoleProfile destinationSecRoleProfile = new SecRoleProfile();
							destinationSecRoleProfile
									.setI18nResId(listOfSourceSecRoleProfile.get(index).getI18nResId());
							destinationSecRoleProfile.setModifyTs(listOfSourceSecRoleProfile.get(index).getModifyTs());
							destinationSecRoleProfile
									.setModifyUser(listOfSourceSecRoleProfile.get(index).getModifyUser());

							destinationSecRoleProfile.setRoleId(listOfSourceSecRoleProfile.get(index).getRoleId());

							destinationSecRoleProfile.setRoleName(listOfSourceSecRoleProfile.get(index).getRoleName());

							listOfDestinationSecRoleProfile.add(destinationSecRoleProfile);
						}

						if (!listOfDestinationSecRoleProfile.isEmpty()) {

							/*
							 * destinationDbService.saveAllSecRoleProfileDetails(
							 * listOfDestinationSecRoleProfile);
							 * logger.info("SecRoleProfile Details Saved In DB");
							 */

							List<Object> listOfSecRoleProfileAsObject = listOfDestinationSecRoleProfile.parallelStream()
									.map(element -> (Object) element).collect(Collectors.toList());
							insertQueryMap.put("SecRoleProfile", listOfSecRoleProfileAsObject);

						}

					}

					break;

				case "DashSectionH":

					List<com.retelzy.brim.entity.source.DashSectionH> listOfSourceDashSectionH = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.DashSectionH) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceDashSectionH.size(); index++) {

						DashSectionH dashSectionH = new DashSectionH();
						dashSectionH.setAcl(listOfSourceDashSectionH.get(index).getAcl());
						dashSectionH.setBackground(listOfSourceDashSectionH.get(index).getBackground());
						dashSectionH.setColPos(listOfSourceDashSectionH.get(index).getColPos());
						dashSectionH.setGroupId(listOfSourceDashSectionH.get(index).getGroupId());
						dashSectionH.setHeight(listOfSourceDashSectionH.get(index).getHeight());
						dashSectionH.setI18nResId(listOfSourceDashSectionH.get(index).getI18nResId());
						dashSectionH.setModifyTs(listOfSourceDashSectionH.get(index).getModifyTs());
						dashSectionH.setModifyUser(listOfSourceDashSectionH.get(index).getModifyUser());
						dashSectionH.setRowPos(listOfSourceDashSectionH.get(index).getRowPos());
						dashSectionH.setSectionClass(listOfSourceDashSectionH.get(index).getSectionClass());
						dashSectionH.setSectionType(listOfSourceDashSectionH.get(index).getSectionType());
						dashSectionH.setWidth(listOfSourceDashSectionH.get(index).getWidth());

						DashSectionHPK dashSectionHPK = new DashSectionHPK();
						dashSectionHPK.setRoleId(listOfSourceDashSectionH.get(index).getId().getRoleId());
						dashSectionHPK.setSectionId(listOfSourceDashSectionH.get(index).getId().getSectionId());

						dashSectionH.setId(dashSectionHPK);

						listOfDestinationDashSectionH.add(dashSectionH);
					}

					if (!listOfDestinationDashSectionH.isEmpty()) {

						List<Object> listOfDashSectionAsObject = listOfDestinationDashSectionH.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("DashSectionH", listOfDashSectionAsObject);

					} else {
						logger.info("DashSectionH Table Don't have data to insert");
					}

					break;

				case "SecRoleResCtl":

					List<com.retelzy.brim.entity.source.SecRoleResCtl> listOfSourceSecRoleResCtl = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecRoleResCtl) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecRoleResCtl.size(); index++) {

						SecRoleResCtl destionSecRoleResCtl = new SecRoleResCtl();
						destionSecRoleResCtl.setEnabled(listOfSourceSecRoleResCtl.get(index).getEnabled());
						destionSecRoleResCtl.setModifyTs(listOfSourceSecRoleResCtl.get(index).getModifyTs());
						destionSecRoleResCtl.setModifyUser(listOfSourceSecRoleResCtl.get(index).getModifyUser());

						SecRoleResCtlPK secRoleResCtlPK = new SecRoleResCtlPK();
						secRoleResCtlPK.setDocId(listOfSourceSecRoleResCtl.get(index).getId().getDocId());
						secRoleResCtlPK.setParentDocId(listOfSourceSecRoleResCtl.get(index).getId().getParentDocId());
						secRoleResCtlPK
								.setParentLevelId(listOfSourceSecRoleResCtl.get(index).getId().getParentLevelId());
						secRoleResCtlPK.setRelationId(listOfSourceSecRoleResCtl.get(index).getId().getRelationId());
						secRoleResCtlPK.setRoleId(listOfSourceSecRoleResCtl.get(index).getId().getRoleId());
						secRoleResCtlPK.setSmuResId(listOfSourceSecRoleResCtl.get(index).getId().getSmuResId());
						secRoleResCtlPK.setSmuTypeId(listOfSourceSecRoleResCtl.get(index).getId().getSmuTypeId());

						destionSecRoleResCtl.setId(secRoleResCtlPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecRoleResCtl.get(index).
						 * getSecRoleProfile().getRoleId());
						 * destionSecRoleResCtl.setSecRoleProfile(secRoleProfile);
						 */

						listOfDestionationSecRoleResCtl.add(destionSecRoleResCtl);

					}

					if (!listOfDestionationSecRoleResCtl.isEmpty()) {

						List<Object> listOfSecRoleResCtlAsObject = listOfDestionationSecRoleResCtl.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("SecRoleResCtl", listOfSecRoleResCtlAsObject);

					} else {
						logger.info("SecRoleResCtl Table Don't have data to insert");
					}

					break;

				case "SmuRoleRootMenu":
					List<com.retelzy.brim.entity.source.SmuRoleRootMenu> listOfSourceSmuRoleRootMenu = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SmuRoleRootMenu) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSmuRoleRootMenu.size(); index++) {

						SmuRoleRootMenu destinationSmuRoleRootMenu = new SmuRoleRootMenu();
						destinationSmuRoleRootMenu.setGroupId(listOfSourceSmuRoleRootMenu.get(index).getGroupId());
						destinationSmuRoleRootMenu.setMenuPos(listOfSourceSmuRoleRootMenu.get(index).getMenuPos());
						destinationSmuRoleRootMenu.setModifyTs(listOfSourceSmuRoleRootMenu.get(index).getModifyTs());
						destinationSmuRoleRootMenu
								.setModifyUser(listOfSourceSmuRoleRootMenu.get(index).getModifyUser());
						destinationSmuRoleRootMenu.setSortType(listOfSourceSmuRoleRootMenu.get(index).getSortType());

						SmuRoleRootMenuPK smuRoleRootMenuPK = new SmuRoleRootMenuPK();
						smuRoleRootMenuPK.setDocViewId(listOfSourceSmuRoleRootMenu.get(index).getId().getDocViewId());
						smuRoleRootMenuPK
								.setDocViewRelId(listOfSourceSmuRoleRootMenu.get(index).getId().getDocViewRelId());
						smuRoleRootMenuPK.setMenuItemId(listOfSourceSmuRoleRootMenu.get(index).getId().getMenuItemId());
						smuRoleRootMenuPK
								.setMenuItemType(listOfSourceSmuRoleRootMenu.get(index).getId().getMenuItemType());
						smuRoleRootMenuPK.setRoleId(listOfSourceSmuRoleRootMenu.get(index).getId().getRoleId());

						destinationSmuRoleRootMenu.setId(smuRoleRootMenuPK);

						listOfDestinationSmuRoleRootMenu.add(destinationSmuRoleRootMenu);

						if (!listOfDestinationSmuRoleRootMenu.isEmpty()) {
							List<Object> listOfSmuRoleRootMenuAsObject = listOfDestinationSmuRoleRootMenu
									.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

							insertQueryMap.put("SmuRoleRootMenu", listOfSmuRoleRootMenuAsObject);

						} else {
							logger.info("SmuRoleRootMenu Table Don't have data to Insert");
						}

					}

					break;

				case "SmuRoleMenuItem":

					List<com.retelzy.brim.entity.source.SmuRoleMenuItem> listOfSourceSmuRoleMenuItem = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SmuRoleMenuItem) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSmuRoleMenuItem.size(); index++) {

						SmuRoleMenuItem destinationSmuRoleMenuItem = new SmuRoleMenuItem();
						destinationSmuRoleMenuItem.setEnabled(listOfSourceSmuRoleMenuItem.get(index).getEnabled());
						destinationSmuRoleMenuItem.setMenuPos(listOfSourceSmuRoleMenuItem.get(index).getMenuPos());
						destinationSmuRoleMenuItem.setModifyTs(listOfSourceSmuRoleMenuItem.get(index).getModifyTs());
						destinationSmuRoleMenuItem
								.setModifyUser(listOfSourceSmuRoleMenuItem.get(index).getModifyUser());

						SmuRoleMenuItemPK smuRoleMenuItemPK = new SmuRoleMenuItemPK();
						smuRoleMenuItemPK.setDocViewId(listOfSourceSmuRoleMenuItem.get(index).getId().getDocViewId());
						smuRoleMenuItemPK.setMenuItemId(listOfSourceSmuRoleMenuItem.get(index).getId().getMenuItemId());
						smuRoleMenuItemPK.setRoleId(listOfSourceSmuRoleMenuItem.get(index).getId().getRoleId());

						destinationSmuRoleMenuItem.setId(smuRoleMenuItemPK);

						listOfDestinationSmuRoleMenuItem.add(destinationSmuRoleMenuItem);

					}

					if (!listOfDestinationSmuRoleMenuItem.isEmpty()) {

						List<Object> listOfSmuRoleMenuItemAsObject = listOfDestinationSmuRoleMenuItem.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("SmuRoleMenuItem", listOfSmuRoleMenuItemAsObject);

					} else {
						logger.info("SmuRoleMenuItem Table Don't have data to insert");
					}

					break;

				case "CollabFields":

					List<CollabFields> listOfSourceCollabFields = entry.getValue().parallelStream()
							.map(element -> (CollabFields) element).collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceCollabFields.size(); index++) {

						CollabField destinationCollabField = new CollabField();
						destinationCollabField.setModifyTs(listOfSourceCollabFields.get(index).getModifyTs());
						destinationCollabField.setModifyUser(listOfSourceCollabFields.get(index).getModifyUser());
						destinationCollabField.setType(listOfSourceCollabFields.get(index).getType());

						CollabFieldPK collabFieldPK = new CollabFieldPK();
						collabFieldPK.setDocId(listOfSourceCollabFields.get(index).getId().getDocId());
						collabFieldPK.setFieldId(listOfSourceCollabFields.get(index).getId().getFieldId());
						collabFieldPK.setLevelId(listOfSourceCollabFields.get(index).getId().getLevelId());
						collabFieldPK.setRoleId(listOfSourceCollabFields.get(index).getId().getRoleId());

						destinationCollabField.setId(collabFieldPK);

						listOfDestinationCollabFields.add(destinationCollabField);
					}

					if (!listOfDestinationCollabFields.isEmpty()) {

						List<Object> listOfCollabFieldsAsObject = listOfDestinationCollabFields.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("CollabFields", listOfCollabFieldsAsObject);
					} else {
						logger.info("CollabFields Table Don't have to insert");
					}

					break;

				case "RoleResAttr":
					List<com.retelzy.brim.entity.source.RoleResAttr> listOfSourseRoleResAttr = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.RoleResAttr) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourseRoleResAttr.size(); index++) {

						RoleResAttr destinationRoleResAttr = new RoleResAttr();
						destinationRoleResAttr.setModifyTs(listOfSourseRoleResAttr.get(index).getModifyTs());
						destinationRoleResAttr.setModifyUser(listOfSourseRoleResAttr.get(index).getModifyUser());
						destinationRoleResAttr.setPropValue(listOfSourseRoleResAttr.get(index).getPropValue());

						RoleResAttrPK roleResAttrPK = new RoleResAttrPK();
						roleResAttrPK.setDocViewId(listOfSourseRoleResAttr.get(index).getId().getDocViewId());
						roleResAttrPK.setPropName(listOfSourseRoleResAttr.get(index).getId().getPropName());
						roleResAttrPK.setRelationId(listOfSourseRoleResAttr.get(index).getId().getRelationId());
						roleResAttrPK.setRoleId(listOfSourseRoleResAttr.get(index).getId().getRoleId());
						roleResAttrPK.setSmuResId(listOfSourseRoleResAttr.get(index).getId().getSmuResId());
						roleResAttrPK.setSmuTypeId(listOfSourseRoleResAttr.get(index).getId().getSmuTypeId());

						destinationRoleResAttr.setId(roleResAttrPK);

						listOfDestinationRoleResAttr.add(destinationRoleResAttr);

					}

					if (!listOfDestinationRoleResAttr.isEmpty()) {

						List<Object> listOfRoleResAttarAsObject = listOfDestinationRoleResAttr.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("RoleResAttr", listOfRoleResAttarAsObject);

					} else {
						logger.info("RoleResAttr Table don't data to insert");
					}

					break;

				case "SecQuery":

					List<com.retelzy.brim.entity.source.SecQuery> listOfSourceSecQuery = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecQuery) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecQuery.size(); index++) {

						SecQuery destinationSecQuery = new SecQuery();
						destinationSecQuery.setModifyTs(listOfSourceSecQuery.get(index).getModifyTs());
						destinationSecQuery.setModifyUser(listOfSourceSecQuery.get(index).getModifyUser());

						SecQueryPK secQueryPK = new SecQueryPK();
						secQueryPK.setQueryId(listOfSourceSecQuery.get(index).getId().getQueryId());
						secQueryPK.setRoleId(listOfSourceSecQuery.get(index).getId().getRoleId());

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecQuery.get(index).getId().getRoleId())
						 * ; destinationSecQuery.setSecRoleProfile(secRoleProfile);
						 */

						destinationSecQuery.setId(secQueryPK);

						listOfDestinationSecQuery.add(destinationSecQuery);
					}

					if (!listOfDestinationSecQuery.isEmpty()) {

						List<Object> listOfSecQuerieAsObject = listOfDestinationSecQuery.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("SecQuery", listOfSecQuerieAsObject);

					} else {
						logger.info("SecQuery Table Don't have data to insert");
					}

					break;

				case "SecQueryGroup":

					List<com.retelzy.brim.entity.source.SecQueryGroup> listOfSourceSecQueryGroup = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecQueryGroup) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecQueryGroup.size(); index++) {

						SecQueryGroup destinationSecQueryGroup = new SecQueryGroup();
						destinationSecQueryGroup.setModifyTs(listOfSourceSecQueryGroup.get(index).getModifyTs());
						destinationSecQueryGroup.setModifyUser(listOfSourceSecQueryGroup.get(index).getModifyUser());
						destinationSecQueryGroup.setPos(listOfSourceSecQueryGroup.get(index).getPos());

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecQueryGroup.get(index).getId().
						 * getRoleId()); destinationSecQueryGroup.setSecRoleProfile(secRoleProfile);
						 */

						SecQueryGroupPK secQueryGroupPK = new SecQueryGroupPK();
						secQueryGroupPK.setQueryGroupId(listOfSourceSecQueryGroup.get(index).getId().getQueryGroupId());
						secQueryGroupPK.setRoleId(listOfSourceSecQueryGroup.get(index).getId().getRoleId());

						destinationSecQueryGroup.setId(secQueryGroupPK);

						listOfDestinationSecQueryGroups.add(destinationSecQueryGroup);

					}

					if (!listOfDestinationSecQueryGroups.isEmpty()) {

						List<Object> listOfSecQueryGroupAsObject = listOfDestinationSecQueryGroups.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("SecQueryGroup", listOfSecQueryGroupAsObject);

					} else {
						logger.info("SecQueryGroup Table Don't have data to insert");
					}

					break;

				case "SecBoLevelAcl":

					List<com.retelzy.brim.entity.source.SecBoLevelAcl> listOfSourceSecBoLevelAcl = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecBoLevelAcl) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecBoLevelAcl.size(); index++) {

						SecBoLevelAcl destinationSecBoLevelAcl = new SecBoLevelAcl();
						destinationSecBoLevelAcl.setAcl(listOfSourceSecBoLevelAcl.get(index).getAcl());
						destinationSecBoLevelAcl.setModifyTs(listOfSourceSecBoLevelAcl.get(index).getModifyTs());
						destinationSecBoLevelAcl.setModifyUser(listOfSourceSecBoLevelAcl.get(index).getModifyUser());

						SecBoLevelAclPK secBoLevelAclPK = new SecBoLevelAclPK();
						secBoLevelAclPK.setRoleId(listOfSourceSecBoLevelAcl.get(index).getId().getRoleId());
						secBoLevelAclPK.setDocId(listOfSourceSecBoLevelAcl.get(index).getId().getDocId());
						secBoLevelAclPK.setLevelId(listOfSourceSecBoLevelAcl.get(index).getId().getLevelId());
						secBoLevelAclPK.setParentDocId(listOfSourceSecBoLevelAcl.get(index).getId().getParentDocId());
						secBoLevelAclPK
								.setParentLevelId(listOfSourceSecBoLevelAcl.get(index).getId().getParentLevelId());
						secBoLevelAclPK.setQueryId(listOfSourceSecBoLevelAcl.get(index).getId().getQueryId());
						secBoLevelAclPK.setRelationId(listOfSourceSecBoLevelAcl.get(index).getId().getRelationId());

						destinationSecBoLevelAcl.setId(secBoLevelAclPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecBoLevelAcl.get(index).getId().
						 * getRoleId()); destinationSecBoLevelAcl.setSecRoleProfile(secRoleProfile);
						 */

						listOfDestinationSecBoLevelAcls.add(destinationSecBoLevelAcl);

					}

					if (!listOfDestinationSecBoLevelAcls.isEmpty()) {
						List<Object> listOfSecBolevelAclAsObject = listOfDestinationSecBoLevelAcls.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecBoLevelAcl", listOfSecBolevelAclAsObject);

					} else {
						logger.info("SecBoLevelAcl Table Don't have data to Insert");
					}

					break;

				case "SecBoFieldAcl":

					List<com.retelzy.brim.entity.source.SecBoFieldAcl> listOfSourceSecBoFieldAcl = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecBoFieldAcl) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecBoFieldAcl.size(); index++) {

						SecBoFieldAcl destinationSecBoFieldAcl = new SecBoFieldAcl();
						destinationSecBoFieldAcl.setAcl(listOfSourceSecBoFieldAcl.get(index).getAcl());
						destinationSecBoFieldAcl.setModifyTs(listOfSourceSecBoFieldAcl.get(index).getModifyTs());
						destinationSecBoFieldAcl.setModifyUser(listOfSourceSecBoFieldAcl.get(index).getModifyUser());

						SecBoFieldAclPK secBoFieldAclPK = new SecBoFieldAclPK();
						secBoFieldAclPK.setDocId(listOfSourceSecBoFieldAcl.get(index).getId().getDocId());
						secBoFieldAclPK.setQueryId(listOfSourceSecBoFieldAcl.get(index).getId().getQueryId());
						secBoFieldAclPK.setRoleId(listOfSourceSecBoFieldAcl.get(index).getId().getRoleId());
						destinationSecBoFieldAcl.setId(secBoFieldAclPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecBoFieldAcl.get(index).
						 * getSecRoleProfile().getRoleId());
						 * destinationSecBoFieldAcl.setSecRoleProfile(secRoleProfile);
						 */

						listOfDestinationSecBoFieldAcls.add(destinationSecBoFieldAcl);

					}

					if (!listOfDestinationSecBoFieldAcls.isEmpty()) {

						List<Object> listOfSecBoFieldAclAsObject = listOfDestinationSecBoFieldAcls.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecBoFieldAcl", listOfSecBoFieldAclAsObject);

					} else {
						logger.info("SecBoFieldAcl Table Don't have data to insert");
					}

					break;

				case "SecBoFilter":

					List<com.retelzy.brim.entity.source.SecBoFilter> listOfSourceSecBoFilter = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecBoFilter) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecBoFilter.size(); index++) {

						SecBoFilter destinationSecBoFilter = new SecBoFilter();
						destinationSecBoFilter.setModifyTs(listOfSourceSecBoFilter.get(index).getModifyTs());
						destinationSecBoFilter.setModifyUser(listOfSourceSecBoFilter.get(index).getModifyUser());

						SecBoFilterPK secBoFilterPK = new SecBoFilterPK();
						secBoFilterPK.setDocId(listOfSourceSecBoFilter.get(index).getId().getDocId());
						secBoFilterPK.setQueryId(listOfSourceSecBoFilter.get(index).getId().getQueryId());
						secBoFilterPK.setRoleId(listOfSourceSecBoFilter.get(index).getId().getRoleId());

						destinationSecBoFilter.setId(secBoFilterPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecBoFilter.get(index).getId().getRoleId
						 * ()); destinationSecBoFilter.setSecRoleProfile(secRoleProfile);
						 */

						listOfDestinationSecBoFilters.add(destinationSecBoFilter);
					}

					if (!listOfDestinationSecBoFilters.isEmpty()) {

						List<Object> listOfSecBoFiltersAsObject = listOfDestinationSecBoFilters.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecBoFilter", listOfSecBoFiltersAsObject);

					} else {
						logger.info("SecBoFilter Table Don't have to insert");
					}

					break;

				case "SecBointFilter":

					List<com.retelzy.brim.entity.source.SecBointFilter> listOfSourceSecBointFilter = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecBointFilter) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceSecBointFilter.size(); index++) {

						SecBointFilter destinationSecBointFilter = new SecBointFilter();
						destinationSecBointFilter.setModifyTs(listOfSourceSecBointFilter.get(index).getModifyTs());
						destinationSecBointFilter.setModifyUser(listOfSourceSecBointFilter.get(index).getModifyUser());

						SecBointFilterPK secBointFilterPK = new SecBointFilterPK();
						secBointFilterPK.setDocId(listOfSourceSecBointFilter.get(index).getId().getDocId());
						secBointFilterPK.setLevelId(listOfSourceSecBointFilter.get(index).getId().getLevelId());
						secBointFilterPK.setParentDocId(listOfSourceSecBointFilter.get(index).getId().getParentDocId());
						secBointFilterPK
								.setParentLevelId(listOfSourceSecBointFilter.get(index).getId().getParentLevelId());
						secBointFilterPK.setQueryId(listOfSourceSecBointFilter.get(index).getId().getQueryId());
						secBointFilterPK.setRelationId(listOfSourceSecBointFilter.get(index).getId().getRelationId());
						secBointFilterPK.setRoleId(listOfSourceSecBointFilter.get(index).getId().getRoleId());

						destinationSecBointFilter.setId(secBointFilterPK);
						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecBointFilter.get(index).getId().
						 * getRoleId()); destinationSecBointFilter.setSecRoleProfile(secRoleProfile);
						 */

						listOfDestinationSecBointFilters.add(destinationSecBointFilter);

					}

					if (!listOfDestinationSecBointFilters.isEmpty()) {

						List<Object> listOfSecBointFilterAsObject = listOfDestinationSecBointFilters.parallelStream()
								.map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecBointFilter", listOfSecBointFilterAsObject);

					} else {
						logger.info("SecBointFilters Table Don't have data to insert");
					}

					break;

				case "SecuritySecBoFilter":

					List<com.retelzy.brim.entity.source.SecBoFilter> listOfSourceSecuritySecBoFilter = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SecBoFilter) element)
							.collect(Collectors.toList());

					/* This is Set type SecBoFilter data */
					Set<SecBoFilter> setOfSecuritySecBoFilter = listOfDestinationSecBoFilters.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceSecuritySecBoFilter.size(); index++) {

						SecBoFilter destinationSecBoFilter = new SecBoFilter();
						destinationSecBoFilter.setModifyTs(listOfSourceSecuritySecBoFilter.get(index).getModifyTs());
						destinationSecBoFilter
								.setModifyUser(listOfSourceSecuritySecBoFilter.get(index).getModifyUser());

						SecBoFilterPK secBoFilterPK = new SecBoFilterPK();
						secBoFilterPK.setDocId(listOfSourceSecuritySecBoFilter.get(index).getId().getDocId());
						secBoFilterPK.setQueryId(listOfSourceSecuritySecBoFilter.get(index).getId().getQueryId());
						secBoFilterPK.setRoleId(listOfSourceSecuritySecBoFilter.get(index).getId().getRoleId());

						destinationSecBoFilter.setId(secBoFilterPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecuritySecBoFilter.get(index).getId().
						 * getRoleId()); destinationSecBoFilter.setSecRoleProfile(secRoleProfile);
						 */

						if (!setOfSecuritySecBoFilter.contains(destinationSecBoFilter))
							setOfSecuritySecBoFilter.add(destinationSecBoFilter);

						else {
							logger.info("Duplcate Object Found For SecuritySecBoFilter Table");
							continue;
						}
					}
					setOfSecuritySecBoFilter = null;

					if (!listOfDestinationSecurityBoFilters.isEmpty()) {

						List<Object> listOfSecuritySecBoFilterAsObject = listOfDestinationSecurityBoFilters
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecuritySecBoFilter", listOfSecuritySecBoFilterAsObject);

					} else {
						logger.info("SecuritySecBoFilter Table Don't have data to insert");
					}

					break;

				case "SecuritySecBointFilter":

					List<com.retelzy.brim.entity.source.SecBointFilter> listOfSourceSecuritySecBointFilter = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecBointFilter) element)
							.collect(Collectors.toList());

					/* This is the Set type SecBointFilter */
					Set<SecBointFilter> setOfSecBointFilter = listOfDestinationSecBointFilters.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceSecuritySecBointFilter.size(); index++) {

						SecBointFilter destinationSecBointFilter = new SecBointFilter();
						destinationSecBointFilter
								.setModifyTs(listOfSourceSecuritySecBointFilter.get(index).getModifyTs());
						destinationSecBointFilter
								.setModifyUser(listOfSourceSecuritySecBointFilter.get(index).getModifyUser());

						SecBointFilterPK secBointFilterPK = new SecBointFilterPK();
						secBointFilterPK.setDocId(listOfSourceSecuritySecBointFilter.get(index).getId().getDocId());
						secBointFilterPK.setLevelId(listOfSourceSecuritySecBointFilter.get(index).getId().getLevelId());
						secBointFilterPK
								.setParentDocId(listOfSourceSecuritySecBointFilter.get(index).getId().getParentDocId());
						secBointFilterPK.setParentLevelId(
								listOfSourceSecuritySecBointFilter.get(index).getId().getParentLevelId());
						secBointFilterPK.setQueryId(listOfSourceSecuritySecBointFilter.get(index).getId().getQueryId());
						secBointFilterPK
								.setRelationId(listOfSourceSecuritySecBointFilter.get(index).getId().getRelationId());
						secBointFilterPK.setRoleId(listOfSourceSecuritySecBointFilter.get(index).getId().getRoleId());

						destinationSecBointFilter.setId(secBointFilterPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceSecuritySecBointFilter.get(index).getId(
						 * ).getRoleId()); destinationSecBointFilter.setSecRoleProfile(secRoleProfile);
						 */

						if (!setOfSecBointFilter.contains(destinationSecBointFilter))
							listOfDestinationSecurityBointFilters.add(destinationSecBointFilter);
						else {
							logger.info("Duplcate Object Found For SecurityBointFilter Table");
							continue;
						}

					}

					if (!listOfDestinationSecurityBointFilters.isEmpty()) {

						List<Object> listOfSecuritySecBointFilterAsObject = listOfDestinationSecurityBointFilters
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecuritySecBointFilter", listOfSecuritySecBointFilterAsObject);

					} else {
						logger.info("SecuritySecBointFilter Table Don't have data to insert");
					}

					break;

				case "SecuritySecBoFieldAcl":

					List<com.retelzy.brim.entity.source.SecBoFieldAcl> listOfSourceSecuritySecBoFieldAcl = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecBoFieldAcl) element)
							.collect(Collectors.toList());

					/* This is the Set type of SecBoFieldAcl */
					Set<SecBoFieldAcl> setOfSecBoFieldAcl = listOfDestinationSecBoFieldAcls.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceSecuritySecBoFieldAcl.size(); index++) {

						SecBoFieldAcl destinationSecBoFieldAcl = new SecBoFieldAcl();
						destinationSecBoFieldAcl.setAcl(listOfSourceSecuritySecBoFieldAcl.get(index).getAcl());
						destinationSecBoFieldAcl
								.setModifyTs(listOfSourceSecuritySecBoFieldAcl.get(index).getModifyTs());
						destinationSecBoFieldAcl
								.setModifyUser(listOfSourceSecuritySecBoFieldAcl.get(index).getModifyUser());

						SecBoFieldAclPK secBoFieldAclPK = new SecBoFieldAclPK();
						secBoFieldAclPK.setDocId(listOfSourceSecuritySecBoFieldAcl.get(index).getId().getDocId());
						secBoFieldAclPK.setQueryId(listOfSourceSecuritySecBoFieldAcl.get(index).getId().getQueryId());
						secBoFieldAclPK.setRoleId(listOfSourceSecuritySecBoFieldAcl.get(index).getId().getRoleId());

						destinationSecBoFieldAcl.setId(secBoFieldAclPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(
						 * listOfSourceSecuritySecBoFieldAcl.get(index).getSecRoleProfile().getRoleId())
						 * ; destinationSecBoFieldAcl.setSecRoleProfile(secRoleProfile);
						 */

						if (!setOfSecBoFieldAcl.contains(destinationSecBoFieldAcl))
							listOfDestinationSecuritySecBoFieldAcls.add(destinationSecBoFieldAcl);
						else {
							logger.info("Duplicate Object Found for SecuritySecBoFieldAcl");
							continue;
						}

					}

					if (!listOfDestinationSecuritySecBoFieldAcls.isEmpty()) {

						List<Object> listOfSecBoFieldAclAsObject = listOfDestinationSecuritySecBoFieldAcls
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecuritySecBoFieldAcl", listOfSecBoFieldAclAsObject);

					} else {
						logger.info("SecuritySecBoFieldAcl Table Don't have data to insert");
					}

					break;

				case "SecuritySecBoLevelAcl":

					List<com.retelzy.brim.entity.source.SecBoLevelAcl> listOfSourceSecuritySecBoLevelAcl = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecBoLevelAcl) element)
							.collect(Collectors.toList());

					/* This is Set type of SecBoLevelAcl */
					Set<SecBoLevelAcl> setSecBoLevelAcl = listOfDestinationSecBoLevelAcls.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceSecuritySecBoLevelAcl.size(); index++) {

						SecBoLevelAcl destinationSecBoLevelAcl = new SecBoLevelAcl();
						destinationSecBoLevelAcl.setAcl(listOfSourceSecuritySecBoLevelAcl.get(index).getAcl());
						destinationSecBoLevelAcl
								.setModifyTs(listOfSourceSecuritySecBoLevelAcl.get(index).getModifyTs());
						destinationSecBoLevelAcl
								.setModifyUser(listOfSourceSecuritySecBoLevelAcl.get(index).getModifyUser());

						SecBoLevelAclPK secBoLevelAclPK = new SecBoLevelAclPK();
						secBoLevelAclPK.setRoleId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getRoleId());
						secBoLevelAclPK.setDocId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getDocId());
						secBoLevelAclPK.setLevelId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getLevelId());
						secBoLevelAclPK
								.setParentDocId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getParentDocId());
						secBoLevelAclPK.setParentLevelId(
								listOfSourceSecuritySecBoLevelAcl.get(index).getId().getParentLevelId());
						secBoLevelAclPK.setQueryId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getQueryId());
						secBoLevelAclPK
								.setRelationId(listOfSourceSecuritySecBoLevelAcl.get(index).getId().getRelationId());

						destinationSecBoLevelAcl.setId(secBoLevelAclPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(
						 * listOfSourceSecuritySecBoLevelAcl.get(index).getSecRoleProfile().getRoleId())
						 * ; destinationSecBoLevelAcl.setSecRoleProfile(secRoleProfile);
						 */

						if (!setSecBoLevelAcl.contains(destinationSecBoLevelAcl))
							listOfDestinationSecuritySecBoLevelAcls.add(destinationSecBoLevelAcl);
						else {
							logger.info("Duplicate Object Found for SecuritySecBoLevelAcl");
							continue;
						}

					}

					if (!listOfDestinationSecuritySecBoLevelAcls.isEmpty()) {
						List<Object> listOfSecuirtySecBolevelAclAsObject = listOfDestinationSecuritySecBoLevelAcls
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("SecuritySecBoLevelAcl", listOfSecuirtySecBolevelAclAsObject);

					} else {
						logger.info("SecuritySecBoLevelAcl Table Don't have data to Insert");
					}

					break;

				case "CustomProcessSecBoProcess":

					List<com.retelzy.brim.entity.source.SecBoProcess> listOfSourceCustomProcessSecBoProcess = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecBoProcess) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceCustomProcessSecBoProcess.size(); index++) {

						SecBoProcess destinationCustomProcessSecBoProcess = new SecBoProcess();
						destinationCustomProcessSecBoProcess
								.setActionClass(listOfSourceCustomProcessSecBoProcess.get(index).getActionClass());
						destinationCustomProcessSecBoProcess
								.setI18nResId(listOfSourceCustomProcessSecBoProcess.get(index).getI18nResId());
						destinationCustomProcessSecBoProcess
								.setModifyTs(listOfSourceCustomProcessSecBoProcess.get(index).getModifyTs());
						destinationCustomProcessSecBoProcess
								.setModifyUser(listOfSourceCustomProcessSecBoProcess.get(index).getModifyUser());
						destinationCustomProcessSecBoProcess
								.setParams(listOfSourceCustomProcessSecBoProcess.get(index).getParams());
						destinationCustomProcessSecBoProcess
								.setProcessName(listOfSourceCustomProcessSecBoProcess.get(index).getProcessName());

						SecBoProcessPK secBoProcessPK = new SecBoProcessPK();
						secBoProcessPK.setDocId(listOfSourceCustomProcessSecBoProcess.get(index).getId().getDocId());
						secBoProcessPK
								.setProcessId(listOfSourceCustomProcessSecBoProcess.get(index).getId().getProcessId());

						destinationCustomProcessSecBoProcess.setId(secBoProcessPK);

						listOfDestinationCustProcSecBoProcess.add(destinationCustomProcessSecBoProcess);

					}

					if (!listOfDestinationCustProcSecBoProcess.isEmpty()) {

						List<Object> listOfCustProcSecBoProcessAsObject = listOfDestinationCustProcSecBoProcess
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomProcessSecBoProcess", listOfCustProcSecBoProcessAsObject);

					} else {
						logger.info("CustomProcessSecBoProcess Table Don't have data to insert");
					}

					break;

				case "CustomProcessSecBoviewRes":

					List<com.retelzy.brim.entity.source.SecBoviewRes> listOfSourceCustProcSecBoviewRes = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecBoviewRes) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceCustProcSecBoviewRes.size(); index++) {

						SecBoviewRes destinationCustProcSecBoviewRes = new SecBoviewRes();
						destinationCustProcSecBoviewRes
								.setModifyTs(listOfSourceCustProcSecBoviewRes.get(index).getModifyTs());
						destinationCustProcSecBoviewRes
								.setModifyUser(listOfSourceCustProcSecBoviewRes.get(index).getModifyUser());
						destinationCustProcSecBoviewRes
								.setPropName(listOfSourceCustProcSecBoviewRes.get(index).getPropName());
						destinationCustProcSecBoviewRes
								.setPropValue(listOfSourceCustProcSecBoviewRes.get(index).getPropValue());

						SecBoviewResPK secBoviewResPK = new SecBoviewResPK();
						secBoviewResPK.setDocViewId(listOfSourceCustProcSecBoviewRes.get(index).getId().getDocViewId());
						secBoviewResPK.setResId(listOfSourceCustProcSecBoviewRes.get(index).getId().getResId());
						secBoviewResPK.setTypeId(listOfSourceCustProcSecBoviewRes.get(index).getId().getTypeId());

						destinationCustProcSecBoviewRes.setId(secBoviewResPK);

						listOfDestinationCustomProcSecBoviewRes.add(destinationCustProcSecBoviewRes);

					}

					if (!listOfDestinationCustomProcSecBoviewRes.isEmpty()) {

						List<Object> listOfCustProcSecBoviewResAsObject = listOfDestinationCustomProcSecBoviewRes
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomProcessSecBoviewRes", listOfCustProcSecBoviewResAsObject);

					} else {
						logger.info("CustomProcessSecBoviewRes Table don't have to insert");
					}

					break;

				case "CustomProcessSmuResShowAttr":

					List<com.retelzy.brim.entity.source.SmuResShowAttr> listOfSourceCustProcSmuResShowAttr = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SmuResShowAttr) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceCustProcSmuResShowAttr.size(); index++) {

						SmuResShowAttr destinationCustProcSmuResShowAttr = new SmuResShowAttr();
						destinationCustProcSmuResShowAttr
								.setModifyTs(listOfSourceCustProcSmuResShowAttr.get(index).getModifyTs());
						destinationCustProcSmuResShowAttr
								.setModifyUser(listOfSourceCustProcSmuResShowAttr.get(index).getModifyUser());
						destinationCustProcSmuResShowAttr
								.setPropValue(listOfSourceCustProcSmuResShowAttr.get(index).getPropValue());

						SmuResShowAttrPK smuResShowAttrPK = new SmuResShowAttrPK();
						smuResShowAttrPK.setDocId(listOfSourceCustProcSmuResShowAttr.get(index).getId().getDocId());
						smuResShowAttrPK
								.setPropName(listOfSourceCustProcSmuResShowAttr.get(index).getId().getPropName());
						smuResShowAttrPK
								.setSmuResId(listOfSourceCustProcSmuResShowAttr.get(index).getId().getSmuResId());
						smuResShowAttrPK
								.setSmuTypeId(listOfSourceCustProcSmuResShowAttr.get(index).getId().getSmuTypeId());

						destinationCustProcSmuResShowAttr.setId(smuResShowAttrPK);

						listOfDestinationCustProcSmuResShowAttr.add(destinationCustProcSmuResShowAttr);

					}

					if (!listOfDestinationCustProcSmuResShowAttr.isEmpty()) {

						List<Object> listOfCustProcSmuResShowAttrAsObject = listOfDestinationCustProcSmuResShowAttr
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomProcessSmuResShowAttr", listOfCustProcSmuResShowAttrAsObject);

					} else {
						logger.info("CustomeProcessSmuResShowAttr Table dont have data to insert");
					}

					break;

				case "CustomProcessSecRoleResCtl":

					List<com.retelzy.brim.entity.source.SecRoleResCtl> listOfSourceCustProcSecRoleResCtl = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SecRoleResCtl) element)
							.collect(Collectors.toList());

					Set<SecRoleResCtl> setOfSecRoleResCtl = listOfDestionationSecRoleResCtl.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceCustProcSecRoleResCtl.size(); index++) {

						SecRoleResCtl destinationCustProcSecRoleResCtl = new SecRoleResCtl();
						destinationCustProcSecRoleResCtl
								.setEnabled(listOfSourceCustProcSecRoleResCtl.get(index).getEnabled());
						destinationCustProcSecRoleResCtl
								.setModifyTs(listOfSourceCustProcSecRoleResCtl.get(index).getModifyTs());
						destinationCustProcSecRoleResCtl
								.setModifyUser(listOfSourceCustProcSecRoleResCtl.get(index).getModifyUser());

						SecRoleResCtlPK secRoleResCtlPK = new SecRoleResCtlPK();
						secRoleResCtlPK.setDocId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getDocId());
						secRoleResCtlPK
								.setParentDocId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getParentDocId());
						secRoleResCtlPK.setParentLevelId(
								listOfSourceCustProcSecRoleResCtl.get(index).getId().getParentLevelId());
						secRoleResCtlPK
								.setRelationId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getRelationId());
						secRoleResCtlPK.setRoleId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getRoleId());
						secRoleResCtlPK.setSmuResId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getSmuResId());
						secRoleResCtlPK
								.setSmuTypeId(listOfSourceCustProcSecRoleResCtl.get(index).getId().getSmuTypeId());

						destinationCustProcSecRoleResCtl.setId(secRoleResCtlPK);

						/*
						 * SecRoleProfile secRoleProfile = new SecRoleProfile();
						 * secRoleProfile.setRoleId(listOfSourceCustProcSecRoleResCtl.get(index).getId()
						 * .getRoleId());
						 * destinationCustProcSecRoleResCtl.setSecRoleProfile(secRoleProfile);
						 */

						if (!setOfSecRoleResCtl.contains(destinationCustProcSecRoleResCtl))
							listOfDestinationCustProcSecRoleResCtl.add(destinationCustProcSecRoleResCtl);
						else
							logger.info("Duplicate Object Found For CustomProcessSecRoleResCtl Table");

					}

					setOfSecRoleResCtl = null;

					if (!listOfDestinationCustProcSecRoleResCtl.isEmpty()) {

						List<Object> listOfCustProcSecRoleResCtlAsObject = listOfDestinationCustProcSecRoleResCtl
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomProcessSecRoleResCtl", listOfCustProcSecRoleResCtlAsObject);

					} else {
						logger.info("CustomProcessSecRoleResCtl Table dont have data to insert");
					}

					break;

				case "CustomMenuSmuCommon":

					List<com.retelzy.brim.entity.source.SmuCommon> listOfSourceCustProcSmuCommon = entry.getValue()
							.parallelStream().map(element -> (com.retelzy.brim.entity.source.SmuCommon) element)
							.collect(Collectors.toList());

					for (Integer index = 0; index < listOfSourceCustProcSmuCommon.size(); index++) {

						SmuCommon destinationCustomMenuSmuCommon = new SmuCommon();
						destinationCustomMenuSmuCommon
								.setI18nResId(listOfSourceCustProcSmuCommon.get(index).getI18nResId());
						destinationCustomMenuSmuCommon
								.setMenuItemId(listOfSourceCustProcSmuCommon.get(index).getMenuItemId());
						destinationCustomMenuSmuCommon
								.setMenuItemName(listOfSourceCustProcSmuCommon.get(index).getMenuItemName());
						destinationCustomMenuSmuCommon
								.setMenuPos(listOfSourceCustProcSmuCommon.get(index).getMenuPos());
						destinationCustomMenuSmuCommon
								.setModifyTs(listOfSourceCustProcSmuCommon.get(index).getModifyTs());
						destinationCustomMenuSmuCommon
								.setParentItemId(listOfSourceCustProcSmuCommon.get(index).getParentItemId());
						destinationCustomMenuSmuCommon.setUrl(listOfSourceCustProcSmuCommon.get(index).getUrl());

						listOfDestinationCustMenuSmuCommon.add(destinationCustomMenuSmuCommon);

					}

					if (!listOfDestinationCustMenuSmuCommon.isEmpty()) {

						List<Object> listOfCustMenuSmuCommonAsObject = listOfDestinationCustMenuSmuCommon
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomMenuSmuCommon", listOfCustMenuSmuCommonAsObject);

					} else {
						logger.info("CustomMenuSmuCommon Table don't have data to insert");
					}

					break;

				case "CustomMenuSmuRoleRootMenu":

					List<com.retelzy.brim.entity.source.SmuRoleRootMenu> listOfSourceCustMenuSmuRoleRootMenu = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SmuRoleRootMenu) element)
							.collect(Collectors.toList());

					Set<SmuRoleRootMenu> setOfSmuRoleRootMenu = listOfDestinationSmuRoleRootMenu.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceCustMenuSmuRoleRootMenu.size(); index++) {

						SmuRoleRootMenu destiinationCustMenuSmuRoleRootMenu = new SmuRoleRootMenu();
						destiinationCustMenuSmuRoleRootMenu
								.setGroupId(listOfSourceCustMenuSmuRoleRootMenu.get(index).getGroupId());
						destiinationCustMenuSmuRoleRootMenu
								.setMenuPos(listOfSourceCustMenuSmuRoleRootMenu.get(index).getMenuPos());
						destiinationCustMenuSmuRoleRootMenu
								.setModifyTs(listOfSourceCustMenuSmuRoleRootMenu.get(index).getModifyTs());
						destiinationCustMenuSmuRoleRootMenu
								.setModifyUser(listOfSourceCustMenuSmuRoleRootMenu.get(index).getModifyUser());
						destiinationCustMenuSmuRoleRootMenu
								.setSortType(listOfSourceCustMenuSmuRoleRootMenu.get(index).getSortType());

						SmuRoleRootMenuPK smuRoleRootMenuPK = new SmuRoleRootMenuPK();
						smuRoleRootMenuPK
								.setDocViewId(listOfSourceCustMenuSmuRoleRootMenu.get(index).getId().getDocViewId());
						smuRoleRootMenuPK.setDocViewRelId(
								listOfSourceCustMenuSmuRoleRootMenu.get(index).getId().getDocViewRelId());
						smuRoleRootMenuPK
								.setMenuItemId(listOfSourceCustMenuSmuRoleRootMenu.get(index).getId().getMenuItemId());
						smuRoleRootMenuPK.setMenuItemType(
								listOfSourceCustMenuSmuRoleRootMenu.get(index).getId().getMenuItemType());
						smuRoleRootMenuPK.setRoleId(listOfSourceCustMenuSmuRoleRootMenu.get(index).getId().getRoleId());

						destiinationCustMenuSmuRoleRootMenu.setId(smuRoleRootMenuPK);

						if (!setOfSmuRoleRootMenu.contains(destiinationCustMenuSmuRoleRootMenu))
							listOfDestinationCustMenuSmuRoleRootMenu.add(destiinationCustMenuSmuRoleRootMenu);
						else
							logger.info("Duplicate Object Found For CutomMenuSmuRoleRootMenu Table");
					}

					setOfSmuRoleRootMenu = null;

					if (!listOfDestinationCustMenuSmuRoleRootMenu.isEmpty()) {

						List<Object> listOfCustomMenuSmuRoleRootMenu = listOfDestinationCustMenuSmuRoleRootMenu
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());
						insertQueryMap.put("CustomMenuSmuRoleRootMenu", listOfCustomMenuSmuRoleRootMenu);

					} else {
						logger.info("CustomMenuSmuRoleRootMenu Table Don't have data to insert");
					}

					break;

				case "CustomMenuSmuRoleMenuItem":

					List<com.retelzy.brim.entity.source.SmuRoleMenuItem> listOfSourceCustMenuSmuRoleMenuItem = entry
							.getValue().parallelStream()
							.map(element -> (com.retelzy.brim.entity.source.SmuRoleMenuItem) element)
							.collect(Collectors.toList());

					Set<SmuRoleMenuItem> setOfSmuRoleMenuItem = listOfDestinationSmuRoleMenuItem.parallelStream()
							.collect(Collectors.toSet());

					for (Integer index = 0; index < listOfSourceCustMenuSmuRoleMenuItem.size(); index++) {

						SmuRoleMenuItem destinationCustMenuSmuRoleMenuItem = new SmuRoleMenuItem();
						destinationCustMenuSmuRoleMenuItem
								.setEnabled(listOfSourceCustMenuSmuRoleMenuItem.get(index).getEnabled());
						destinationCustMenuSmuRoleMenuItem
								.setMenuPos(listOfSourceCustMenuSmuRoleMenuItem.get(index).getMenuPos());
						destinationCustMenuSmuRoleMenuItem
								.setModifyTs(listOfSourceCustMenuSmuRoleMenuItem.get(index).getModifyTs());
						destinationCustMenuSmuRoleMenuItem
								.setModifyUser(listOfSourceCustMenuSmuRoleMenuItem.get(index).getModifyUser());

						SmuRoleMenuItemPK smuRoleMenuItemPK = new SmuRoleMenuItemPK();
						smuRoleMenuItemPK
								.setDocViewId(listOfSourceCustMenuSmuRoleMenuItem.get(index).getId().getDocViewId());
						smuRoleMenuItemPK
								.setMenuItemId(listOfSourceCustMenuSmuRoleMenuItem.get(index).getId().getMenuItemId());
						smuRoleMenuItemPK.setRoleId(listOfSourceCustMenuSmuRoleMenuItem.get(index).getId().getRoleId());

						destinationCustMenuSmuRoleMenuItem.setId(smuRoleMenuItemPK);

						if (!setOfSmuRoleMenuItem.contains(destinationCustMenuSmuRoleMenuItem))
							listOfDestinationCustMenuSmuRoleMenuItem.add(destinationCustMenuSmuRoleMenuItem);
						else
							logger.info("Duplicate Object Found for CustomMenuSmuRoleMenuItem Table");

					}

					setOfSmuRoleMenuItem = null;

					if (!listOfDestinationCustMenuSmuRoleMenuItem.isEmpty()) {

						List<Object> listOfCustMenuSmuRoleMenuItemAsObject = listOfDestinationCustMenuSmuRoleMenuItem
								.parallelStream().map(element -> (Object) element).collect(Collectors.toList());

						insertQueryMap.put("CustomMenuSmuRoleMenuItem", listOfCustMenuSmuRoleMenuItemAsObject);

					} else {
						logger.info("CustomMenuSmuRoleMenuItem Table dont have data to insert");
					}

					break;

				case "SequenceNumbers":

					if (!entry.getValue().isEmpty()) {
						insertQueryMap.put("SequenceNumbers", entry.getValue());

					} else {
						logger.info("SequenceNumbers Table don't have data to insert");
					}

					break;

				default:
					logger.error(
							"Table name not added in Switch Statement of insertTheSecurityData() method of MigationSecuirtyModule class :"
									+ entry.getKey());
					return false;
				}

			}

		}

		CompletableFuture<Boolean> futureStatus = destinationDbService
				.saveAllTheDestinationTableDetails(insertQueryMap);

		Boolean insertStatus = false;

		try {
			insertStatus = futureStatus.get();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occured In insertTheSecurityData() method Of MigrationSecurityModule Class :"
					+ e.getMessage());
			insertStatus = false;
		}

		return insertStatus;

		/*
		 * Boolean insertStatus =
		 * destinationDbService.saveAllTheDestinationTableDetails(insertQueryMap);
		 * 
		 * logger.info("Insert Status :" + insertStatus);
		 * 
		 * if (insertStatus == true) { return true; } else { return false; }
		 */

	}

	@SuppressWarnings("deprecation")
	public void insertQueryHandQueryDtableData(Integer roleId) throws Exception {

		logger.info("QueryH And QueryD Table Insertion started");

		File queryHInsertfile = new File(
				configFileReader.getPropertyValue("dir.path.source") + "/query_h_insert" + roleId + ".xml");
		File queryDInsertfile = new File(
				configFileReader.getPropertyValue("dir.path.source") + "/query_d_insert" + roleId + ".xml");

		IDatabaseConnection connection = null;

		if (queryHInsertfile.exists() && queryDInsertfile.exists()) {
			connection = destinationDbService.getConnection();

			try {
				logger.info("Inserting QueryHandQueryDtableData()..... ");
				DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));
				DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
				connection.getConnection().commit();
				logger.info("Completed Inserting QueryHandQueryDtableData()..... ");
			} catch (Exception e) {
				logger.error("Exception Occured While Inserting QueryD And QueryH Table :" + e.getMessage());
				logger.info("Updating QueryHandQueryDtableData()..... ");
				DatabaseOperation.UPDATE.execute(connection, new FlatXmlDataSet(new FileInputStream(queryHInsertfile)));
				DatabaseOperation.UPDATE.execute(connection, new FlatXmlDataSet(new FileInputStream(queryDInsertfile)));
				connection.getConnection().commit();
				logger.info("Updating QueryHandQueryDtableData() is completed..... ");
			} finally {

				connection.close();
				System.gc();

				queryHInsertfile.delete();
				queryDInsertfile.delete();
				try {
					Files.deleteIfExists(Paths.get(queryHInsertfile.getAbsolutePath()));
					Files.deleteIfExists(Paths.get(queryDInsertfile.getAbsolutePath()));
				} catch (NoSuchFileException e) {
					logger.error("No such file/directory exists");
				} catch (DirectoryNotEmptyException e) {
					logger.error("Directory is not empty.");
				} catch (IOException e) {
					logger.error("Invalid permissions.");

				}

				logger.info("Deletion successful.");
			}

			logger.info("QueryH And QueryD Table Insertion Completed");

		}

	}

	/**
	 * Get the file name for SQL file
	 *
	 * @param securityOption
	 * @param dateTime
	 * @return
	 */
	private String getSQLFileName(String securityOption) {
		SimpleDateFormat sdtf = new SimpleDateFormat("MMddyyHHmmss");
		String dateTime = sdtf.format(new java.util.Date());
		String fileName = "";
		if (securityOption.equals("ALL_SECURITY")) {
			fileName = "security_ALL_SECURITY-" + dateTime + ".sql";
		} else if (securityOption.equals("MULTIPLE")) {
			fileName = "security_MULTIPLE-" + dateTime + ".sql";
		} else {
			fileName = "security_" + securityOption + "-" + dateTime + ".sql";
		}
		return fileName;
	}

}
