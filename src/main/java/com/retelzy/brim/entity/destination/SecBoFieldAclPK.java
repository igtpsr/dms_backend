package com.retelzy.brim.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors
@Embeddable
public class SecBoFieldAclPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "ROLE_ID")
	private Integer roleId;

	@Column(name = "DOC_ID")
	private Integer docId;

	@Column(name = "QUERY_ID")
	private Integer queryId;

}
